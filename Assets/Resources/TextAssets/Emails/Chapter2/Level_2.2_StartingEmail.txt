Acclimation Friend
Earthcorp on high alert. Security threats.
I'm following up from our acclimation meeting. I had a great time chatting with you. Hope you did too. While you search for your memory, just a reminder - someone out there did this to you. Probably an Earthcorp employee. Due to a high alert warning issued by Earthcorp, employees are restricted and limited to certain floors. Earthcorp is monitoring the activity of every floor. You may get flagged as suspicious when searching for your memory. Be careful. Good luck finding your memory!
AcclimationFriend