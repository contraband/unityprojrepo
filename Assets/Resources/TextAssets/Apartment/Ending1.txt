<SetClearOnNewLine><false>

<SetWait><false>
<color=#bdc3c7>[Press SPACE to continue]</color>
<SetNewLineClip><UserHasJoinedChat>
<color=#bdc3c7>***USER Evelyn_T has JOINED the chat***</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Hi.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Hi.</color>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>I sent you the schematics.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: I saw that...
Mad_Mal: Thank you.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>You're welcome.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: How do you feel?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>I don't know. Scared, excited. Somewhere in between.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Just say the word and I'll let the world know of Evelyn Turner. 
Mad_Mal: Shill turned humanitarian.
Mad_Mal: :)

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Don't.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Don't do what?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Don't use my name.</color>

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Just say it was some anonymous employee at Earthcorp.</color>

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>A whistle-blower or something.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: You sure?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Yes. I'm sure.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Whatever you want.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>That's what I want.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: I understand.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Ok...</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: You ok?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>There's a group headed towards my office.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Stay calm.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Heavy security...</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: You're going to be alright.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Don't do that.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Do what?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>I thought we were finally being honest with one another.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Evelyn, is there anything you want me to do? Anything at all?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>I have a son right?</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Yeah. Peter Turner.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Peter Turner.</color>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Huh.</color>

<SetNewLineClip><UserHasLeftChat>
<color=#bdc3c7>***USER Evelyn_T has LEFT the chat***</color>
