<SetClearOnNewLine><false>

<SetWait><false>
<color=#bdc3c7>[Press SPACE to continue]</color>
<SetNewLineClip><UserHasJoinedChat>
<color=#bdc3c7>***USER Mad_Mal has joined the chat***</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Hello, Iago.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Hello, Dear.</color>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Is my favorite Ghost ready?</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Just need an address and we're ready.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Sending you the IP.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: I see it. Who's the target?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>An Earthcorp executive. Someone in finance.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: An accountant...?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Is that a problem?</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Nope...
Mad_Mal: Establishing connection to her AI. 
Mad_Mal: Listening to her current conversation.
Mad_Mal: Should have control of the AI in a few minutes.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Be careful. No time for being flashy.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: No flash, be safe. Got it.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Remember, you're playing for keeps.</color>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>No takes backsies.</color>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Update me when you're in.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Sure thing.

<SetWait><false>
<SetNewLineClip><UserHasLeftChat>
<color=#bdc3c7>***USER Kind_Usurper has LEFT the chat***</color>