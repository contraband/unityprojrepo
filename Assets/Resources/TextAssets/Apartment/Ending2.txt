<SetClearOnNewLine><false>

<SetWait><false>
<color=#bdc3c7>[Press SPACE to continue]</color>
<SetNewLineClip><UserHasJoinedChat>
<color=#bdc3c7>***USER Evelyn_T has JOINED the chat***</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Hi.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>Hi.</color>

<SetWait><false>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>I sent you the schematics.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: I saw that...
Mad_Mal: Thank you.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>I also sent you something else...</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Let me look.
Mad_Mal: I don't see anything.

<SetWait><false>
<SetNewLineClip><UserHasJoinedChat>
<color=#bdc3c7>***USER Cairo_Kid has JOINED the chat***</color>

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Check again.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Hello.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Hi. Did you get our present?</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Yeah. I think I did.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#4b77be>Evelyn_T:</color> <color=#bdc3c7>I'm sorry.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: I'm sorry too.

<SetWait><false>
<SetNewLineClip><UserHasLeftChat>
<color=#bdc3c7>***USER Evelyn_T has LEFT the chat***</color>

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Do you know how this ends?</color>

<SetWait><true>
Mad_Mal: Yeah. I think so.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Do you want me to wait with you?</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Do you mind?


<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>No. I'll wait with you.</color>

<SetWait><false>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Good work, by the way.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: With?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>I usually have an easier time catching Ghosts.</color>

<SetWait><false>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Whoever hired you is losing a lot of talent.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Thanks.
Mad_Mal: What are you going to do with Evelyn?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Do you care?</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Just curious.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>She'll be looked after.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: You ever think about leaving Earthcorp? Go freelance?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Ghost work? Not really.</color>

<SetWait><false>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>I like being the scary Earthcorp guy. Hard to give up.</color>

<SetWait><true>
<SetNewLineClip><>
<PlaySound><Knock>
Mad_Mal: Makes sense.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Looks like my boys have arrived.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Yeah. I hear them.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>You want me to stay?</color>

<SetWait><true>
<SetNewLineClip><>
<PlaySound><Knock>
Mad_Mal: No thanks.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Ok. Good luck, Ghost.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Thanks. I appreciate the kind words.

<SetWait><false>
<SetNewLineClip><UserHasLeftChat>
<color=#bdc3c7>***USER Cairo_Kid has LEFT the chat***</color>