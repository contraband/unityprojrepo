//Choices
//<Choice><NameOfChoice|IntegerValue>

//Settings
<SetClearOnNewLine><true>
<SetWait><true>
<SetWaitTime><0.01>

//Lines
***USER Mad_Mal has joined the chat***
Mad_Mal: Hello, Iago.
Kind_Usurper: Hello, Dear.
Kind_Usurper: How's the project going?
Mad_Mal: We're in.
Mad_Mal: I have her Earthcorp credentials.
Mad_Mal: I turned off her AI.
Mad_Mal: Running an acclimation program right now inside her monitor.
Kind_Usurper: This is why you're my favorite Ghost.
Mad_Mal: She'll be mobile in a few moments.
Kind_Usurper: Perfect. Update me when you locate the package.
Mad_Mal: Will do.
Kind_Usurper: Any signs of Osiris?
Mad_Mal: Not so far.
Kind_Usurper: Well, don't get too comfortable.
Kind_Usurper: She's bound to get noticed at some point.
Kind_Usurper: And when she does, they'll do their best to figure out why their CFO is hacking into Earthcorp data centers.
Mad_Mal: Understood.
Kind_Usurper: Be careful.
Mad_Mal: I always am.
Kind_Usurper: If she gets caught and this gets back to you...
Mad_Mal: I will no longer be your favorite...?
Kind_Usurper: You'll always be my favorite.
Kind_Usurper: I'll just have to admire you...
Mad_Mal: Yes...?
Kind_Usurper: ...from afar.
Mad_Mal: Got it.
Kind_Usurper: Good work. Chat soon.
