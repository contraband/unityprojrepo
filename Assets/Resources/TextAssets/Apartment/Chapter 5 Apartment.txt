<SetClearOnNewLine><false>

<SetWait><false>
<color=#bdc3c7>[Press SPACE to continue]</color>
<SetNewLineClip><UserHasJoinedChat>
<color=#bdc3c7>***USER Kind_Usurper has joined the chat***</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Hello, Iago.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Two questions.</color>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Where is my package?</color>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Has the Osiris situation been addressed...?</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Sending you the latest acquisition.
Mad_Mal: The Osiris situation has been handled.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Perfect. I knew you were my favorite.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: The accountant is on standby. Send me the next lead.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>Sent.</color>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>2 more packages, my little specter. You're very close.</color>

<SetWait><false>
<SetNewLineClip><UserHasJoinedChat>
<color=#bdc3c7>***USER Cairo_Kid has joined the chat***</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Is this a friend of yours?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>I was going to ask you the same thing.</color>

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Hello.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: Hi...

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>This is a private party, sport. Did you receive an invitation?</color>

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>No. I did not. </color>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>I'll be brief.</color>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>One or both of you is using one of our employees as a proxy to procure patented information.</color>

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>That's a serious accusation.</color>
<color=#f2784b>Kind_Usurper:</color> <color=#bdc3c7>If you want to chat, we should get to know one another.</color>


<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>I go by several names.</color>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>People in your line of work know me as Osiris.</color>

<SetWait><false>
<SetNewLineClip><UserHasLeftChat>
<color=#bdc3c7>***USER Kind_Usurper has LEFT the chat***</color>

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Looks we have the place to ourselves.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: So it would seem.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>I'm guessing you're the one responsible for this mess.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: I'm afraid I don't I understand.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>I'm close to pinpointing your location, Ghost.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: That right?

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>When I do, things are going to grow complicated for you.</color>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>I would advise you to leave the proxy alone.</color> 
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Concentrate on your own well-being.</color>

<SetWait><true>
<SetNewLineClip><>
Mad_Mal: I'll take that under advisement.

<SetWait><false>
<SetNewLineClip><ReceiveMessage>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7>Please do.</color>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7> I'll let you keep your connection with her.</color>
<color=#f62459>Cairo_Kid:</color> <color=#bdc3c7> It'll make it easier to find you.</color>

<SetWait><false>
<SetNewLineClip><UserHasLeftChat>
<color=#bdc3c7>***USER Cairo_Kid has LEFT the chat***</color>