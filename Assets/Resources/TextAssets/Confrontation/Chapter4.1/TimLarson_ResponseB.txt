//Change suspicion level (optional)
<ChangeSuspicion><10>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Right!
So, you know, I've been thinking a lot lately...
<SetName><Mr. Larson>
<SetPortrait><Tim>
About...?
<SetName><Evelyn>
<SetPortrait><Evelyn>
...about you! And I was wondering...
<SetName><Mr. Larson>
<SetPortrait><Tim>
Yeah...?
<SetName><Evelyn>
<SetPortrait><Evelyn>
...if, you know, sometime, you and me...
...a drink or whatever. If you had -
<SetName><Mr. Larson>
<SetPortrait><Tim>
Evelyn, I can't do this. Not again.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Wait. You can't do...?
<SetName><Mr. Larson>
<SetPortrait><Tim>
You and me. I thought we agreed.
Becky and I almost got a divorce over...
I'd be lying if I said I didn't have feelings -
<SetName><Evelyn>
<SetPortrait><Evelyn>
Right! Of course. I don't want to put you in any sort of...
<SetName><Mr. Larson>
<SetPortrait><Tim>
Thanks.
<SetName><Mr. Larson>
<SetPortrait><Tim>
Great to see you, but I should go. I'll see you around.