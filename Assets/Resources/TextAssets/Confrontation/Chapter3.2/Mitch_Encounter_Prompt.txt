//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><Coworker>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Mitch>
<SetPortrait><MaleGaurd>
Howdy!

//Responses (optional)
<Response><1|Ignore.|Mitch_Response_A>
<Response><2|Greet.|Mitch_Response_B>