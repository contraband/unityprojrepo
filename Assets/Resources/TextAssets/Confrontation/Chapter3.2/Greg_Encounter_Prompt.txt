//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><20>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Greg>
<SetPortrait><Greg>
Evelyn! I bought you a latte but it was getting cold so I drank it. 
<SetName><Greg>
<SetPortrait><Greg>
Did you do something different with your hair?
<SetName><Evelyn>
<SetPortrait><Evelyn>
No. This is the same - 
<SetName><Greg>
<SetPortrait><Greg>
I love it. Makes you look younger. Now a good time to chat?

//Responses (optional)
<Response><1|Shut Greg down.|GregII_Response_A>
<Response><2|Encourage Greg.|GregII_Response_B>