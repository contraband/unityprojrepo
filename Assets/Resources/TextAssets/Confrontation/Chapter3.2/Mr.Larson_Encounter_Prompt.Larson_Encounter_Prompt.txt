//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><25>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Mr. Larson>
<SetPortrait><Mr. Larson>
Evelyn? Is everything ok? What are you doing here? 

//Responses (optional)
<Response><1|Maiden in distress.|Mr.Larson_Response_A>
<Response><2|Go on a date?|Mr.Larson_Response_B>