//Change suspicion level (optional)
<ChangeSuspicion><-10>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Greg. I don't know how else to say this...
<SetName><Greg>
<SetPortrait><Greg>
Fire away!
<SetName><Evelyn>
<SetPortrait><Evelyn>
I will never have time for you. Ever.
<SetName><Greg>
<SetPortrait><Greg>
Direct. Love it. I'll come back another time. Ciao!