//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><LightSecurity>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Tanner>
<SetPortrait><Tanner>
Ms. Turner...
<SetName><Evelyn>
<SetPortrait><Evelyn>
Hi. How's it going? What are you working on?
<SetName><Tanner>
<SetPortrait><Tanner>
...
That's classified.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Of course. My mistake. Apologies for my ignorance.
<SetName><Tanner>
<SetPortrait><Tanner>
They're monitoring your visit. They're always watching.
Be careful...