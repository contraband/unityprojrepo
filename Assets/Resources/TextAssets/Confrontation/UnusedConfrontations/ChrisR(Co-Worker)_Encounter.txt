//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><25>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Chris R.>
<SetPortrait><Chris R.>
Hey, Evelyn. Missed you last night.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Oh! Yeah...? 
I...missed you too...
<SetName><Chris R.>
<SetPortrait><Chris R.>
Can I see you tonight?

//Responses (optional)
<Response><1|Rain-check.|Chris R._Response_A>
<Response><2|Break up.|Chris R._Response_B>