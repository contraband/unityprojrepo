//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><LightSecurity>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Drake>
<SetPortrait><Drake>
Badge and security clearance.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Here you are.
<SetName><Drake>
<SetPortrait><Drake>
Aren't you some sort of financial executive? What are you doing on this floor?
<SetName><Evelyn>
<SetPortrait><Evelyn>
Educational purposes.
<SetName><Drake>
<SetPortrait><Drake>
There is no mention of your visit on today's logs.
<SetName><Evelyn>
<SetPortrait><Evelyn>
There must be some sort of a - 
<SetName><Drake>
<SetPortrait><Drake>
I don't care who you are. Get proper clearance next time. There are rules here for a reason.