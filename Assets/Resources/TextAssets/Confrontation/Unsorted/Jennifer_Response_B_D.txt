//Change suspicion level (optional)
<ChangeSuspicion><-15>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Do you beat yourself up a lot? All day? All week?
<SetName><Jennifer>
<SetPortrait><Jennifer>
Wait, what are -  
<SetName><Evelyn>
<SetPortrait><Evelyn>
This whole aggressive act - it's not worth it.
<SetName><Jennifer>
<SetPortrait><Jennifer>
...
<SetName><Evelyn>
<SetPortrait><Evelyn>
This is just a job.
<SetName><Jennifer>
<SetPortrait><Jennifer>
I've been under a lot of pressure and stress...
<SetName><Evelyn>
<SetPortrait><Evelyn>
Hang in there.
<SetName><Jennifer>
<SetPortrait><Jennifer>
You too.