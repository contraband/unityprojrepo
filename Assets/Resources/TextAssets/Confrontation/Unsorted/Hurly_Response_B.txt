//Change suspicion level (optional)
<ChangeSuspicion><-10>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
No. I can explain...
<SetName><Hurly>
<SetPortrait><Hurly>
Ok...
<SetName><Evelyn>
<SetPortrait><Evelyn>
Evelyn: Are you feeling valued by Earthcorp?
<SetName><Hurly>
<SetPortrait><Hurly>
Yes...I think so, yes.
<SetName><Evelyn>
<SetPortrait><Evelyn>
We're conducting surprise visits. Making sure everyone at Earthcorp feels valued.
<SetName><Hurly>
<SetPortrait><Hurly>
I didn't realize executives did this sort of thing or had time.
<SetName><Evelyn>
<SetPortrait><Evelyn>
New company direction! Executives, security guys, we're all part of Earthcorp.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Anything we can do to improve employee moral?
<SetName><Hurly>
<SetPortrait><Hurly>
...
<SetName><Hurly>
<SetPortrait><Hurly>
Bigger bathroom stalls. The ones we have now are kind of small. 
<SetName><Hurly>
<SetPortrait><Hurly>
Same with the pipes. Lots of clogging...
<SetName><Evelyn>
<SetPortrait><Evelyn>
We will look into that.
<SetName><Hurly>
<SetPortrait><Hurly>
Great. Thank you. And I'm serious about the toilets. Sometimes you flush and flush and flush...