//Choices
//<Choice><NameOfChoice|IntegerValue>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Can I ask you another question? Sorry.
<SetName><Voice>
<SetPortrait><DefaultMale>
No problem. I'd be thrilled.
<SetName><Evelyn>
<SetPortrait><Evelyn>
What is acclimation?
<SetName><Voice>
<SetPortrait><DefaultMale>
Acclimation is an automated safety program triggered by your AI.
<SetName><Voice>
<SetPortrait><DefaultMale>
Your Earthcorp monitor - the chip inside your head - was under attack.
<SetName><Voice>
<SetPortrait><DefaultMale>
Robyn triggered me to prevent future malware from being introduced into your monitor.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Sounds...
<SetName><Voice>
<SetPortrait><DefaultMale>
...Technologically impressive...?
<SetName><Evelyn>
<SetPortrait><Evelyn>
...creepy.
<SetName><Voice>
<SetPortrait><DefaultMale>
Without acclimation, you could lose many of your cognitive functions.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Like brain damage?
<SetName><Voice>
<SetPortrait><DefaultMale>
Like brain damage! Pretty exciting stuff!!!
<SetName><Voice>
<SetPortrait><DefaultMale>
Acclimation will create a safe environment for your mind to adjust to the resetting process.
<SetName><Voice>
<SetPortrait><DefaultMale>
Now, walk in front of the man with the weird face.