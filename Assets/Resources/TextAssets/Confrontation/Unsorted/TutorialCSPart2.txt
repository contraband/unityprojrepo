//Choices
//<Choice><NameOfChoice|IntegerValue>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Where are we going?
<SetName><Voice>
<SetPortrait><DefaultMale>
To a safe place.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Are you kidnapping me?
<SetName><Voice>
<SetPortrait><DefaultMale>
No.
<SetName><Voice>
<SetPortrait><DefaultMale>
I'm here to help.
<SetName><Voice>
<SetPortrait><DefaultMale>
You may feel a little lightheaded. Totally natural. You experienced a stressful event.
<SetName><Voice>
<SetPortrait><DefaultMale>
I'm here to help you acclimate.
<SetName><Evelyn>
<SetPortrait><Evelyn>
That sounds like a good plan...
<SetName><Evelyn>
<SetPortrait><Evelyn>
Because - I'll be honest with you, mysterious voice presumbably in my head...
<SetName><Evelyn>
<SetPortrait><Evelyn>
...I don't know who I am.
<SetName><Voice>
<SetPortrait><DefaultMale>
Oh.
<SetName><Evelyn>
<SetPortrait><Evelyn>
At all.
<SetName><Voice>
<SetPortrait><DefaultMale>
That's curious.
<SetName><Voice>
<SetPortrait><DefaultMale>
...
<SetName><Voice>
<SetPortrait><DefaultMale>
I'm sure that's natural too.
<SetName><Voice>
<SetPortrait><DefaultMale>
Please. If you'll just walk through the glowing door, we can begin the acclimation process.