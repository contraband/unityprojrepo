//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><20>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>

//Lines
<SetName><Jennifer>
<SetPortrait><Jennifer>
Evelyn? What are you doing here? I wasn't expecting a visit. Is everything ok? Are we behind schedule?


//Responses (optional)
<Response><1|Be a Manager.|Jennifer_Response_A>
<Response><2|Flattery Never Hurt No One.|Jennifer_Response_B>