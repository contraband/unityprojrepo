//Change suspicion level (optional)
<ChangeSuspicion><15>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
You're behind schedule, and we've noticed it. Improve your team's performance or we'll find someone who will.
<SetName><Jennifer>
<SetPortrait><Jennifer>
There you are, Evelyn. Frosty and unsympathetic.
<SetName><Jennifer>
<SetPortrait><Jennifer>
I hope I'm here long enough to watch you crash and burn. It will be the best day of my career.