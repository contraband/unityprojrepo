//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Rumor has it your team is doing a tremendous job.
<SetName><Jennifer>
<SetPortrait><Jennifer>
Is that right?
<SetName><Evelyn>
<SetPortrait><Evelyn>
I wanted to stop in, send my congratulations in person -
<SetName><Jennifer>
<SetPortrait><Jennifer>
We're both adults - why don't you just cut the BS? Why are you really here?

//Responses (optional)
<Response><1|Fight Fire with Fire.|Jennifer_Response_B_C>
<Response><2|Ease Pain.|Jennifer_Response_B_D>