//Change suspicion level (optional)
<ChangeSuspicion><-10>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Greg. I need you to back up, turn around, and never make eye contact with me again.
<SetName><Greg>
<SetPortrait><Greg>
Honesty. Straight shooter. I like that. Thanks for the note.