<SetClearOnNewLine><true>
<SetWait><true>
<SetWaitTime><0.01>
<SetName><NPC>
<SetPortrait><NPC>
Hey.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Hi.
<SetName><NPC>
<SetPortrait><NPC>
I just wanted to let you know that I see you.
<SetName><Evelyn>
<SetPortrait><Evelyn>
I figured.
<SetName><NPC>
<SetPortrait><NPC>
Cool. Every time one of your coworkers sees you, like me, you lose bandwidth.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Oh. That's not good.
<SetName><NPC>
<SetPortrait><NPC>
No. It's not.
<SetName><NPC>
<SetPortrait><NPC>
You may have noticed that your bandwidth went down every time you made a series of moves.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Did it?
<SetName><NPC>
<SetPortrait><NPC>
It did.
<SetName><NPC>
<SetPortrait><NPC>
Taking turns exhausts your bandwidth. You'll need bandwidth to move around the office.
<SetName><Evelyn>
<SetPortrait><Evelyn>
What happens when I run out of bandwidth?
<SetName><NPC>
<SetPortrait><NPC>
The person inside of your head loses control of you. You become immobile.
<SetName><Evelyn>
<SetPortrait><Evelyn>
The person in my head? I don't know what that means.
<SetName><NPC>
<SetPortrait><NPC>
It'll make more sense next semester.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Thank you for the advice.
<SetName><NPC>
<SetPortrait><NPC>
You're welcome. Good luck!