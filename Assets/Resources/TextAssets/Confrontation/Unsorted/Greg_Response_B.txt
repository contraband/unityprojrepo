//Change suspicion level (optional)
<ChangeSuspicion><5>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Thanks, but I don't drink coffee. I'll take a look at your resume later today. Sound good?
<SetName><Greg>
<SetPortrait><Greg>
Really? Your AI said that you drink one-pump, sugar-free vanilla, half soy, half cream, extra hot lattes. 
<SetName><Evelyn>
<SetPortrait><Evelyn>
Right! Sure - but I already had, like, four today. So, I'm good.
<SetName><Greg>
<SetPortrait><Greg>
No. I insist. Are you going to be here for a while? I'll run down and get you one. Back in sec!