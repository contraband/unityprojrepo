<SetClearOnNewLine><true>
<SetWait><true>
<SetWaitTime><0.01>
<SetName><NPC>
<SetPortrait><NPC2>
I see you.
<SetName><Evelyn>
<SetPortrait><Evelyn>
You see me?
<SetName><NPC>
<SetPortrait><NPC2>
I see you.
<SetName><Evelyn>
<SetPortrait><Evelyn>
I lost bandwidth, didn't I?
<SetName><NPC>
<SetPortrait><NPC2>
I'm afraid so.
<SetName><NPC>
<SetPortrait><NPC2>
If you're running low on bandwidth, you may want to re-upload at the mini-terminal located inside this room.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Is that difficult?
<SetName><NPC>
<SetPortrait><NPC2>
It depends on the skill of the person inside your head.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Ok. Sounds scary.
<SetName><NPC>
<SetPortrait><NPC2>
It'll be ok.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Thank you for the support.
<SetName><NPC>
<SetPortrait><NPC2>
You're very welcome. Hang in there!