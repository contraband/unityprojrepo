//Choices
//<Choice><NameOfChoice|IntegerValue>

//Lines
<SetName><Voice>
<SetPortrait><DefaultMale>
Hi.
<SetName><Voice>
<SetPortrait><DefaultMale>
Hey there.
<SetName><Voice>
<SetPortrait><DefaultMale>
Time to wake up.
<SetName><Voice>
<SetPortrait><DefaultMale>
Howdy.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Hi...
<SetName><Voice>
<SetPortrait><DefaultMale>
Sleep well?
<SetName><Evelyn>
<SetPortrait><Evelyn>
I don't know. Was I sleeping?
<SetName><Voice>
<SetPortrait><DefaultMale>
Yeah. Snoring even.
<SetName><Voice>
<SetPortrait><DefaultMale>
Loud snoring.
<SetName><Evelyn>
<SetPortrait><Evelyn>
That's embarassing.
<SetName><Voice>
<SetPortrait><DefaultMale>
Very embarassing.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Where are we?
<SetName><Voice>
<SetPortrait><DefaultMale>
Your office.
<SetName><Evelyn>
<SetPortrait><Evelyn>
My office...
<SetName><Voice>
<SetPortrait><DefaultMale>
At Earthcorp.
<SetName><Evelyn>
<SetPortrait><Evelyn>
At Earthcorp...
<SetName><Voice>
<SetPortrait><DefaultMale>
Yes.
<SetName><Evelyn>
<SetPortrait><Evelyn>
...
<SetName><Evelyn>
<SetPortrait><Evelyn>
Are you an AI?
<SetName><Voice>
<SetPortrait><DefaultMale>
Sort of.
<SetWaitTime><0.03>
<SetName><Evelyn>
<SetPortrait><Evelyn>
I don't understand any of this.
<SetWaitTime><DEFAULT>
<SetName><Voice>
<SetPortrait><DefaultMale>
It'll make more sense. Allow me.