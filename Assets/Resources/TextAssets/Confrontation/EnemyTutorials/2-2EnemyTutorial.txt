<SetName><Voice>
<SetPortrait><AcclimationFriend>
Hello again, Evelyn Turner!
You're about to encounter some hostile Earthcorp employees.
<SetImage><Coworker>
This is a coworker.
Floors of Earthcorp are typically inhabited by coworkers.
Coworkers that spot you increase your floor suspicion by 40.
<SetImage><LightSecurity>
This is an Earthcorp light security guard.
As your floor suspicion increases, light security will arrive to investigate.
Light security guards that sees you increase your suspicion by 60.
<SetImage><HeavySecurity>
This is a heavy security guard.
Heavy security will enter the area if the alarm is sounded.
If a heavy security guard catches you, your activities will come to an end.
<SetImage><>
You can click on coworkers and guards to predict their next move. 
This will cost you 2 actions.
Good luck!