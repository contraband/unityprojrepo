<SetName><Voice>
<SetPortrait><AcclimationFriend>
Evelyn Turner!
I'm gonna tell you about robots, specifically robotic cameras.
Also known as regular cameras.
Or just - cameras.
<SetImage><Camera>
This is a camera.
If a camera sees you, your suspicion will increase by 40.
<SetImage><>
You can click on cameras to predict the direction they will face on their next turn.
This will cost you 2 actions.
Hang in there and be careful!