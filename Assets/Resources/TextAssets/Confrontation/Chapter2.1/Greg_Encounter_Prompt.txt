//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><5>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>

//Lines
<SetName><Greg>
<SetPortrait><Greg>
Hey! Evelyn, right? How's it going? That's a sharp looking blouse.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Thank you...
<SetName><Greg>
<SetPortrait><Greg>
Saw the junior executive job posting. Sent my resume 30 minutes ago. I'm your guy.
You, me, coffee. What do you think?

//Responses (optional)
<Response><1|Shut down Greg.|Greg_Response_A>
<Response><2|Get rid of Greg.|Greg_Response_B>