//Change suspicion level (optional)
<ChangeSuspicion><-5>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
What do you think? Do you feel behind schedule?
<SetName><Jennifer>
<SetPortrait><Jennifer>
Well, I suppose there is always room for improvement.
<SetName><Evelyn>
<SetPortrait><Evelyn>
My thoughts exactly.
<SetName><Jennifer>
<SetPortrait><Jennifer>
We'll double our efforts. I'll send you an updated plan of attack. Do you need anything from me?
<SetName><Evelyn>
<SetPortrait><Evelyn>
No. Just making the rounds.
<SetName><Jennifer>
<SetPortrait><Jennifer>
Right, right...
<SetName><Jennifer>
<SetPortrait><Jennifer>
Suppose I should get back to work. Thanks for stopping in!