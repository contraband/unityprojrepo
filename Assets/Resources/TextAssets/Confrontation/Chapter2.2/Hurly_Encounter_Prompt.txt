//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><Coworker>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Hurly>
<SetPortrait><Hurly>
Hey, Ms. Turner.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Hey, you...
<SetName><Hurly>
<SetPortrait><Hurly>
I hate to ask but did security issue an authorization for today's visit?
<SetName><Evelyn>
<SetPortrait><Evelyn>
No. We're conducting surprise visits. Making sure everyone at Earthcorp feels valued.
<SetName><Hurly>
<SetPortrait><Hurly>
I didn't realize executives did this sort of thing...
<SetName><Evelyn>
<SetPortrait><Evelyn>
Anything we can do to improve employee moral?
<SetName><Hurly>
<SetPortrait><Hurly>
Yeah...
<SetName><Hurly>
<SetPortrait><Hurly>
Bigger bathroom stalls. The ones we have now are kind of small. 
<SetName><Hurly>
<SetPortrait><Hurly>
Same with the pipes. Lots of clogging...
<SetName><Evelyn>
<SetPortrait><Evelyn>
We'll look into that.
<SetName><Hurly>
<SetPortrait><Hurly>
Great. Thanks.
And I'm serious about the toilets.
Sometimes you flush and flush and flush...