//Change suspicion level (optional)
<ChangeSuspicion><5>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>

//Lines
<SetName><Jennifer>
<SetPortrait><Jennifer>
Evelyn. Hi. Do you have a moment?
<SetName><Evelyn>
<SetPortrait><Evelyn>
Yes! Sure. What's going on?
<SetName><Jennifer>
<SetPortrait><Jennifer>
I got your email last night. You hinted that we were falling behind schedule.
I just wanted to hear it from you in person. Talk it over.

//Responses (optional)
<Response><1|Take the manager route.|Jennifer_Response_A>
<Response><2|Be kind to strangers.|Jennifer_Response_B>