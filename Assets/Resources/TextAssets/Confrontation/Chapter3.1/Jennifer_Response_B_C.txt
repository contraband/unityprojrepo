//Change suspicion level (optional)
<ChangeSuspicion><10>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Ok.
You're behind schedule, and we've noticed it. Improve your team's performance or we'll find someone who will.
Do I need to spell out anything else for you?
<SetName><Jennifer>
<SetPortrait><Jennifer>
There you are, Evelyn. Cold and terse.
You had me worried that you had a midlife crisis or something.
<SetName><Jennifer>
<SetPortrait><Jennifer>
I hope I'm here long enough to watch you crash and burn. 
It will be the best day of my career.