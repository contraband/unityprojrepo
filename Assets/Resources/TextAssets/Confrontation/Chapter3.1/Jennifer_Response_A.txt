//Change suspicion level (optional)
<ChangeSuspicion><-5>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
What do you think? Do you feel behind schedule?
<SetName><Jennifer>
<SetPortrait><Jennifer>
Well, I suppose there is always room for improvement.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Don't work too hard though. You still need to balance, you know, your life and work and everything.
<SetName><Jennifer>
<SetPortrait><Jennifer>
Right...
I'll send you an updated plan of attack. Do you need anything from me?
<SetName><Evelyn>
<SetPortrait><Evelyn>
No. Just headed down stairs to make the rounds.
<SetName><Jennifer>
<SetPortrait><Jennifer>
You doing ok?
<SetName><Evelyn>
<SetPortrait><Evelyn>
Yeah. Why?
<SetName><Jennifer>
<SetPortrait><Jennifer>
You just seem, I don't know, nicer...
Sorry! That came out totally rude. I'm sorry. I should go.
<SetName><Jennifer>
<SetPortrait><Jennifer>
Thank you. For the chat. And everything.