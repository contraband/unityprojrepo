//Choices
//<Choice><NameOfChoice|IntegerValue>

//Lines
<SetName><Weird Faced Woman>
<SetPortrait><FacelessWoman>
You again! 
<SetName><Evelyn>
<SetPortrait><Evelyn>
Me again...
<SetName><Weird Faced Woman>
<SetPortrait><FacelessWoman>
Welcome...to the data vault.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Thanks. 
Am I going to hack this thing too?
<SetName><Weird Faced Woman>
<SetPortrait><FacelessWoman>
This "thing" is a super computer, commonly referred to as a data vault.
You're familiar with hacking now.
Time to put your talents to the test.
More code. More data retrieved.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Are they difficult to hack?
<SetName><Weird Faced Woman>
<SetPortrait><FacelessWoman>
They can be.
Data vaults contain sensitive material...
When you infiltrate these machines in the real world, you're going to need to vacate that floor ASAP.
Give it a shot, then exit the acclimation floor.