//Change suspicion level (optional)
<ChangeSuspicion><-10>

//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
How was that?
<SetName><Weird Faced Woman>
<SetPortrait><FacelessWoman>
Beautiful.
Your suspicion went down.
You made a great decision.
<SetName><Weird Faced Woman>
<SetPortrait><FacelessWoman>
In certain situations, you may feel blocked by a co-worker.
It's not the end of the world.
Chat with them and, if they have a response choice, you may be able to negotiate the suspicion you take.
<SetName><Evelyn>
<SetPortrait><Evelyn>
That's helpful.
<SetName><Weird Faced Woman>
<SetPortrait><FacelessWoman>
You're adjusting really well to acclimation. You're nearly halfway through.
<SetName><Evelyn>
<SetPortrait><Evelyn>
Great. Thank you. Everyone has been very helpful in acclimation.
<SetName><Weird Faced Woman>
<SetPortrait><FacelessWoman>
Sure thing, Evelyn Turner.
Go ahead and approach the weird looking guy next to the terminal to continue your edification.