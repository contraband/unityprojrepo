//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><60>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Heath>
<SetPortrait><MaleGaurd>
Wait, didn't I see you earlier?
<SetName><Evelyn>
<SetPortrait><Evelyn>
Yes. Heath, right?
<SetName><Heath>
<SetPortrait><MaleGaurd>
I'm going to pretend I didn't see you.
My colleagues will not be as generous if they find you.
Please leave as soon as you can or we'll both be in trouble.