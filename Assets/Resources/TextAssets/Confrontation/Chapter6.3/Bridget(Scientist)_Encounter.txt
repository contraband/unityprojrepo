//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><Coworker>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Bridget>
<SetPortrait><Hannah>
Ah!
<SetName><Evelyn>
<SetPortrait><Evelyn>
Ah!
<SetName><Bridget>
<SetPortrait><Hannah>
Ms. Turner! You scared me. 
Only a select few Earthcorp employees are allowed here.
I won't tell anyone, but they're always watching.