//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><Coworker>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Gary>
<SetPortrait><OlderMale>
Oh dear.
Are you firing me? 
I didn't steal anything!
<SetName><Evelyn>
<SetPortrait><Evelyn>
I believe you.
<SetName><Gary>
<SetPortrait><OlderMale>
It wasn't me!
<SetName><Evelyn>
<SetPortrait><Evelyn>
Ok!
<SetName><Gary>
<SetPortrait><OlderMale>
If you fire me, know this...
...I have company secrets that would ruin this place.