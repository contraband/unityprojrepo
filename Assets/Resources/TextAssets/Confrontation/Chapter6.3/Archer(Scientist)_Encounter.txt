//Persistent choices (optional)
//<PersistentChoice><NameOfChoice|IntegerValue>

//Change suspicion level (optional)
<ChangeSuspicion><Coworker>

//Settings (optional)
//Giving DEFAULT as a value will return the setting to its default value
//<SetClearOnNewLine><BooleanValue>
//<SetWait><BooleanValue>
//<SetWaitTime><FloatValue>

//Audio (optional)
//<Audio><NameOfAudioResource>


//Lines
<SetName><Archer>
<SetPortrait><Hurly>
Evelyn Turner.
I know it was you.
<SetName><Evelyn>
<SetPortrait><Evelyn>
I'm not sure I understand what you're suggesting.
<SetName><Archer>
<SetPortrait><Hurly>
Someone has been leaking Earthcorp data.
Someone in high places.
Repent, and perhaps Earthcorp will grant you mercy.