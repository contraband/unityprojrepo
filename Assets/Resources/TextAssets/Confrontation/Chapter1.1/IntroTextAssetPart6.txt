//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
Since when?
<SetName><Robyn>
<SetPortrait><Robyn>
...
<SetName><Robyn>
<SetPortrait><Robyn>
...
<SetName><Evelyn>
<SetPortrait><Evelyn>
This is ridiculous. Robyn, contact IT. Have them update my credentials.
<SetName><Robyn>
<SetPortrait><Robyn>
...
<SetName><Robyn>
<SetPortrait><Robyn>
...
<SetName><Robyn>
<SetPortrait><Robyn>
Sure thing.
<SetName><Evelyn>
<SetPortrait><Evelyn>
"Sure thing...?"
<SetName><Robyn>
<SetPortrait><Robyn>
Daily reminder #48. Your Earthcorp password will expire in 73 seconds.
<SetName><Robyn>
<SetPortrait><Robyn>
Please locate the nearest terminal and update your credentials.
<SetName><Evelyn>
<SetPortrait><Evelyn>
73 seconds? That's very precise, Robyn.
<SetName><Robyn>
<SetPortrait><Robyn>
Daily reminder #48. Your Earthcorp password will expire in 41 seconds -
<SetName><Evelyn>
<SetPortrait><Evelyn>
Ok!