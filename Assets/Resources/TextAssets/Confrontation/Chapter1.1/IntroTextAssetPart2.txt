//Lines
<SetName><Evelyn>
<SetPortrait><Evelyn>
"A 7 a.m. briefing tomorrow on the profitability of drug patent 01192."
That's just... a tremendous...
<SetName><Robyn>
<SetPortrait><Robyn>
May I be of assistance?
<SetName><Evelyn>
<SetPortrait><Evelyn>
Contact Quinton, Robyn. If I have to stay late, so does he. And I'll need a cost benefit analysis of this... of drug patent 01192. 
<SetName><Robyn>
<SetPortrait><Robyn>
Ringing Quinton Johnson.