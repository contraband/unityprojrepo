﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : AudioObject.cs
// Author : Samuel Schimmel
// Purpose : Abstraction of the AudioSource class.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class AudioObject : MonoBehaviour
{
    public AudioSource audioSource { get; set; }
    public AudioLowPassFilter audioLowPassFilter { get; set; }
    private bool fading;
    private Coroutine fadeCoroutine;
    public bool isMusic { get; set; }
    public static List<AudioObject> audioObjects = new List<AudioObject>();

    public const float FADE_IN_TIME = 4f;
    public const float FADE_OUT_TIME = 4f;
    private const float LOW_PASS_FILTER_ON_FREQUENCY = 400f;
    private const float LOW_PASS_FILTER_OFF_FREQUENCY = 22000f;
    private const float minVolume = 0.8f;
    private const float maxVolume = 1f;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioLowPassFilter = GetComponent<AudioLowPassFilter>();
        audioSource.loop = true;
        audioObjects.Add(this);
    }

    void Start()
    {

    }

    void Update()
    {
    }

    void OnDestroy()
    {
        audioObjects.Remove(this);
    }

    public void PlayLooping(AudioClip clip_, bool fadeIn = false, float startVolume = 1f)
    {
        //Set start volume
        audioSource.volume = startVolume;

        //Set clip
        audioSource.clip = clip_;

        //Fade
        if (fadeIn)
        {
            if (audioSource.volume == 1f) audioSource.volume = 0f;
            FadeIn();
        }

        audioSource.Play();
    }

    public void PlayOnce(AudioClip clip_, bool randomizeVolume = false, bool fadeIn = false)
    {
        float v = 1f;

        if (randomizeVolume)
        {
            v = Random.Range(minVolume, maxVolume);
        }

        //Fade
        if (fadeIn)
        {
            if (audioSource.volume == 1f) audioSource.volume = 0f;
            FadeIn();
        }

        audioSource.PlayOneShot(clip_, v);
    }

    public void Stop(bool destroyAfterStopping = false)
    {
        StopAudio(destroyAfterStopping);
    }

    public void FadeIn(float fadeTime = FADE_IN_TIME)
    {
        if (fading)
        {
            StopCoroutine(fadeCoroutine);
            fading = false;
        }

        fadeCoroutine = StartCoroutine(InterpolateVolume(audioSource.volume, 1f, fadeTime, false, false));
    }

    public void FadeOut(bool stopAfterFading = false, bool destroyAfterStopping = false, float fadeTime = FADE_OUT_TIME)
    {
        if (fading)
        {
            StopCoroutine(fadeCoroutine);
            fading = false;
        }

        fadeCoroutine = StartCoroutine(InterpolateVolume(audioSource.volume, 0f, fadeTime, stopAfterFading, destroyAfterStopping));
    }

    private IEnumerator InterpolateVolume(float startVolume, float endVolume, float fadeTime, bool stop, bool destroy)
    {
        fading = true;

        audioSource.volume = startVolume;
        float elapsedTime = 0;

        while (elapsedTime < fadeTime)
        {
            float t = elapsedTime / fadeTime;
            audioSource.volume = Mathf.Lerp(startVolume, endVolume, t);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        audioSource.volume = endVolume;

        fading = false;
        
        //Stop audio?
        if (endVolume == 0 && stop) StopAudio(destroy);
    }

    private void StopAudio(bool destroy)
    {
        audioSource.Stop();

        //Destroy audio?
        if (destroy) Destroy(gameObject);
    }

    public void ToggleLowPassFilter(bool on)
    {
        print("set low pass filter to " + on);

        if (on) audioLowPassFilter.cutoffFrequency = LOW_PASS_FILTER_ON_FREQUENCY;
        else audioLowPassFilter.cutoffFrequency = LOW_PASS_FILTER_OFF_FREQUENCY;
    }

    public void Restart()
    {
        audioSource.timeSamples = 0;
    }

    public static void ToggleMusic(bool on)
    {
        for (int i = 0; i < audioObjects.Count; ++i)
        {
            if (audioObjects[i].isMusic) audioObjects[i].ToggleAudio(on);
        }
    }

    public void ToggleAudio(bool on)
    {
        audioSource.enabled = on;
    }

    public static void DestroyAll()
    {
        for (int i = 0; i < audioObjects.Count; ++i)
        {
            Destroy(audioObjects[i].gameObject);
        }
    }
}
