﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : controls movement of enemies and the player within the pathfinding
//			 system.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public Vector3 desiredVelocity { get; private set; }
    public float speed { get; private set; }
    public bool isMoving { get; private set; }
    public delegate void MoveStart();
    public delegate void MoveEnd(TileOccupant acceptableCollision, bool instant);
    public event MoveStart onMoveStart;
    public event MoveEnd onMoveEnd;

    private Vector3 destination;
    private TileOccupant acceptableCollision;
    private new Rigidbody2D rigidbody2D;
    private float lastSquareMagnitude;
    private bool instant;

    void Awake()
    {
        desiredVelocity = new Vector3();
        lastSquareMagnitude = Mathf.Infinity;
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
    }

    void Update()
    {
    }

    public void FixedUpdate()
    {
        if (isMoving)
            CheckProgress();

        if (isMoving)
            rigidbody2D.velocity = desiredVelocity;
    }

    public void Move(Vector3 destination_, TileOccupant acceptableCollision_, bool instant_)
    {
        destination = destination_;
        acceptableCollision = acceptableCollision_;
        isMoving = true;
        instant = instant_;

        if (onMoveStart != null)
            onMoveStart();

        //Check if already at destination or instant
        if (transform.position == destination)
        {
            MoveComplete();
        }
        else
        {
            Vector3 velocity = new Vector3();
            velocity = destination - transform.position;
            velocity = velocity.normalized;
            velocity *= speed;
            desiredVelocity = velocity;
            lastSquareMagnitude = Mathf.Infinity;
        }
    }

    public void CheckProgress()
    {
        //Get current square magnitude
        float squareMagnitude = (destination - transform.position).sqrMagnitude;

        //Destination has been reached
        if (squareMagnitude > lastSquareMagnitude)
        {
            MoveComplete();
        }
        //Destination has not been reached
        else
        {
            //Update last square magnitude
            lastSquareMagnitude = squareMagnitude;
        }
    }

    public void MoveComplete()
    {
        isMoving = false;
        transform.position = destination;
        desiredVelocity = Vector3.zero;
        rigidbody2D.velocity = desiredVelocity;
        lastSquareMagnitude = Mathf.Infinity;

        if (onMoveEnd != null)
            onMoveEnd(acceptableCollision, instant);
    }


    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public void Stop()
    {
        isMoving = false;
        desiredVelocity = Vector3.zero;
        rigidbody2D.velocity = desiredVelocity;
        lastSquareMagnitude = Mathf.Infinity;

        if (onMoveEnd != null)
            onMoveEnd(acceptableCollision, instant);
    }
}
