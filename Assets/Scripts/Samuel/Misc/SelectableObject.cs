﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : SelectableObject.cs
// Author : Samuel Schimmel
// Purpose : component tag for anything that is selectable.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class SelectableObject : MonoBehaviour
{
    public bool isCurrentSelection;

    private Player player;

    void Awake()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
    }

    void Start()
    {
        
    }

    void Update()
    {
        isCurrentSelection = false;

        if (player.currentSelection == this.gameObject)
            isCurrentSelection = true;
    }
}
