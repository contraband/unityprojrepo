﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationFadeImage.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConfrontationFadeImage : ImageEffectImage
{
    public delegate void EventHandler();
    public event EventHandler onTransitionEnd;
    public const float TRANSITION_TIME = 2f;
    private Color DEFAULT_COLOR;

    protected override void Awake()
    {
        base.Awake();

        DEFAULT_COLOR = image.color;
    }

    protected override void Start()
    {
        base.Start();

        canvas.sortingLayerName = "ConfrontationFadeImage";
    }

    protected override void Update()
    {
    }

    public void Transition(float endAlpha, float fadeTime = TRANSITION_TIME)
    {
        Color startColor = DEFAULT_COLOR;
        Color endColor = DEFAULT_COLOR;

        startColor.a = image.color.a;
        endColor.a = endAlpha;

        StartCoroutine(Fade(startColor, endColor, fadeTime));
    }

    public void Transition(Color startColor, Color endColor, float fadeTime = TRANSITION_TIME)
    {
        StartCoroutine(Fade(startColor, endColor, fadeTime));
    }

    public void Transition(Color endColor, float fadeTime = TRANSITION_TIME)
    {
        Color startColor = DEFAULT_COLOR;

        startColor.a = image.color.a;

        StartCoroutine(Fade(startColor, endColor, fadeTime));
    }

    private IEnumerator Fade(Color startColor, Color endColor, float fadeTime)
    {
        float elapsedTime = 0;

        while (elapsedTime < fadeTime)
        {
            float t = elapsedTime / fadeTime;
            Color newColor = Color.Lerp(startColor, endColor, Mathf.SmoothStep(0f, 1f, t));
            image.color = newColor;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        image.color = endColor;

        if (onTransitionEnd != null)
            onTransitionEnd();
    }
}
