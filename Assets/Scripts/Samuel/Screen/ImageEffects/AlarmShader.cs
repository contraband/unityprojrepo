﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : AlarmShader.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AlarmShader : ImageEffectImage
{
    public AnimationCurve curve;

    private float time;
    private float maxAlpha = 0.75f;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();

        canvas.sortingLayerName = "AlarmShader";
    }

    protected override void Update()
    {
        if (!player) return;

        if (!player.alarmRaised) return;

        //Color color = image.color;

        //color.a = (Mathf.Sin(time * FREQUENCY) * 0.5f + 0.5f) * MAX;

        //image.color = color;

        //time += Time.deltaTime;

        

        Color color = image.color;
        color.a = Mathf.Lerp(0f, maxAlpha, curve.Evaluate(time));
        image.color = color;
        time += Time.deltaTime;
    }
}
