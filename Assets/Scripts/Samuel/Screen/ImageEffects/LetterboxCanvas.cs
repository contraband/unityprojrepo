﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : LetterboxCanvas.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterboxCanvas : MonoBehaviour
{
    //Set in inspector

    //Const

    //Properties
    private Letterbox top;
    private Letterbox bottom;
	
	private void Awake ()
	{
        top = transform.GetChild(0).GetComponent<Letterbox>();
        bottom = transform.GetChild(1).GetComponent<Letterbox>();
	}
	
	private void Start ()
	{

	}
	
	private void Update ()
	{

	}

    public void ToggleVisibility(bool visible)
    {
        top.ToggleVisibility(visible);
        bottom.ToggleVisibility(visible);
    }
}
