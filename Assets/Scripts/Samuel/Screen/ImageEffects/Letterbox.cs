﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Letterbox.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Letterbox : ImageEffectImage
{
    private const float SCALE_TIME = 1f;
    private float maxHeight;

    protected override void Awake()
    {
        base.Awake();

        maxHeight = rectTransform.sizeDelta.y;

        Vector2 newSizeDelta = rectTransform.sizeDelta;
        newSizeDelta.y = 0;
        rectTransform.sizeDelta = newSizeDelta;
    }

    void Start()
    {
        base.Start();

        canvas.sortingLayerName = "Letterbox";
    }

    protected virtual void Update()
    {
    }

    public void ToggleVisibility(bool visible)
    {
        FindObjectOfType<Player>().inCutscene = visible;

        if (visible)
            StartCoroutine(Scale(maxHeight));
        else
            StartCoroutine(Scale(0f));
    }

    private IEnumerator Scale(float endHeight)
    {
        float elapsedTime = 0;
        float startHeight = rectTransform.sizeDelta.y;

        while (elapsedTime < SCALE_TIME)
        {
            float t = elapsedTime / SCALE_TIME;
            float y = Mathf.SmoothStep(startHeight, endHeight, t);
            Vector2 newSizeDelta = rectTransform.sizeDelta;
            newSizeDelta.y = y;
            rectTransform.sizeDelta = newSizeDelta;

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
}
