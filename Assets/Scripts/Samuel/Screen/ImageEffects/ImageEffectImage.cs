﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ImageEffectImage.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageEffectImage : MonoBehaviour
{
    public Canvas canvas { get; protected set; }
    public Camera hudCamera { get; protected set; }
    public Image image { get; protected set; }
    public Player player { get; protected set; }
    public RectTransform rectTransform { get; protected set; }

    protected virtual void Awake()
    {
        image = GetComponent<Image>();
        canvas = transform.parent.GetComponent<Canvas>();
        player = FindObjectOfType<Player>();
        rectTransform = GetComponent<RectTransform>();
    }

	protected virtual void Start ()
    {
        if (FindObjectOfType<HUDCamera>())
        {
            hudCamera = FindObjectOfType<HUDCamera>().GetComponent<Camera>();
            canvas.worldCamera = hudCamera;
            canvas.planeDistance = 1;
        }
    }

    protected virtual void Update ()
    {
		
	}
}
