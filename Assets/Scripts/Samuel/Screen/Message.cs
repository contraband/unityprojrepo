﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : StateChangeMessage.cs
// Author : Samuel Schimmel
// Purpose : Outputs a text message regarding the status of the current turn.
//			 Moves the message, handles message appearance, content, and triggers
//			 events based on message context.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class Message : MonoBehaviour
{
    //Set in inspector
    public bool waitForPlayerAction = false;
    public Vector2 start;
    public Vector2 middle;
    public Vector2 end;
    public float smoothTime;
    public float waitTime;

    private float xOffset;
    private Vector3 foregroundOffset = new Vector3(2, 0, 0);
    private TextMeshProUGUI text;

    private bool isBeingRemoved;
    private AudioSource audioSource;
    private RectTransform rectTransform;
    private string htmlColorString;
    private string contents;

    //Queue
    private static List<Message> queue;
    private static int activeInstances;
    private const int maxActiveInstances = 1;

    void Awake()
    {
        if (queue == null) queue = new List<Message>(); 

        rectTransform = GetComponent<RectTransform>();

        audioSource = GetComponent<AudioSource>();
        text = GetComponent<TextMeshProUGUI>();

        Color newColor = text.color;
        newColor.a = 0;
        text.color = newColor;

        AddToQueue();
    }

    void SetPositions()
    {
        //xOffset = Screen.width / 2;
        //xOffset += LayoutUtility.GetPreferredWidth(rectTransform) / 2;

        ////Start
        //if (start == Vector2.zero)
        //{
        //    start = transform.TransformPoint(start);
        //    start.x -= xOffset;
        //}
        //else
        //{
        //    start = transform.TransformPoint(start);
        //}

        ////Middle
        //middle = transform.TransformPoint(middle);

        ////End
        //if (end == Vector2.zero)
        //{
        //    end = transform.TransformPoint(end);
        //    end.x += xOffset;
        //}
        //else
        //{
        //    end = transform.TransformPoint(end);
        //}

        start = transform.TransformPoint(start);
        middle = transform.TransformPoint(middle);
        end = transform.TransformPoint(end);
        AvoidVerticalOverlap();
    }

    private void AvoidVerticalOverlap()
    {
        //Check for other messages 
        Message[] stateChangeMessages = (Message[])GameObject.FindObjectsOfType(typeof(Message));
        Message lowestMessage = this;
        for(int i = 0; i < stateChangeMessages.Length; ++i)
        {
            //Ignore messages from different prefabs
            if (stateChangeMessages[i].name != name)
                continue;

            //Ignore messages being removed
            if (stateChangeMessages[i].isBeingRemoved)
                continue;

            //Find lowest message
            if (stateChangeMessages[i].transform.position.y <= lowestMessage.transform.position.y)
                lowestMessage = stateChangeMessages[i];
        }

        //Offset y position to avoid overlap
        if (lowestMessage != this)
        {
            start.y = lowestMessage.start.y - text.fontSize;
            middle.y = lowestMessage.middle.y - text.fontSize;
            end.y = lowestMessage.end.y - text.fontSize;
        }

        //Move to start position
        transform.position = start;
    }

    void Update ()
    {
        text.alignment = TextAlignmentOptions.Center;
    }

    private void AddToQueue()
    {
        if (activeInstances >= maxActiveInstances)
        {
            queue.Add(this);
        }
    }

    public static void DispatchQueue()
    {
        if (queue == null) return;

        if (queue.Count < 1) return;

        if (activeInstances >= maxActiveInstances) return;

        Message firstMessageInQueue = queue[0];
        firstMessageInQueue.text.text = firstMessageInQueue.contents;
        firstMessageInQueue.SetPositions();
        firstMessageInQueue.StartMovement();
        queue.Remove(firstMessageInQueue);
    }

    public void ShowMessage(string contents_)
    {
        contents = contents_;

        if (queue.Contains(this)) return;

        text.text = contents;
        SetPositions();
        StartMovement();
    }

    public void RemoveMessage()
    {
        StartCoroutine(Move(end));
        StartCoroutine(Fade(0f));
        if(htmlColorString != null) StartCoroutine(FadeHTMLTextColor(0f));
        isBeingRemoved = true;
    }

    void StartMovement()
    {
        ++activeInstances;
        StartCoroutine(Move(middle));
        StartCoroutine(Fade(1f));
    }

    private IEnumerator Move(Vector3 endPosition)
    {
        float lastSquareMagnitude = Mathf.Infinity;
        Vector3 velocity = Vector3.zero;

        while ((endPosition - transform.position).sqrMagnitude > 0.01f)
        {
            transform.position = Vector3.SmoothDamp(transform.position, endPosition, ref velocity, smoothTime);

            lastSquareMagnitude = (endPosition - transform.position).sqrMagnitude;
            yield return null;
        }

        if (!isBeingRemoved)
        {
            //Remove immediately
            if (!waitForPlayerAction)
                Invoke("RemoveMessage", waitTime);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator Fade(float endAlpha)
    {
        float elapsedTime = 0;
        float startAlpha = text.color.a;

        while (elapsedTime < smoothTime)
        {
            float a = Mathf.SmoothStep(startAlpha, endAlpha, (elapsedTime / smoothTime));
            Color newColor = text.color;
            newColor.a = a;
            text.color = newColor;

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator FadeHTMLTextColor(float endAlpha)
    {        
        //Coroutine
        float elapsedTime = 0;

        //Convert hex to RGB
        byte R = byte.Parse(htmlColorString.Substring(7, 2), System.Globalization.NumberStyles.HexNumber);
        byte G = byte.Parse(htmlColorString.Substring(9, 2), System.Globalization.NumberStyles.HexNumber);
        byte B = byte.Parse(htmlColorString.Substring(11, 2), System.Globalization.NumberStyles.HexNumber);
        Color32 startColor = new Color32(R, G, B, 255);
        Color32 endColor = new Color32(R, G, B, 0);

        while (elapsedTime < smoothTime)
        {
            Color32 newColor = Color32.Lerp(startColor, endColor, (elapsedTime / smoothTime));

            //Convert RGB to hex and update text
            string newText = "";
            bool color = false;
            string hex = newColor.r.ToString("X2") + newColor.g.ToString("X2") + newColor.b.ToString("X2") + newColor.a.ToString("X2");

            for (int c = 0; c < text.text.Length; ++c)
            {
                if(color == true)
                {
                    for (int i = 0; i < 8; ++i)
                    {
                        newText += hex[i];
                        ++c;
                    }
                    color = false;
                }

                if (text.text[c] == '#')
                    color = true;
                    
                newText += text.text[c];
            }
            text.text = newText;

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    private void OnDestroy()
    {
        --activeInstances;
        queue.Remove(this);
    }

    public static void DestroyAllMessages()
    {
        Message[] stateChangeMessages = (Message[])GameObject.FindObjectsOfType(typeof(Message));
        for (int i = 0; i < stateChangeMessages.Length; ++i)
        {
            Destroy(stateChangeMessages[i].gameObject);
        }
    }
}
