﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Apartment.cs
// Author : Samuel Schimmel
// Purpose : This script handles the logic of the apartment -dialogue, sound cues, 
//			 etc. 
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(DiegeticMonitor))]
public class Apartment : MonoBehaviour
{
    private Dialogue dialogue;
	private TextMeshProUGUI text;
    private LevelManager levelManager;
    private GameManager gameManager;
    public AudioObject audioObject { get; set; }

	void Awake()
	{
		text = transform.FindChild("Textbox").FindChild("Text").GetComponent<TextMeshProUGUI>();
        dialogue = text.GetComponent<Dialogue>();
        levelManager = FindObjectOfType<LevelManager>();
        audioObject = gameObject.AddComponent<AudioObject>();
    }

	void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        audioObject.PlayLooping(gameManager.apartmentClip, true);
    }

	void OnEnable()
	{
		dialogue.onCommand += ExecuteCommand;
		dialogue.onDialogueFinished += DialogueFinished;
		dialogue.onDialogueClosed += DialogueClosed;
	}

	void OnDisable()
	{
		dialogue.onCommand -= ExecuteCommand;
		dialogue.onDialogueFinished -= DialogueFinished;
		dialogue.onDialogueClosed -= DialogueClosed;
	}

    public void SetScript(string newScript)
    {
        dialogue.SetScript(newScript);
        dialogue.dialogueState = Dialogue.DialogueStates.During;
        dialogue.Continue();
    }

    void Update ()
	{
	
	}

	void ExecuteCommand(string command, string resource)
	{

	}

	void DialogueFinished()
	{

	}

	void DialogueClosed()
	{
        levelManager.TransitionToInfiltrationState();
	}

    public void ClearText()
    {
        text.text = "";
    }
}
