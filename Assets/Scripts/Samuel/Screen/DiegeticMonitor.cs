﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : DiegeticMonitor.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
sealed public class DiegeticMonitor : MonoBehaviour
{
    //Set in inspector
    public bool apartment = false;

    public Canvas canvas { get; private set; }
    public MainCamera mainCamera { get; private set; }
    public GameManager gameManager { get; private set; }
    private CustomCursor customCursor;
    private List<Image> images;
    private List<TextMeshProUGUI> texts;
    public static float transitionTime = 2f;

    void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        customCursor = gameManager.GetComponent<CustomCursor>();
        canvas = GetComponent<Canvas>();

        mainCamera = FindObjectOfType<MainCamera>();
        canvas.worldCamera = mainCamera.GetComponent<Camera>();
        canvas.planeDistance = 5;
        canvas.sortingLayerName = "DiegeticMonitors";

        mainCamera.ToggleMonitorEffects(true, apartment, 0f);

        InitializeImagesAndTexts();
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    void OnDestroy()
    {
    }

    private void InitializeImagesAndTexts()
    {
        images = new List<Image>();
        texts = new List<TextMeshProUGUI>();

        Transform[] children = gameObject.GetComponentsInChildren<Transform>();

        foreach (var child in children)
        {
            Image image = child.GetComponent<Image>();
            if (image) images.Add(image);

            TextMeshProUGUI text = child.GetComponent<TextMeshProUGUI>();
            if (text) texts.Add(text);
        }
    }

    public void Transition(bool transitionIn)
    {
        if (transitionIn) StartCoroutine(TransitionCoroutine(1f));
        else StartCoroutine(TransitionCoroutine(0f));

        mainCamera.ToggleMonitorEffects(transitionIn, apartment);
    }

    private IEnumerator TransitionCoroutine(float endAlpha)
    {
        float elapsedTime = 0f;
        float startAlpha = images[0].color.a;

        while (elapsedTime < transitionTime)
        {
            float t = elapsedTime / transitionTime;

            foreach (var image in images)
            {
                Color newColor = image.color;
                newColor.a = Mathf.Lerp(startAlpha, endAlpha, t);
                image.color = newColor;
            }
            foreach (var text in texts)
            {
                Color newColor = text.color;
                newColor.a = Mathf.Lerp(startAlpha, endAlpha, t);
                text.color = newColor;
            }

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        foreach (var image in images)
        {
            Color newColor = image.color;
            newColor.a = endAlpha;
            image.color = newColor;
        }
        foreach (var text in texts)
        {
            Color newColor = text.color;
            newColor.a = endAlpha;
            text.color = newColor;
        }

        FindObjectOfType<Apartment>().ClearText();
    }
}
