﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : DiegeticMonitorBackground.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class DiegeticMonitorBackground : MonoBehaviour
{
    public Canvas canvas { get; private set; }
    public Camera hudCamera { get; private set; }
    private RectTransform backgroundRectTransform;
    private const float transitionTime = 6f;
    private Vector2 onSizeDelta;
    private Vector2 offSizeDelta;

    protected virtual void Awake()
    {
        canvas = GetComponent<Canvas>();

        hudCamera = FindObjectOfType<HUDCamera>().GetComponent<Camera>();
        canvas.worldCamera = hudCamera;
        canvas.planeDistance = 5;
        canvas.sortingLayerName = "DiegeticMonitorBackgrounds";
        backgroundRectTransform = transform.GetChild(0).GetComponent<RectTransform>();
        onSizeDelta = backgroundRectTransform.sizeDelta;
        offSizeDelta = onSizeDelta * 3f;
    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {

    }

    public void Transition(bool transitionIn)
    {
        if (transitionIn) StartCoroutine(TransitionCoroutine(onSizeDelta));
        else StartCoroutine(TransitionCoroutine(offSizeDelta));
    }

    private IEnumerator TransitionCoroutine(Vector2 endSizeDelta)
    {
        Vector2 startSizeDelta = backgroundRectTransform.sizeDelta;
        float elapsedTime = 0;

        while (elapsedTime < transitionTime)
        {
            float t = elapsedTime / transitionTime;
            float x = Mathf.SmoothStep(startSizeDelta.x, endSizeDelta.x, t);
            float y = Mathf.SmoothStep(startSizeDelta.y, endSizeDelta.y, t);
            Vector2 newSizeDelta = backgroundRectTransform.sizeDelta;
            newSizeDelta.x = x;
            newSizeDelta.y = y;
            backgroundRectTransform.sizeDelta = newSizeDelta;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (endSizeDelta == onSizeDelta) FindObjectOfType<LevelManager>().GoToNextChapter();
    }
}
