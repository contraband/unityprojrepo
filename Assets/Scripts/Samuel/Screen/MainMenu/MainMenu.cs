﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioObject))]
public class MainMenu : MonoBehaviour
{
    //Set in inspector
    public AudioClip enterClip;
    public AudioClip selectionClip;
    public AudioClip returnClip;

    public List<MainMenuText> texts { get; private set; }
    public int textsComplete { get; private set; }
    public MainMenuText headerText { get; private set; }
    public MainMenuText infoText { get; private set; }
    public MainMenuText returnText { get; private set; }
    public LevelManager levelManager { get; private set; }
    public AudioObject audioObject { get; set; }
    public string creditsString { get; private set; }
    public string howToPlayString { get; private set; }
 
    void Awake()
    {
        texts = new List<MainMenuText>();
        Transform defaultGroup = transform.FindChild("Textbox").FindChild("Default");
        headerText = defaultGroup.FindChild("HeaderText").GetComponent<MainMenuText>();
        infoText = defaultGroup.FindChild("InfoText").GetComponent<MainMenuText>();
        returnText = defaultGroup.FindChild("Return").FindChild("ReturnText").GetComponent<MainMenuText>();
        levelManager = FindObjectOfType<LevelManager>();
        audioObject = GetComponent<AudioObject>();

        creditsString = Resources.Load<TextAsset>("TextAssets/MainMenu/Credits").ToString();
        howToPlayString = Resources.Load<TextAsset>("TextAssets/MainMenu/HowToPlay").ToString();
    }

	void Start ()
    {
    }
	
	void Update ()
    {
		
	}

    public void InitializeMenu()
    {
        textsComplete = 0;
        headerText.FillDefault();
        infoText.FillDefault();
        infoText.onTextComplete += TextInitialized;
    }

    public void TextInitialized()
    {
        texts[textsComplete].onTextComplete -= TextInitialized;
        ++textsComplete;

        if (textsComplete < texts.Count)
        {
            //Main menu selection
            if (texts[textsComplete].group == MainMenuText.MainMenuTextGroups.Main)
            {
                texts[textsComplete].onTextComplete += TextInitialized;
                texts[textsComplete].FillDefault();
            }
            //Other
            else
            {
                TextInitialized();
            }
        }

        ToggleSelections(MainMenuText.MainMenuTextGroups.Main, true);
    }

    public void AddMainMenuText(MainMenuText text)
    {
        texts.Add(text);
    }

    public void SelectionMade(MainMenuText.MainMenuTextTypes type)
    {
        ToggleSelections(MainMenuText.MainMenuTextGroups.Main, false);

        EraseAll();

        switch(type)
        {
            case MainMenuText.MainMenuTextTypes.Play:
                Play();
                audioObject.PlayOnce(selectionClip, true);
                break;

            case MainMenuText.MainMenuTextTypes.HowToPlay:
                HowToPlay();
                audioObject.PlayOnce(selectionClip, true);
                break;

            case MainMenuText.MainMenuTextTypes.Credits:
                Credits();
                audioObject.PlayOnce(selectionClip, true);
                break;

            case MainMenuText.MainMenuTextTypes.Quit:
                Quit();
                audioObject.PlayOnce(selectionClip, true);
                break;

            case MainMenuText.MainMenuTextTypes.Return:
                Return();
                audioObject.PlayOnce(returnClip, true);
                break;

            case MainMenuText.MainMenuTextTypes.QuitYes:
                ConfirmQuit();
                break;
        }
    }

    void EraseAll()
    {
        for(int i = 0; i < texts.Count; ++i)
        {
            if (texts[i].type == MainMenuText.MainMenuTextTypes.Header)
                continue;

            texts[i].Erase();
        }
    }

    void ToggleSelections(MainMenuText.MainMenuTextGroups group, bool enabled)
    {
        for (int i = 0; i < texts.Count; ++i)
        {
            if (texts[i].group != group) continue;

            texts[i].enabled = enabled;
        }
    }

    void ToggleReturnText(bool enabled)
    {
        returnText.enabled = enabled;

        if (returnText.enabled) returnText.FillDefault();
        else returnText.Erase();
    }

    private void Play()
    {
        headerText.Erase();

        levelManager.TransitionToApartmentState();

        GetComponent<GraphicRaycaster>().enabled = false;
    }

    private void HowToPlay()
    {
        infoText.Fill(howToPlayString);

        ToggleReturnText(true);
    }

    private void Credits()
    {
        infoText.Fill(creditsString);

        ToggleReturnText(true);
    }

    private void Quit()
    {
        infoText.Fill("Really quit?");

        ToggleSelections(MainMenuText.MainMenuTextGroups.Quit, true);

        for (int i = 0; i < texts.Count; ++i)
        {
            if (texts[i].group != MainMenuText.MainMenuTextGroups.Quit)
                continue;

            texts[i].FillDefault();
        }
    }

    private void ConfirmQuit()
    {
        Application.Quit();
    }

    private void Return()
    {
        ToggleSelections(MainMenuText.MainMenuTextGroups.Quit, false);
        ToggleReturnText(false);
        infoText.Erase();

        textsComplete = 1;
        texts[textsComplete].onTextComplete += TextInitialized;
        texts[textsComplete].FillDefault();
    }
}
