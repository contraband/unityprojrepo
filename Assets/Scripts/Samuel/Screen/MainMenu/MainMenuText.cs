﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : MainMenuText.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuText : MonoBehaviour
{
    //Enums
    public enum MainMenuTextTypes { Header, Info, Play, HowToPlay, Credits, Quit, Return, QuitYes };
    public enum MainMenuTextGroups { Default, Main, Quit };

    //Events
    public delegate void Delegate();
    public event Delegate onTextComplete;

    //Set in inspector
    public MainMenuTextTypes type = MainMenuTextTypes.Header;
    public MainMenuTextGroups group = MainMenuTextGroups.Default;
    public bool interactive = false;
    public string altText;
    //public Color hoverColor;

    //Const
    private const float WAIT_TIME = 0.01f;
    private const float fadeTime = 0.5f;

    //Public get, public set
    new public bool enabled { get; set; }

    //Public get, private set
    public MainMenu mainMenu { get; private set; }
    public TextMeshProUGUI text { get; private set; }
    public Image image { get; private set; }
    public string defaultText { get; private set; }
    public string hoverText { get; private set; }
    public Color defaultColorOpaque { get; private set; }
    public Color defaultColorTransparent { get; private set; }
    public Coroutine coroutine { get; private set; }
    public bool coroutineRunning { get; private set; }
    public Vector3 defaultPosition { get; private set; }

    void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
        if(interactive) image = GetComponentInParent<Image>();
        mainMenu = transform.parent.GetComponentInParent<MainMenu>();

        defaultText = text.text;
        text.text = "";
        text.enabled = false;
        defaultColorOpaque = text.color;
        Color c = defaultColorOpaque;
        c.a = 0f;
        defaultColorTransparent = c;
        defaultPosition = transform.localPosition;

        if(altText == "") hoverText = "mad_mal@mal-desktop:~$ " + defaultText;
        else hoverText = altText;

        mainMenu.AddMainMenuText(this);
    }

    void Start()
    {
    }

    void OnGUI()
    {
    }

    public void Enter()
    {
        if (!enabled) return;

        StopCoroutine(coroutine);
        coroutineRunning = false;
        if (onTextComplete != null) onTextComplete();

        mainMenu.audioObject.PlayOnce(mainMenu.enterClip, true);

        Fill(hoverText);
        if (image) image.enabled = true;
    }

    public void Exit()
    {
        if (!enabled) return;

        Fill(defaultText);
        text.color = defaultColorOpaque;
        if (image) image.enabled = false;
    }

    public void Click()
    {
        if (!enabled) return;

        mainMenu.SelectionMade(type);
    }

    public void FillDefault()
    {
        Fill(defaultText);
        text.color = defaultColorOpaque;
    }

    public void Fill(string text_)
    {
        text.text = "";
        text.enabled = true;

        if (coroutineRunning)
        {
            StopCoroutine(coroutine);
            coroutineRunning = false;
        }

        coroutine = StartCoroutine(FillCoroutine(text_));
    }

    private IEnumerator FillCoroutine(string text_)
    {
        coroutineRunning = true;
        int currentCharacterIndex = 0;

        while (currentCharacterIndex < text_.Length)
        {
            text.text += text_[currentCharacterIndex];
            ++currentCharacterIndex;

            yield return new WaitForSeconds(WAIT_TIME);
        }

        coroutineRunning = false;
        if (onTextComplete != null) onTextComplete();
    }

    private IEnumerator FadeCoroutine()
    {
        float elapsedTime = 0;

        while (elapsedTime < fadeTime)
        {
            float t = elapsedTime / fadeTime;
            float a = Mathf.SmoothStep(0f, 1f, t);
            Color newColor = text.color;
            newColor.a = a;
            text.color = newColor;

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    public void Erase()
    {
        if (coroutineRunning)
        {
            StopCoroutine(coroutine);
            coroutineRunning = false;
        }

        text.text = "";
        text.enabled = false;
        if (image) image.enabled = false;
    }
}
