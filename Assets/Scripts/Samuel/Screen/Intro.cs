﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Intro.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
sealed public class Intro : MonoBehaviour
{
    public Canvas canvas { get; private set; }
    public Camera hudCamera { get; private set; }
    public GameManager gameManager { get; private set; }
    private CustomCursor customCursor;
    private List<Image> images;
    private List<TextMeshProUGUI> texts;
    private FadeImage fadeImage;
    private float waitTime = 2f;
    private bool fadingOut;

    void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        canvas = GetComponent<Canvas>();

        hudCamera = FindObjectOfType<HUDCamera>().GetComponent<Camera>();
        canvas.worldCamera = hudCamera;
        canvas.planeDistance = 5;
        canvas.sortingLayerName = "IntroAndOutroCanvases";
    }

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)
            || Input.GetKeyDown(KeyCode.Return)
            || Input.GetKeyDown(KeyCode.Escape)
            || Input.GetMouseButtonDown(0)
            || Input.GetMouseButtonDown(1))
        {
            if(!fadingOut) FindObjectOfType<LevelManager>().EnterMainMenuState();
        }

    }

    public void FadeInEnded()
    {
        gameManager.otherMusicAudioObject.PlayOnce(gameManager.pianoClip, false);
        FindObjectOfType<FadeImage>().onTransitionEnd -= FadeInEnded;
        if(this) Invoke("ExitIntro", waitTime);
    }

    private void ExitIntro()
    {
        fadingOut = true;
        FindObjectOfType<LevelManager>().ExitIntro();
    }

    public void FadeOutEnded()
    {

    }
}
