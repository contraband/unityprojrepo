// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Dialogue.cs
// Author : Samuel Schimmel
// Purpose : Generic dialogue system that uses TextAssets
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

[RequireComponent(typeof(AudioObject))]
public class Dialogue : MonoBehaviour
{
    public enum DialogueStates { Before, During, After }

    public delegate void EventHandler();
    public delegate void CommandHandler(string command, string resource);
    public event EventHandler onDialogueClosed;
    public event EventHandler onDialogueFinished;
    public event CommandHandler onCommand;

    public TextMeshProUGUI text;
    private bool isAppending = false;
    private int indexOfCurrentCharacter;
    private Coroutine appendCharactersCoroutine;
    private RectTransform textboxRectTransform;
    public string script { get; private set; }
    private GameManager gameManager;
    public bool firstLineOfTextAsset { get; set; }

    //Settings
    public bool clearOnNewLine { get; set; }
    public bool wait { get; set; }
    public float waitTime { get; set; }
    public DialogueStates dialogueState { get; set; }
    private const bool DEFAULT_CLEAR_ON_NEW_LINE = true;
    private const bool DEFAULT_WAIT = true;
    private const float DEFAULT_WAIT_TIME = 0.01f;
    private TextAlignment DEFAULT_HORIZONTAL_ALIGNMENT;
    private const float DEFAULT_DELAY_TIME = 2f;

    //Audio
    private AudioObject audioObject;
    private AudioClip currentNewCharacterClip;
    private AudioClip currentNewLineClip;

    void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = "";
        textboxRectTransform = transform.parent.GetComponent<RectTransform>();
        AlignTop();
        gameManager = FindObjectOfType<GameManager>();
        audioObject = gameObject.AddComponent<AudioObject>();
        dialogueState = DialogueStates.Before;

        //Defaults
        GetDefaultHorizontalAlignment();
        clearOnNewLine = DEFAULT_CLEAR_ON_NEW_LINE;
        wait = DEFAULT_WAIT;
        waitTime = DEFAULT_WAIT_TIME;
        currentNewCharacterClip = gameManager.defaultNewCharacterClip;
        currentNewLineClip = gameManager.defaultNewLineClip;
    }

    void Start()
    {
    }

    void Update()
    {
        if (script == null) return;

        //Dialogue finished?
        if (dialogueState == DialogueStates.During && indexOfCurrentCharacter >= script.Length)
        {
            dialogueState = DialogueStates.After;
            if (onDialogueFinished != null) onDialogueFinished();
        }

        //Continue
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return)) Continue();
    }

    public void Continue()
    {
        if (script == null) return;

        //Skip appending
        if (isAppending)
        {
            SkipAppending(true);
            return;
        }

        //End of text asset
        if (indexOfCurrentCharacter >= script.Length)
        {
            script = null;
            if (onDialogueClosed != null) onDialogueClosed();
            return;
        }

        if (dialogueState != DialogueStates.During) return;

        //Empty line
        if (script[indexOfCurrentCharacter] == '\r'
            && script[indexOfCurrentCharacter + 1] == '\n')
        {
            ++indexOfCurrentCharacter;
            ++indexOfCurrentCharacter;
            Continue();
            return;
        }

        //Comment
        if (script[indexOfCurrentCharacter] == '/' && script[indexOfCurrentCharacter + 1] == '/')
        {
            while (script[indexOfCurrentCharacter] != '\n') ++indexOfCurrentCharacter;
            ++indexOfCurrentCharacter;

            Continue();
            return;
        }

        //Command
        if (script[indexOfCurrentCharacter] == '<' && IssueCommand()) return;

        //Lines
        if (wait) appendCharactersCoroutine = StartCoroutine(AppendCharacters());
        else SkipAppending(false);
    }

    private IEnumerator AppendCharacters()
    {
        if (clearOnNewLine && !firstLineOfTextAsset)
        {
            text.text = "";
            AlignTop();
        }

        audioObject.PlayOnce(currentNewCharacterClip);
        isAppending = true;
        while (indexOfCurrentCharacter < script.Length
            && script[indexOfCurrentCharacter] != '\n')
        {
            text.text += script[indexOfCurrentCharacter];
            ++indexOfCurrentCharacter;
            CheckVerticalOverflow();
            yield return new WaitForSeconds(waitTime);
        }
        isAppending = false;
        audioObject.Stop();
        audioObject.PlayOnce(currentNewLineClip);

        if (!clearOnNewLine)
        {
            text.text += '\n';
            text.text += '\n';
        }
        ++indexOfCurrentCharacter;

        firstLineOfTextAsset = false;
    }

    private void SkipAppending(bool interrupt)
    {
        if (interrupt)
            StopCoroutine(appendCharactersCoroutine);
        else
        {
            if (clearOnNewLine && !firstLineOfTextAsset)
            {
                text.text = "";
                AlignTop();
            }
        }

        while (indexOfCurrentCharacter < script.Length
            && script[indexOfCurrentCharacter] != '\n')
        {
            text.text += script[indexOfCurrentCharacter];
            ++indexOfCurrentCharacter;
        }
        CheckVerticalOverflow();
        isAppending = false;
        audioObject.Stop();
        audioObject.PlayOnce(currentNewLineClip);

        if (!clearOnNewLine)
        {
            text.text += '\n';
            text.text += '\n';
        }
        ++indexOfCurrentCharacter;

        firstLineOfTextAsset = false;
    }

    private bool IssueCommand()
    {
        int startIndex = indexOfCurrentCharacter;
        float delayTime = 0f;

        //Get command
        string command = "";
        ++indexOfCurrentCharacter;
        while (script[indexOfCurrentCharacter] != '>')
        {
            command += script[indexOfCurrentCharacter];
            ++indexOfCurrentCharacter;
        }
        ++indexOfCurrentCharacter;

        //Return false and reset index if rich text is detected
        if (command == "b" || command == "/b" || command == "i" || command == "/i" || command.Contains("size") || command.Contains("color"))
        {
            indexOfCurrentCharacter = startIndex;
            return false;
        }

        //Get resource
        string resource = "";
        ++indexOfCurrentCharacter;
        while (script[indexOfCurrentCharacter] != '>')
        {
            resource += script[indexOfCurrentCharacter];
            ++indexOfCurrentCharacter;
        }
        ++indexOfCurrentCharacter;

        //Set clear on new line
        if (command == "SetClearOnNewLine")
        {
            if (resource == "") clearOnNewLine = DEFAULT_CLEAR_ON_NEW_LINE;
            else clearOnNewLine = bool.Parse(resource);
        }
        //Set wait
        else if (command == "SetWait")
        {
            if (resource == "") wait = DEFAULT_WAIT;
            else wait = bool.Parse(resource);
        }
        //Set wait time
        else if (command == "SetWaitTime")
        {
            if (resource == "") waitTime = DEFAULT_WAIT_TIME;
            else waitTime = float.Parse(resource);
        }
        //Set alignment
        else if (command == "SetAlignment")
        {
            if (resource == "") AlignHorizontal(DEFAULT_HORIZONTAL_ALIGNMENT);
            else if (resource == "LEFT") AlignHorizontal(TextAlignment.Left);
            else if (resource == "CENTER") AlignHorizontal(TextAlignment.Center);
            else if (resource == "RIGHT") AlignHorizontal(TextAlignment.Right);
        }
        //Choices
        else if (command == "Choice")
        {
            //Split resource
            string[] resources = resource.Split('|');
            string key = resources[0];
            string value = resources[1];

            //Update choice
            gameManager.choices[key] = int.Parse(value);
        }
        //Playing a sound
        else if (command == "PlaySound")
        {
            AudioClip clip = Resources.Load<AudioClip>("Audio/" + resource);
            audioObject.PlayOnce(clip);
        }
        //Changing the new character sound
        else if(command == "SetNewCharacterClip")
        {
            AudioClip clip = null;

            if (resource == "") clip = gameManager.defaultNewCharacterClip;
            else clip = Resources.Load<AudioClip>("Audio/" + resource);

            currentNewCharacterClip = clip;
        }
        //Changing the new line sound
        else if (command == "SetNewLineClip")
        {
            AudioClip clip = null;

            if (resource == "") clip = gameManager.defaultNewLineClip;
            else clip = Resources.Load<AudioClip>("Audio/" + resource);

            currentNewLineClip = clip;
        }
        //Fade
        else if (command == "Fade")
        {
            float value = 0f;

            if (resource == "In")
            {
                value = 0f;
                FindObjectOfType<Player>().audioObject.ToggleAudio(true);
                gameManager.otherMusicAudioObject.PlayLooping(gameManager.tutorialClip, true);
            }
            else if (resource == "Out")
            {
                value = 1f;
                FindObjectOfType<Player>().audioObject.ToggleAudio(false);
                gameManager.otherMusicAudioObject.FadeOut(false, false, 0f);
            }

            FindObjectOfType<FadeImage>().Transition(value);
        }
        //Delay
        else if (command == "Delay")
        {
            if (resource == "") delayTime = DEFAULT_DELAY_TIME;
            else delayTime = int.Parse(resource);
        }
        //Clear
        else if (command == "Clear")
        {
            text.text = "";
        }
        //Custom objective
        else if (command == "SetCustomObjective")
        {
            FindObjectOfType<CurrentObjective>().customObjective = resource;
        }
        //Set can exit
        else if (command == "SetCanExit")
        {
            FindObjectOfType<Player>().canExit = bool.Parse(resource);
        }
        //Dispatch other command
        else if (onCommand != null) onCommand(command, resource);

        //Continue
        indexOfCurrentCharacter += 2;
        Invoke("Continue", delayTime);

        return true;
    }

    private void CheckVerticalOverflow()
    {
        float textHeight = LayoutUtility.GetPreferredHeight(text.rectTransform);
        float parentHeight = textboxRectTransform.rect.height;

        if (textHeight > parentHeight) AlignBottom();
    }

    private void GetDefaultHorizontalAlignment()
    {
        //switch (text.alignment)
        //{
        //    case TextAnchor.UpperLeft:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Left;
        //        break;
        //    case TextAnchor.UpperCenter:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Center;
        //        break;
        //    case TextAnchor.UpperRight:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Right;
        //        break;
        //    case TextAnchor.MiddleLeft:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Left;
        //        break;
        //    case TextAnchor.MiddleCenter:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Center;
        //        break;
        //    case TextAnchor.MiddleRight:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Right;
        //        break;
        //    case TextAnchor.LowerLeft:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Left;
        //        break;
        //    case TextAnchor.LowerCenter:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Center;
        //        break;
        //    case TextAnchor.LowerRight:
        //        DEFAULT_HORIZONTAL_ALIGNMENT = TextAlignment.Right;
        //        break;
        //}
    }

    private void AlignHorizontal(TextAlignment alignment)
    {
        ////Text is vertically aligned to the top
        //if (text.alignment == TextAnchor.UpperLeft
        //    || text.alignment == TextAnchor.UpperCenter
        //    || text.alignment == TextAnchor.UpperRight)
        //{
        //    //Upper left
        //    if (alignment == TextAlignment.Left)
        //        text.alignment = TextAnchor.UpperLeft;
        //    else if (alignment == TextAlignment.Center)
        //        text.alignment = TextAnchor.UpperCenter;
        //    else
        //        text.alignment = TextAnchor.UpperRight;
        //}

        ////Text is vertically aligned to the bottom
        //else if (text.alignment == TextAnchor.LowerLeft
        //    || text.alignment == TextAnchor.LowerCenter
        //    || text.alignment == TextAnchor.LowerRight)
        //{
        //    //Upper left
        //    if (alignment == TextAlignment.Left)
        //        text.alignment = TextAnchor.LowerLeft;
        //    else if (alignment == TextAlignment.Center)
        //        text.alignment = TextAnchor.LowerCenter;
        //    else
        //        text.alignment = TextAnchor.LowerRight;
        //}
    }

    private void AlignTop()
    {
        //text.rectTransform.anchorMin = new Vector2(0.5f, 1);
        //text.rectTransform.anchorMax = new Vector2(0.5f, 1);
        //text.rectTransform.pivot = new Vector2(0.5f, 1);

        //switch (text.alignment)
        //{
        //    case TextAnchor.UpperLeft:
        //        text.alignment = TextAnchor.UpperLeft;
        //        break;
        //    case TextAnchor.UpperCenter:
        //        text.alignment = TextAnchor.UpperCenter;
        //        break;
        //    case TextAnchor.UpperRight:
        //        text.alignment = TextAnchor.UpperRight;
        //        break;
        //    case TextAnchor.MiddleLeft:
        //        text.alignment = TextAnchor.UpperLeft;
        //        break;
        //    case TextAnchor.MiddleCenter:
        //        text.alignment = TextAnchor.UpperCenter;
        //        break;
        //    case TextAnchor.MiddleRight:
        //        text.alignment = TextAnchor.UpperRight;
        //        break;
        //    case TextAnchor.LowerLeft:
        //        text.alignment = TextAnchor.UpperLeft;
        //        break;
        //    case TextAnchor.LowerCenter:
        //        text.alignment = TextAnchor.UpperCenter;
        //        break;
        //    case TextAnchor.LowerRight:
        //        text.alignment = TextAnchor.UpperRight;
        //        break;
        //}
    }

    private void AlignBottom()
    {
        switch (text.alignment)
        {
            case TextAlignmentOptions.TopLeft:
                text.alignment = TextAlignmentOptions.BottomLeft;
                break;
            case TextAlignmentOptions.Top:
                text.alignment = TextAlignmentOptions.Bottom;
                break;
            case TextAlignmentOptions.TopRight:
                text.alignment = TextAlignmentOptions.BottomRight;
                break;
            case TextAlignmentOptions.Left:
                text.alignment = TextAlignmentOptions.BottomLeft;
                break;
            case TextAlignmentOptions.Center:
                text.alignment = TextAlignmentOptions.Bottom;
                break;
            case TextAlignmentOptions.Right:
                text.alignment = TextAlignmentOptions.BottomRight;
                break;
            case TextAlignmentOptions.BottomLeft:
                text.alignment = TextAlignmentOptions.BottomLeft;
                break;
            case TextAlignmentOptions.Bottom:
                text.alignment = TextAlignmentOptions.Bottom;
                break;
            case TextAlignmentOptions.BottomRight:
                text.alignment = TextAlignmentOptions.BottomRight;
                break;
        }
    }

    public void SetScript(string newScript)
    {
        script = newScript;
        dialogueState = DialogueStates.Before;
        indexOfCurrentCharacter = 0;
        text.text = "";
        AlignTop();
        firstLineOfTextAsset = true;
    }

    public void SkipDialogue()
    {
        if (script == null) return;

        StopAllCoroutines();
        indexOfCurrentCharacter = script.Length;
        Continue();
        Continue();
    }

    static public void SkipAllDialogues()
    {
        Dialogue[] dialogues = (Dialogue[])GameObject.FindObjectsOfType(typeof(Dialogue));
        for (int i = 0; i < dialogues.Length; ++i)
        {
            dialogues[i].SkipDialogue();
        }
    }
}