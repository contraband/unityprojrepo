﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum WorldSpaceCanvasScalerTypes { Fade, Scale }

public class WorldSpaceCanvasScaler : MonoBehaviour
{
    public WorldSpaceCanvasScalerTypes type = WorldSpaceCanvasScalerTypes.Fade;
    public Vector2 maxSize;

    private RectTransform rectTransform;
    private Vector2 defaultSize;
    private MainCamera mainCamera;
    private List<Image> images;
    private List<TextMeshProUGUI> texts;
    private float d;
    private const float fadeCoefficient = 1f;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        defaultSize = rectTransform.sizeDelta;
        InitializeImagesAndTexts();
    }

	void Start ()
    {
        mainCamera = FindObjectOfType<MainCamera>();
        d = Mathf.Abs(MainCamera.MIN_Z) - Mathf.Abs(MainCamera.DEFAULT_Z);
    }
	

	void Update ()
    {
        if (LevelManager.currentGameState != GameStates.Infiltration) return;

        float n = Mathf.Abs(mainCamera.transform.position.z) - Mathf.Abs(MainCamera.DEFAULT_Z);
        float value = n / d;

        if (type == WorldSpaceCanvasScalerTypes.Fade) Fade(value);
        else Scale(value);
	}

    private void Fade(float value)
    {
        value *= fadeCoefficient;

        value = Mathf.Clamp(value, 0f, 1f);

        foreach (var image in images)
        {
            Color newColor = image.color;
            newColor.a = Mathf.Lerp(1f, 0f, value);
            image.color = newColor;
        }
        foreach (var text in texts)
        {
            Color newColor = text.color;
            newColor.a = Mathf.Lerp(1f, 0f, value);
            text.color = newColor;
        }
    }

    private void Scale(float value)
    {
        value = Mathf.Clamp(value, 0f, 1f);

        rectTransform.sizeDelta = Vector2.Lerp(defaultSize, maxSize, value);
    }

    private void InitializeImagesAndTexts()
    {
        images = new List<Image>();
        texts = new List<TextMeshProUGUI>();

        Transform[] children = gameObject.GetComponentsInChildren<Transform>();

        foreach (var child in children)
        {
            Image image = child.GetComponent<Image>();
            if (image) images.Add(image);

            TextMeshProUGUI text = child.GetComponent<TextMeshProUGUI>();
            if (text) texts.Add(text);
        }
    }
}
