﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : CurrentObjective.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class CurrentObjective : HUDElement
{
    private DataVault dataVault;
    private EmailTerminal emailTerminal;
    private Exit exit;
    public string customObjective;
    private string currentText;
    private string previousText;
    private float waitTime = 0.05f;
    private Image image;

    protected override void Awake()
    {
        base.Awake();

        text = transform.GetComponentInChildren<TextMeshProUGUI>();
        image = GetComponent<Image>();

        text.text = "";

        dataVault = FindObjectOfType<DataVault>();
        emailTerminal = FindObjectOfType<EmailTerminal>();
    }

    protected override void Start()
    {
        base.Start();

        exit = FindObjectOfType<Exit>();
    }

    protected override void Update()
    {
        base.Update();

        frostedGlassBackground.enabled = false;
        image.enabled = false;

        if (customObjective != "") currentText = customObjective;
        else if (emailTerminal && !player.emailsRead) currentText = "READ YOUR EMAILS";
        else if (dataVault && !player.dataVaultHacked) currentText = "HACK THE DATA VAULT";
        else if (exit) currentText = "PROCEED TO THE EXIT";

        if (currentText != previousText)
        {
            text.text = "";
            StopAllCoroutines();
            StartCoroutine(AppendCharacters());
        }

        if (text.text != "")
        {
            frostedGlassBackground.enabled = true;
            image.enabled = true;
        }

        previousText = currentText;
    }

    private IEnumerator AppendCharacters()
    {
        while (text.text != currentText)
        {
            text.text += currentText[text.text.Length];
            yield return new WaitForSeconds(waitTime);
        }
    }

    public override void Transition(bool transitionIn)
    {
        base.Transition(transitionIn);
    }
}
