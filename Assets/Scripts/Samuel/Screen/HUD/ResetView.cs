﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ResetView.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetView : HUDElement
{
    private Button button;

    //Camera
    private MainCamera mainCamera;

    //Animation
    private const float transitionTime = 0.5f;
    private bool animatingIn;
    private bool animatingOut;
    private Coroutine animationCoroutine;
    private Image image;
    public bool visible { get; set; }

    protected override void Awake()
    {
        base.Awake();

        button = GetComponent<Button>();
        image = GetComponent<Image>();

        Color c = image.color;
        c.a = 0f;
        image.color = c;
    }

    protected override void Start()
    {
        base.Start();
        button.onClick.AddListener(player.V);
        mainCamera = FindObjectOfType<MainCamera>();
    }

    protected override void Update()
    {
        base.Update();
    }

    public void Show()
    {
        if (animatingIn) return;

        if (animatingIn || animatingOut) StopCoroutine(animationCoroutine);

        animationCoroutine = StartCoroutine(InterpolateAlpha(1f));
        animatingIn = true;
    }

    public void Hide()
    {
        if (animatingOut) return;

        if (animatingIn || animatingOut)
        {
            StopCoroutine(animationCoroutine);
            animatingIn = false;
            animatingOut = false;
        }

        animationCoroutine = StartCoroutine(InterpolateAlpha(0f));
        animatingOut = true;
    }

    IEnumerator InterpolateAlpha(float endAlpha)
    {
        float elapsedTime = 0;
        float startAlpha = image.color.a;

        while (elapsedTime < transitionTime)
        {
            float t = elapsedTime / transitionTime;
            Color newColor = image.color;
            newColor.a = Mathf.Lerp(startAlpha, endAlpha, t);
            image.color = newColor;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (endAlpha == 0f) visible = false;
        else visible = true;

        animatingIn = false;
        animatingOut = false;
    }
}
