﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Controls.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class Controls : HUDElement
{
    private List<Image> images;
    private List<TextMeshProUGUI> texts;
    private bool hidden;
    private const float fadeTime = 1f;

    protected override void Awake()
    {
        base.Awake();

        InitializeImagesAndTexts();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    private void InitializeImagesAndTexts()
    {
        images = new List<Image>();
        texts = new List<TextMeshProUGUI>();

        Transform[] children = gameObject.GetComponentsInChildren<Transform>();

        foreach(var child in children)
        {
            Image image = child.GetComponent<Image>();
            if (image) images.Add(image);

            TextMeshProUGUI text = child.GetComponent<TextMeshProUGUI>();
            if (text) texts.Add(text);
        }
    }

    public void Toggle(bool visible)
    {
        if (hidden) return;

        hidden = true;
        float endAlpha = 0f;
        if (visible) endAlpha = 1f;

        StartCoroutine(Fade(endAlpha));
    }

    private IEnumerator Fade(float endAlpha)
    {
        float elapsedTime = 0;
        float startAlpha = images[0].color.a;

        if (endAlpha == 1f) transform.FindChild("FrostedGlass").GetComponent<Image>().enabled = true;
        else transform.FindChild("FrostedGlass").GetComponent<Image>().enabled = false;

        while (elapsedTime < fadeTime)
        {
            float t = elapsedTime / fadeTime;

            foreach (var image in images)
            {
                Color newColor = image.color;
                newColor.a = Mathf.Lerp(startAlpha, endAlpha, t);
                image.color = newColor;
            }
            foreach (var text in texts)
            {
                Color newColor = text.color;
                newColor.a = Mathf.Lerp(startAlpha, endAlpha, t);
                text.color = newColor;
            }

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        foreach (var image in images)
        {
            Color newColor = image.color;
            newColor.a = endAlpha;
            image.color = newColor;
        }
        foreach (var text in texts)
        {
            Color newColor = text.color;
            newColor.a = endAlpha;
            text.color = newColor;
        }
    }
}
