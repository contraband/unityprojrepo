﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HelpText.cs
// Author : Samuel Schimmel
// Purpose : This script handles HUD help text movementBehavior.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class HelpText : HUDElement
{
    //Inspector
    public Color coworkerColor;
    public Color lightSecurityColor;
    public Color heavySecurityColor;

    private DataVault dataVault;
    private EmailTerminal emailTerminal;
    private Image image;

    protected override void Awake()
    {
        base.Awake();

        text = transform.GetComponentInChildren<TextMeshProUGUI>();
        image = GetComponent<Image>();

        text.text = "";
    }

    protected override void Start()
    {
        base.Start();

        dataVault = FindObjectOfType<DataVault>();
        emailTerminal = FindObjectOfType<EmailTerminal>();
    }

    protected override void Update()
    {
        base.Update();

        text.text = "";
        
        frostedGlassBackground.enabled = false;
        image.enabled = false;

        if (LevelManager.currentGameState != GameStates.Infiltration)
            return;

        //Autoplay and debug modes simultaneously
        if (player.inAutoPlay && player.debug)
        {
            text.text = "Autoplay and debug modes have been activated.";
            return;
        }

        //Just auto play
        if (player.inAutoPlay)
        {
            text.text = "Autoplay has been activated.";
            return;
        }

        //Just debug mode
        if (player.debug)
        {
            text.text = "Debug mode has been activated.";
            return;
        }

        //Check for current selection
        if (!player.currentSelection)
            return;

        //Check if ready for player input
        if (!player.readyForPlayerInput)
            return;

        //Room
        RoomCollider room = player.currentSelection.GetComponent<RoomCollider>();
        if(room)
        {
            text.text = "Move outside rooms to reveal them.";
        }

        //Tile
        Tile tile = player.currentSelection.GetComponent<Tile>();
        if(tile)
        {
            //Exit
            if(tile.exit)
            {
                if (player.canExit)
                {
                    text.text = "Move here to exit.";
                }
                else
                {
                    if (dataVault && dataVault.usable) text.text = "Hack the data vault, then move here to exit.";
                    else if (emailTerminal) text.text = "Read your emails, then move here to exit.";
                }
            }
            
            //Occupied tile
            if (tile.currentOccupant)
            {
                //Enemy
                Enemy enemy = tile.currentOccupant.GetComponent<Enemy>();
                if (enemy)
                {
                    if (enemy.enemyClass == EnemyClasses.NPC || levelManager.levelType != LevelTypes.Stealth)
                    {
                        text.text = enemy.name + " has something to say to you.";
                    }
                    else if (enemy.enemyClass == EnemyClasses.Coworker)
                    {
                        string rgb = Helpers.ColorToHex(coworkerColor);
                        string tag = "<color=#" + rgb + ">";
                        text.text = "Getting spotted by " + tag + "Coworker " + enemy.name + "</color> will increase suspicion by " + GameManager.coworkerSuspicion + ".";
                    }
                    else if (enemy.enemyClass == EnemyClasses.LightSecurity)
                    {
                        string rgb = Helpers.ColorToHex(lightSecurityColor);
                        string tag = "<color=#" + rgb + ">";
                        text.text = "Getting spotted by " + tag + "Light Security " + enemy.name + "</color> will increase suspicion by " + GameManager.lightSecuritySuspicion + ".";
                    }
                    else if (enemy.enemyClass == EnemyClasses.HeavySecurity)
                    {
                        string rgb = Helpers.ColorToHex(heavySecurityColor);
                        string tag = "<color=#" + rgb + ">";
                        text.text = "Getting spotted by " + tag + "Heavy Security</color> will end the mission.";
                    }
                    else if (enemy.enemyClass == EnemyClasses.Camera)
                    {
                        text.text = "Getting spotted by a camera will increase suspicion by " + GameManager.cameraSuspicion + ".";
                    }
                }

                //Door
                Wall wall = tile.currentOccupant.GetComponent<Wall>();
                if(wall && wall.isDoor && !wall.isOpen)
                {
                    text.text = "Hack the data vault to unlock this door.";
                }

                //Terminal
                Terminal terminal = tile.currentOccupant.GetComponent<Terminal>();
                if (terminal)
                {
                    //Email terminal
                    if (terminal.GetComponent<EmailTerminal>())
                    {
                        text.text = "Log in to your personal terminal to read email.";
                    }
                    //Hackable terminal
                    else if(terminal.GetComponent<HackableTerminal>())
                    {
                        string type = null;
                        string result = null;

                        //Determine type and result
                        if(terminal.GetComponent<DataVault>())
                        {
                            type = "data vault";
                            result = "open doors and unlock the exit";
                        }
                        else if(terminal.GetComponent<SecurityTerminal>())
                        {
                            type = "security terminal";
                            result = "reduce suspicion by " + Mathf.Abs(GameManager.securityTerminalSuspicion);
                        }

                        //Set text
                        if (terminal.locked) text.text = "This " + type + " is locked.";
                        else text.text = "Hack this " + type + " to " + result + ".";
                    }
                }

                //Unique help text
                if(tile.currentOccupant.uniqueHelpText != "")
                {
                    text.text = tile.currentOccupant.uniqueHelpText;
                }
            }
        }

        if (text.text != "")
        {
            frostedGlassBackground.enabled = true;
            image.enabled = true;
        }
    }
}
