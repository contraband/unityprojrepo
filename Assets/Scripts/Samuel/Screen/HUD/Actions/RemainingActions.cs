﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : RemainingMovesScreen.cs
// Author : Samuel Schimmel
// Purpose : Sets text on HUD object for how many moves the player has left
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class RemainingActions : HUDElement
{
    private readonly Vector3 ELEMENT_OFFSET = new Vector3(90f, 0f, 0f);
    public List<RemainingActionsElement> elements { get; set; }
    private bool readyForNextElement;
    private bool animating;
    private Coroutine animationCoroutine;
    private int previousRemainingActions;
    private List<int> updateQueue;

    protected override void Awake()
    {
        text = transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>();
        updateQueue = new List<int>();
        elements = new List<RemainingActionsElement>();
        text.text = "";

        base.Awake();
    }

    protected override void Start()
    {
        base.Start();

        InitializeElements();
    }

    protected override void Update()
    {
        base.Update();

        text.text = "";

        if (LevelManager.currentGameState != GameStates.Infiltration)
            return;

        if (!player.isThisActorsTurn)
            return;

        if (value == 0)
        {
            text.text = "NO ACTIONS";
        }
        else
        {
            if (value == 1)
                text.text = value + " ACTION";
            else
                text.text = value + " ACTIONS";
        }
    }

    private void InitializeElements()
    {
        for (int i = 0; i < player.maxMoves; ++i)
        {
            RemainingActionsElement element = Instantiate(gameManager.remainingActionsElementPrefab, transform.position, Quaternion.identity, transform).GetComponent<RemainingActionsElement>();
            element.transform.localPosition += ELEMENT_OFFSET * i;
            elements.Add(element);
        } 
    }

    public override void Animate(int remainingActions, int maxRemainingActions)
    {
        if (!animating) StartAnimation(remainingActions);
        else updateQueue.Add(remainingActions);
    }

    private void StartAnimation(int remainingActions)
    {
        if(remainingActions == previousRemainingActions)
        {
            NextUpdate();
            return;
        }

        if(player.isThisActorsTurn) Toggle(true);
       
        animationCoroutine = StartCoroutine(AnimateElements(previousRemainingActions, remainingActions));

        previousRemainingActions = remainingActions;
    }

    private void NextUpdate()
    {
        if (updateQueue.Count == 0)
        {
            StartCoroutine(ScaleDown());
            return;
        }

        int nextValue = updateQueue[0];
        updateQueue.RemoveAt(0);
        StartAnimation(nextValue);
    }

    public void ClearQueue()
    {
        updateQueue.Clear();
    }

    private IEnumerator AnimateElements(int start, int end)
    {
        animating = true;

        //Scale up to attract attention
        float elapsedTime = 0f;
        while (elapsedTime < HUD.SCALE_INTERPOLATION_TIME)
        {
            float t = elapsedTime / HUD.SCALE_INTERPOLATION_TIME;
            float startScale = transform.localScale.x;
            float scale = Mathf.Lerp(startScale, HUDElement.ATTRACT_SCALE, t);
            transform.localScale = new Vector3(scale, scale, scale);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        //Decide which meter to animate first
        bool opaqueMeterFirst = false;
        int delta = 1;
        bool fill = true;
        int current = 0;
        value = start;
        if (end < start)
        {
            opaqueMeterFirst = true;
            delta *= -1;
            fill = false;
            --start;
            --end;
        }

        //Signal change with first meter
        current = start;
        while (current != end)
        {
            elements[current].Animate(opaqueMeterFirst, fill, 0f);
            current += delta;
        }

        //Update second meter
        readyForNextElement = true;
        current = start;
        while(current != end)
        {
            if (readyForNextElement)
            {
                elements[current].Animate(!opaqueMeterFirst, fill, RemainingActionsElement.ANIMATION_TIME);
                current += delta;
                value += delta;
                readyForNextElement = false;
            }

            yield return null;
        }

        //Wait
        while (!readyForNextElement) yield return null;

        NextUpdate();
    }

    private IEnumerator ScaleDown()
    {
        //Scale down
        float elapsedTime = 0f;
        while (elapsedTime < HUD.SCALE_INTERPOLATION_TIME)
        {
            float t = elapsedTime / HUD.SCALE_INTERPOLATION_TIME;

            float startScale = transform.localScale.x;
            float scale = Mathf.Lerp(startScale, 1f, t);
            transform.localScale = new Vector3(scale, scale, scale);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        animating = false;
    }

    public void ElementAnimationComplete()
    {
        readyForNextElement = true;
    }

    public void ShowActionCost(int remainingActions)
    {
        if (animating)
        {
            for (int i = 0; i < elements.Count; ++i)
            {
                elements[i].ShowCost(false);
            }
        }
        else
        {
            for (int i = 0; i < elements.Count; ++i)
            {
                if(i >= remainingActions && i < previousRemainingActions)
                    elements[i].ShowCost(true);
               else
                    elements[i].ShowCost(false);
            }
        }
    }

    public void Toggle(bool show)
    {
        for (int i = 0; i < elements.Count; ++i)
        {
            elements[i].Toggle(show);
        }
    }
}
