﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : RemainingActionsElement.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemainingActionsElement : MonoBehaviour
{
    private RectTransform opaqueMeterRectTransform;
    private Image opaqueMeterImage;
    private RectTransform transparentMeterRectTransform;
    private Image transparentMeterImage;
    private Vector2 FULL_SIZE_DELTA = new Vector2(60, 60);
    private Vector2 EMPTY_SIZE_DELTA = new Vector2 (30, 30);
    private RemainingActions remainingActions;
    private bool animating;
    private Coroutine animationCoroutine;
    public static float ANIMATION_TIME;

    void Awake()
    {
        opaqueMeterRectTransform = transform.FindChild("OpaqueMeter").GetComponent<RectTransform>();
        opaqueMeterImage = opaqueMeterRectTransform.GetComponent<Image>();
        transparentMeterRectTransform = transform.FindChild("TransparentMeter").GetComponent<RectTransform>();
        transparentMeterImage = transparentMeterRectTransform.GetComponent<Image>();
        remainingActions = transform.parent.GetComponent<RemainingActions>();
        opaqueMeterRectTransform.sizeDelta = EMPTY_SIZE_DELTA;
        transparentMeterRectTransform.sizeDelta = EMPTY_SIZE_DELTA;
        ANIMATION_TIME = HUD.WIDTH_INTERPOLATION_TIME / FindObjectOfType<Player>().maxMoves;
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {

    }

    public void Animate(bool animateOpaqueMeter, bool fill, float time)
    {
        Vector2 sizeDelta = Vector2.zero;
        if (fill) sizeDelta = FULL_SIZE_DELTA;
        else sizeDelta = EMPTY_SIZE_DELTA;

        animationCoroutine = StartCoroutine(InterpolateSizeDelta(animateOpaqueMeter, sizeDelta, time));
    }

    private IEnumerator InterpolateSizeDelta(bool animateOpaqueMeter, Vector2 endSizeDelta, float time)
    {
        animating = true;

        RectTransform rt = null;
        if (animateOpaqueMeter) rt = opaqueMeterRectTransform;
        else rt = transparentMeterRectTransform;
        Vector2 startSizeDelta = rt.sizeDelta;
        float elapsedTime = 0;

        while (elapsedTime < time)
        {
            float x = Mathf.SmoothStep(startSizeDelta.x, endSizeDelta.x, (elapsedTime / time));
            float y = Mathf.SmoothStep(startSizeDelta.y, endSizeDelta.y, (elapsedTime / time));
            Vector2 newSizeDelta = rt.sizeDelta;
            newSizeDelta.x = x;
            newSizeDelta.y = y;
            rt.sizeDelta = newSizeDelta;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        remainingActions.ElementAnimationComplete();
        rt.sizeDelta = endSizeDelta;

        animating = false;
    }

    public void ShowCost(bool showCost)
    {
        opaqueMeterImage.enabled = !showCost;
    }

    public void Toggle(bool show)
    {
        opaqueMeterImage.enabled = show;
        transparentMeterImage.enabled = show;
    }
}
