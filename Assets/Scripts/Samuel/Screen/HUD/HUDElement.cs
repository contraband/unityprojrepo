﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : controls movementBehavior of individual hud elements including movement
//			 and relationships of hud info to the player.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class HUDElement : MonoBehaviour
{
    public HUD hud;
    public Vector2 offPositionOffset;
    [HideInInspector] public bool stayOffscreen;

    public delegate void EventHandler();
    public event EventHandler onTransitionEnd;

    protected Player player; 
    protected float maxWidth;
    public const float ATTRACT_SCALE = 1.1f;
    protected TextMeshProUGUI text;
    private Actors actors;
    public LevelManager levelManager { get; set; }
    public bool translating { get; set; }

    private Vector3 onPosition;
    private Vector3 offPosition;

    //Meter
    public RectTransform transparentMeterRectTransform { get; protected set; }
    public RectTransform opaqueMeterRectTransform { get; protected set; }
    public Image transparentMeterImage { get; protected set; }
    public Image opaqueMeterImage { get; protected set; }
    protected Coroutine meterAnimation { get; set; }
    protected bool meterAnimating;

    //Text
    protected float value;

    public Image frostedGlassBackground { get; set; }
    
    public GameManager gameManager { get; set; }

    protected virtual void Awake()
    {
        hud = FindObjectOfType<HUD>();
        player = FindObjectOfType<Player>();
        actors = FindObjectOfType<Actors>();
        levelManager = FindObjectOfType<LevelManager>();
        if (transform.FindChild("FrostedGlassBackground")) frostedGlassBackground = transform.FindChild("FrostedGlassBackground").GetComponent<Image>();

        onPosition = transform.localPosition;
        offPosition = onPosition;
        offPosition.x += offPositionOffset.x;
        offPosition.y += offPositionOffset.y;
        transform.localPosition = offPosition;

        if (transform.FindChild("TransparentMeter"))
        {
            transparentMeterRectTransform = transform.FindChild("TransparentMeter").GetComponent<RectTransform>();
            transparentMeterImage = transparentMeterRectTransform.GetComponent<Image>();
        }
        if (transform.FindChild("OpaqueMeter"))
        {
            opaqueMeterRectTransform = transform.FindChild("OpaqueMeter").GetComponent<RectTransform>();
            maxWidth = opaqueMeterRectTransform.sizeDelta.x;
            opaqueMeterImage = opaqueMeterRectTransform.GetComponent<Image>();
        }
    }

	protected virtual void Start ()
    {
        gameManager = FindObjectOfType<GameManager>();
        hud.AddHUDElement(this);
    }

    protected virtual void Update ()
    { 
	}

    public virtual void Transition(bool transitionIn)
    {
        if (transitionIn)
        {
            StartCoroutine(TranslateCoroutine(onPosition));
        }
        else
        {
            StartCoroutine(TranslateCoroutine(offPosition));
        }
    }

    public virtual void Animate(int current, int total) { }

    protected void SetWidth(float endWidth)
    {
        opaqueMeterRectTransform.sizeDelta = new Vector2(endWidth, opaqueMeterRectTransform.sizeDelta.y);
        transparentMeterRectTransform.sizeDelta = new Vector2(endWidth, transparentMeterRectTransform.sizeDelta.y);
    }

    protected void AnimateWidth(float endWidth, float startValue = 0, float endValue = 0)
    {
        if (meterAnimating)
        {
            StopCoroutine(meterAnimation);
            meterAnimating = false;
        }

        meterAnimation = StartCoroutine(InterpolateWidth(endWidth, startValue, endValue));
    }

    private IEnumerator InterpolateWidth(float endWidth, float startValue, float endValue)
    {
        meterAnimating = true;

        float startWidth = opaqueMeterRectTransform.sizeDelta.x;

        //Scale up to attract attention
        float elapsedTime = 0f;
        while (elapsedTime < HUD.SCALE_INTERPOLATION_TIME)
        {
            float t = elapsedTime / HUD.SCALE_INTERPOLATION_TIME;
            float startScale = transform.localScale.x;
            float scale = Mathf.Lerp(startScale, ATTRACT_SCALE, t);
            transform.localScale = new Vector3(scale, scale, scale);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        //Decide which meter to animate first
        RectTransform firstMeter;
        RectTransform secondMeter;
        if(endWidth > startWidth)
        {
            firstMeter = transparentMeterRectTransform;
            secondMeter = opaqueMeterRectTransform;
        }
        else
        {
            firstMeter = opaqueMeterRectTransform;
            secondMeter = transparentMeterRectTransform;
        }

        //Signal change with first meter
        firstMeter.sizeDelta = new Vector2(endWidth, firstMeter.sizeDelta.y);

        //Wait
        yield return new WaitForSeconds(HUD.INTERPOLATION_DELAY_TIME);

        //Update second meter
        elapsedTime = 0f;
        while (elapsedTime < HUD.WIDTH_INTERPOLATION_TIME)
        {
            float t = elapsedTime / HUD.WIDTH_INTERPOLATION_TIME;

            value = Mathf.RoundToInt(Mathf.SmoothStep(startValue, endValue, t));

            float w = Mathf.SmoothStep(startWidth, endWidth, (t));
            secondMeter.sizeDelta = new Vector2(w, secondMeter.sizeDelta.y);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        value = Mathf.RoundToInt(endValue);
        secondMeter.sizeDelta = new Vector2(endWidth, secondMeter.sizeDelta.y);

        //Scale down
        elapsedTime = 0f;
        while (elapsedTime < HUD.SCALE_INTERPOLATION_TIME)
        {
            float t = elapsedTime / HUD.SCALE_INTERPOLATION_TIME;
            float startScale = transform.localScale.x;
            float scale = Mathf.Lerp(startScale, 1f, t);
            transform.localScale = new Vector3(scale, scale, scale);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        meterAnimating = false;
    }

    protected IEnumerator TranslateCoroutine(Vector3 endPosition)
    {
        translating = true;

        stayOffscreen = false;

        Vector3 startPosition = transform.localPosition;
        float elapsedTime = 0;

        while (elapsedTime < HUD.TRANSLATE_TIME)
        {
            float t = elapsedTime / HUD.TRANSLATE_TIME;
            transform.localPosition = Vector3.Lerp(startPosition, endPosition, Mathf.SmoothStep(0f, 1f, t));

            elapsedTime += Time.deltaTime;
            yield return null;
        }
        
        transform.localPosition = endPosition;
        if (onTransitionEnd != null) onTransitionEnd();

        translating = false;
    }
}
