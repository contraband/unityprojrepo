﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : EndTurn.cs
// Author : Samuel Schimmel
// Purpose : This script handles the logic behind ending a player's turn. 
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class EndTurn : HUDElement
{
    private Button button;
    private EndTurnControlText endTurnControlText;
    private Color defaultColor;
    public Color alertColor;

    protected override void Awake()
    {
        base.Awake();

        text = transform.GetComponentInChildren<TextMeshProUGUI>();
        endTurnControlText = text.transform.parent.GetComponentInChildren<EndTurnControlText>();

        button = GetComponent<Button>();
        defaultColor = text.color;
    }

    protected override void Start()
    {
        base.Start();
        button.onClick.AddListener(player.Space);
    }

    protected override void Update()
    {
        base.Update();

        Color newColor = defaultColor;

        text.text = "";
        endTurnControlText.text.text = "";

        if (LevelManager.currentGameState != GameStates.Infiltration)
        {
            button.interactable = false;
            return;
        }

        if (player.readyForPlayerInput)
        {
            button.interactable = true;

            if (player.remainingMoves == player.maxMoves) text.text = "SKIP TURN";
            else text.text = "END TURN";

            if (player.remainingMoves == 0 || player.noInputTimer >= Player.noInputTime) newColor = alertColor;

            if (levelManager.levelType == LevelTypes.Stealth) text.text += ": +" + Mathf.Abs(GameManager.startTurnSuspicion) + " SUSPICION";

            endTurnControlText.text.text = "SPACE";

            float speed = 5f;
            float t = (Mathf.Sin(Time.time * speed) + 1f) / 2f;
            text.color = Color.Lerp(defaultColor, newColor, t);
        }
        else if (player.playerCanAccelerate)
        {
            button.interactable = true;
            text.text = "SPEED UP";

            endTurnControlText.text.text = "SPACE";
        }
        else
        {
            button.interactable = false;
        }
    }
}
