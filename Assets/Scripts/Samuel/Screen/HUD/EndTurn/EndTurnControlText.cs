﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : EndTurnControlText.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndTurnControlText : MonoBehaviour
{
    public TextMeshProUGUI text { get; set; }

    void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
