﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : RemainingHP.cs
// Author : Samuel Schimmel
// Purpose : Sets HUD text for how much health the player left.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class RemainingHP : HUDElement
{
    public Color emptyMeterColor;
    public Color fullMeterColor;
    public Color defaultTextColor;
    public Color alertTextColor;

    private const int colorAnimationSuspicionThreshold = 60;

    protected override void Awake()
    {
        base.Awake();

        SetWidth(0f);

        text = transform.GetComponentInChildren<TextMeshProUGUI>();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();

        text.text = "";

        if (LevelManager.currentGameState != GameStates.Infiltration) return;

        //Text
        if (player.remainingHP > 0) text.text = value + " / " + Player.MAX_HP + " SUSPICION";
        else text.text = "ALARM ACTIVE";

        //Meter color
        float percentage = value / Player.MAX_HP;
        Color c = Color.Lerp(emptyMeterColor, fullMeterColor, percentage);
        opaqueMeterImage.color = c;
        c.a = 0.5f;
        transparentMeterImage.color = c;

        //Text color
        Color newColor = defaultTextColor;
        if (player.suspicionLevel >= colorAnimationSuspicionThreshold && player.suspicionLevel < 100) newColor = alertTextColor;
        float speed = player.suspicionLevel / 20f;
        float t = (Mathf.Sin(Time.time * 5f) + 1f) / 2f;
        text.color = Color.Lerp(defaultTextColor, newColor, t);
    }

    public override void Animate(int remainingHP, int MAX_HP)
    {
        if (MAX_HP - remainingHP == value) return;

        float endWidth = (1 - ((float)remainingHP / MAX_HP)) * maxWidth;

        AnimateWidth(endWidth, value, MAX_HP - remainingHP);
    }
}
