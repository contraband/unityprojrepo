﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : controls all hud movementBehavior, adds and removes hud elements
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public List<HUDElement> hudElements;

    public delegate void EventHandler();
    public event EventHandler onTransitionEnd;

    private int hudElementTransitionsComplete;
    private int hudElementsToTransition;
    private Player player;
    private HUDCamera hudCamera;
    private Canvas canvas;
    public LevelManager levelManager;
    private GraphicRaycaster graphicRaycaster;

    //Const
    public const float TRANSLATE_TIME = 1f;
    public const float WIDTH_INTERPOLATION_TIME = 2f;
    public const float SCALE_INTERPOLATION_TIME = 0.25f;
    public const float INTERPOLATION_DELAY_TIME = 0.25f;

    void Awake()
    {
        hudElements = new List<HUDElement>();
        player = FindObjectOfType<Player>();
        canvas = GetComponent<Canvas>();
        levelManager = FindObjectOfType<LevelManager>();
        graphicRaycaster = GetComponent<GraphicRaycaster>();
    }

    void Start()
    {
        hudCamera = FindObjectOfType<HUDCamera>();
        SetCanvasProperties();
    }

    void OnGUI()
    {
        
    }

    void OnDisable()
    {
        for (int i = 0; i < hudElements.Count; ++i)
        {
            hudElements[i].onTransitionEnd -= HUDElementTransitionEnded;
        }
    }

    private void SetCanvasProperties()
    {
        canvas.worldCamera = hudCamera.GetComponent<Camera>();
        canvas.planeDistance = 1;
        canvas.sortingLayerName = "HUD";
    }

    public void AddHUDElement(HUDElement hudElement)
    {
        hudElements.Add(hudElement);

        hudElement.onTransitionEnd += HUDElementTransitionEnded;
    }

    public void Transition(bool transitionIn, List<HUDElement> elementsToIgnore = null)
    {
        graphicRaycaster.enabled = transitionIn;
        
        //Don't bring in HUD after confrontation in safe levels
        if (transitionIn && levelManager.levelType == LevelTypes.Safe)
        {
            if (!player.inCutscene) FindObjectOfType<CurrentObjective>().Transition(true);
            if (onTransitionEnd != null) Invoke("SkipTransition", TRANSLATE_TIME);
            return;
        }

        hudElementTransitionsComplete = 0;
        hudElementsToTransition = 0;

        //Transition HUD elements
        for (int i = 0; i < hudElements.Count; ++i)
        {
            //Check if element should stay off screen
            if (hudElements[i].stayOffscreen) continue;

            //Check if element should be ignored
            if (elementsToIgnore != null && elementsToIgnore.Contains(hudElements[i])) continue;

            hudElements[i].Transition(transitionIn);
            ++hudElementsToTransition;
        }
    }

    void HUDElementTransitionEnded()
    {
        ++hudElementTransitionsComplete;
       
        if(hudElementTransitionsComplete == hudElementsToTransition)
        {
            if (onTransitionEnd != null)
                onTransitionEnd();
        }
    }

    void SkipTransition()
    {
        if (onTransitionEnd != null)
            onTransitionEnd();
    }
}
