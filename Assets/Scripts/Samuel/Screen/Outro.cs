﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Outro.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Outro : MonoBehaviour
{
    public AnimationCurve curve;
    public bool scrolling { get; set; }

    private RectTransform rectTransform;
    private float speed = 0.02f;
    private Vector3 startPosition = new Vector3(0f, -240f, 80f);
    private Vector3 endPosition = new Vector3(0f, 240f, 80f);
    private float time;

    private void Awake()
    {
        rectTransform = transform.GetChild(0).GetComponent<RectTransform>();
        rectTransform.position = startPosition;
    }

    void Start ()
    {
		
	}

	void Update ()
    {
        if (!scrolling) return;

        if (Input.GetKeyDown(KeyCode.Space)
            || Input.GetKeyDown(KeyCode.Return)
            || Input.GetKeyDown(KeyCode.Escape)
            || Input.GetMouseButtonDown(0)
            || Input.GetMouseButtonDown(1))
        {
            Destroy(FindObjectOfType<GameManager>().gameObject);
            AudioObject.DestroyAll();
            SceneManager.LoadScene(0);
        }

        rectTransform.position = Vector3.Lerp(startPosition, endPosition, time * speed);

        time += Time.deltaTime;
    }
}
