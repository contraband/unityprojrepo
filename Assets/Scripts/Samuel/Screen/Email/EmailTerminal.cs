﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : EmailTerminal.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmailTerminal : Terminal
{
    //Set in inspector
    public List<TextAsset> emailTextAssets;
    public TextAsset secretEmailTextAsset;

    //Const

    //Properties

    protected override void Awake()
    {
        base.Awake();

        terminalType = TerminalTypes.Email;
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void TerminalUpdate()
    {
        base.TerminalUpdate();
    }

    public override void AttemptUse()
    {
        if (emailTextAssets.Count < 1) return;

        if (!CanUse(false)) return;

        if (gameManager.nextSecretEmailUnlocked && secretEmailTextAsset) emailTextAssets.Add(secretEmailTextAsset);

        gameManager.nextSecretEmailUnlocked = false;

        player.StartEmail(this);
    }

    private void Update()
    {
        TerminalUpdate();
    }

    protected override void AnimateWaypoint()
    {
        if (!used) base.AnimateWaypoint();
    }
}
