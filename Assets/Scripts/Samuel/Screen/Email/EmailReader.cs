﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : EmailReader.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EmailReader : MonoBehaviour
{
    //Set in inspector
    public Color readerImageColor;
    public Color readerTextColor;

    //Const

    //Properties
    public EmailClient emailClient { get; set; }
    public EmailInbox emailInbox { get; set; }
    public Image readerImage { get; set; }
    public Image portraitImage { get; set; }
    public TextMeshProUGUI senderText { get; set; }
    public TextMeshProUGUI recipientText { get; set; }
    public TextMeshProUGUI subjectText { get; set; }
    public TextMeshProUGUI bodyText { get; set; }

    private void Awake ()
	{
        emailClient = transform.GetComponentInParent<EmailClient>();
        emailInbox = emailClient.GetComponentInChildren<EmailInbox>();
        readerImage = transform.parent.GetComponent<Image>();
        portraitImage = transform.GetComponentInChildren<EmailPortrait>().GetComponent<Image>();
        senderText = transform.GetComponentInChildren<Sender>().GetComponent<TextMeshProUGUI>();
        recipientText = transform.GetComponentInChildren<Recipient>().GetComponent<TextMeshProUGUI>();
        subjectText = transform.GetComponentInChildren<Subject>().GetComponent<TextMeshProUGUI>();
        bodyText = transform.GetComponentInChildren<Mask>().GetComponentInChildren<Body>().GetComponent<TextMeshProUGUI>();
    }
	
	private void Start ()
	{
        InitializeReader();
    }
	
	private void OnGUI()
	{
    }

    private void InitializeReader()
    {
        portraitImage.enabled = false;
        senderText.text = "";
        recipientText.text = "";
        subjectText.text = "";
        bodyText.text = "";

        //Color
        readerImage.color = readerImageColor;
        recipientText.color = readerTextColor;
        subjectText.color = readerTextColor;
        bodyText.color = readerTextColor;
    }

    public void DisplayEmail(Email email)
    {
        portraitImage.enabled = true;
        portraitImage.sprite = email.portrait;
        senderText.text = "FROM: " + email.sender;
        recipientText.text = "TO: Evelyn Turner";
        subjectText.text = "SUBJECT: " + email.subject;
        bodyText.text = email.body;
    }
}
