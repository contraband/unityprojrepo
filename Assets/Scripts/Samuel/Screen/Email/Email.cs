﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Email.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Email : MonoBehaviour
{
    //Set in inspector
    public Color defaultEmailImageColor = new Color(1, 0, 1, 1);
    public Color selectedEmailImageColor;
    public Color defaultEmailTextColor;
    public Color selectedEmailTextColor;

    public static Vector2 defaultSizeDelta;
    public static Vector2 highlightedSizeDelta;
    public static float highlightSizeDeltaExtraWidth = 20f;

    //Properties
    new public bool enabled { get; set; }
    public bool read { get; set; }
    public EmailClient emailClient { get; set; }
    public EmailInbox emailInbox { get; set; }
    public EmailReader emailReader { get; set; }
    public string sender { get; set; }
    public string subject { get; set; }
    public string body { get; set; }
    public RectTransform emailRectTransform { get; set; }
    public Image emailImage { get; set; }
    public Sprite portrait { get; set; }
    public TextMeshProUGUI senderText { get; set; }
    public TextMeshProUGUI subjectText { get; set; }
    public TextMeshProUGUI unreadIndicator { get; set; }

    private void Awake ()
	{
        emailInbox = GetComponentInParent<EmailInbox>();
        emailClient = emailInbox.GetComponentInParent<EmailClient>();
        emailReader = emailClient.GetComponentInChildren<EmailReader>();

        emailRectTransform = transform.GetComponentInChildren<EmailImage>().GetComponent<RectTransform>();
        emailImage = transform.GetComponentInChildren<EmailImage>().GetComponent<Image>();
        senderText = transform.GetComponentInChildren<Sender>().GetComponent<TextMeshProUGUI>();
        subjectText = transform.GetComponentInChildren<Subject>().GetComponent<TextMeshProUGUI>();
        unreadIndicator = emailImage.GetComponentInChildren<UnreadIndicator>().GetComponent<TextMeshProUGUI>();

        defaultSizeDelta = emailRectTransform.sizeDelta;
        highlightedSizeDelta = defaultSizeDelta;
        highlightedSizeDelta.x += highlightSizeDeltaExtraWidth;
    }
	
	private void Start ()
	{

	}
	
	private void OnGUI ()
	{

    }

    public void InitializeEmail(string str)
    {
        string[] lines = str.Split('\n');

        sender = lines[0];
        subject = lines[1];
        body = lines[2];
        portrait = Resources.Load<Sprite>("Portraits/" + lines[3]);

        senderText.text = sender;
        subjectText.text = subject;

        //Feedback
        emailImage.color = defaultEmailImageColor;
        senderText.color = defaultEmailTextColor;
        subjectText.color = defaultEmailTextColor;
        unreadIndicator.color = defaultEmailTextColor;
        emailRectTransform.sizeDelta = defaultSizeDelta;
    }

    public void Enter()
    {
        if (!enabled) return;
        if (emailClient.selectedEmail == this) return;

        //Audio
        emailClient.audioObject.PlayOnce(emailClient.enterClip);

        //Feedback
        emailRectTransform.sizeDelta = highlightedSizeDelta;
    }

    public void Exit()
    {
        if (!enabled) return;

        //Feedback
        emailRectTransform.sizeDelta = defaultSizeDelta;
    }

    public void Click()
    {
        if (!enabled) return;
        if (emailClient.selectedEmail == this) return;

        emailInbox.DeselectEmails();
        emailClient.selectedEmail = this;
        emailReader.DisplayEmail(this);
        read = true;
        unreadIndicator.enabled = false;

        //Audio
        emailClient.audioObject.PlayOnce(emailClient.selectionClip, true);

        //Feedback
        emailImage.color = selectedEmailImageColor;
        senderText.color = selectedEmailTextColor;
        subjectText.color = selectedEmailTextColor;
        unreadIndicator.color = selectedEmailTextColor;
    }

    public void Deselect()
    {
        //Feedback
        emailImage.color = defaultEmailImageColor;
        senderText.color = defaultEmailTextColor;
        subjectText.color = defaultEmailTextColor;
        unreadIndicator.color = defaultEmailTextColor;
    }
}
