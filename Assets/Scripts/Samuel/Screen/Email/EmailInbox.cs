﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : EmailInbox.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmailInbox : MonoBehaviour
{
    //Set in inspector

    //Const
    public static readonly Vector3 EMAIL_ORIGIN = new Vector3(0f, 0, 0f);
    public static readonly Vector3 EMAIL_OFFSET = new Vector3(0f, -200f, 0f);

    //Properties
    public GameManager gameManager { get; set; }
    public EmailClient emailClient { get; set; }
    public EmailReader emailReader { get; set; }
    public List<Email> emails { get; set; }

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        emailClient = transform.GetComponentInParent<EmailClient>();
    }
	
	private void Start ()
	{
        emailReader = emailClient.emailReader;
    }
	
	private void OnGUI()
	{

    }

    public void InitializeEmails(List<TextAsset> emailTextAssets)
    {
        emails = new List<Email>();

        for(int i = 0; i < emailTextAssets.Count; ++i)
        {
            Vector3 position = EMAIL_ORIGIN + (EMAIL_OFFSET * emails.Count);

            Email email = (Instantiate(gameManager.emailPrefab, Vector3.zero, Quaternion.identity, transform)).GetComponent<Email>();

            email.GetComponent<RectTransform>().anchoredPosition3D = position;

            email.InitializeEmail(emailTextAssets[i].ToString());

            emails.Add(email);
        }
    }

    public void DeselectEmails()
    {
        for (int i = 0; i < emails.Count; ++i)
        {
            emails[i].Deselect();
        }
    }

    public void ToggleEnabled(bool enabled)
    {
        for (int i = 0; i < emails.Count; ++i)
        {
            emails[i].enabled = enabled;
        }
    }
}
