﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : EmailClient.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioObject))]
public class EmailClient : MonoBehaviour
{
    //Set in inspector
    public AudioClip startClip;
    public AudioClip enterClip;
    public AudioClip selectionClip;
    public AudioClip returnClip;

    public static Vector2 defaultSizeDelta;
    public static float transitionSizeDeltaHeight = 2500f;
    public static Vector2 transitionSizeDelta;
    public const float TRANSITION_TIME = 1f;

    //Properties
    public LevelManager levelManager { get; set; }
    public RectTransform rectTransform { get; set; }
    public EmailInbox emailInbox { get; set; }
    public EmailReader emailReader { get; set; }
    public Email selectedEmail { get; set; }
    public Image fadeImage { get; set; }
    public EmailReturn emailReturn { get; set; }
    public AudioObject audioObject { get; set; }

    private void Awake ()
	{
        levelManager = FindObjectOfType<LevelManager>();
        rectTransform = GetComponent<RectTransform>();
        emailInbox = GetComponentInChildren<EmailInbox>();
        emailReader = GetComponentInChildren<EmailReader>();
        fadeImage = transform.parent.FindChild("FadeImage").GetComponent<Image>();
        emailReturn = transform.GetComponentInChildren<EmailReturn>();
        audioObject = GetComponent<AudioObject>();

        defaultSizeDelta = rectTransform.sizeDelta;
        transitionSizeDelta = defaultSizeDelta;
        transitionSizeDelta.y = transitionSizeDeltaHeight;
        rectTransform.sizeDelta = transitionSizeDelta;
	}
	
	private void Start ()
	{

	}
	
	private void OnGUI()
	{

	}

    public void TransitionIn()
    {
        StartCoroutine(Transition(defaultSizeDelta, 0f));
        
        //Audio
        audioObject.PlayOnce(startClip);
    }

    public void InitializeEmails(List<TextAsset> emailTextAssets)
    {
        emailInbox.InitializeEmails(emailTextAssets);
    }

    public void TransitionOut()
    {
        ToggleEnabled(false);
        StartCoroutine(Transition(transitionSizeDelta, 1f));

        //Audio
        audioObject.PlayOnce(returnClip);
    }

    private IEnumerator Transition(Vector2 endSizeDelta, float endAlpha)
    {
        Vector2 startSizeDelta = rectTransform.sizeDelta;
        Color startColor = fadeImage.color;
        Color endColor = startColor;
        endColor.a = endAlpha;
        float elapsedTime = 0f;
        fadeImage.enabled = true;

        while (elapsedTime < TRANSITION_TIME)
        {
            float t = elapsedTime / TRANSITION_TIME;
            rectTransform.sizeDelta = Vector2.Lerp(startSizeDelta, endSizeDelta, Mathf.SmoothStep(0f, 1f, t));
            fadeImage.color = Color.Lerp(startColor, endColor, Mathf.SmoothStep(0f, 1f, t));

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (endAlpha == 1f)
            levelManager.ExitEmailState();
        else
            ToggleEnabled(true);
    }

    private void ToggleEnabled(bool enabled)
    {
        emailInbox.ToggleEnabled(enabled);

        emailReturn.enabled = enabled;
    }
}
