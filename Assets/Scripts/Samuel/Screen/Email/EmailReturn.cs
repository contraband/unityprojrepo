﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : EmailReturn.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EmailReturn : MonoBehaviour
{
    //Set in inspector
    public Color defaultColor;
    public Color highlightedColor;
    
    //Const

    //Properties
    new public bool enabled { get; set; }
    public EmailClient emailClient { get; set; }
    public TextMeshProUGUI text { get; set; }

    private void Awake ()
	{
        emailClient = transform.parent.GetComponentInParent<EmailClient>();
        text = GetComponentInChildren<TextMeshProUGUI>();

        text.color = defaultColor;
	}
	
	private void Start ()
	{

	}
	
	private void OnGUI ()
	{

	}

    public void Enter()
    {
        if (!enabled) return;
        text.color = highlightedColor;
        emailClient.audioObject.PlayOnce(emailClient.enterClip);
    }

    public void Exit()
    {
        if (!enabled) return;

        text.color = defaultColor;
    }

    public void Click()
    {
        if (!enabled) return;

        emailClient.TransitionOut();

        enabled = false;
    }
}
