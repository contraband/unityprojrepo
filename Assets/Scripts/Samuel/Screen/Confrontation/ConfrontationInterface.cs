﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationInterface.cs
// Author : Samuel Schimmel
// Purpose : This script deals with the handling of UI/UX asset movementBehavior during 
//			 confrontations. 
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ConfrontationInterface : MonoBehaviour
{
    public List<ConfrontationInterfaceElement> confrontationInterfaceElements;

    private AudioSource audioSource;
    private int confrontationInterfaceElementsReady;
    private ConfrontationDialogue confrontationDialogue;
    private HUD hud;
    private Player player;
    private MainCamera mainCamera;
    private HUDCamera hudCamera;
    private Canvas canvas;
    private ConfrontationFadeImage confrontationFadeImage;
    private GraphicRaycaster graphicRaycaster;
    private int elementsToActivate;

    public readonly Color transparentColor = new Color(0f, 0f, 0.3f, 0f);
    public readonly Color opaqueColor = new Color(0f, 0f, 0.3f, 0.3f);
    public const float transitionTime = 1f;

    void Awake()
    {
        confrontationInterfaceElements = new List<ConfrontationInterfaceElement>();
        confrontationDialogue = transform.FindChild("Dialogue").GetComponent<ConfrontationDialogue>();
        player = GameObject.Find("Player").GetComponent<Player>();
        audioSource = GetComponent<AudioSource>();
        canvas = GetComponentInParent<Canvas>();
        graphicRaycaster = GetComponentInParent<GraphicRaycaster>();
    }

    void Start()
    {
        hud = GameObject.Find("HUDCanvas").GetComponent<HUD>();
        mainCamera = FindObjectOfType<MainCamera>();
        hudCamera = FindObjectOfType<HUDCamera>();
        confrontationFadeImage = FindObjectOfType<ConfrontationFadeImage>();

        SetCanvasProperties();
    }

    void OnDisable()
    {
        for (int i = 0; i < confrontationInterfaceElements.Count; ++i)
        {
            confrontationInterfaceElements[i].onActivation -= ConfrontationElementActivated;
            confrontationInterfaceElements[i].onDeactivation -= ConfrontationElementDeactivated;
        }
    }

    private void SetCanvasProperties()
    {
        canvas.worldCamera = hudCamera.GetComponent<Camera>();
        canvas.planeDistance = 1;
        canvas.sortingLayerName = "ConfrontationInterface";
        graphicRaycaster.enabled = false;
    }

    public void AddConfrontationInterfaceElement(ConfrontationInterfaceElement confrontationInterfaceElement)
    {
        confrontationInterfaceElements.Add(confrontationInterfaceElement);

        confrontationInterfaceElement.onActivation += ConfrontationElementActivated;
        confrontationInterfaceElement.onDeactivation += ConfrontationElementDeactivated;
    }

    void Update()
    {
    }

    public void Activate(TextAsset newConfrontationScript)
    {
        //Transition HUD
        List<HUDElement> elementsToIgnore = new List<HUDElement>();
        elementsToIgnore.Add(FindObjectOfType<RemainingHP>());
        hud.Transition(false, elementsToIgnore);

        confrontationFadeImage.Transition(opaqueColor, transitionTime);
        graphicRaycaster.enabled = true;

        confrontationDialogue.SetScript(newConfrontationScript.ToString());
        confrontationInterfaceElementsReady = 0;
        elementsToActivate = 0;

        for (int i = 0; i < confrontationInterfaceElements.Count; ++i)
        {
            //Skip responses
            if (confrontationInterfaceElements[i].GetComponent<ConfrontationResponses>()) continue;

            //Skip picture
            if (confrontationInterfaceElements[i].GetComponent<ConfrontationPicture>()) continue;

            confrontationInterfaceElements[i].Activate();

            ++elementsToActivate;
        }
    }

    public void Deactivate()
    {
        confrontationDialogue.SetScript(null);
        confrontationInterfaceElementsReady = 0;
        confrontationFadeImage.Transition(transparentColor, transitionTime);
        graphicRaycaster.enabled = false;

        //Cutscene over
        if(!player.nextTileAfterDialogue)
        {
            //Hide letterbox
            FindObjectOfType<LetterboxCanvas>().ToggleVisibility(false);
        }

        for (int i = 0; i < confrontationInterfaceElements.Count; ++i)
        {
            confrontationInterfaceElements[i].Deactivate();
        }
    }

    void ConfrontationElementActivated()
    {
        ++confrontationInterfaceElementsReady;

        if(confrontationInterfaceElementsReady == elementsToActivate)
        {
            confrontationDialogue.dialogue.dialogueState = Dialogue.DialogueStates.During;
            confrontationDialogue.Continue();
        }
    }

    void ConfrontationElementDeactivated()
    {
        ++confrontationInterfaceElementsReady;

        if (confrontationInterfaceElementsReady == elementsToActivate)
        {
            AllConfrontationElementsDeactivated();
        }
    }

    void AllConfrontationElementsDeactivated()
    {
        hud.onTransitionEnd += EndConfrontation;
        hud.Transition(true);
    }

    public void EndConfrontation()
    {
        hud.onTransitionEnd -= EndConfrontation;
        player.EndConfrontation();
    }
}
