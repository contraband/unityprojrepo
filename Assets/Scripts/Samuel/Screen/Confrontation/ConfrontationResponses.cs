﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationResponse.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;

public class ConfrontationResponses : ConfrontationInterfaceElement
{
    public List<ConfrontationResponse> confrontationResponses { get; set; }
    public Dialogue dialogue { get; set; }
    public bool awaitingPlayerResponse { get; set; }
    public int numberOfResponses { get; set; }
    private bool resizeCalled { get; set; }
    private Vector2 defaultSizeDelta { get; set; }

    protected override void Awake()
    {
        base.Awake();
            
        //Modify scale
        deactivatedSizeDelta.x = activatedSizeDelta.x;
        deactivatedSizeDelta.y = 0f;
        rectTransform.sizeDelta = deactivatedSizeDelta;
        defaultSizeDelta = activatedSizeDelta;

        dialogue = transform.parent.parent.FindChild("Textbox").FindChild("Text").GetComponent<Dialogue>();
        confrontationResponses = new List<ConfrontationResponse>();
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void Deactivate()
    {
        base.Deactivate();
    }

    void OnEnable()
    {
        dialogue.onCommand += ExecuteCommand;
    }

    void OnDisable()
    {
        dialogue.onCommand -= ExecuteCommand;
    }

    void Update()
    {

    }

    public void ExecuteCommand(string command, string resource)
    {
        //Check for correct command
        if (command != "Response")
            return;

        //Split resource
        string[] resources = resource.Split('|');
        int number = int.Parse(resources[0]);
        string text = resources[1];
        string nextScriptPath = "TextAssets/Confrontation/";
        nextScriptPath += SceneManager.GetActiveScene().name + "/";
        nextScriptPath += resources[2];
        TextAsset nextScript = Resources.Load<TextAsset>(nextScriptPath);

        //Get reference to confrontation response
        ConfrontationResponse response = confrontationResponses[number - 1];

        //Set text
        response.SetResponse(text, nextScript);

        //Set awaiting player response flag
        awaitingPlayerResponse = true;

        //Increment number of responses
        ++numberOfResponses;

        //Resize and activate responses box
        if (numberOfResponses == TotalNumberOfResponses()) ResizeAndActivate();
    }

    private void ResizeAndActivate()
    {
        if (resizeCalled) return;

        resizeCalled = true;
        
        //Resize responses box
        activatedSizeDelta.y = defaultSizeDelta.y * (numberOfResponses / 3f);

        //Move responses
        MoveResponses();

        //Activate responses
        onActivation += EnableResponses;
        Activate();
    }

    public void AddToConfrontationResponses(ConfrontationResponse confrontationResponse)
    {
        confrontationResponses.Add(confrontationResponse);
    }

    private void EnableResponses()
    {
        //Loop through responses and enable text components
        for (int i = 0; i < confrontationResponses.Count; ++i)
        {
            confrontationResponses[i].EnableText();
        }
    }

    public void ResetConfrontationResponses()
    {
        Deactivate();

        for (int i = 0; i < confrontationResponses.Count; ++i)
        {
            confrontationResponses[i].ResetResponse();
        }

        numberOfResponses = 0;

        resizeCalled = false;

        awaitingPlayerResponse = false;
    }

    private void MoveResponses()
    {
        Vector2 top = new Vector2(0.5f, 1f);
        Vector2 middle = new Vector2(0.5f, 0.5f);
        Vector2 bottom = new Vector2(0.5f, 0f);

        if (numberOfResponses == 1)
        {
            confrontationResponses[0].GetComponent<RectTransform>().anchorMin = bottom;
            confrontationResponses[0].GetComponent<RectTransform>().anchorMax = bottom;
            confrontationResponses[0].GetComponent<RectTransform>().pivot = bottom;

            confrontationResponses[1].GetComponent<RectTransform>().anchorMin = middle;
            confrontationResponses[1].GetComponent<RectTransform>().anchorMax = middle;
            confrontationResponses[1].GetComponent<RectTransform>().pivot = middle;

            confrontationResponses[2].GetComponent<RectTransform>().anchorMin = top;
            confrontationResponses[2].GetComponent<RectTransform>().anchorMax = top;
            confrontationResponses[2].GetComponent<RectTransform>().pivot = top;
        }
        else if (numberOfResponses == 2)
        {
            confrontationResponses[0].GetComponent<RectTransform>().anchorMin = middle;
            confrontationResponses[0].GetComponent<RectTransform>().anchorMax = middle;
            confrontationResponses[0].GetComponent<RectTransform>().pivot = middle;

            confrontationResponses[1].GetComponent<RectTransform>().anchorMin = bottom;
            confrontationResponses[1].GetComponent<RectTransform>().anchorMax = bottom;
            confrontationResponses[1].GetComponent<RectTransform>().pivot = bottom;

            confrontationResponses[2].GetComponent<RectTransform>().anchorMin = top;
            confrontationResponses[2].GetComponent<RectTransform>().anchorMax = top;
            confrontationResponses[2].GetComponent<RectTransform>().pivot = top;
        }
        else
        {
            confrontationResponses[0].GetComponent<RectTransform>().anchorMin = top;
            confrontationResponses[0].GetComponent<RectTransform>().anchorMax = top;
            confrontationResponses[0].GetComponent<RectTransform>().pivot = top;

            confrontationResponses[1].GetComponent<RectTransform>().anchorMin = middle;
            confrontationResponses[1].GetComponent<RectTransform>().anchorMax = middle;
            confrontationResponses[1].GetComponent<RectTransform>().pivot = middle;

            confrontationResponses[2].GetComponent<RectTransform>().anchorMin = bottom;
            confrontationResponses[2].GetComponent<RectTransform>().anchorMax = bottom;
            confrontationResponses[2].GetComponent<RectTransform>().pivot = bottom;
        }
    }

    private int TotalNumberOfResponses()
    {
        int total = 0;

        foreach (Match match in Regex.Matches(dialogue.script, "<Response>"))
        {
            total++;
        }

        return total;
    }
}
