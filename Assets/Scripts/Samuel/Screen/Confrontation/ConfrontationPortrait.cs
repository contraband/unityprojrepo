﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationPortrait.cs
// Author : Samuel Schimmel
// Purpose : This script deals with setting the player/enemy portraits during 
//			 confrontations. 
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConfrontationPortrait : ConfrontationInterfaceElement
{
    protected override void Awake()
    {
        base.Awake();

        image.enabled = false;
    }

	protected override void Start ()
    {
        base.Start();
	}

    public void SetPortrait(string newPortrait)
    {
        if (newPortrait == "")
        {
            image.enabled = false;
        }
        else
        {
            image.enabled = true;
            image.sprite = Resources.Load<Sprite>("Portraits/" + newPortrait);
        }
    }

    public override void Activated()
    {
        base.Activated();
    }

    public override void Deactivate()
    {
        image.enabled = false;

        base.Deactivate();
    }

    void Update ()
    {
	}
}
