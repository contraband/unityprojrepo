﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationResponse.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ConfrontationResponse : MonoBehaviour
{
    //Set in script
    public int responseNumber { get; private set; }
    public TextAsset nextScript { get; private set; }

    //Private
    private RectTransform rectTransform;
    private TextMeshProUGUI text;
    private Button button;
    private ConfrontationResponses confrontationResponses;

    void Awake()
    {
        GetNumber();

        confrontationResponses = transform.parent.parent.GetChild(0).GetComponent<ConfrontationResponses>();
        text = GetComponent<TextMeshProUGUI>();
        text.text = "";
        button = GetComponent<Button>();
        rectTransform = GetComponent<RectTransform>();
        button.interactable = false;
    }

    void Start()
    {
        confrontationResponses.AddToConfrontationResponses(this);

        button.onClick.AddListener(ResponseSelected);
    }

    void Update()
    {
        if (!confrontationResponses.activated)
            return;

        if (Input.GetKey(responseNumber.ToString()))
            ResponseSelected();
    }

    private void ResponseSelected()
    {
        confrontationResponses.dialogue.SetScript(nextScript.ToString());
        confrontationResponses.dialogue.dialogueState = Dialogue.DialogueStates.During;
        confrontationResponses.dialogue.Continue();
        confrontationResponses.ResetConfrontationResponses();
    }

    private void GetNumber()
    {
        if (name == "Response1") responseNumber = 1;
        else if (name == "Response2") responseNumber = 2;
        else if (name == "Response3") responseNumber = 3;
        else if (name == "Response4") responseNumber = 4;
    }

    private void SetText(string text_)
    {
        text.enabled = false;
        
        //Blank
        if (text_ == "")
            text.text = "";
        //Text
        else
            text.text = responseNumber + ". " + text_;
    }

    private void SetNextScript(TextAsset nextScript_)
    {
        nextScript = nextScript_;
    }

    public void SetResponse(string text_, TextAsset nextScript_)
    {
        SetText(text_);
        SetNextScript(nextScript_);
        button.interactable = true;
    }

    public void ResetResponse()
    {
        SetText("");
        SetNextScript(null);
        button.interactable = false;
    }
    
    public void EnableText()
    {
        text.enabled = true;
    }

    public void PointerEnter()
    {
        text.fontStyle = FontStyles.Bold;
    }

    public void PointerExit()
    {
        text.fontStyle = FontStyles.Normal;
    }
}
