﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationPicture.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConfrontationPicture : ConfrontationInterfaceElement
{
    private Image pictureImage;
    private Vector2 defaultSizeDelta { get; set; }

    protected override void Awake()
    {
        base.Awake();
        pictureImage = transform.FindChild("PictureImage").GetComponent<Image>();
        pictureImage.enabled = false;

        //Modify scale
        deactivatedSizeDelta.x = activatedSizeDelta.x;
        deactivatedSizeDelta.y = 0f;
        rectTransform.sizeDelta = deactivatedSizeDelta;
        defaultSizeDelta = activatedSizeDelta;
    }

    protected override void Start()
    {
        base.Start();
    }

    public void SetImage(string newPicture)
    {
        if (newPicture == "")
        {
            Deactivate();
            pictureImage.enabled = false;
        }
        else
        {
            pictureImage.sprite = Resources.Load<Sprite>("ConfrontationPictures/" + newPicture);
            pictureImage.color = Color.white;
            onActivation += EnableImage;
            Activate();
        }
    }

    private void EnableImage()
    {
        pictureImage.enabled = true;
    }

    public override void Activated()
    {
        base.Activated();
    }

    public override void Deactivate()
    {
        pictureImage.enabled = false;

        base.Deactivate();
    }

    void Update()
    {

    }
}