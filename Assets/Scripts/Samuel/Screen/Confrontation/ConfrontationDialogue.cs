﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationDialogue.cs
// Author : Samuel Schimmel
// Purpose : This script handles the dialogue logic during player/npc confrontations
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConfrontationDialogue : ConfrontationInterfaceElement
{
    public event EventHandler onDialogueEnd;

    public bool dialogueFinished { get; private set; }

    public ConfrontationContinue continueButton { get; private set; }
    public ConfrontationName confrontationName { get; private set; }
    public ConfrontationPortrait confrontationPortrait { get; private set; }
    public ConfrontationResponses confrontationResponses { get; private set; }
    public ConfrontationPicture confrontationImage { get; set; }
    public Dialogue dialogue { get; private set; }
    public Player player { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        continueButton = FindObjectOfType<ConfrontationContinue>();
        confrontationName = FindObjectOfType<ConfrontationName>();
        confrontationPortrait = FindObjectOfType<ConfrontationPortrait>();
        confrontationResponses = FindObjectOfType<ConfrontationResponses>();
        confrontationImage = FindObjectOfType<ConfrontationPicture>();
        dialogue = transform.FindChild("Textbox").FindChild("Text").GetComponent<Dialogue>();
        player = FindObjectOfType<Player>();
    }

    protected override void Start()
    {
        base.Start();
    }

    void OnEnable()
    {
        dialogue.onCommand += ExecuteCommand;
        dialogue.onDialogueFinished += DialogueFinished;
        dialogue.onDialogueClosed += DialogueClosed;
    }

    void OnDisable()
    {
        dialogue.onCommand -= ExecuteCommand;
        dialogue.onDialogueFinished -= DialogueFinished;
        dialogue.onDialogueClosed -= DialogueClosed;
    }

    void Update()
    {
    }

    public void SetScript(string newScript)
    {
        dialogue.SetScript(newScript);
        dialogueFinished = false;
    }

    public void Continue()
    {
        dialogue.Continue();
    }

    public void ExecuteCommand(string command, string resource)
    {
        if (command == "SetName") confrontationName.SetName(resource);
        else if (command == "SetPortrait") confrontationPortrait.SetPortrait(resource);
        else if (command == "ChangeSuspicion") ChangeSuspicion(resource);
        else if (command == "SetImage") confrontationImage.SetImage(resource);
    }

    private void ChangeSuspicion(string input)
    {
        int value = 0;

        if (input == "Coworker") value = GameManager.coworkerSuspicion;
        else if (input == "LightSecurity") value = GameManager.lightSecuritySuspicion;
        else if (input == "Camera") value = GameManager.cameraSuspicion;
        else value = int.Parse(input);

        //Audio feedback
        if (value > 0) player.audioObject.PlayOnce(gameManager.suspicionIncreaseClip);
        else if (value < 0) player.audioObject.PlayOnce(gameManager.suspicionDecreaseClip);
        player.ChangeHP(-value);

        string sign = "+";
        string color = "<color=red>";
        if (value < 0)
        {
            sign = "";
            color = "<color=green>";
        }

        dialogue.text.text = color + "<b>[" + sign + value + " SUSPICION]</b></color>\n";
        
    }

    public void DialogueFinished()
    {
        dialogueFinished = true;
    }

    public void DialogueClosed()
    {
        if (!confrontationResponses.awaitingPlayerResponse)
            confrontationInterface.Deactivate();
    }

    public override void Deactivate()
    {
        base.Deactivate();
    }
}
