﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationName.cs
// Author : Samuel Schimmel
// Purpose : This script handles naming the enemy/npc portraits during 
//			 player/enemy confrontations
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class ConfrontationName : ConfrontationInterfaceElement
{
    private TextMeshProUGUI text;

    protected override void Awake()
    {
        base.Awake();

        text = transform.FindChild("Text").GetComponent<TextMeshProUGUI>();
        text.text = "";
        image.enabled = false;
        frostedGlassBackground.enabled = false;
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void Activated()
    {

        base.Activated();
    }

    public override void Deactivate()
    {
        text.text = "";
        image.enabled = false;
        frostedGlassBackground.enabled = false;

        base.Deactivate();
    }

    public void SetName(string newName)
    {
        text.text = newName;

        if (newName == "")
        {
            image.enabled = false;
            frostedGlassBackground.enabled = false;
        }
        else
        {
            image.enabled = true;
            frostedGlassBackground.enabled = true;
        }

    }

    void Update()
    {

    }
}
