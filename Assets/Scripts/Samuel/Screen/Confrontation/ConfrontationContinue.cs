﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationContinue.cs
// Author : Samuel Schimmel
// Purpose : This script handles the logic behind changing buttons and their 
// behaviors in dialogue confrontations.  
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class ConfrontationContinue : ConfrontationInterfaceElement
{
    private TextMeshProUGUI continueText;
    private TextMeshProUGUI controlText;
    private Button button;
    private ConfrontationDialogue confrontationDialogue;

    protected override void Awake()
    {
        base.Awake();

        continueText = transform.FindChild("ContinueText").GetComponent<TextMeshProUGUI>();
        continueText.text = "";
        controlText = transform.FindChild("ControlText").GetComponent<TextMeshProUGUI>();
        controlText.text = "";

        button = GetComponent<Button>();
        confrontationDialogue = transform.parent.FindChild("Dialogue").GetComponent<ConfrontationDialogue>();
    }

    protected override void Start()
    {
        base.Start();

        button.onClick.AddListener(confrontationDialogue.Continue);
    }

    public override void Activated()
    {
        base.Activated();
    }

    public override void Deactivate()
    {
        continueText.text = "";
        controlText.text = "";
        base.Deactivate();
    }

    void Update()
    {
        button.interactable = false;
        continueText.text = "";
        controlText.text = "";

        //Inactive
        if (!activated)
            return;

        //Awaiting player response
        if (confrontationDialogue.confrontationResponses.awaitingPlayerResponse)
            return;

        if (confrontationDialogue.dialogueFinished)
        {
            button.interactable = true;
            continueText.text = "RETURN";
            controlText.text = "SPACE";
        }
        else
        {
            button.interactable = true;
            continueText.text = "CONTINUE";
            controlText.text = "SPACE";
        }
    }
}
