﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConfrontationInterfaceElement.cs
// Author : Samuel Schimmel
// Purpose : This script deals with UI/HUD movementBehavior during confrontations.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConfrontationInterfaceElement : MonoBehaviour
{
    public ConfrontationInterface confrontationInterface { get; set; }
    protected Image image;
    public Image frostedGlassBackground { get; set; }
    public delegate void EventHandler();
    public event EventHandler onActivation;
    public event EventHandler onDeactivation;
    public bool activated { get; protected set; }
    public GameManager gameManager { get; set; }

    [HideInInspector] public Vector2 activatedSizeDelta;
    [HideInInspector] public Vector2 deactivatedSizeDelta;
    public RectTransform rectTransform { get; set; }

    protected virtual void Awake()
    {
        confrontationInterface = FindObjectOfType<ConfrontationInterface>();
        rectTransform = GetComponent<RectTransform>();
        image = GetComponent<Image>();

        activatedSizeDelta = rectTransform.sizeDelta;
        deactivatedSizeDelta = activatedSizeDelta;
        deactivatedSizeDelta.x = 0;
        rectTransform.sizeDelta = deactivatedSizeDelta;

        if (transform.FindChild("FrostedGlassBackground")) frostedGlassBackground = transform.FindChild("FrostedGlassBackground").GetComponent<Image>();
    }

    protected virtual void Start()
    {
        confrontationInterface.AddConfrontationInterfaceElement(this);
        gameManager = FindObjectOfType<GameManager>();
    }

    public void Activate()
    {
        onActivation += Activated;
        StartCoroutine(InterpolateSizeDelta(activatedSizeDelta));
    }

    public virtual void Activated()
    {
        onActivation -= Activated;
    }

    public virtual void Deactivate()
    {
        if (onDeactivation != null) onDeactivation();

        StartCoroutine(InterpolateSizeDelta(deactivatedSizeDelta));
        activated = false;
    }

    private IEnumerator InterpolateSizeDelta(Vector2 endSizeDelta)
    {
        float elapsedTime = 0;
        Vector2 startSizeDelta = rectTransform.sizeDelta;
        
        //Start and end colors
        Color startColor = image.color;
        Color endColor = startColor;
        if (endSizeDelta.x > startSizeDelta.x || endSizeDelta.y > startSizeDelta.y) endColor.a = 1f;
        else endColor.a = 0f;

        while (elapsedTime < ConfrontationInterface.transitionTime)
        {
            float t = elapsedTime / ConfrontationInterface.transitionTime;
            float x = Mathf.SmoothStep(startSizeDelta.x, endSizeDelta.x, t);
            float y = Mathf.SmoothStep(startSizeDelta.y, endSizeDelta.y, t);
            Vector2 newSizeDelta = rectTransform.sizeDelta;
            newSizeDelta.x = x;
            newSizeDelta.y = y;
            rectTransform.sizeDelta = newSizeDelta;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (endSizeDelta.x > startSizeDelta.x || endSizeDelta.y > startSizeDelta.y)
        {
            if (onActivation != null)
                onActivation();

            activated = true;
        }
        else
        {
            if (onDeactivation != null) onDeactivation();
        }

        rectTransform.sizeDelta = endSizeDelta;
    }

    void Update()
    {

    }
}
