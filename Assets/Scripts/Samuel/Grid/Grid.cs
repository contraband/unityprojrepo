﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Grid.cs
// Author : Samuel Schimmel
// Purpose : This script handles grid logic and movementBehavior.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
[RequireComponent(typeof(LevelManager))]
public class Grid : MonoBehaviour
{
    //Set in inspector
    public GameObject tilePrefab;
    public GameObject roomPrefab;
    public GameObject background3DPrefab;
    public Sprite tileRender;
    public Sprite tileOccupantRender;
    public int width;
    public int height;
    public Vector2 exitCoordinates;
    public bool hideExitFeedback;
    public Vector2[] UnreachableTiles;

    public Player player { get; set; }
    public List<List<Tile>> grid;
    public List<RoomCollider> rooms { get; set; }
    private Actors actors;
    private GameManager gameManager;
    public List<Wall> doors { get; set; }
    public List<Tile> unreachableTiles { get; set; }
    private bool prefabsInstantiated;
    public Pathfinder pathfinder { get; set; }
    public MainCamera mainCamera { get; set; }
    public LevelManager levelManager { get; set; }

    void Awake()
    {
        actors = FindObjectOfType<Actors>();
        doors = new List<Wall>();
        unreachableTiles = new List<Tile>();
        pathfinder = GetComponent<Pathfinder>();
        player = FindObjectOfType<Player>();
        levelManager = FindObjectOfType<LevelManager>();

        GenerateGrid();
        GetDoors();
        GetUnreachableTiles();
        AddExits();
    }

    void Start()
    {
    }

    public void GridUpdate()
    {
        InstantiatePrefabs();
        ResetTiles();
        actors.ActorsUpdate();
        UpdateTiles();
    }

    void GenerateGrid()
    {
        if (Application.isEditor)
        {
            var children = new List<GameObject>();
            foreach (Transform child in transform) children.Add(child.gameObject);
            children.ForEach(child => DestroyImmediate(child));
        }
        else
        {
            var children = new List<GameObject>();
            foreach (Transform child in transform) children.Add(child.gameObject);
            children.ForEach(child => Destroy(child));
        }

        grid = new List<List<Tile>>();

        for (int r = 0; r < width; ++r)
        {
            List<Tile> row = new List<Tile>();

            for (int c = 0; c < height; ++c)
            {
                Vector3 worldPosition = new Vector3(r, c, 0);
                string name = "(" + r + ", " + c + ")";
                Tile tile = Instantiate(tilePrefab, worldPosition, Quaternion.identity).GetComponent<Tile>();
                tile.gridPosition = new Vector2(r, c);
                row.Add(tile);
                tile.transform.parent = transform;
                tile.name = "(" + tile.gridPosition.x + ", " + tile.gridPosition.y + ")";
            }

            grid.Add(row);
        }
    }

    private void GenerateRooms()
    {
        rooms = new List<RoomCollider>();

        ////Loop through grid
        //for (int r = 0; r < width; ++r)
        //{
        //    for (int c = 0; c < height; ++c)
        //    {
        //        GenerateRoom(r, c);
        //    }
        //}
    }

    private void GenerateRoom(int xCoordinate, int yCoordinate)
    {
        bool enemyFound = false;
        
        //Get tile
        Tile roomOrigin = GetTileAbsolute(xCoordinate, yCoordinate);

        //Check if tile is a room origin
        if (!CheckTileForWall(roomOrigin, -1, 0)) return;
        if (!CheckTileForWall(roomOrigin, -1, -1)) return;
        if (!CheckTileForWall(roomOrigin, 0, -1)) return;

        //Get width and height
        int roomWidth = GetRoomDimension(roomOrigin, 1, 0);
        int roomHeight = GetRoomDimension(roomOrigin, 0, 1);

        //Check if room boundaries were found
        if (roomWidth == -1 || roomHeight == -1)
            return;

        //Make sure room doesn't contain player's starting location,
        //and contains at least one enemy
        for (int r = 0; r < roomWidth; ++r)
        {
            for (int c = 0; c < roomHeight; ++c)
            {
                TileOccupant occupant = GetTileRelative(roomOrigin.transform, r, c).currentOccupant;

                //Enemy
                if (occupant && occupant.GetComponent<Enemy>())
                    enemyFound = true;

                //Player
                if (occupant && occupant.GetComponent<Player>())
                    return;
            }
        }

        //Abort if no enemies were found
        if (!enemyFound)
            return;

        //Create and place room
        Vector3 roomPosition = roomOrigin.transform.position;
        roomPosition.x += roomWidth / 2f - 0.5f;
        roomPosition.y += roomHeight / 2f - 0.5f;
        roomPosition.z = 0;
        GameObject roomParent = Instantiate(roomPrefab, roomPosition, Quaternion.Euler(new Vector3()));

        //Initialize room
        RoomCollider roomCollider = roomParent.transform.GetChild(0).GetComponent<RoomCollider>();
        roomCollider.Initialize(roomOrigin, roomWidth, roomHeight);

        //Add room to list
        rooms.Add(roomCollider);
    }

    private bool CheckTileForWall(Tile roomOrigin, int xDirection, int yDirection)
    {
        Tile tile = GetTileRelative(roomOrigin.transform, xDirection, yDirection);

        if (!tile)
            return false;

        if (!tile.currentOccupant)
            return false;

        if (!tile.currentOccupant.GetComponent<Wall>())
            return false;

        if (tile.currentOccupant.GetComponent<Wall>().isScenery)
            return false;

        return true;
    }

    private int GetRoomDimension(Tile roomOrigin, int xDirection, int yDirection)
    {
        int dimension = 0;

        //Look for edge of grid
        while (roomOrigin)
        {
            //Check for wall
            if (roomOrigin.currentOccupant
                && roomOrigin.currentOccupant.GetComponent<Wall>()
                && !roomOrigin.currentOccupant.GetComponent<Wall>().isScenery)
                break;
            
            //Offset roomOrigin 1 tile at a time
            roomOrigin = GetTileRelative(roomOrigin.transform, xDirection, yDirection);

            ++dimension;
        }

        //Wall found
        if (roomOrigin)
            return dimension;
        //Wall not found
        else
            return -1;
    }

    public Tile GetTileAbsolute(int xCoordinate, int yCoordinate)
    {
        //Check if tile exists
        if(xCoordinate >= 0 && xCoordinate < width && yCoordinate >= 0 && yCoordinate < height)
            return grid[xCoordinate][yCoordinate];

        return null;
    }

    public Tile GetTileAbsolute(Vector2 position)
    {
        return GetTileAbsolute((int)position.x, (int)position.y);
    }

    public Tile GetTileRelative(Transform origin, int xOffset = 0, int yOffset = 0)
    {
        int xCoordinate = Mathf.RoundToInt(origin.position.x) + xOffset;
        int yCoordinate = Mathf.RoundToInt(origin.position.y) + yOffset;

        return GetTileAbsolute(xCoordinate, yCoordinate);
    }

    public void ResetPathfindingData()
    {
        for(int r = 0; r < grid.Count; ++r)
        {
            for (int c = 0; c < grid[r].Count; ++c)
            {
                if (!grid[r][c])
                    continue;

                grid[r][c].f = 0;
                grid[r][c].g = 0;
                grid[r][c].h = 0;
                grid[r][c].parentTile = null;
            }
        }
    }

    public void ResetTiles()
    {
        for (int r = 0; r < grid.Count; ++r)
        {
            for (int c = 0; c < grid[r].Count; ++c)
            {
                if (!grid[r][c])
                    continue;

                //Next enemy move feedback
                grid[r][c].SetNextEnemyMove(false, Quaternion.identity);
            }
        }
    }

    public void UpdateTiles()
    {
        for (int r = 0; r < grid.Count; ++r)
        {
            for (int c = 0; c < grid[r].Count; ++c)
            {
                if (!grid[r][c])
                    continue;

                grid[r][c].TileUpdate();
            }
        }
    }

    //Returns true if the relationship between two tiles is orthogonal
    public bool TilesAreOrthogonal(Tile a, Tile b)
    {
        if (a != null && b != null)
        {

            int xDistance = (int)a.gridPosition.x - (int)b.gridPosition.x;
            int yDistance = (int)a.gridPosition.y - (int)b.gridPosition.y;
            int totalDistance = xDistance + yDistance;

            //When grid position components are added, the sums will be even for diagonal tiles and odd for orthogonal tiles
            if (totalDistance % 2 == 0)
                return false;
            else
                return true;
        }
        return false;
    }

    private void AddExits()
    {
        if (levelManager.outro) return;

        Tile exitTile = GetTileAbsolute((int)exitCoordinates.x, (int)exitCoordinates.y);
        exitTile.gameObject.AddComponent<Exit>();

        GetTileRelative(exitTile.transform, 1, 0).gameObject.AddComponent<Exit>();
        GetTileRelative(exitTile.transform, 1, -1).gameObject.AddComponent<Exit>();
        GetTileRelative(exitTile.transform, 0, -1).gameObject.AddComponent<Exit>();
        GetTileRelative(exitTile.transform, -1, -1).gameObject.AddComponent<Exit>();
        GetTileRelative(exitTile.transform, -1, 0).gameObject.AddComponent<Exit>();
        GetTileRelative(exitTile.transform, -1, 1).gameObject.AddComponent<Exit>();
        GetTileRelative(exitTile.transform, 0, 1).gameObject.AddComponent<Exit>();
        GetTileRelative(exitTile.transform, 1, 1).gameObject.AddComponent<Exit>();
    }

    private void GetUnreachableTiles()
    {
        for (int i = 0; i < UnreachableTiles.Length; ++i)
        {
            Tile tile = GetTileAbsolute((int)UnreachableTiles[i].x, (int)UnreachableTiles[i].y);
            unreachableTiles.Add(tile);
        }
    }

    private void GetDoors()
    {
        Wall[] walls = FindObjectsOfType<Wall>();

        for (int i = 0; i < walls.Length; ++i)
        {
            if (walls[i].isDoor)
                doors.Add(walls[i]);
        }
    }

    public void OpenDoors()
    {
        FindObjectOfType<MainCamera>().onCameraMoveEnd -= OpenDoors;

        //Open doors
        for (int i = 0; i < doors.Count; ++i)
        {
            doors[i].OpenDoor();
        }

        //Unlock entrances
        for (int j = 0; j < rooms.Count; ++j)
        {
            rooms[j].UnlockEntrances();
        }

        player.audioObject.PlayOnce(gameManager.doorsOpeningClip);
        player.GetPossibleMoves();
    }

    public void InstantiatePrefabs()
    {
        if (prefabsInstantiated) return;

        prefabsInstantiated = true;

        gameManager = FindObjectOfType<GameManager>();
        mainCamera = FindObjectOfType<MainCamera>();

        GenerateBackgrounds();
        GenerateRooms();
    }

    private void GenerateBackgrounds()
    {
        //3D backround
        if (background3DPrefab) Instantiate(background3DPrefab, Vector3.zero, Quaternion.identity);
        levelManager.background3DCamera = (Instantiate(gameManager.background3DCameraPrefab, Vector3.zero, Quaternion.identity)).GetComponent<ImageEffectCamera>();

        if (levelManager.outro) return;

        //Pre-rendered backgrounds
        Vector3 position = Vector3.zero;
        position.x = width / 2f;
        position.y = height / 2f;
        position.x -= 0.5f;
        position.y -= 0.5f;
        Transform parent = Instantiate(gameManager.preRenderedBackgroundsPrefab, position, Quaternion.identity).GetComponent<Transform>();

        //Building
        Transform building = Instantiate(gameManager.buildingPrefab, Vector3.zero, Quaternion.identity).transform.FindChild("Building");
        float buildingHeight = 50f;
        position.z = buildingHeight;
        building.position = position;
        building.localScale = new Vector3(width + 1f, height + 1f, buildingHeight * 2f);

        parent.FindChild("Tiles").GetComponent<SpriteRenderer>().sprite = tileRender;
        parent.FindChild("TileOccupants").GetComponent<SpriteRenderer>().sprite = tileOccupantRender;
    }

    public void RemoveRooms()
    {
        for (int i = 0; i < rooms.Count; ++i)
        {
            rooms[i].EnterRoom(true);
        }
    }
}
