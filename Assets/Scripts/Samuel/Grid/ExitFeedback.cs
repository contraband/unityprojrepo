﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ExitFeedback.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitFeedback : MonoBehaviour
{
    public AnimationCurve curve;
    public Color offColor;
    public Color onStartColor;
    public Color onEndColor;
    public float speed;

    private Image content { get; set; }
    private Image frostedGlass { get; set; }
    private Image frostedGlassBackground { get; set; }
    private Player player { get; set; }

    void Awake()
    {
        content = GetComponentInChildren<Image>().transform.FindChild("Content").GetComponent<Image>();
        frostedGlass = GetComponentInChildren<Image>().transform.FindChild("FrostedGlass").GetComponent<Image>();
        frostedGlassBackground = GetComponentInChildren<Image>().transform.FindChild("FrostedGlassBackground").GetComponent<Image>();
        player = FindObjectOfType<Player>();
    }

	void Start ()
    {
		
	}
	
	void Update ()
    {
        AnimateSign();
	}

    private void AnimateSign()
    {

        if (!player.canExit)
        {
            content.color = offColor;
            return;
        }

        content.color = Color.Lerp(onStartColor, onEndColor, curve.Evaluate(Time.time * speed));
    }
}
