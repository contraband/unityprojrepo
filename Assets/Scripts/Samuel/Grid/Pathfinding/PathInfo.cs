﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : stores next tile in pathfinding system
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathInfo
{
    public Tile nextTileInPath;
    public int remainingMovesInPath;
    public List<Tile> tiles { get; set; }

    public PathInfo()
    {
        tiles = new List<Tile>();
    }

    public void AddTile(Tile tile)
    {
        tiles.Add(tile);
    }
}
