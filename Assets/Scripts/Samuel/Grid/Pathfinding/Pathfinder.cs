﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : Controls all pathfinding logic including how actors are able to
//			 navigate spaces.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathfinder : MonoBehaviour
{
    private Player player;
    private Actor actor;
    private Tile start;
    private Tile destination;
    private TileOccupant acceptableCollision;
    private Grid grid;
    private int orthogonalFCost = 10;
    private int diagonalFCost = 14;
    private int hMultiplier = 10;
    private List<Tile> openList;
    private List<Tile> closedList;

    void Start ()
    {
        player = FindObjectOfType<Player>();
        grid = GameObject.Find("Grid").GetComponent<Grid>();
        openList = new List<Tile>();
        closedList = new List<Tile>();
    }

	void Update ()
    {
	
	}

    public PathInfo FindPath(Tile start_, Tile destination_, Actor actor_, TileOccupant acceptableCollision_ = null)
    {
        //Set start, destination, and actor
        start = start_;
        destination = destination_;
        actor = actor_;
        acceptableCollision = acceptableCollision_;

        //Check if target path exists
        if (!destination_)
            return null;

        //Check if target is designated as unreachable
        if (TileIsUnreachable(destination_))
            return null;

        //Check if tile has an occupant,
        //unless the actor can collide with that occupant
        if (destination.currentOccupant)
        {
            if (destination.currentOccupant != acceptableCollision)
                return null;
        }

        //If actor is player, check if tile is in a room that the player is not in
        if (actor == player && destination.room && destination.room.state != RoomStates.Present)
            return null;

        //Clear data
        ClearData();

        //Initially populate open and closed lists
        PopulateListsAndSetParentTiles(start, actor);
        
        //Core loop
        while (openList.Count > 0)
        {           
            //Calculate all scores
            CalculateAllScores();

            //Find tile with min F score
            Tile selectedTile = FindTileWithMinFScore();

            //Check if current tile is destination
            if (selectedTile == destination)
            {
                //Backtrack from destination to get first tile in path, and return it
                return BacktrackFromDestination(selectedTile);
            }
            else
            {
                //Move current tile to closed list
                SwitchLists(selectedTile);

                //Repopulate open and closed lists
                PopulateListsAndSetParentTiles(selectedTile, actor);
            }
        }
        
        //No path found
        return null;
    }

    private void PopulateListsAndSetParentTiles(Tile parentTile, Actor actor)
    {
        closedList.Add(parentTile);
        AddTileToOpenList(parentTile, 0, 1, actor);
        AddTileToOpenList(parentTile, 1, 0, actor);
        AddTileToOpenList(parentTile, 0, -1, actor);
        AddTileToOpenList(parentTile, -1, 0, actor);
    }

    private void AddTileToOpenList(Tile parentTile, int xOffset, int zOffset, Actor actor)
    {        
        //Get tile
        Tile tileToAdd = grid.GetTileRelative(parentTile.transform, xOffset, zOffset);

        //Check if tile exists
        if (tileToAdd == null)
            return;

        //Check if tile has an occupant
        if (tileToAdd.currentOccupant)
        {
            bool allowTile = false;

            //Check if the actor can collide with that occupant
            if (tileToAdd.currentOccupant == acceptableCollision) allowTile = true;

            //Check if the occupant is an open door
            Wall wall = tileToAdd.currentOccupant.GetComponent<Wall>();
            if (wall && wall.isOpen) allowTile = true;

            if (!allowTile) return;
        }

        //Check if tile is unreachable
        if (TileIsUnreachable(tileToAdd))
            return;

        //Check if tile is an exit,
        //and actor is not the player
        if (tileToAdd.exit && actor != player)
            return;

        //Check if tile is an enemy spawner
        if (tileToAdd.enemySpawner)
            return;

        //If actor is player, check if tile is in a room that the player is not in
        if (actor == player && tileToAdd.room && tileToAdd.room.state != RoomStates.Present)
            return;

        //Check if tile is in the closed list
        if (closedList.Contains(tileToAdd))
            return;

        //Check if path to tile cuts a corner
        if (CornerCheck(parentTile, tileToAdd))
            return;

        //Check adjacent tiles if 2x2 actor
        if (actor.size == 2)
        {
            Tile right = grid.GetTileRelative(tileToAdd.transform, 1, 0);
            Tile down = grid.GetTileRelative(tileToAdd.transform, 0, -1);
            Tile corner = grid.GetTileRelative(tileToAdd.transform, 1, -1);
            
            if (!right) return;
            if (right.currentOccupant && right.currentOccupant != actor) return;
            if (CornerCheck(parentTile, right)) return;
            if (!down) return;
            if (CornerCheck(parentTile, down)) return;
            if (down.currentOccupant && down.currentOccupant != actor) return;
            if (!corner) return;
            if (corner.currentOccupant && corner.currentOccupant != actor) return;
            if (CornerCheck(parentTile, corner)) return;
        }

        //Check if tile is already in open list
        if (openList.Contains(tileToAdd))
        {
            //Is this path better?
            if (CompareGScores(parentTile, tileToAdd))
                CalculateFScore(tileToAdd);
            else
                return;
        }

        //If tile exists, is empty, is not in the closed list,
        //and is not already in open list via a better path,
        //add it to the open list and set its parent tile
        openList.Add(tileToAdd);
        tileToAdd.parentTile = parentTile;
    }

    private void CalculateAllScores()
    {
        for (int i = 0; i < openList.Count; ++i)
        {
            CalculateFScore(openList[i]);
        }
    }

    private void CalculateFScore(Tile selectedTile)
    {
        CalculateGScore(selectedTile);
        CalculateHScore(selectedTile);

        selectedTile.f = selectedTile.g + selectedTile.h;
    }

    private void CalculateGScore(Tile selectedTile)
    {
        if (grid.TilesAreOrthogonal(selectedTile, selectedTile.parentTile))
            selectedTile.g = orthogonalFCost;
        else
            selectedTile.g = diagonalFCost;
        
        //Add parent tile's G
        selectedTile.g += selectedTile.parentTile.g;
    }

    private void CalculateHScore(Tile selectedTile)
    {
        int xDistance = Mathf.Abs((int)destination.gridPosition.x - (int)selectedTile.gridPosition.x);
        int yDistance = Mathf.Abs((int)destination.gridPosition.y - (int)selectedTile.gridPosition.y);
        int totalDistance = xDistance + yDistance;
        selectedTile.h = totalDistance * hMultiplier;
    }

    private bool CompareGScores(Tile newParentTile, Tile selectedTile)
    {
        //Get current G score
        int currentG = selectedTile.g;

        //Get new G score
        int newG;
        if (grid.TilesAreOrthogonal(newParentTile, selectedTile))
            newG = orthogonalFCost;
        else
            newG = diagonalFCost;
        newG += newParentTile.g;

        //Compare current and new G scores, returning true if new is better
        if (newG < currentG)
            return true;
        else
            return false;
    }

    private Tile FindTileWithMinFScore()
    {
        Tile tileWithMinF = openList[0];
        int minF = tileWithMinF.f;

        for (int i = 0; i < openList.Count; ++i)
        {
            if (openList[i].f < minF)
            {
                tileWithMinF = openList[i];
                minF = tileWithMinF.f;
            }
        }

        return tileWithMinF;
    }

    private void SwitchLists(Tile tile)
    {
        openList.Remove(tile);
        closedList.Add(tile);
    }

    private PathInfo BacktrackFromDestination(Tile selectedTile)
    {
        PathInfo pathInfo = new PathInfo();
        Tile previousTile = selectedTile;
        int remainingMovesInPath = 0;

        while (selectedTile && selectedTile != actor.currentTile)
        {
            //Add tile to path info's list of tiles
            pathInfo.AddTile(selectedTile);

            previousTile = selectedTile;
            selectedTile = selectedTile.parentTile;

            //Add to remaining moves in path unless we've reached the first tile in the path
            if (grid.TilesAreOrthogonal(selectedTile, previousTile))
                remainingMovesInPath += Actor.ORTHOGONAL_MOVES_COST;
            else
                remainingMovesInPath += Actor.DIAGONAL_MOVES_COST;
        }

        pathInfo.nextTileInPath = previousTile;
        pathInfo.remainingMovesInPath = remainingMovesInPath;
        return pathInfo;
    }

    public void ClearData()
    {
        openList.Clear();
        closedList.Clear();
        grid.ResetPathfindingData();
    }

    private bool CornerCheck(Tile parentTile, Tile tileToAdd)
    {
        RaycastHit2D[] hits = Physics2D.LinecastAll(parentTile.transform.position, tileToAdd.transform.position);

        //Identify hits
        for (int i = 0; i < hits.Length; ++i)
        {
            //Walls
            if (hits[i].collider.gameObject.GetComponent<Wall>())
            {
                return true;
            }
        }

        return false;
    }

    private bool TileIsUnreachable(Tile tile)
    {
        for(int i = 0; i < grid.unreachableTiles.Count; ++i)
        {
            if (tile == grid.unreachableTiles[i])
                return true;
        }

        return false;
    }
}
