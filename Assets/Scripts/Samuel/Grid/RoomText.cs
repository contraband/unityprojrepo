﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : outputs the number of enemies in a room to the HUD element
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class RoomText : MonoBehaviour
{
    private Player player;
    private TextMeshProUGUI textMesh;
    public RoomCollider roomCollider { get; set; }

    void Awake()
    {
        player = FindObjectOfType<Player>();
        textMesh = GetComponent<TextMeshProUGUI>();
        roomCollider = transform.parent.parent.GetComponentInChildren<RoomCollider>();
    }

    void Start()
    {
        
    }

    void Update()
    {
        UpdateText();
    }

    void UpdateText()
    {
        textMesh.text = "";

        if (roomCollider.state != RoomStates.Undiscovered)
            return;

        if (!player.readyForPlayerInput)
            return;

        if (player.currentSelection != roomCollider.gameObject && !roomCollider.EntranceIsCurrentSelection())
            return;

        if (roomCollider.enemies.Count == 1)
            textMesh.text = roomCollider.enemies.Count + " COWORKER";
        else
            textMesh.text = roomCollider.enemies.Count + " COWORKERS";
    }
}
