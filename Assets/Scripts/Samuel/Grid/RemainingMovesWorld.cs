﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : RemainingMovesWorld.cs
// Author : Samuel Schimmel
// Purpose : sets HUD object text about how many moves the player has left.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class RemainingMovesWorld : MonoBehaviour
{
    public TextMesh textMesh;

    private Player player;
    private Tile tile;
    private int fontSize = 80;
    private int debugFontSize = 20;
    private MeshRenderer meshRenderer;
    public LevelManager levelManager { get; set; }

    void Awake()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        textMesh = GetComponent<TextMesh>();
        tile = transform.parent.GetComponent<Tile>();
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.sortingLayerName = "TileTexts";
        levelManager = FindObjectOfType<LevelManager>();
    }

    void Start()
    {

    }

    void Update()
    {
        //Editor
        if (Application.isEditor && !Application.isPlaying)
        {
            textMesh.text = tile.name.ToString();
            textMesh.fontSize = debugFontSize;
            return;
        }

        if (!meshRenderer.isVisible) return;

        //Debug
        if (player.debug)
        {
            textMesh.fontSize = debugFontSize;
            return;
        }

        textMesh.text = "";
        textMesh.fontSize = fontSize;

        if (!player.readyForPlayerInput) return;

        if (levelManager.levelType == LevelTypes.Safe) return;

        if (player.currentSelection != transform.parent.gameObject) return;

        if (!tile.availableMove) return;

        if (tile.currentOccupant) return;

        textMesh.text = player.remainingMovesAfterMove.ToString();
    }
}
