﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : RoomIcon.cs
// Author : Samuel Schimmel
// Purpose : Creates icons for room entrances.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RoomIcon : MonoBehaviour
{
    private Player player;
    private SpriteRenderer spriteRenderer;
    public RoomCollider roomCollider;

    void Awake()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        roomCollider = transform.parent.parent.FindChild("Collider").GetComponent<RoomCollider>();
    }

    void Start()
    {

    }

    void Update()
    {
        UpdateSprite();
    }

    void UpdateSprite()
    {
        spriteRenderer.enabled = false;

        if (roomCollider.state != RoomStates.Undiscovered)
            return;

        if (!player.readyForPlayerInput)
            return;

        if (player.currentSelection != roomCollider.gameObject)
            return;

        spriteRenderer.enabled = true;
    }
}
