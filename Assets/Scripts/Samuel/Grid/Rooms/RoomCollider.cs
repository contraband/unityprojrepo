﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : RoomCollider.cs
// Author : Samuel Schimmel
// Purpose : handles enemy movementBehavior in relation to rooms, fades room obstructions.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum RoomStates { Undiscovered, Present, NotPresent };

public class RoomCollider : MonoBehaviour
{
    public bool hasMainTerminal;
    public bool hasMiniterminal;

    private Grid grid;
    private SelectableObject selectableObject;
    private Transform icons;
    private Vector3 iconOffset;
    private Player player;
    private GameManager gameManager;

    private Tile origin;
    public List<Tile> tiles;
    public List<Tile> entrances;
    public List<Tile> lockedEntrances;
    public List<Enemy> enemies;

    //Set in code
    public int width { get; private set; }
    public int height { get; private set; }
    public RoomStates state { get; private set; }    

    //Fading
    public Coroutine fadeTilesCoroutine;
    public bool fading { get; private set; }
    public const float HALF_FOW_ALPHA = 0.5f;
    public const float FOW_FADE_TIME = 2f;

    void Awake()
    {
        selectableObject = GetComponent<SelectableObject>();
        tiles = new List<Tile>();
        entrances = new List<Tile>();
        enemies = new List<Enemy>();
        icons = transform.parent.FindChild("Icons");
        iconOffset = new Vector3(0.5f, 0f, 0f);
        player = FindObjectOfType<Player>();
        gameManager = FindObjectOfType<GameManager>();
        grid = FindObjectOfType<Grid>();
    }

    void Start ()
    {
	
	}
	
	void Update ()
    {
        SetEntranceIcons();
    }

    private void SetEntranceIcons()
    {
        for (int i = 0; i < entrances.Count; ++i)
        {
            entrances[i].transform.FindChild("EntranceIcon").GetComponent<SpriteRenderer>().enabled = false;
        }

        if (state != RoomStates.Undiscovered) return;
        if (!player.readyForPlayerInput) return;
        if (player.currentSelection != gameObject && !EntranceIsCurrentSelection()) return;


        for (int i = 0; i < entrances.Count; ++i)
        {
            entrances[i].transform.FindChild("EntranceIcon").GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    public void EnterRoom(bool destroy = false)
    {
        if (state == RoomStates.Present)
            return;

        state = RoomStates.Present;

        //Show tiles
        FadeTiles(0f, destroy);

        //Show enemies
        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].Fade(1f, FOW_FADE_TIME);
        }

        selectableObject.enabled = false;
    }

    public void ExitRoom()
    {
        if (state != RoomStates.Present)
            return;

        state = RoomStates.NotPresent;

        //Hide tiles
        FadeTiles(HALF_FOW_ALPHA);

        //Hide enemies
        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].Fade(0f, RoomCollider.FOW_FADE_TIME);
        }
    }

    private void FadeTiles(float endAlpha, bool destroy = false)
    {
        if (fading)
        {
            StopCoroutine(fadeTilesCoroutine);
            fading = false;
        }

        fadeTilesCoroutine = StartCoroutine(FadeTilesCoroutine(endAlpha, destroy));
    }

    private IEnumerator FadeTilesCoroutine(float endAlpha, bool destroy = false)
    {
        fading = true;

        float elapsedTime = 0f;

        //Get start alpha
        SpriteRenderer startAlphaFog = tiles[0].transform.FindChild("FogOfWar").GetComponent<SpriteRenderer>();
        float startAlpha = startAlphaFog.color.a;

        while (elapsedTime < FOW_FADE_TIME)
        {
            //Loop through all of this room's tiles
            for(int i = 0; i < tiles.Count; ++i)
            {
                SpriteRenderer fog = tiles[i].transform.FindChild("FogOfWar").GetComponent<SpriteRenderer>();
                float a = Mathf.SmoothStep(startAlpha, endAlpha, (elapsedTime / FOW_FADE_TIME));
                Color newColor = fog.color;
                newColor.a = a;
                fog.color = newColor;
            }

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (destroy)
            Destroy(this.gameObject);

        fading = false;
    }

    public void Initialize(Tile origin_, int width_, int height_)
    {
        origin = origin_;
        width = width_;
        height = height_;

        //Set scale
        Vector3 scale = new Vector3(width, height, 0);
        transform.localScale = scale;

        GetTiles();
        InstantiateIcons();
    }

    private void GetTiles()
    {
        //Loop through tiles in room
        for (int r = 0; r < width; ++r)
        {
            for (int c = 0; c < height; ++c)
            {
                GetTile(r, c);
            }
        }
    }

    private void GetTile(int xOffset, int yOffset)
    {
        Tile tile = grid.GetTileRelative(origin.transform, xOffset, yOffset);

        //Add to list
        tiles.Add(tile);

        //Set tile
        tile.room = this;

        //Activate tile's fog of war
        SpriteRenderer fog = tile.transform.FindChild("FogOfWar").GetComponent<SpriteRenderer>();
        Color newColor = fog.color;
        newColor.a = 1f;
        fog.color = newColor;

        //Check for entrances
        CheckForEntrance(tile, 0, 1);
        CheckForEntrance(tile, 1, 0);
        CheckForEntrance(tile, 0, -1);
        CheckForEntrance(tile, -1, 0);

        //Check for occupants
        if (tile.currentOccupant)
        {
            //Check for enemy
            Enemy enemy = tile.currentOccupant.GetComponent<Enemy>();
            if (enemy)
            {
                enemy.SetCurrentRoom(this);
                enemies.Add(enemy);
                return;
            }

            //Check for data vault
            if (tile.currentOccupant.GetComponent<DataVault>())
            {
                hasMainTerminal = true;
            }

            //Check for security terminal
            if (tile.currentOccupant.GetComponent<SecurityTerminal>())
            {
                hasMiniterminal = true;
            }
        }
    }

    private void CheckForEntrance(Tile tile, int xDirection, int yDirection)
    {
        Tile entranceOrigin = grid.GetTileRelative(tile.transform, xDirection, yDirection);
        bool locked = false;

        //Check if entrance origin exists
        if (!entranceOrigin)
            return;

        //Check if entrance origin contains wall
        if (entranceOrigin.currentOccupant)
        {
            Wall wall = entranceOrigin.currentOccupant.GetComponent<Wall>();
            if(wall)
            {
                //Door
                if (wall.isDoor) locked = true;
                //Non-door
                else return;
            }
        }

        //Look for adjacent tiles with walls
        Tile wallTile1 = grid.GetTileRelative(entranceOrigin.transform, yDirection, xDirection);
        Tile wallTile2 = grid.GetTileRelative(entranceOrigin.transform, -yDirection, -xDirection);

        //Check if adjacent tiles exist
        if (!wallTile1) return;
        if (!wallTile2) return;

        //Check if adjacent tiles have walls
        if (!wallTile1.currentOccupant || !wallTile1.currentOccupant.GetComponent<Wall>()) return;
        if (!wallTile2.currentOccupant || !wallTile2.currentOccupant.GetComponent<Wall>()) return;

        //Check if adjacent tiles' walls are scenery
        if (wallTile1.currentOccupant.GetComponent<Wall>().isScenery) return;
        if (wallTile2.currentOccupant.GetComponent<Wall>().isScenery) return;

        //Get entrance tiles
        Tile entranceTile1 = grid.GetTileRelative(entranceOrigin.transform, xDirection, yDirection);
        Tile entranceTile2 = grid.GetTileRelative(entranceTile1.transform, yDirection, xDirection);
        Tile entranceTile3 = grid.GetTileRelative(entranceTile1.transform, -yDirection, -xDirection);

        //Locked
        if(locked)
        {
            //Add entrance tiles to this room's locked entrances list
            lockedEntrances.Add(entranceTile1);
            lockedEntrances.Add(entranceTile2);
            lockedEntrances.Add(entranceTile3);
        }
        //Not locked
        else
        {
            //Set room to enter
            entranceOrigin.roomToEnter = this;
            entranceTile1.roomToEnter = this;
            entranceTile2.roomToEnter = this;
            entranceTile3.roomToEnter = this;

            //Add entrance tiles to this room's entrances list
            entrances.Add(entranceTile1);
            entrances.Add(entranceTile2);
            entrances.Add(entranceTile3);
        }
    }

    private void InstantiateIcons()
    {
        //Move text and icons up
        if (hasMainTerminal || hasMiniterminal)
            transform.parent.transform.position += new Vector3(0f, 0.5f, 0f);

        if (hasMainTerminal && !hasMiniterminal)
        {
            Instantiate(gameManager.terminalIconPrefab, icons.transform.position, Quaternion.identity, icons);
        }
        else if (!hasMainTerminal && hasMiniterminal)
        {
            Instantiate(gameManager.miniterminalIconPrefab, icons.transform.position, Quaternion.identity, icons);
        }
        else if (hasMainTerminal && hasMiniterminal)
        {
            Instantiate(gameManager.terminalIconPrefab, icons.transform.position - iconOffset, Quaternion.identity, icons);
            Instantiate(gameManager.miniterminalIconPrefab, icons.transform.position + iconOffset, Quaternion.identity, icons);
        }
    }

    public bool EntranceIsCurrentSelection()
    {
        for (int i = 0; i < entrances.Count; ++i)
        {
            if (entrances[i].gameObject == player.currentSelection)
                return true;
        }

        return false;
    }

    public void UnlockEntrances()
    {
        for (int i = 0; i < lockedEntrances.Count; ++i)
        {
            //Set room to enter
            lockedEntrances[i].roomToEnter = this;

            //Add entrance tiles to this room's entrances list
            entrances.Add(lockedEntrances[i]);
        }
    }
}
