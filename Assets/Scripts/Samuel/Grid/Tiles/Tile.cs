﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Tile.cs
// Author : Samuel Schimmel
// Purpose : controls functions of tiles on grid includes what VFX appear on tile.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[RequireComponent(typeof(BoxCollider2D))] //Required for current selection detection
public class Tile : MonoBehaviour
{
    //Move availability
    public bool availableMove { get; private set; }
    public SpriteRenderer topEdgeSpriteRenderer { get; private set; }
    public SpriteRenderer bottomEdgeSpriteRenderer { get; private set; }
    public SpriteRenderer leftEdgeSpriteRenderer { get; private set; }
    public SpriteRenderer rightEdgeSpriteRenderer { get; private set; }
    public SpriteRenderer topLeftCornerSpriteRenderer { get; private set; }
    public SpriteRenderer topRightCornerSpriteRenderer { get; private set; }
    public SpriteRenderer bottomLeftCornerSpriteRenderer { get; private set; }
    public SpriteRenderer bottomRightCornerSpriteRenderer { get; private set; }

    public Vector2 gridPosition;
    public TileOccupant currentOccupant;
    public int movesFromPlayer;
    public int f, g, h;
    public Tile parentTile;

    private BoxCollider boxCollider;
    private Vector3 defaultPosition;
    private Vector3 selectedPosition;
    private Player player;
    private Grid grid { get; set; }
    public RoomCollider room { get; set; }
    public RoomCollider roomToEnter { get; set; }
    public SpriteRenderer availableMovesSpriteRenderer { get; private set; }
    public SpriteRenderer lineOfSightHatchingSpriteRenderer { get; private set; }
    public SpriteRenderer readyForPlayerInputSpriteRenderer { get; private set; }
    public SpriteRenderer selectionFeedbackSpriteRenderer { get; private set; }
    public SpriteRenderer nextEnemyMoveSpriteRenderer { get; private set; }
    public SelectableObject selectableObject { get; private set; }

    //Triggers
    public Exit exit { get; private set; }
    public EnemySpawner enemySpawner { get; private set; }

    //Line of sight
    public List<Enemy> enemiesWatchingTile { get; set; }

    //Selection
    private const float SELECTION_FEEDBACK_ALPHA = 0.5f;

    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        defaultPosition = transform.position;
        selectedPosition = defaultPosition;
        selectedPosition.y -= 0.1f;
        selectableObject = GetComponent<SelectableObject>();
        player = GameObject.Find("Player").GetComponent<Player>();
        enemySpawner = GetComponent<EnemySpawner>();
        enemiesWatchingTile = new List<Enemy>();
        grid = FindObjectOfType<Grid>();

        availableMovesSpriteRenderer = transform.FindChild("AvailableMoves").GetComponent<SpriteRenderer>();
        lineOfSightHatchingSpriteRenderer = transform.FindChild("LineOfSightHatching").GetComponent<SpriteRenderer>();
        readyForPlayerInputSpriteRenderer = transform.FindChild("CurrentPlayerTileFeedback").GetComponent<SpriteRenderer>();
        selectionFeedbackSpriteRenderer = transform.FindChild("SelectionFeedback").GetComponent<SpriteRenderer>();
        nextEnemyMoveSpriteRenderer = transform.FindChild("NextEnemyMove").GetComponent<SpriteRenderer>();

        //Edges and corners
        Transform availableMovesEdgesAndCorners = transform.FindChild("AvailableMovesEdgesAndCorners");
        topEdgeSpriteRenderer = availableMovesEdgesAndCorners.FindChild("TopEdge").GetComponent<SpriteRenderer>();
        bottomEdgeSpriteRenderer = availableMovesEdgesAndCorners.FindChild("BottomEdge").GetComponent<SpriteRenderer>();
        leftEdgeSpriteRenderer = availableMovesEdgesAndCorners.FindChild("LeftEdge").GetComponent<SpriteRenderer>();
        rightEdgeSpriteRenderer = availableMovesEdgesAndCorners.FindChild("RightEdge").GetComponent<SpriteRenderer>();
        topLeftCornerSpriteRenderer = availableMovesEdgesAndCorners.FindChild("TopLeftCorner").GetComponent<SpriteRenderer>();
        topRightCornerSpriteRenderer = availableMovesEdgesAndCorners.FindChild("TopRightCorner").GetComponent<SpriteRenderer>();
        bottomLeftCornerSpriteRenderer = availableMovesEdgesAndCorners.FindChild("BottomLeftCorner").GetComponent<SpriteRenderer>();
        bottomRightCornerSpriteRenderer = availableMovesEdgesAndCorners.FindChild("BottomRightCorner").GetComponent<SpriteRenderer>();

        if (Application.isEditor && !Application.isPlaying) transform.FindChild("EditorSprite").GetComponent<SpriteRenderer>().enabled = true;
        else transform.FindChild("EditorSprite").GetComponent<SpriteRenderer>().enabled = false;
    }

    void Start()
    {
        exit = GetComponent<Exit>();
    }

    public void TileUpdate()
    {
        SetHatching();
        SetSelectionFeedback();
        SetCurrentPlayerTileFeedback();
    }

    void SetSelectionFeedback()
    {
        Color color = selectionFeedbackSpriteRenderer.color;
        color.a = 0f;
        selectionFeedbackSpriteRenderer.color = color;

        if (!player.readyForPlayerInput) return;

        if (!selectableObject.isCurrentSelection) return;

        if (!IsSelectable()) return;

        color.a = SELECTION_FEEDBACK_ALPHA;
        selectionFeedbackSpriteRenderer.color = color;
    }

    public bool IsSelectable()
    {
        //No occupant
        if (!currentOccupant) return true;

        //Supports interaction from any range
        if (SupportsInteraction(true)) return true;

        //Has a door
        Wall wall = currentOccupant.GetComponent<Wall>();
        if (wall && wall.isDoor) return true;

        return false;
    }

    void SetHatching()
    {
        lineOfSightHatchingSpriteRenderer.enabled = false;

        //Tile is in enemy line of sight, tile is unoccupied, and it's the current selection
        if (InEnemyLineOfSight() && !currentOccupant && selectableObject.isCurrentSelection)
            lineOfSightHatchingSpriteRenderer.enabled = true;
    }

    void SetCurrentPlayerTileFeedback()
    {
        if (currentOccupant == player && player.readyForPlayerInput)
            readyForPlayerInputSpriteRenderer.enabled = true;
        else
            readyForPlayerInputSpriteRenderer.enabled = false;
    }

    public void SetNextEnemyMove(bool visible, Quaternion rotation)
    {
        nextEnemyMoveSpriteRenderer.enabled = visible;
        nextEnemyMoveSpriteRenderer.transform.rotation = rotation;
    }

    public bool InEnemyLineOfSight()
    {
        if (enemiesWatchingTile.Count > 0) return true;

        return false;
    }

    public bool SupportsInteraction(bool anyRange)
    {
        //No occupant
        if (!currentOccupant) return false;

        //Enemy
        Enemy enemy = currentOccupant.GetComponent<Enemy>();
        if (enemy)
        {
            if (enemy.CanTalk(anyRange)) return true;

            if (enemy.CanPredict()) return true;
        }

        //Terminal
        Terminal terminal = currentOccupant.GetComponent<Terminal>();
        if (terminal)
        {
            if (terminal.CanUse(anyRange)) return true;
        }

        return false;
    }

    public void SetMoveAvailability(bool available)
    {
        availableMove = available;
        availableMovesSpriteRenderer.enabled = available;
    }

    public void ResetAvailableMovesEdges()
    {
        leftEdgeSpriteRenderer.enabled = false;
        rightEdgeSpriteRenderer.enabled = false;
        topEdgeSpriteRenderer.enabled = false;
        bottomEdgeSpriteRenderer.enabled = false;

        topLeftCornerSpriteRenderer.enabled = false;
        topRightCornerSpriteRenderer.enabled = false;
        bottomLeftCornerSpriteRenderer.enabled = false;
        bottomRightCornerSpriteRenderer.enabled = false;
}

    public void SetAvailableMovesEdges()
    {
        Tile t = null;

        t = grid.GetTileRelative(transform, -1, 0);
        if (t != player.currentTile && !player.availableMoves.Contains(t))
            leftEdgeSpriteRenderer.enabled = true;

        t = grid.GetTileRelative(transform, 1, 0);
        if (t != player.currentTile && !player.availableMoves.Contains(t))
            rightEdgeSpriteRenderer.enabled = true;

        t = grid.GetTileRelative(transform, 0, 1);
        if (t != player.currentTile && !player.availableMoves.Contains(t))
            topEdgeSpriteRenderer.enabled = true;

        t = grid.GetTileRelative(transform, 0, -1);
        if (t != player.currentTile && !player.availableMoves.Contains(t))
            bottomEdgeSpriteRenderer.enabled = true;

        if (topEdgeSpriteRenderer.enabled && leftEdgeSpriteRenderer.enabled) topLeftCornerSpriteRenderer.enabled = true;
        if (topEdgeSpriteRenderer.enabled && rightEdgeSpriteRenderer.enabled) topRightCornerSpriteRenderer.enabled = true;
        if (bottomEdgeSpriteRenderer.enabled && leftEdgeSpriteRenderer.enabled) bottomLeftCornerSpriteRenderer.enabled = true;
        if (bottomEdgeSpriteRenderer.enabled && rightEdgeSpriteRenderer.enabled) bottomRightCornerSpriteRenderer.enabled = true;

        t = grid.GetTileRelative(transform, 1, 1);
        if (t && !t.availableMove && player.currentTile != t) topRightCornerSpriteRenderer.enabled = true;

        t = grid.GetTileRelative(transform, 1, -1);
        if (t && !t.availableMove && player.currentTile != t) bottomRightCornerSpriteRenderer.enabled = true;

        t = grid.GetTileRelative(transform, -1, -1);
        if (t && !t.availableMove && player.currentTile != t) bottomLeftCornerSpriteRenderer.enabled = true;

        t = grid.GetTileRelative(transform, -1, 1);
        if (t && !t.availableMove && player.currentTile != t) topLeftCornerSpriteRenderer.enabled = true;
    }

    public bool CheckForConfrontation()
    {
        //Check if in an enemy's line of sight
        if (enemiesWatchingTile.Count == 0) return false;

        //Check if occupied by player
        if (currentOccupant != player) return false;

        Enemy enemy = enemiesWatchingTile[0];
        player.StartConfrontation(enemy.confrontationTextAsset, enemy, enemy.currentTile);
        return true;
    }
}
