﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : InteractionIcon.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InteractionIcon : InteractionFeedbackElement
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Set(Tile tile)
    {
        if (!tile)
        {
            image.enabled = false;
            return;
        }

        image.enabled = true;
    }
}
