﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : InteractionText.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InteractionText : InteractionFeedbackElement
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Set(Tile tile)
    {
        //Reset all interaction feedback elements
        interactionFeedback.interactionIcon.Set(null);
        textMeshProUGUI.text = "";

        if (!tile)
        {
            textMeshProUGUI.enabled = false;
            return;
        }

        textMeshProUGUI.enabled = true;
        bool canInteract = false;

        //Enemy
        Enemy enemy = tile.currentOccupant.GetComponent<Enemy>();
        if(enemy)
        {
            if (enemy.CanPredict())
            {
                if (player.remainingMoves >= GameManager.predictEnemyCost)
                {
                    textMeshProUGUI.text = "PREDICT\n COSTS " + GameManager.predictEnemyCost + " ACTIONS";
                    canInteract = true;
                }
                else
                {
                    textMeshProUGUI.text = "INSUFFICIENT ACTIONS";
                }
            }
            else if (enemy.CanTalk(true))
            {
                if (enemy.CanTalk(false))
                {
                    textMeshProUGUI.text = "TALK";
                    canInteract = true;
                }
                else
                {
                    textMeshProUGUI.text = "GET CLOSER TO TALK";
                }
            }
        }

        //Terminal
        Terminal terminal = tile.currentOccupant.GetComponent<Terminal>();
        if(terminal)
        {
            if (terminal.CanUse(true))
            {
                if (terminal.CanUse(false))
                {
                    if (terminal.terminalType == TerminalTypes.Email) textMeshProUGUI.text = "EMAIL";
                    else textMeshProUGUI.text = "HACK\n COSTS 0 ACTIONS";

                    canInteract = true;
                }
                else
                {
                    textMeshProUGUI.text = "GET CLOSER TO USE";
                }
            }
        }

        if(canInteract) interactionFeedback.interactionIcon.Set(tile);
    }
}
