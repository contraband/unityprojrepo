﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : InteractionFeedbackElement.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InteractionFeedbackElement : MonoBehaviour
{
    public Player player;
    public TextMeshProUGUI textMeshProUGUI;
    public Image image;
    public InteractionFeedback interactionFeedback;
    public GameManager gameManager;
    public LevelManager levelManager { get; set; }

    protected virtual void Awake()
    {
        player = FindObjectOfType<Player>();
        levelManager = FindObjectOfType<LevelManager>();
        textMeshProUGUI = GetComponent<TextMeshProUGUI>();
        image = GetComponent<Image>();

        if (image) image.enabled = false;
        if (textMeshProUGUI) textMeshProUGUI.enabled = false;
    }

    protected virtual void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    protected virtual void Update()
    {
    }

    public virtual void Set(Tile tile)
    {
    }
}