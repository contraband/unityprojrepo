﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : InteractionFeedback.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionFeedback : MonoBehaviour
{
    private Player player;
    public InteractionText interactionText;
    public InteractionIcon interactionIcon;

    void Awake()
    {
        player = FindObjectOfType<Player>();

        interactionText = transform.GetChild(0).GetChild(0).GetComponentInChildren<InteractionText>();
        interactionIcon = transform.GetChild(0).GetChild(0).GetComponentInChildren<InteractionIcon>();

        interactionText.interactionFeedback = this;
        interactionIcon.interactionFeedback = this;
    }

	void Start ()
    {
		
	}

	void Update ()
    {
        Tile tile = GetSelectedTile();

        interactionText.Set(tile);
    }

    private Tile GetSelectedTile()
    {
        SelectableObject selectableObject = transform.parent.GetComponent<TileOccupant>().currentTile.GetComponent<SelectableObject>();

        //Check if ready for player input
        if (!player.readyForPlayerInput) return null;

        //Check if player has a current selection
        if (!player.currentSelection) return null;

        //Check if player's current selection is a tile
        Tile tile = player.currentSelection.GetComponent<Tile>();
        if (!tile) return null;

        //Check if player's currently selected tile has an occupant
        if (!tile.currentOccupant) return null;

        //Check if player's current selection is this tile occupant's tile
        if (tile.gameObject != selectableObject.gameObject) return null;

        return tile;
    }
}
