﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Wall.cs
// Author : Samuel Schimmel
// Purpose : tag for a wall component
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class Wall : TileOccupant
{
    public bool isScenery;
    public bool isDoor;
    public bool isOpen { get; set; }

    private bool doorInitialized;
    private float doorSize = 0.2f;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    void Update()
    {
        InitializeDoor();
    }

    private void InitializeDoor()
    {
        if (doorInitialized) return;
        if (!isDoor) return;

        doorInitialized = true;

        spriteRenderer.transform.localScale = new Vector3(doorSize, doorSize, doorSize);
        spriteRenderer.transform.localPosition = new Vector3(-doorSize, doorSize, 0f);

        Animator animator = spriteRenderer.gameObject.AddComponent<Animator>();

        string pathStart = "Animations/Doors/";
        string pathEnd = null;
        if (DoorIsVertical()) pathEnd = "Side/Side";
        else pathEnd = "Front/Front";

        animator.runtimeAnimatorController = Resources.Load(pathStart + pathEnd) as RuntimeAnimatorController;
    }

    public void OpenDoor()
    {
        spriteRenderer.GetComponent<Animator>().SetTrigger("opening");
        isOpen = true;
    }

    private bool DoorIsVertical()
    {
        Tile aboveTile = grid.GetTileRelative(transform, 0, 1);
        if (!aboveTile) return false;

        Tile belowTile = grid.GetTileRelative(transform, 0, -1);
        if (!belowTile) return false;

        Wall aboveWall = null;
        if (aboveTile.currentOccupant) aboveWall = aboveTile.currentOccupant.GetComponent<Wall>();
        if (!aboveWall) return false;

        Wall belowWall = null;
        if (belowTile.currentOccupant) belowWall = belowTile.currentOccupant.GetComponent<Wall>();
        if (!belowWall) return false;

        return true;
    }
}
