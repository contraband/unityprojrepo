﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Enemy.cs
// Author : Samuel Schimmel
// Purpose : This script handles the movementBehavior of NPC logic/movement
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum EnemyMovementBehaviors { Stationary, Patrol, RotateClockwise, RotateCounterclockwise, FollowPlayer };
public enum EnemyInteractionBehaviors { Confronts, Kills, Friendly };
public enum EnemyClasses { Coworker, LightSecurity, HeavySecurity, Camera, NPC };

public class Enemy : Actor
{
    //Set in inspector
    public EnemyClasses enemyClass;
    public EnemyMovementBehaviors movementBehavior = EnemyMovementBehaviors.Stationary;
    public EnemyInteractionBehaviors interactionBehavior = EnemyInteractionBehaviors.Confronts;
    public Vector2[] PatrolNodes;
    public TextAsset confrontationTextAsset;

    public Tile currentPatrolNode { get; private set; }
    public int currentPatrolNodeIndex { get; private set; }
    public RoomCollider currentRoom { get; private set; }
    public bool confrontedPlayer { get; private set; }

    public const float DEFAULT_TURN_TIME = 0.5f;
    public const float ACCELERATED_TURN_TIME = 0.2f;
    private Tile[] patrolNodes;
    private Coroutine rotateCoroutine;
    private bool isRotating;
    private float turnTime;
    public bool predicted { get; set; }

    //Active
    public bool previouslyActive { get; private set; }

    //Vision
    public const float DEFAULT_VISION_ANGLE = 30f;
    Camera visionCamera;
    public float currentVisionAngle { get; set; }
    const float REDUCED_VISION_ANGLE = 1f;
    const float MIN_VISION_RANGE = 1f;
    const float CIRCLE_CAST_RADIUS = 0.5f;
    public List<Tile> tilesInLineOfSight { get; set; }
    public ConeOfVision coneOfVision { get; private set; }
    public const float MAX_VISION_RANGE = 4f;

    protected override void Awake()
    {
        base.Awake();

        currentRoom = null;
        turnTime = DEFAULT_TURN_TIME;
        player = FindObjectOfType<Player>();
        tilesInLineOfSight = new List<Tile>();

        if(enemyClass == EnemyClasses.HeavySecurity) maxMoves = GameManager.maxHeavySecurityActions;
        else maxMoves = GameManager.maxEnemyActions;
    }

    protected override void Start()
    {
        base.Start();

        if(!justSpawned) GetPatrolNodes(PatrolNodes);

        if (movementBehavior == EnemyMovementBehaviors.FollowPlayer) FacePosition(player.transform.position);

        if (interactionBehavior == EnemyInteractionBehaviors.Confronts)
        {
            coneOfVision = Instantiate(gameManager.actorVisionPrefab, transform.position, Quaternion.identity, boxCollider2D.transform).GetComponent<ConeOfVision>();
            visionCamera = coneOfVision.GetComponent<Camera>();
            currentVisionAngle = DEFAULT_VISION_ANGLE;
            ReinitializeVisionCamera();
            GetLineOfSight();
        }

        icon = Instantiate(gameManager.speechIconPrefab, transform.position, Quaternion.identity, transform).GetComponent<ActorIcon>();
    }

    public void GetPatrolNodes(Vector2[] inputArray)
    {
        //Patrolling enemy
        if (movementBehavior != EnemyMovementBehaviors.Patrol) return;

        patrolNodes = new Tile[inputArray.Length];

        for (int i = 0; i < patrolNodes.Length; ++i)
        {
            patrolNodes[i] = grid.GetTileAbsolute((int)inputArray[i].x, (int)inputArray[i].y);
        }

        currentPatrolNodeIndex = patrolNodes.Length;
        SetNextPatrolNode();
    }

    public override void ActorReset()
    {
        base.ActorReset();
    }

    public override void ActorUpdate()
    {
        base.ActorUpdate();

        //Detect if enemy has just gone active or inactive
        EnemyJustWentActive();
        EnemyJustWentInactive();

        SetNextMoveFeedback();

        //Set previously active
        previouslyActive = IsActive();
    }

    protected override void GetCurrentTile()
    {
        base.GetCurrentTile();
    }

    protected override void TurnUpdate()
    {
        base.TurnUpdate();

        //Get line of sight
        if(IsActive()) GetLineOfSight();
    }

    public override void TurnStarted(bool instant)
    {
        base.TurnStarted(instant);

        predicted = false;

        //Patrol
        if (movementBehavior == EnemyMovementBehaviors.Patrol)
        {
            Patrol(instant);
        }
        //Rotate
        else if (movementBehavior == EnemyMovementBehaviors.RotateClockwise || movementBehavior == EnemyMovementBehaviors.RotateCounterclockwise)
        {
            if (instant) RotateInstantly();
            else rotateCoroutine = StartCoroutine(Rotate());
        }
        //Follow player
        else if (movementBehavior == EnemyMovementBehaviors.FollowPlayer)
        {
            FollowPlayer(instant);
        }
        //Stationary
        else if (movementBehavior == EnemyMovementBehaviors.Stationary)
        {
            if (instant) MoveEnded(null, false);
            else StartCoroutine(Wait());
        }
    }

    private void Patrol(bool instant)
    {
        //Attempt to move to current patrol node
        if (MoveToTile(currentPatrolNode, null, instant)) return;

        //Attempt to move near current patrol node
        if (MoveToTile(grid.GetTileRelative(currentPatrolNode.transform, -1, 1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(currentPatrolNode.transform, 0, 1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(currentPatrolNode.transform, 1, 1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(currentPatrolNode.transform, 1, 0), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(currentPatrolNode.transform, 1, -1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(currentPatrolNode.transform, 0, -1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(currentPatrolNode.transform, -1, -1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(currentPatrolNode.transform, -1, 0), null, instant)) return;

        //If all move attempts fail
        //End turn
        if (instant)
        {
            MoveEnded(null, true);
        }
        //Wait
        else
        {
            StartCoroutine(Wait());
        }
    }

    private void FollowPlayer(bool instant)
    {
        //If this enemy kills the player, and the enemy is not hidden, attempt to move to player's current tile
        if (interactionBehavior == EnemyInteractionBehaviors.Kills && !instant)
            if (MoveToTile(player.currentTile, player, false)) return;

        //Attempt to move near player's current tile
        if (MoveToTile(grid.GetTileRelative(player.currentTile.transform, -1, 1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(player.currentTile.transform, 0, 1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(player.currentTile.transform, 1, 1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(player.currentTile.transform, 1, 0), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(player.currentTile.transform, 1, -1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(player.currentTile.transform, 0, -1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(player.currentTile.transform, -1, -1), null, instant)) return;
        if (MoveToTile(grid.GetTileRelative(player.currentTile.transform, -1, 0), null, instant)) return;

        //Attempt move to or near player's previous tile
        if (player.previousTile)
        {
            if (MoveToTile(player.previousTile, null, instant)) return;
            if (MoveToTile(grid.GetTileRelative(player.previousTile.transform, -1, 1), null, instant)) return;
            if (MoveToTile(grid.GetTileRelative(player.previousTile.transform, 0, 1), null, instant)) return;
            if (MoveToTile(grid.GetTileRelative(player.previousTile.transform, 1, 1), null, instant)) return;
            if (MoveToTile(grid.GetTileRelative(player.previousTile.transform, 1, 0), null, instant)) return;
            if (MoveToTile(grid.GetTileRelative(player.previousTile.transform, 1, -1), null, instant)) return;
            if (MoveToTile(grid.GetTileRelative(player.previousTile.transform, 0, -1), null, instant)) return;
            if (MoveToTile(grid.GetTileRelative(player.previousTile.transform, -1, -1), null, instant)) return;
            if (MoveToTile(grid.GetTileRelative(player.previousTile.transform, -1, 0), null, instant)) return;
        }

        //If all move attempts fail
        FacePosition(player.transform.position);
        //End turn
        if (instant)
        {
            MoveEnded(null, true);
        }
        //Wait
        else
        {
            StartCoroutine(Wait());
        }
    }

    private IEnumerator Rotate()
    {
        isRotating = true;

        StartCoroutine(InterpolateFOV(REDUCED_VISION_ANGLE));
        audioObject.PlayOnce(gameManager.cameraRotationClip);

        float elapsedTime = 0f;

        Quaternion originalRotation = boxCollider2D.transform.rotation;
        Quaternion goalRotation = originalRotation;
        if (movementBehavior == EnemyMovementBehaviors.RotateClockwise)
            goalRotation *= Quaternion.Euler(0, 0, -90);
        else if (movementBehavior == EnemyMovementBehaviors.RotateCounterclockwise)
            goalRotation *= Quaternion.Euler(0, 0, 90);

        while (elapsedTime < turnTime)
        {
            Quaternion q = Quaternion.Lerp(originalRotation, goalRotation, (elapsedTime / turnTime));
            FaceAngle(q.eulerAngles.z);

            CheckForConfrontation();

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        FaceAngle(goalRotation.eulerAngles.z);

        StartCoroutine(InterpolateFOV(DEFAULT_VISION_ANGLE));

        isRotating = false;
    }

    private void RotateInstantly()
    {
        Quaternion originalRotation = boxCollider2D.transform.rotation;
        Quaternion goalRotation = originalRotation;
        if (movementBehavior == EnemyMovementBehaviors.RotateClockwise)
            goalRotation *= Quaternion.Euler(0, 0, -90);
        else if (movementBehavior == EnemyMovementBehaviors.RotateCounterclockwise)
            goalRotation *= Quaternion.Euler(0, 0, 90);

        FaceAngle(goalRotation.eulerAngles.z);

        MoveEnded(null, false);
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(turnTime);

        MoveEnded(null, false);
    }

    public override void TurnEnded()
    {
        base.TurnEnded();
    }

    protected override void MoveStarted()
    {
        base.MoveStarted();
    }

    protected override void MoveEnded(TileOccupant acceptableCollision, bool instant)
    {
        if (!isThisActorsTurn) return;

        base.MoveEnded(acceptableCollision, instant);

        if (movementBehavior != EnemyMovementBehaviors.RotateClockwise
            && movementBehavior != EnemyMovementBehaviors.RotateCounterclockwise
            && movementBehavior != EnemyMovementBehaviors.Stationary)
        {
            audioObject.PlayOnce(gameManager.walkClip, true);
        }

        if (confrontedPlayer) return;

        if (!isMoving)
        {
            actors.NextTurn();

            //Update next patrol node
            if (currentTile == currentPatrolNode) SetNextPatrolNode();

            //Face player if following them
            if (movementBehavior == EnemyMovementBehaviors.FollowPlayer) FacePosition(player.transform.position);
        }
    }

    protected override bool CheckForConfrontation()
    {
        for (int i = 0; i < tilesInLineOfSight.Count; ++i)
        {
            Tile tile = tilesInLineOfSight[i];

            if (!tile.currentOccupant) continue;

            if (tile.currentOccupant == player)
            {
                player.StartConfrontation(confrontationTextAsset, this, currentTile);
                return true;
            }
        }

        return false;
    }

    private Tile GetNextPatrolNode()
    {
        int index = currentPatrolNodeIndex + 1;

        if (index >= patrolNodes.Length)
            index = 0;

        return patrolNodes[index];
    }

    private void SetNextPatrolNode()
    {
        if (movementBehavior != EnemyMovementBehaviors.Patrol) return;

        ++currentPatrolNodeIndex;

        if (currentPatrolNodeIndex >= patrolNodes.Length)
            currentPatrolNodeIndex = 0;

        currentPatrolNode = patrolNodes[currentPatrolNodeIndex];
    }

    private void CleanUpLineOfSight()
    {
        //Loop through all tiles in this enemy's line of sight
        for (int i = 0; i < tilesInLineOfSight.Count; ++i)
        {
            //Remove this enemy from that tile's list of enemies
            //watching it
            tilesInLineOfSight[i].enemiesWatchingTile.Remove(this);
        }

        tilesInLineOfSight.Clear();
    }

    private void GetLineOfSight()
    {
        //Clean up line of sight
        CleanUpLineOfSight();

        //Skip inactive enemies
        if (!IsActive())
            return;

        //Skip enemies that do not confront
        if (interactionBehavior != EnemyInteractionBehaviors.Confronts)
            return;

        //Reinitialize vision camera
        ReinitializeVisionCamera();

        //Get all colliders in range
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, MAX_VISION_RANGE * 2);

        //Calculate vision camera frustrum planes
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(visionCamera);

        //Loop through all colliders in range
        for (int i = 0; i < colliders.Length; ++i)
        {
            //Test if colliders are in enemy FOV
            if (GeometryUtility.TestPlanesAABB(planes, colliders[i].bounds))
            {
                //Test if colliders are blocked by obstacles
                List<RaycastHit2D> obstacles;
                float distance = (colliders[i].transform.position - boxCollider2D.transform.position).magnitude;
                Vector3 direction = (colliders[i].transform.position - boxCollider2D.transform.position).normalized;

                obstacles = Physics2D.CircleCastAll(boxCollider2D.transform.position, CIRCLE_CAST_RADIUS, direction, distance).ToList();
                for (int j = 0; j < obstacles.Count; ++j)
                {
                    //Walls
                    Transform colliderTransform = obstacles[j].collider.transform;
                    if (colliderTransform && colliderTransform.parent && colliderTransform.parent.GetComponent<Wall>())
                    {
                        goto Outer;
                    }
                }

                //Tiles
                TileRaycastDetector tileRaycastDetector = colliders[i].gameObject.GetComponent<TileRaycastDetector>();
                if (tileRaycastDetector)
                {
                    Tile tile = tileRaycastDetector.transform.parent.GetComponent<Tile>();

                    //Ignore current tile
                    if (tile == currentTile)
                        continue;

                    //Ignore adjacent tiles if necessary
                    if (size == 2)
                    {
                        Tile right = grid.GetTileRelative(currentTile.transform, 1, 0);
                        Tile down = grid.GetTileRelative(currentTile.transform, 0, -1);
                        Tile corner = grid.GetTileRelative(currentTile.transform, 1, -1);

                        if (tile == right || tile == down || tile == corner) continue;
                    }

                    //Add tile to list of tiles in line of sight
                    tile.enemiesWatchingTile.Add(this);
                    tilesInLineOfSight.Add(tile);
                }
            }
        Outer: continue;
        }

        UpdateLineLengths();
    }

    public void UpdateLineLengths()
    {
        if (coneOfVision) coneOfVision.UpdateLineLengths();
    }

    void SetNextMoveFeedback()
    {
        if(predictionLine) predictionLine.positionCount = 0;

        if (!player.readyForPlayerInput) return; //Return if not ready for player input
        if (!predicted) return; //Return if enemy is not predicted
        if (!currentTile.selectableObject.isCurrentSelection) return; //Return if enemy's current tile is not currently selected

        Tile lineTarget = null;

        //Patrolling enemies
        if (movementBehavior == EnemyMovementBehaviors.Patrol)
        {
            PathInfo pathInfo = pathfinder.FindPath(currentTile, currentPatrolNode, this);

            Vector3 vector = Vector3.zero;
            if (pathInfo.tiles.Count == 1) vector = currentPatrolNode.transform.position - transform.position;
            else vector = currentPatrolNode.transform.position - pathInfo.tiles[1].transform.position;

            float angle = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

            currentPatrolNode.SetNextEnemyMove(true, q);
            lineTarget = currentPatrolNode;
        }
        //Stationary enemies
        else if (movementBehavior == EnemyMovementBehaviors.Stationary)
        {
            currentTile.SetNextEnemyMove(true, boxCollider2D.transform.rotation);
        }
        //Rotate clockwise enemies
        else if (movementBehavior == EnemyMovementBehaviors.RotateClockwise)
        {
            Vector3 rotation = boxCollider2D.transform.rotation.eulerAngles;
            rotation.z -= 90f;
            currentTile.SetNextEnemyMove(true, Quaternion.Euler(rotation));
        }
        //Rotate counterclockwise enemies
        else if (movementBehavior == EnemyMovementBehaviors.RotateCounterclockwise)
        {
            Vector3 rotation = boxCollider2D.transform.rotation.eulerAngles;
            rotation.z += 90f;
            currentTile.SetNextEnemyMove(true, Quaternion.Euler(rotation));
        }
        //Following enemies
        else if (movementBehavior == EnemyMovementBehaviors.FollowPlayer)
        {
            Vector3 v = player.transform.position - transform.position;
            float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

            player.currentTile.SetNextEnemyMove(true, q);

            lineTarget = player.currentTile;
        }

        DrawPredictionLine(lineTarget);
    }

    public override void Accelerate()
    {
        base.Accelerate();

        turnTime = ACCELERATED_TURN_TIME;
    }

    public override void Decelerate()
    {
        base.Decelerate();

        turnTime = DEFAULT_TURN_TIME;
    }

    public void SetCurrentRoom(RoomCollider room)
    {
        currentRoom = room;
    }

    public bool CanTalk(bool anyRange)
    {
        //Check if enemy is friendly
        if (interactionBehavior != EnemyInteractionBehaviors.Friendly) return false;

        //Check if enemy is in interaction range
        if (!anyRange && !InPlayerInteractionRange()) return false;

        //Check if player has already talked to player
        if (confrontedPlayer) return false;

        //Check if alarm has been raised
        if (player.alarmRaised) return false;

        return true;
    }

    public bool CanPredict()
    {
        //Check if in safe area
        if (levelManager.levelType == LevelTypes.Safe) return false;

        //Return false if enemy is friendly
        if (interactionBehavior == EnemyInteractionBehaviors.Friendly) return false;

        //Return false if enemy has confronted player
        if (confrontedPlayer) return false;

        //Return false if alarm is active and enemy does not kill
        if (player.alarmRaised && interactionBehavior != EnemyInteractionBehaviors.Kills) return false;

        //Check if enemy has already been predicted
        if (predicted) return false;

        return true;
    }

    public override bool IsActive()
    {
        //Return false if enemy is friendly
        if (interactionBehavior == EnemyInteractionBehaviors.Friendly) return false;

        //Return false if enemy has confronted player
        if (confrontedPlayer) return false;

        //Return false if alarm is active and enemy does not kill
        if (player.alarmRaised && interactionBehavior != EnemyInteractionBehaviors.Kills) return false;

        //Return false if enemy has just spawned
        if (justSpawned) return false;

        return true;
    }

    public bool EnemyJustWentActive()
    {
        //The enemy just went active
        if (IsActive() && !previouslyActive)
        {
            if (coneOfVision) coneOfVision.Fade(true);

            return true;
        }

        return false;
    }

    public bool EnemyJustWentInactive()
    {
        //The enemy just went inactive
        if (!IsActive() && previouslyActive)
        {
            CleanUpLineOfSight();
            if (coneOfVision) coneOfVision.Fade(false);

            return true;
        }

        return false;
    }

    private void ReinitializeVisionCamera()
    {
        visionCamera.nearClipPlane = MIN_VISION_RANGE;
        visionCamera.farClipPlane = MAX_VISION_RANGE;
        visionCamera.fieldOfView = currentVisionAngle;
        visionCamera.depth = -100;

        //Rotate camera
        visionCamera.transform.localRotation = Quaternion.LookRotation(new Vector3(1, 0, 0));
    }

    private IEnumerator InterpolateFOV(float endFOV)
    {
        float elapsedTime = 0f;
        float startFOV = currentVisionAngle;

        while (elapsedTime < turnTime)
        {
            float t = elapsedTime / turnTime;
            currentVisionAngle = Mathf.SmoothStep(startFOV, endFOV, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        //End turn
        if (endFOV == DEFAULT_VISION_ANGLE)
        {
            MoveEnded(null, false);
        }
    }

    public void StartConfrontation()
    {
        confrontedPlayer = true;
        icon.Toggle(true);
        Stop();

        //Stop rotating
        if (isRotating)
        {
            StopCoroutine(rotateCoroutine);
            isRotating = false;
        }

        //Face player
        FacePosition(player.transform.position);
    }

    public void EndConfrontation()
    {
        Despawn();

        audioObject.PlayOnce(gameManager.actorDespawnClip);
    }
}