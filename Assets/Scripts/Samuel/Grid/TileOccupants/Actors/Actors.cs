﻿
// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Actors.cs
// Author : Samuel Schimmel
// Purpose : Manages all actors.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
public class Actors : MonoBehaviour
{
    public delegate void NewPhaseDelegate(bool playerPhase);
    public event NewPhaseDelegate onNewPhase;
    public bool accelerated { get; private set; }
    public List<Actor> actors { get; set; }
    private Player player;
    private MainCamera mainCamera;
    private GameManager gameManager;
    private Grid grid;
    private bool startingEnemyPhase = false;
    public List<EnemySpawner> enemySpawners { get; set; }
    public LevelManager levelManager { get; set; }

    public Actor currentActor { get; private set; }
    public Actor nextActor { get; private set; }

    void Awake()
    {
        actors = new List<Actor>();
        player = FindObjectOfType<Player>();
        grid = FindObjectOfType<Grid>();
        enemySpawners = new List<EnemySpawner>();
        levelManager = FindObjectOfType<LevelManager>();
    }

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        mainCamera = FindObjectOfType<MainCamera>();
        GetEnemySpawners();

        for (int i = 0; i < actors.Count; ++i)
        {
            actors[i].justSpawned = false;
        }
    }

    public void NextTurn()
    {
        //Phase
        startingEnemyPhase = false;

        if (player.isThisActorsTurn) startingEnemyPhase = true;

        //Go to next actor
        EndTurnOfCurrentActor();
    }

    private void EndTurnOfCurrentActor()
    {
        if(currentActor) currentActor.TurnEnded();

        player.remainingActionsUI.Toggle(false);

        //Ending player turn and phase?
        if (player.isThisActorsTurn)
        {
            //Designate all enemies as not having just spawned
            EnableSpawnedEnemies();

            //Attempt an enemy spawn,
            //and return if it is successful
            if (AttemptEnemySpawn()) return;
        }

        GoToNextActor();
    }

    public void GoToNextActor()
    {
        if (!nextActor)
        {
            nextActor = GetNextActor();
            currentActor = null;
        }

        //Starting enemy turn (and possibly phase)
        Enemy enemy = nextActor.GetComponent<Enemy>();
        if (enemy)
        {
            //Starting enemy phase?
            if (startingEnemyPhase)
            {
                onNewPhase(false);
                player.playerCanAccelerate = true;
                mainCamera.audioObject.PlayOnce(gameManager.enemyPhaseStartedClip);
                if (player.alarmRaised) levelManager.DispatchMessage("SECURITY PHASE");
                else levelManager.DispatchMessage("COWORKER PHASE");
            }
        }
        //Starting player turn and phase
        else
        {
            onNewPhase(true);
            Decelerate();

            string message = "EVELYN'S TURN";
            string color = "<color=#A92929FF>";
            if (player.remainingHP > 0 && levelManager.levelType == LevelTypes.Stealth)
                message += "\n" + color + "+" + Mathf.Abs(GameManager.startTurnSuspicion).ToString() + "</color>" + " SUSPICION";
            levelManager.DispatchMessage(message);
        }

        //View actor
        mainCamera.onCameraMoveEnd += nextActor.CameraMoveEnded;
        mainCamera.ViewPosition(nextActor.transform.position, true);
    }

    public Actor GetNextActor()
    {
        int indexOfCurrentActor = actors.IndexOf(currentActor);
        Actor newActor;

        do
        {
            ++indexOfCurrentActor;
            if (indexOfCurrentActor >= actors.Count) indexOfCurrentActor = 0;
            newActor = actors[indexOfCurrentActor];
        }
        while (!newActor.IsActive());

        return newActor;
    }

    public void Accelerate()
    {
        player.playerCanAccelerate = false;
        accelerated = true;
        mainCamera.audioObject.PlayOnce(gameManager.turnAccelerationClip);
        mainCamera.Accelerate();

        for(int i = 0; i < actors.Count; ++i)
        {
            if (actors[i].GetComponent<Player>()) continue;

            actors[i].Accelerate();
        }
    }

    public void Decelerate()
    {
        accelerated = false;

        mainCamera.Decelerate();

        for (int i = 0; i < actors.Count; ++i)
        {
            if (actors[i].GetComponent<Player>()) continue;

            actors[i].Decelerate();
        }
    }

    public void StartPlayerTurn()
    {
        if (currentActor) currentActor.TurnEnded();
        player.TurnStarted();
        Decelerate();
    }

    public void NewCurrentActor(Actor newActor)
    {
        currentActor = newActor;
        nextActor = null;
    }

    public void ActorsUpdate()
    {
        //Reset actors
        for (int i = 0; i < actors.Count; ++i)
        {
            actors[i].ActorReset();
        }

        //Update actors
        for (int i = 0; i < actors.Count; ++i)
        {
            actors[i].ActorUpdate();
        }
    }

    public void AddActor(Actor actor)
    {
        actors.Add(actor);
    }

    public void RemoveActor(Actor actorToRemove)
    {
        if (actorToRemove == currentActor)
        {
            nextActor = GetNextActor();
            currentActor = null;
        }

        actors.Remove(actorToRemove);
    }

    public void DespawnInactiveEnemies()
    {
        for(int i = 0; i < actors.Count; ++i)
        {
            Enemy enemy = actors[i].GetComponent<Enemy>();

            if (!enemy) continue; //Skip the player
            if (enemy.interactionBehavior == EnemyInteractionBehaviors.Kills) continue; //Skip enemies that kill

            enemy.Despawn();
        }
    }

    public int NumberOfActiveEnemies()
    {
        int activeEnemies = 0;
        
        //Loop through all actors
        for (int i = 0; i < actors.Count; ++i)
        {
            Enemy enemy = actors[i].GetComponent<Enemy>();

            if (!enemy) continue; //Skip the player
            if (!enemy.IsActive()) continue; //Skip inactive enemies

            ++activeEnemies;
        }

        return activeEnemies;
    }

    public int NumberOfEnemies()
    {
        int activeEnemies = 0;

        //Loop through all actors
        for (int i = 0; i < actors.Count; ++i)
        {
            Enemy enemy = actors[i].GetComponent<Enemy>();

            if (!enemy) continue; //Skip the player

            ++activeEnemies;
        }

        return activeEnemies;
    }

    public int NumberOfPredictedEnemies()
    {
        int numberOfPredictedEnemies = 0;

        for (int i = 0; i < actors.Count; ++i)
        {
            Enemy enemy = actors[i].GetComponent<Enemy>();

            if (!enemy) continue; //Skip the player

            if (enemy.predicted) ++numberOfPredictedEnemies;
        }

        return numberOfPredictedEnemies;
    }

    public void GetEnemySpawners()
    {
        enemySpawners = FindObjectsOfType<EnemySpawner>().ToList<EnemySpawner>();
    }

    //Returns true if enemy was spawned
    public bool AttemptEnemySpawn()
    {
        //Check if there are spawners in the level
        if (enemySpawners.Count == 0) return false;

        //Alarm has been raised
        if (player.alarmRaised)
        {
            //Sort spawners by distance to player
            enemySpawners = enemySpawners.OrderBy(spawner => Vector3.SqrMagnitude(player.transform.position - spawner.transform.position)).ToList<EnemySpawner>();

            //Attempt to spawn an alarm enemy, and return the result
            return SpawnAlarmEnemy();
        }
        //Alarm has not been raised
        else
        {
            bool enemySpawned = false;

            for (int i = 0; i < enemySpawners.Count; ++i)
            {
                if (enemySpawners[i].AttemptEnemySpawn())
                {
                    enemySpawned = true;
                    break;
                }
            }

            return enemySpawned;
        }
    }

    //Returns true if enemy was spawned
    private bool SpawnAlarmEnemy()
    {
        //Loop through enemy spawners
        for (int i = 0; i < enemySpawners.Count; ++i)
        {
            EnemySpawner enemySpawner = enemySpawners[i];

            //Check if enemySpawner tile is already occupied
            if (enemySpawner.tile.currentOccupant) continue;

            //Spawn the alarm enemy
            Enemy enemy = Instantiate(gameManager.spawnEnemyPrefab, enemySpawner.transform.position, Quaternion.identity, transform).GetComponent<Enemy>();

            //Prepare enemy for spawn
            enemy.PrepareForSpawn();

            //Audio
            enemy.audioObject.PlayOnce(gameManager.heavySecuritySpawnClip);

            //Spawn successful
            return true;
        }

        //Spawn not successful
        return false;    
    }

    public void EnableSpawnedEnemies()
    {
        //Loop through all actors
        for (int i = 0; i < actors.Count; ++i)
        {
            actors[i].justSpawned = false;
        }
    }
}