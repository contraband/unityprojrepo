﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Actor.cs
// Author : Samuel Schimmel
// Purpose : This script deals with the move logic of a single NPC
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

abstract public class Actor : TileOccupant
{
    //Set in inspector
    public Sprite[] sprites;
    public float initialAngle;

    public int maxMoves { get; set; }
    public int size { get; set; }
    public delegate void ActorFadeDelegate();
    public event ActorFadeDelegate onActorFadeEnd;
    public bool isThisActorsTurn { get { return actors.currentActor == this; } }
    public bool isMoving { get; private set; }
    public int remainingMoves { get; protected set; }
    public Tile destination { get; set; }
    public const int ORTHOGONAL_MOVES_COST = 1;
    public const int DIAGONAL_MOVES_COST = 2;
    public const float DEFAULT_MOVEMENT_SPEED = 3f;
    public const float ACCELERATED_MOVEMENT_SPEED = 10f;
    protected Actors actors;
    protected Movement movement;
    protected Pathfinder pathfinder;
    protected Tile nextTileInPath;
    protected MainCamera mainCamera;
    protected HUD hud;
    protected HUDCamera hudCamera { get; set; }
    public bool justSpawned { get; set; }
    public int turnsTaken { get; set; }

    //Fading
    public Coroutine fadeCoroutine { get; set; }
    public bool fading { get; private set; }
    public const float fadeTime = 1f;

    //Icon
    public ActorIcon icon { get; set; }

    //Prediction
    public LineRenderer predictionLine { get; set; }
    public List<LineRenderer> highlightedPredictionLines { get; set; }

    protected override void Awake()
    {
        base.Awake();

        highlightedPredictionLines = new List<LineRenderer>();
        movement = gameObject.AddComponent<Movement>();
        actors = transform.parent.GetComponent<Actors>();
        pathfinder = gameObject.AddComponent<Pathfinder>();
        player = FindObjectOfType<Player>();
        movement.SetSpeed(DEFAULT_MOVEMENT_SPEED);
        boxCollider2D.size /= 4f;
    }

    protected override void Start()
    {
        base.Start();

        remainingMoves = maxMoves;
        mainCamera = FindObjectOfType<MainCamera>();
        hudCamera = FindObjectOfType<HUDCamera>();
        actors.AddActor(this);

        //Prediction line
        predictionLine = Instantiate(gameManager.predictionLinePrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<LineRenderer>();
        predictionLine.sortingLayerName = "PredictionLines";

        destination = currentTile;
        FaceAngle(initialAngle);
    }

    public void CameraMoveEnded()
    {
        mainCamera.onCameraMoveEnd -= CameraMoveEnded;

        //If enemy didn't just spawn, start their turn,
        //otherwise spawn them
        if (!justSpawned) TurnStarted();
        else Spawn();
    }

    public virtual void ActorReset()
    {

    }

    public virtual void ActorUpdate()
    {
        base.TileOccupantUpdate();

        if (hud == null)
            hud = FindObjectOfType<HUD>();

        if (isThisActorsTurn)
            TurnUpdate();
    }

    protected override void GetCurrentTile()
    {
        base.GetCurrentTile();
    }

    protected virtual void TurnUpdate()
    {
    }

    //Returns true if successful
    public bool MoveToTile(Tile destination_, TileOccupant acceptableCollision, bool instant)
    {        
        //Attempting to move to actor's own tile
        if (destination_ == currentTile)
            return false;

        //Move attempt
        PathInfo pathInfo = pathfinder.FindPath(currentTile, destination_, this, acceptableCollision);

        //Move attempt failed
        if (pathInfo == null)
        {
            return false;
        }
        //Move attempt successful
        else
        {            
            //Store destination and next tile in path
            destination = destination_;
            nextTileInPath = pathInfo.nextTileInPath;
            movement.onMoveStart += MoveStarted;
            movement.Move(nextTileInPath.transform.position, acceptableCollision, instant);
            FacePosition(nextTileInPath.transform.position);

            //Non-instant movement
            if (!instant) mainCamera.followTarget = destination.transform;

            return true;
        }
    }

    public void Stop()
    {
        destination = currentTile;
        movement.Stop();
    }

    protected void DrawPredictionLine(Tile lineTarget, bool highlightWatchedTiles = false)
    {
        //No target
        if (!lineTarget) return;

        PathInfo pathInfo = pathfinder.FindPath(currentTile, lineTarget, this, player);
        Vector3 midpoint = Vector3.zero;

        //No path
        if (pathInfo == null) return;

        //Get positions
        Vector3[] positions = new Vector3[pathInfo.tiles.Count + 1];
        for (int i = 0; i < pathInfo.tiles.Count; ++i)
        {
            if (i == 0)
            {
                if(pathInfo.tiles.Count == 1) midpoint = (pathInfo.tiles[0].transform.position + currentTile.transform.position) / 2f;
                else midpoint = (pathInfo.tiles[0].transform.position + pathInfo.tiles[1].transform.position) / 2f;

                positions[i] = midpoint;
            }
            else
            {
                positions[i] = pathInfo.tiles[i].transform.position;
            }

            positions[i].z = -float.Epsilon;
        }

        //Set last position to midpoint between this actor's tile and the first tile
        midpoint = (pathInfo.tiles[pathInfo.tiles.Count - 1].transform.position + currentTile.transform.position) / 2f;
        positions[pathInfo.tiles.Count] = midpoint;
        positions[pathInfo.tiles.Count].z = -float.Epsilon;

        //Set positions
        predictionLine.positionCount = positions.Length;
        predictionLine.SetPositions(positions);

        //Highlight watched tiles
        if(highlightWatchedTiles)
        {
            for (int i = 0; i < pathInfo.tiles.Count; ++i)
            {
                //Ignore tiles not watched by enemies, and tiles currently selected
                if (pathInfo.tiles[i].enemiesWatchingTile.Count < 1) continue;
                if (pathInfo.tiles[i].gameObject == player.currentSelection) continue;

                //Instantiate new prediction line and add to list
                LineRenderer highlightedPredictionLine = Instantiate(gameManager.highlightedPredictionLinePrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<LineRenderer>();
                highlightedPredictionLine.sortingLayerName = "HighlightedPredictionLines";
                highlightedPredictionLines.Add(highlightedPredictionLine);

                //Get positions
                int NUMBER_OF_POSITIONS = 3;
                Vector3[] highlightedPositions = new Vector3[NUMBER_OF_POSITIONS];
                Vector3 highlightedMidpoint = Vector3.zero;

                highlightedMidpoint = (pathInfo.tiles[i - 1].transform.position + pathInfo.tiles[i].transform.position) / 2f;
                highlightedPositions[0] = highlightedMidpoint;

                highlightedMidpoint = pathInfo.tiles[i].transform.position;
                highlightedPositions[1] = highlightedMidpoint;

                Tile t = null;
                if (i == pathInfo.tiles.Count - 1) t = currentTile;
                else t = pathInfo.tiles[i + 1];
                highlightedMidpoint = (pathInfo.tiles[i].transform.position + t.transform.position) / 2f;
                highlightedPositions[2] = highlightedMidpoint;

                //Set positions
                highlightedPredictionLine.positionCount = NUMBER_OF_POSITIONS;
                highlightedPredictionLine.SetPositions(highlightedPositions);
            }
        }
    }

    protected void CleanupHighlightedPredictionLines()
    {
        for(int i = 0; i < highlightedPredictionLines.Count; ++i)
        {
            Destroy(highlightedPredictionLines[i].gameObject);
        }

        highlightedPredictionLines.Clear();
    }

    protected void FacePosition(Vector3 positionToFace)
    {
        Vector3 vectorToTarget = positionToFace - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        FaceAngle(angle);
    }

    protected void FaceAngle(float angle)
    {
        int interval = 45;
        int roundedAngle = ((int)(angle / interval)) * interval;

        //Down
        if (roundedAngle == 270 || roundedAngle == -90) spriteRenderer.sprite = sprites[0];
        //Down right
        else if (roundedAngle == 315 || roundedAngle == -45) spriteRenderer.sprite = sprites[1];
        //Right
        else if (roundedAngle == 0) spriteRenderer.sprite = sprites[2];
        //Up right
        else if (roundedAngle == 45 || roundedAngle == -315) spriteRenderer.sprite = sprites[3];
        //Up
        else if (roundedAngle == 90 || roundedAngle == -270) spriteRenderer.sprite = sprites[4];
        //Up left
        else if (roundedAngle == 135 || roundedAngle == 225) spriteRenderer.sprite = sprites[5];
        //Left
        else if (roundedAngle == 180 || roundedAngle == -180) spriteRenderer.sprite = sprites[6];
        //Down left
        else if (roundedAngle == 225 || roundedAngle == -135) spriteRenderer.sprite = sprites[7];

        boxCollider2D.transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    protected virtual void MoveStarted()
    {
        movement.onMoveStart -= MoveStarted;
        movement.onMoveEnd += MoveEnded;

        isMoving = true;
    }

    protected virtual void MoveEnded(TileOccupant acceptableCollision, bool instant)
    {
        if (LevelManager.currentGameState != GameStates.Infiltration) return;

        if (this != player || levelManager.levelType != LevelTypes.Safe)
        {
            if (grid.TilesAreOrthogonal(currentTile, nextTileInPath)) remainingMoves -= ORTHOGONAL_MOVES_COST;
            else remainingMoves -= DIAGONAL_MOVES_COST;
        }

        movement.onMoveEnd -= MoveEnded;
        GetCurrentTile();
        CheckForConfrontation();
        if(this == player) player.TriggerCutscene();

        //At destination
        if (currentTile == destination)
        {
            mainCamera.followTarget = null;
            isMoving = false;
        }
        //Keep moving
        else if(remainingMoves > 0)
        {
            MoveToTile(destination, acceptableCollision, instant);
            isMoving = true;
        }
        else
        {
            mainCamera.followTarget = null;
            isMoving = false;
        }
    }

    protected abstract bool CheckForConfrontation();

    public virtual void TurnStarted(bool instant = false)
    {
        actors.NewCurrentActor(this);
    }

    public virtual void TurnEnded()
    {
        RestoreRemainingMoves();
        ++turnsTaken;
    }

    public virtual void Accelerate()
    {
        movement.SetSpeed(ACCELERATED_MOVEMENT_SPEED);
    }

    public virtual void Decelerate()
    {
        movement.SetSpeed(DEFAULT_MOVEMENT_SPEED);
    }

    public virtual void RestoreRemainingMoves()
    {
        remainingMoves = maxMoves;
    }

    public abstract bool IsActive();

    public void Fade(float endAlpha, float fadeTime)
    {
        if (fading)
        {
            StopCoroutine(fadeCoroutine);
            fading = false;
        }

        fadeCoroutine = StartCoroutine(FadeCoroutine(endAlpha, fadeTime));
    }

    private IEnumerator FadeCoroutine(float endAlpha, float fadeTime)
    {
        fading = true;

        float startAlpha = spriteRenderer.color.a;
        float elapsedTime = 0f;

        while (elapsedTime < fadeTime)
        {
            float a = Mathf.SmoothStep(startAlpha, endAlpha, (elapsedTime / fadeTime));
            Color newColor = spriteRenderer.color;
            newColor.a = a;
            spriteRenderer.color = newColor;

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (onActorFadeEnd != null)
            onActorFadeEnd();

        fading = false;
    }

    public void SetAlpha(float endAlpha)
    {
        Color newColor = spriteRenderer.color;
        newColor.a = endAlpha;
        spriteRenderer.color = newColor;
    }

    public void PrepareForSpawn()
    {
        player.playerCanAccelerate = false;

        //Set just spawned
        justSpawned = true;
        
        //Change name
        name = name.Replace("(Clone)", "");

        //Message
        string message = name.ToUpper() + "\nHAS ARRIVED";
        if (name.ToUpper() != "HEAVY SECURITY") message = "LIGHT SECURITY " + message;
        levelManager.DispatchMessage(message);

        //Set alpha to 0
        Invoke("MakeNewEnemyInvisible", float.Epsilon);

        //View transform
        mainCamera = FindObjectOfType<MainCamera>();
        mainCamera.onCameraMoveEnd += CameraMoveEnded;
        mainCamera.ViewPosition(transform.position, true);
    }

    private void MakeNewEnemyInvisible()
    {
        SetAlpha(0f);
    }

    public void Spawn()
    {
        //Fade in
        Fade(1f, fadeTime);

        //Subscribe to fade
        onActorFadeEnd += SpawnFinished;
    }

    public void SpawnFinished()
    {
        //Unsubscribe from fade
        onActorFadeEnd -= SpawnFinished;

        //Go to next actor
        actors.GoToNextActor();
    }

    public void Despawn()
    {
        icon.Toggle(false);

        Fade(0f, fadeTime);

        onActorFadeEnd += DespawnFinished;
    }

    public void DespawnFinished()
    {
        onActorFadeEnd -= DespawnFinished;

        actors.RemoveActor(this);

        Destroy(gameObject);
    }
}

//private void UpdateNearbyEnemyLineLengths()
//{
//    if (!isMoving) return;

//    //Loop through all colliders in range
//    Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, Enemy.MAX_VISION_RANGE);
//    for (int i = 0; i < colliders.Length; ++i)
//    {
//        Enemy enemy = colliders[i].transform.parent.GetComponent<Enemy>();
//        if (enemy) enemy.UpdateLineLengths();
//    }
//}