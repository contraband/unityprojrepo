﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ConeOfVision.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeOfVision : MonoBehaviour
{
    private Coroutine fadeCoroutine;
    private bool fading { get; set; }
    private List<LineRenderer> lines { get; set; }
    private const int NUMBER_OF_LINES = 200;
    private const float MAX_WIDTH = 0.01f;
    private const float VISION_ANIMATION_WAIT_TIME = 0.01f;
    private GameManager gameManager;
    private Enemy enemy;
    private int indexOfCurrentLine = 0;
    private int indexDelta = 1;
    private LineRenderer currentLine;
    private LineRenderer[] previousLines;
    private float currentWidth;

    private void Awake()
    {
        enemy = transform.parent.parent.GetComponent<Enemy>();
    }

    private void Start()
    {
    }

    public void Fade(bool fadeIn)
    {
        if (fadeIn)
            Fade(MAX_WIDTH);
        else
            Fade(0f);
    }

    public void UpdateLineLengths()
    {
        //Initialize lines
        if (lines == null)
        {
            gameManager = FindObjectOfType<GameManager>();
            lines = new List<LineRenderer>();
            for (int i = 0; i < NUMBER_OF_LINES; ++i)
            {
                LineRenderer line = Instantiate(gameManager.coneOfVisionLinePrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<LineRenderer>();
                line.sortingLayerName = "ConeOfVisionLines";
                lines.Add(line);
            }

            currentLine = lines[indexOfCurrentLine];
            previousLines = new LineRenderer[NUMBER_OF_LINES];
            StartCoroutine(Animate());
        }

        //Enemy is watching tiles or fading
        if(enemy.tilesInLineOfSight.Count > 0 || fading)
        {
            for (int i = 0; i < lines.Count; ++i)
            {
                //Enable line renderer
                lines[i].enabled = true;

                //Set position 0
                Vector3 offset = new Vector3(0.25f, 0f, 0f);
                Vector3 v0 = transform.position;
                v0 += enemy.boxCollider2D.transform.TransformDirection(offset);
                v0.z = -float.Epsilon;
                lines[i].SetPosition(0, v0);

                //Set position 1
                Vector3 v1 = new Vector3();
                float angle = enemy.currentVisionAngle * 2 * ((float)i / lines.Count);
                angle += enemy.boxCollider2D.transform.rotation.eulerAngles.z;
                angle -= enemy.currentVisionAngle;
                v1 = Helpers.DegreeToVector2(angle);

                RaycastHit2D[] hit = Physics2D.RaycastAll(v0, Helpers.DegreeToVector2(angle), Enemy.MAX_VISION_RANGE);
                float hitDistance = 0f;
                for (int j = 0; j < hit.Length; ++j)
                {
                    if (hit[j].transform.GetComponentInParent<Wall>())
                    {
                        hitDistance = hit[j].distance;
                        break;
                    }
                }

                //Hit wall
                if (hitDistance != 0f) v1 *= hitDistance;
                //Did not hit wall
                else v1 *= Enemy.MAX_VISION_RANGE;

                v1 += v0;

                lines[i].SetPosition(1, v1);
            }
        }
        else
        {
            for (int i = 0; i < lines.Count; ++i)
            {
                lines[i].enabled = false;
            }
        }
        
    }

    private IEnumerator Animate()
    {
        while (true)
        {
            //Update previous lines
            previousLines[NUMBER_OF_LINES - 1] = currentLine;
            for (int i = 0; i < NUMBER_OF_LINES; ++i)
            {
                int index = Mathf.Clamp(i + 1, 0, NUMBER_OF_LINES - 1);
                previousLines[i] = previousLines[index];
                float percentage = (float)i / NUMBER_OF_LINES;
                if (previousLines[i]) previousLines[i].widthMultiplier = currentWidth * percentage;
            }

            //Get current line
            currentLine = lines[indexOfCurrentLine];

            //Set current line width to max
            currentLine.widthMultiplier = currentWidth;

            //Increment current line index
            indexOfCurrentLine += indexDelta;
            if (indexOfCurrentLine >= lines.Count - 1 || indexOfCurrentLine < 1)
                indexDelta *= -1;

            //Calculate wait time
            float waitTime = VISION_ANIMATION_WAIT_TIME;
            yield return new WaitForSeconds(waitTime);
        }
    }

    private void Fade(float endWidth)
    {
        if (fading)
        {
            StopCoroutine(fadeCoroutine);
            fading = false;
        }

        UpdateLineLengths();

        fadeCoroutine = StartCoroutine(InterpolateFade(endWidth));
    }

    private IEnumerator InterpolateFade(float endWidth)
    {
        fading = true;

        float elapsedTime = 0f;
        float totalTime = RoomCollider.FOW_FADE_TIME;
        float startWidth = lines[0].widthMultiplier;

        while (elapsedTime < totalTime)
        {
            float t = elapsedTime / totalTime;
            currentWidth = Mathf.SmoothStep(startWidth, endWidth, t);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        fading = false;
    }
}
