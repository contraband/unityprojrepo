﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : Controls all logic and movementBehavior for player including movement,
//			 health, inetraction with enemies, and audio effects pertaining to
//			 the player.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Player : Actor
{
    public bool debug { get; private set; }
    public GameObject currentSelection { get; private set; }
    public GameObject previousSelection { get; private set; }
    public int remainingHP { get; private set; }
    public int remainingMovesAfterMove { get; private set; }
    public bool readyForPlayerInput { get; set; }
    public CutsceneTrigger nextCutsceneTrigger { get; set; }
    public bool playerCanAccelerate { get; set; }
    public bool inAutoPlay { get; private set; }
    public bool inConfrontation { get; set; }
    public bool canExit { get; set; }
    public bool alarmRaised { get; private set; }
    public int suspicionLevel { get { return MAX_HP - remainingHP; } }
    public const float INTERACTION_RANGE = 2f;
    private const float AUTO_END_TURN_DELAY = 1f;
    private RemainingHP remainingHPui;
    public RemainingActions remainingActionsUI;
    private LetterboxCanvas letterboxCanvas;
    public const int MAX_HP = 100;
    private bool firstTurn = true;
    public bool inCutscene { get; set; }
    private bool godMode { get; set; }

    //Terminals
    public HackingResults hackingResult { get; set; }
    public bool dataVaultHacked { get; set; }
    public bool emailsRead { get; set; }
    public Terminal currentTerminal { get; set; }

    //Movement
    public List<Tile> availableMoves { get; private set; }

    //Dialogue
    public Tile nextTileAfterDialogue { get; set; }
    private ConfrontationInterface confrontationInterface;
    public Enemy confrontingEnemy { get; set; }

    //Cameras
    private ResetView resetView;

    //No input timer
    public float noInputTimer { get; set; }
    public const float noInputTime = 15f;

    protected override void Awake()
    {
        base.Awake();

        maxMoves = GameManager.maxPlayerActions;
        availableMoves = new List<Tile>();

        //Checkpoint
        if (GameManager.playerCheckpointPosition != Vector2.zero)
            transform.position = GameManager.playerCheckpointPosition;
        if (GameManager.playerCheckpointHP == 0) remainingHP = MAX_HP;
        else remainingHP = GameManager.playerCheckpointHP - GameManager.startTurnSuspicion;
        dataVaultHacked = GameManager.dataVaultHackedCheckpoint;

        //Can exit by default if no data vault or email terminal
        if (!FindObjectOfType<DataVault>() && !FindObjectOfType<EmailTerminal>()) canExit = true;
    }

    protected override void Start()
    {
        confrontationInterface = FindObjectOfType<ConfrontationInterface>();
        letterboxCanvas = FindObjectOfType<LetterboxCanvas>();
        resetView = FindObjectOfType<ResetView>();

        base.Start();

        icon = Instantiate(gameManager.exclamationIconPrefab, transform.position, Quaternion.identity, transform).GetComponent<ActorIcon>();
    }

    public override void ActorReset()
    {
        base.ActorReset();
    }

    public override void ActorUpdate()
    {
        predictionLine.positionCount = 0;
        CleanupHighlightedPredictionLines();
        base.ActorUpdate();
        GetInput();
        Cheats();
        noInputTimer += Time.deltaTime;

        if (!remainingHPui)
            remainingHPui = FindObjectOfType<RemainingHP>();

        if (!remainingActionsUI)
            remainingActionsUI = FindObjectOfType<RemainingActions>();
    }

    protected override void GetCurrentTile()
    {
        base.GetCurrentTile();

        //New tile is a level exit
        if (currentTile.exit)
        {
            currentTile.exit.ExitAttempt();
        }

        //New tile is an entrance for a room
        if (currentTile.roomToEnter)
        {
            currentTile.roomToEnter.EnterRoom();
        }

        //Check if player is leaving a room
        if (previousTile && previousTile.roomToEnter)
        {
            if (currentTile.room != previousTile.roomToEnter //New tile is outside room
                && currentTile.roomToEnter != previousTile.roomToEnter) //New tile is not another entrance to the same room
            {
                previousTile.roomToEnter.ExitRoom();
            }
        }
    }

    protected override void TurnUpdate()
    {
        base.TurnUpdate();
        remainingActionsUI.ShowActionCost(maxMoves);
        GetCurrentSelection();
    }

    public override void TurnStarted(bool instant = false)
    {
        base.TurnStarted();

        noInputTimer = 0f;

        //Change suspicion
        if (levelManager.levelType == LevelTypes.Stealth)
        {
            if (turnsTaken == 0) ChangeHP(-GameManager.previousLevelSuspicion);
            else ChangeHP(-GameManager.startTurnSuspicion);
        }

        GetPossibleMoves();
        UpdateHUD();
        if(levelManager.levelType == LevelTypes.Stealth) audioObject.PlayOnce(gameManager.playerTurnStartedClip);

        //Checkpoint
        if (firstTurn)
        {
            if (dataVaultHacked) DataVaultHacked();
            firstTurn = false;
        }

        TriggerCutscene();
    }

    public override void TurnEnded()
    {
        base.TurnEnded();

        CleanUpPossibleMoves();
    }

    protected override void MoveStarted()
    {
        base.MoveStarted();

        CleanUpPossibleMoves();

        resetView.Hide();

        FindObjectOfType<Controls>().Toggle(false);

        //Remove messages waiting for player action
        Message[] messages = FindObjectsOfType(typeof(Message)) as Message[];
        foreach(Message message in messages)
        {
            if (message.waitForPlayerAction)
                message.RemoveMessage();
        }

    }

    protected override void MoveEnded(TileOccupant acceptableCollision, bool instant)
    {
        base.MoveEnded(acceptableCollision, instant);

        audioObject.PlayOnce(gameManager.walkClip, true);

        if (!isMoving) UpdateHUD();

        if (!isMoving && !inConfrontation)
        {
            //Out of moves
            if (remainingMoves < 1)
            {
                audioObject.PlayOnce(gameManager.outOfMovesClip);

                readyForPlayerInput = false;
            }

            if (LevelManager.currentGameState == GameStates.Infiltration)
            {
                GetPossibleMoves();
                letterboxCanvas.ToggleVisibility(false);

                //Show suspicion meter
                FindObjectOfType<RemainingHP>().Transition(true);
            }
        }
    }

    public bool TriggerCutscene()
    {
        if (nextCutsceneTrigger)
        {
            nextCutsceneTrigger.ActivateTrigger();
            nextCutsceneTrigger = null;
            return true;
        }

        return false;
    }

    protected override bool CheckForConfrontation()
    {
        return currentTile.CheckForConfrontation();
    }

    public void CleanUpPossibleMoves()
    {
        currentTile.ResetAvailableMovesEdges();
        for (int i = 0; i < availableMoves.Count; ++i)
        {
            availableMoves[i].movesFromPlayer = 0;
            availableMoves[i].SetMoveAvailability(false);
            availableMoves[i].ResetAvailableMovesEdges();
        }

        availableMoves.Clear();
    }

    public void GetPossibleMoves()
    {        
        if (levelManager.levelType == LevelTypes.Safe)
        {
            readyForPlayerInput = true;
            return;
        }

        CleanUpPossibleMoves();

        int rStart, cStart, rMax, cMax;

        //Set ranges for iterators
        int range = remainingMoves;

        rStart = (int)currentTile.gridPosition.x - range;
        cStart = (int)currentTile.gridPosition.y - range;
        rMax = (int)currentTile.gridPosition.x + range + 1;
        cMax = (int)currentTile.gridPosition.y + range + 1;

        //Clamp ranges
        rStart = Mathf.Clamp(rStart, 0, grid.height);
        cStart = Mathf.Clamp(cStart, 0, grid.width);
        rMax = Mathf.Clamp(rMax, 0, grid.width);
        cMax = Mathf.Clamp(cMax, 0, grid.height);

        //Add tiles to player's list of available moves
        for (int r = rStart; r < rMax; ++r)
        {        
            for (int c = cStart; c < cMax; ++c)
            { 
                PathInfo pathInfo = pathfinder.FindPath(destination, grid.grid[r][c], this);

                if (pathInfo != null)
                {
                    if (pathInfo.remainingMovesInPath <= remainingMoves)
                    {
                        grid.grid[r][c].movesFromPlayer = pathInfo.remainingMovesInPath;
                        grid.grid[r][c].SetMoveAvailability(true);
                        availableMoves.Add(grid.grid[r][c]);
                    }
                }
            }
        }

        //Determine each available move's available move type
        currentTile.SetAvailableMovesEdges();
        for(int i = 0; i < availableMoves.Count; ++i)
        {
            availableMoves[i].SetAvailableMovesEdges();
        }
        
        //Finished
        if (inAutoPlay)
        {
            Invoke("AutoMove", AUTO_END_TURN_DELAY);
        }
        else
        {
            readyForPlayerInput = true;
        }

        UpdateHUD();
    }

    void UpdateHUD()
    {
        if (remainingActionsUI) remainingActionsUI.Animate(remainingMoves, maxMoves);
        if (remainingHPui) remainingHPui.Animate(remainingHP, MAX_HP);
    }

    void GetCurrentSelection()
    {
        currentSelection = null;
        CustomCursor.CursorShouldAnimate = false;

        if (!readyForPlayerInput) return;

        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1)) return;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        foreach (RaycastHit2D hit in Physics2D.GetRayIntersectionAll(ray))
        {
            SelectableObject selectableObject = hit.collider.gameObject.GetComponent<SelectableObject>();
            if (selectableObject && selectableObject.enabled)
            {
                currentSelection = hit.collider.gameObject;
                break;
            }
        }

        //Player has current selection
        if (currentSelection)
        {
            //Tile
            Tile tile = currentSelection.GetComponent<Tile>();
            if (tile)
            {
                ShowPathToSelectedTile(tile);

                //Tile has an occupant
                TileOccupant tileOccupant = tile.currentOccupant;
                if (tileOccupant)
                {
                    //Enemy
                    Enemy enemy = tileOccupant.GetComponent<Enemy>();
                    if (enemy)
                        if (enemy.CanPredict())
                            remainingActionsUI.ShowActionCost(remainingMoves - GameManager.predictEnemyCost);
                }
            }
        }

        previousSelection = currentSelection;
    }

    private void ShowPathToSelectedTile(Tile selectedTile)
    {
        if (!(selectedTile.availableMove || levelManager.levelType == LevelTypes.Safe)) return;

        PathInfo pathInfo = pathfinder.FindPath(currentTile, selectedTile, this, null);
        if (pathInfo == null) return;

        remainingMovesAfterMove = remainingMoves - selectedTile.movesFromPlayer;
        remainingActionsUI.ShowActionCost(remainingMoves - selectedTile.movesFromPlayer);
        CustomCursor.CursorShouldAnimate = true;
        if (currentSelection != previousSelection && selectedTile.availableMove) audioObject.PlayOnce(gameManager.selectionClip);
        DrawPredictionLine(selectedTile, true);
    }

    private void GetInput()
    {
        if (LevelManager.currentGameState != GameStates.Infiltration)
            return;

        if (Input.GetMouseButtonDown(0)) LeftMouseButton();
        else if (Input.GetKeyDown(KeyCode.Space)) Space();
        else if (Input.GetKeyDown(KeyCode.V)) V();
    }

    private void LeftMouseButton()
    {
        if (!readyForPlayerInput) return;
        if (!currentSelection) return;

        noInputTimer = 0f;

        //Player is clicking a tile
        Tile selectedTile = currentSelection.GetComponent<Tile>();
        if (selectedTile)
        {
            //Interaction
            //Player is clicking a tile with an occupant
            if (selectedTile.currentOccupant)
            {
                //Terminal
                Terminal terminal = selectedTile.currentOccupant.GetComponent<Terminal>();
                if (terminal)
                {
                    terminal.AttemptUse();
                    return;
                }

                //Enemy
                Enemy enemy = selectedTile.currentOccupant.GetComponent<Enemy>();
                if (enemy)
                {
                    if (enemy.CanPredict())
                    {
                        PredictEnemy(enemy);
                        return;
                    }
                    else if (enemy.CanTalk(false))
                    {
                        StartConfrontation(enemy.confrontationTextAsset, enemy, enemy.currentTile);
                        return;
                    }
                }
            }

            //Movement
            //Player is in a safe area
            else if (levelManager.levelType == LevelTypes.Safe)
            {
                if (MoveToTile(selectedTile, null, false))
                {
                    readyForPlayerInput = false;
                    audioObject.PlayOnce(gameManager.clickClip);
                    return;
                }
            }
            //Player is clicking a tile they can move to
            else if (selectedTile.availableMove)
            {
                MoveToTile(selectedTile, null, false);
                readyForPlayerInput = false;
                audioObject.PlayOnce(gameManager.clickClip);
                return;
            }
           
            //Player is clicking a tile they can't interact with
            audioObject.PlayOnce(gameManager.badClickClip);
        }
    }

    public void Space()
    {
        if (levelManager.levelType == LevelTypes.Safe) return;

        noInputTimer = 0f;
        if (readyForPlayerInput)  EndPlayerTurn();
        else if (playerCanAccelerate) actors.Accelerate();
    }

    public void V()
    {
        if (!readyForPlayerInput) return;
        if (!resetView.visible) return;

        noInputTimer = 0f;
        mainCamera.onCameraMoveEnd += MainCameraReset;
        mainCamera.ViewPosition(transform.position);
        resetView.Hide();
        mainCamera.audioObject.PlayOnce(gameManager.resetViewClip);
    }

    public void MainCameraReset()
    {
        mainCamera.onCameraMoveEnd -= MainCameraReset;
        readyForPlayerInput = true;
    }

    public void StartConfrontation(TextAsset textAsset, Enemy confrontingEnemy_ = null, Tile tileToFace = null)
    {        
        //Must have text asset
        if (!textAsset) return;
        if (godMode) return;

        audioObject.PlayOnce(gameManager.dialogueStartClip);
        Stop();
        resetView.Hide();
        readyForPlayerInput = false;
        inConfrontation = true;
        playerCanAccelerate = false;
        actors.Decelerate();
        confrontationInterface.Activate(textAsset);
        icon.Toggle(true);
        confrontingEnemy = confrontingEnemy_;
        if(confrontingEnemy) confrontingEnemy.StartConfrontation();

        //Move camera
        Vector3 midpoint = Vector3.zero;
        if (confrontingEnemy_) midpoint = (transform.position + confrontingEnemy_.transform.position) / 2f;
        else midpoint = transform.position;

        //Face enemy
        if (tileToFace) FacePosition(tileToFace.transform.position);
    }

    public void EndConfrontation()
    {
        if (LevelManager.currentGameState != GameStates.Infiltration) return;

        inConfrontation = false;
        icon.Toggle(false);
        if (confrontingEnemy) confrontingEnemy.EndConfrontation();

        if (isThisActorsTurn)
        {
            if (remainingHP <= 0) RaiseAlarm();
            else if (levelManager.levelType == LevelTypes.Safe) GetPossibleMoves();
            else if (remainingMoves > 0) GetPossibleMoves();
            else readyForPlayerInput = true;
        }
        else
        {
            if (remainingHP <= 0)
            {
                RaiseAlarm();
                if (!actors.AttemptEnemySpawn()) actors.NextTurn();
            }
            else
            {
                actors.NextTurn();
            }
        }

        //Move to next tile after dialogue
        if (nextTileAfterDialogue)
        {
            MoveToTile(nextTileAfterDialogue, null, false);
            player.readyForPlayerInput = false;
        }
        nextTileAfterDialogue = null;
    }

    public void StartEmail(EmailTerminal emailTerminal)
    {
        FacePosition(emailTerminal.transform.position);
        resetView.Hide();
        currentTerminal = emailTerminal;
        levelManager.TransitionToEmailState(emailTerminal.emailTextAssets);
    }

    public void EmailsRead()
    {
        hud.onTransitionEnd -= EmailsRead;
        FindObjectOfType<CurrentObjective>().Transition(true);
        readyForPlayerInput = true;
        currentTerminal = null;
        canExit = true;
    }

    public void StartHacking(HackableTerminal hackableTerminal)
    {
        FacePosition(hackableTerminal.transform.position);
        resetView.Hide();
        currentTerminal = hackableTerminal;
        levelManager.TransitionToHackingState(hackableTerminal.levelData,
            hackableTerminal.terminalType,
            hackableTerminal.hackInterfacePrefab,
            hackableTerminal.narrativeLevelData);
    }

    public void HackingComplete()
    {
        hud.onTransitionEnd -= HackingComplete;

        if (hackingResult == HackingResults.DataVaultSuccess)
        {
            DataVaultHacked();
        }
        else if (hackingResult == HackingResults.SecurityTerminalSuccess)
        {
            ChangeHP(-GameManager.securityTerminalSuspicion);
            readyForPlayerInput = true;
            player.audioObject.PlayOnce(gameManager.suspicionDecreaseClip);
        }
        else if(hackingResult == HackingResults.Failure)
        {
            if(levelManager.levelType == LevelTypes.Stealth) ChangeHP(-GameManager.hackFailedSuspicion);
            readyForPlayerInput = true;
        }

        //Reset data
        hackingResult = HackingResults.None;
        currentTerminal = null;
    }

    private void DataVaultHacked()
    {
        dataVaultHacked = true;
        
        //Show message
        levelManager.DispatchMessage("DATA VAULT HACKED");

        //Show map overview and open doors
        mainCamera.ShowMapOverview();
        mainCamera.onCameraMoveEnd += grid.OpenDoors;

        //Save checkpoint
        gameManager.SaveCheckpoint(currentTile.gridPosition, remainingHP, true);

        //Set can exit to true
        canExit = true;
    }

    public void ChangeHP(int delta)
    {
        SetHP(remainingHP + delta);
    }

    public void SetHP(int value)
    {
        //Update HP
        remainingHP = value;

        remainingHP = Mathf.Clamp(remainingHP, 0, MAX_HP);

        UpdateHUD();

        if (remainingHP <= 0) RaiseAlarm();
    }

    public void ChangeRemainingMoves(int delta)
    {
        SetRemainingMoves(remainingMoves + delta);
    }

    public void SetRemainingMoves(int value)
    {
        remainingMoves = value;

        UpdateHUD();
    }

    private void Cheats()
    {
        if (!Input.GetKey(KeyCode.C)) return;
        if (LevelManager.currentGameState == GameStates.Hacking) return;

        if (Input.GetKeyDown(KeyCode.B)) SetHP(90);
        if (Input.GetKeyDown(KeyCode.U)) SetHP(0);
        if (Input.GetKeyDown(KeyCode.Q)) SetHP(5);
        if (Input.GetKeyDown(KeyCode.E)) ChangeHP(-5);
        if (Input.GetKeyDown(KeyCode.X)) SetRemainingMoves(0);
        if (Input.GetKeyDown(KeyCode.I)) gameManager.SaveCheckpoint(currentTile.gridPosition, remainingHP, dataVaultHacked);
        if (Input.GetKeyDown(KeyCode.R)) levelManager.RestartLevel(true); //Restart from checkpoint
        if (Input.GetKeyDown(KeyCode.F2)) levelManager.RestartLevel(false); //Restart level
        if (Input.GetKeyDown(KeyCode.O)) levelManager.EnterWinState();
        if (Input.GetKeyDown(KeyCode.L)) levelManager.EnterLossState();
        if (Input.GetKeyDown(KeyCode.M)) RestoreRemainingMoves();
        if (Input.GetKeyDown(KeyCode.P)) ToggleAutoplay();
        if (Input.GetKeyDown(KeyCode.Z)) ToggleDebug();
        if (Input.GetKeyDown(KeyCode.J)) DataVaultHacked();
        if (Input.GetKeyDown(KeyCode.N)) Debug.Break();
        if (Input.GetKeyDown(KeyCode.F)) levelManager.DispatchMessage("TEST MESSAGE SENT AT " + Time.time);
        if (Input.GetKeyDown(KeyCode.F1)) godMode = !godMode;
        if (Input.GetKeyDown(KeyCode.F4)) Application.Quit();
        if (Input.GetKeyDown(KeyCode.F5)) SceneManager.LoadScene("Outro");
        if (Input.GetKeyDown(KeyCode.Y)) Dialogue.SkipAllDialogues();
        if (Input.GetKeyDown(KeyCode.F6)) gameManager.nextSecretEmailUnlocked = true;
        if (Input.GetKeyDown(KeyCode.Alpha1)) SceneManager.LoadScene("Chapter6.4");
    }

    public void EndPlayerTurn()
    {
        if (!isThisActorsTurn) return;

        remainingMoves = 0;
        UpdateHUD();
        remainingActionsUI.ClearQueue();
        currentSelection = null;

        resetView.Hide();
        FindObjectOfType<Controls>().Toggle(false);
        readyForPlayerInput = false;
        actors.NextTurn();
    }

    private void ToggleAutoplay()
    {
        inAutoPlay = !inAutoPlay;

        if (inAutoPlay && readyForPlayerInput)
        {
            AutoMove();
            readyForPlayerInput = false;
        }
    }

    private void ToggleDebug()
    {
        debug = !debug;
    }

    private void AutoMove()
    {
        if (remainingMoves >= 2)
        {
            int xOffset = Random.Range(-1, 2);
            int yOffset = Random.Range(-1, 2);

            if (!MoveToTile(grid.GetTileRelative(transform, xOffset, yOffset), null, false))
                Invoke("EndPlayerTurn", AUTO_END_TURN_DELAY);
        }
        else
        {
            Invoke("EndPlayerTurn", AUTO_END_TURN_DELAY);
        }
    }

    private void PredictEnemy(Enemy enemy)
    {
        //Check if player has moves remaining
        if (remainingMoves < GameManager.predictEnemyCost) return;

        //Deduct moves cost
        remainingMoves -= GameManager.predictEnemyCost;

        //Set enemy's predicted flag
        enemy.predicted = true;

        //Get moves again
        GetPossibleMoves();

        //Update HUD
        UpdateHUD();

        //Play audio
        audioObject.PlayOnce(gameManager.predictionClip);
    }

    private void RaiseAlarm()
    {
        if (alarmRaised) return;
        if (inConfrontation) return;
        if (levelManager.levelType != LevelTypes.Stealth) return;

        alarmRaised = true;
        audioObject.PlayOnce(gameManager.alarmRaisedClip);
        levelManager.RaiseAlarm();
        actors.DespawnInactiveEnemies();

        SecurityTerminal[] securityTerminals = (SecurityTerminal[])GameObject.FindObjectsOfType(typeof(SecurityTerminal));
        foreach(SecurityTerminal securityTerminal in securityTerminals)
        {
            securityTerminal.locked = true;
        }

        EndPlayerTurn();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (godMode) return;

        Transform parent = collider.transform.parent;
        if (parent)
        {
            Enemy enemy = collider.transform.parent.GetComponent<Enemy>();
            if (enemy && enemy.interactionBehavior == EnemyInteractionBehaviors.Kills)
            {
                levelManager.EnterLossState();
            }
        }
    }

    public override bool IsActive() { return true; }
}