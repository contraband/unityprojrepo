﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ActorIcon.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorIcon : MonoBehaviour
{
    private readonly Vector3 OFFSET = new Vector3(0, 0.75f, 0);
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        transform.position += OFFSET;
    }

	void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public void Toggle(bool enable)
    {
        spriteRenderer.enabled = enable;
    }
}
