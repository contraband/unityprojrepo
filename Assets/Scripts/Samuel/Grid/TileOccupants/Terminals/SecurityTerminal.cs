﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : checks if player collided with a mini-terminal to start hacking
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class SecurityTerminal : HackableTerminal
{
    protected override void Awake()
    {
        base.Awake();

        terminalType = TerminalTypes.Security;
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void TerminalUpdate()
    {
        base.TerminalUpdate();
    }

    private void Update()
    {
        TerminalUpdate();
    }
}
