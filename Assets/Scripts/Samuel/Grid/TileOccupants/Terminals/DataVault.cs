﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : checks for when a player collides with a main terminal to start hacking
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class DataVault : HackableTerminal
{
    protected override void Awake()
    {
        base.Awake();

        terminalType = TerminalTypes.DataVault;
    }

    protected override void Start ()
    {
        base.Start();
	}

    protected override void TerminalUpdate()
    {
        base.TerminalUpdate();
    }

    private void Update()
    {
        TerminalUpdate();
    }
}
