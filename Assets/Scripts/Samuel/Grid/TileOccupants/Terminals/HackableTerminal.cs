﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HackingResults { None, DataVaultSuccess, SecurityTerminalSuccess, Failure }
public enum ExitHackingResults { Abort, Success, Failure }

public abstract class HackableTerminal : Terminal
{
    //Set in inspector
    public GameObject hackInterfacePrefab;

    //Const

    //Properties
    public LevelData levelData { get; set; }
    // ADDED BY GRANT 4/10/2017
    public NarrativeLevelData narrativeLevelData { get; set; }

    protected override void Awake()
    {
        base.Awake();

        levelData = GetComponent<LevelData>();
        // ADDED BY GRANT 4/10/2017
        narrativeLevelData = GetComponent<NarrativeLevelData>();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void TerminalUpdate()
    {
        base.TerminalUpdate();
    }

    public override void AttemptUse()
    {
        if (CanUse(false))
        {
            player.StartHacking(this);
        }
    }

    protected override void AnimateWaypoint()
    {
        if (!locked) base.AnimateWaypoint();
    }
}
