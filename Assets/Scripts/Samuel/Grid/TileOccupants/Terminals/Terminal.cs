﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Terminal.cs
// Author : Samuel Schimmel
// Purpose : detects when player enters terminal to begin hacking.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public enum TerminalTypes { DataVault, Security, Email };

public abstract class Terminal : TileOccupant
{
    public AnimationCurve curve;
    public bool usable = true;
    public Color selectedColor;
    public float minRange;
    public float maxRange;

    public bool used;
    public bool locked { get; set; }
    public TerminalTypes terminalType { get; set; }
    public new Light light { get; set; }
    private const float speed = 0.5f;
    private Color defaultColor;

    protected override void Awake()
    {
        base.Awake();

        light = gameObject.GetComponent<Light>();
        defaultColor = light.color;
    }

    protected override void Start ()
    {
        base.Start();
	}

    protected virtual void TerminalUpdate ()
    {
        base.TileOccupantUpdate();

        AnimateWaypoint();
	}

    public virtual void AttemptUse() { }

    public virtual bool CanUse(bool anyRange)
    {
        //Check if usable
        if (!usable) return false;

        //Check range
        if (!anyRange && !InPlayerInteractionRange()) return false;

        //Check if locked
        if (locked) return false;

        return true;
    }

    protected virtual void AnimateWaypoint()
    {
        if (LevelManager.currentGameState == GameStates.Infiltration
            || LevelManager.currentGameState == GameStates.LoadHacking
            || LevelManager.currentGameState == GameStates.LoadEmail) light.enabled = true;
        else light.enabled = false;

        if (TileIsCurrentSelection()) light.color = selectedColor;
        else light.color = defaultColor;

        light.range = Mathf.Lerp(minRange, maxRange, curve.Evaluate(Time.time * speed));
    }

    private bool TileIsCurrentSelection()
    {
        SelectableObject selectableObject = currentTile.GetComponent<SelectableObject>();

        //Check if ready for player input
        if (!player.readyForPlayerInput) return false;

        //Check if player has a current selection
        if (!player.currentSelection) return false;

        //Check if player's current selection is a tile
        Tile tile = player.currentSelection.GetComponent<Tile>();
        if (!tile) return false;

        //Check if player's currently selected tile has an occupant
        if (!tile.currentOccupant) return false;

        //Check if player's current selection is this tile occupant's tile
        if (tile.gameObject != selectableObject.gameObject) return false;

        return true;
    }
}
