﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : TileOccupant.cs
// Author : Samuel Schimmel
// Purpose : Component tag for any object that can occupy a tile.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

[SelectionBase]
abstract public class TileOccupant : MonoBehaviour
{
    public string uniqueHelpText;

    public LevelManager levelManager { get; set; }
    public Tile currentTile { get; protected set; }
    public Tile previousTile { get; protected set; }
    public Grid grid { get; private set; }
    public Player player { get; protected set; }
    public BoxCollider2D boxCollider2D { get; set; }
    public new Rigidbody2D rigidbody2D { get; set; }
    public SpriteRenderer spriteRenderer { get; set; }
    public AudioObject audioObject { get; set; }
    public GameManager gameManager { get; set; }

    protected virtual void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
        grid = FindObjectOfType<Grid>();
        player = FindObjectOfType<Player>();
        transform.FindChild("EditorSprite").GetComponent<SpriteRenderer>().enabled = false;

        rigidbody2D = gameObject.AddComponent<Rigidbody2D>();
        boxCollider2D = new GameObject("BoxCollider2D").AddComponent<BoxCollider2D>();
        boxCollider2D.isTrigger = true;
        boxCollider2D.transform.SetParent(transform, false);
        spriteRenderer = new GameObject("SpriteRenderer").AddComponent<SpriteRenderer>();
        spriteRenderer.transform.SetParent(transform, false);
        audioObject = new GameObject("AudioObject").AddComponent<AudioObject>();
        audioObject.transform.SetParent(transform, false);

        rigidbody2D.isKinematic = true;
        spriteRenderer.transform.localScale /= 4f;
        spriteRenderer.sortingLayerName = "TileOccupants";

        if (levelManager.outro) spriteRenderer.enabled = false;
    }

    protected virtual void Start ()
    {
        gameManager = FindObjectOfType<GameManager>();

        Instantiate(gameManager.interactionFeedbackPrefab, transform.position, Quaternion.identity, transform);

        GetCurrentTile();
    }

    protected void TileOccupantUpdate ()
    {
	}

    protected virtual void GetCurrentTile()
    {
        previousTile = currentTile;
        if(previousTile && previousTile.currentOccupant == this) previousTile.currentOccupant = null;

        currentTile = grid.GetTileRelative(transform);
        currentTile.currentOccupant = this;
    }

    public bool InPlayerInteractionRange()
    {
        float squareMagnitude = (transform.position - player.transform.position).sqrMagnitude;

        if (squareMagnitude < Player.INTERACTION_RANGE) return true;
        else return false;
    }
}
