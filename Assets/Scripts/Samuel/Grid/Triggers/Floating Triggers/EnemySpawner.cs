﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : EnemySpawner.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : FloatingTrigger
{
    //Set in inspector
    public int suspicionLevel;
    public GameObject enemyPrefab;
    public string enemyName;
    public EnemyMovementBehaviors movementBehavior;
    public Vector2[] PatrolNodes;
    public TextAsset confrontationTextAsset;
    public float initialAngle;

    //Const

    //Properties
    public Actors actors { get; private set; }
    public bool spawned { get; private set; }
    public MainCamera mainCamera { get; private set; }

    protected override void Awake ()
	{
        base.Awake();

        actors = FindObjectOfType<Actors>();
	}

    protected override void Start ()
	{
        base.Start();

        mainCamera = FindObjectOfType<MainCamera>();
    }

    protected override void Update ()
	{
        base.Update();
    }

    //Returns true if enemy was spawned
    public bool AttemptEnemySpawn()
    {
        if (spawned) return false;

        if (player.suspicionLevel >= suspicionLevel && suspicionLevel > 0)
        {
            //Spawn the enemy
            Enemy enemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity, actors.transform).GetComponent<Enemy>();

            //Name
            if (enemyName != "") enemy.name = enemyName;

            //Audio
            enemy.audioObject.PlayOnce(gameManager.lightSecuritySpawnClip);

            //Initial angle
            enemy.initialAngle = initialAngle;

            //Movement behavior
            enemy.movementBehavior = movementBehavior;

            //Patrol nodes
            enemy.GetPatrolNodes(PatrolNodes);

            //Confrontation text asset
            enemy.confrontationTextAsset = confrontationTextAsset;

            //Prepare enemy for spawn
            enemy.PrepareForSpawn();

            //Don't reuse this spawner until alarm state
            spawned = true;

            return true;
        }

        //Not time to spawn yet
        return false;
    }
}
