﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : CutsceneTrigger.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTrigger : FloatingTrigger
{
    //Set in inspector
    public TextAsset dialogueTextAsset;
    public Vector2 tileToFaceInDialogueCoordinates;
    public Vector2 nextTileCoordinates;
    public Actor user;

    //Const

    //Properties
    public Tile nextTile { get; set; }
    private LetterboxCanvas letterboxCanvas { get; set; }

    protected override void Awake()
    {
        base.Awake();

        //Get user, which is the player by default
        if (user == null) user = player;
    }

    protected override void Start()
    {
        base.Start();

        letterboxCanvas = FindObjectOfType<LetterboxCanvas>();

        //Get next tile
        if (nextTileCoordinates != Vector2.zero)
            nextTile = grid.GetTileAbsolute((int)nextTileCoordinates.x, (int)nextTileCoordinates.y);
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (!triggerEnabled) return;
        Actor actor = collider.transform.parent.GetComponent<Actor>();
        if (!actor) return;
        if (actor != user) return;

        base.OnTriggerEnter2D(collider);

        player.inCutscene = true;

        if (player.readyForPlayerInput) ActivateTrigger();
        else player.nextCutsceneTrigger = this;
    }

    public void ActivateTrigger()
    {
        //Dialogue
        if (dialogueTextAsset)
        {
            //Don't change facing
            if (tileToFaceInDialogueCoordinates == Vector2.zero)
            {
                player.StartConfrontation(dialogueTextAsset);
            }
            //Face a tile
            else
            {
                Tile tileToFace = grid.GetTileAbsolute(tileToFaceInDialogueCoordinates);
                player.StartConfrontation(dialogueTextAsset, null, tileToFace);
            }
        }

        //Path
        if (nextTile)
        {
            //Dialogue
            if (dialogueTextAsset)
                player.nextTileAfterDialogue = nextTile;
            //No dialogue
            else
                user.MoveToTile(nextTile, null, false);

            player.readyForPlayerInput = false;
        }

        //Activate letterbox
        letterboxCanvas.ToggleVisibility(true);

        //Destroy
        Destroy(gameObject);
    }
}
