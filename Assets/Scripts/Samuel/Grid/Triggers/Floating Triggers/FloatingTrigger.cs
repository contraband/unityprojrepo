﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : FloatingTrigger.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FloatingTrigger : Trigger
{
    //Set in inspector
    public bool triggerEnabled = true;
    public FloatingTrigger[] triggersToEnable;

    public new Rigidbody2D rigidbody2D { get; set; }
    BoxCollider2D boxCollider2D { get; set; }
    SpriteRenderer editorSpriteRenderer { get; set; }

    protected override void Awake()
    {
        base.Awake();

        rigidbody2D = gameObject.AddComponent<Rigidbody2D>();
        rigidbody2D.isKinematic = true;
        boxCollider2D = new GameObject("BoxCollider2D").AddComponent<BoxCollider2D>();
        boxCollider2D.transform.SetParent(transform, false);
        boxCollider2D.isTrigger = true;
        editorSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        editorSpriteRenderer.enabled = false;
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (!triggerEnabled) return;

        for(int i = 0; i < triggersToEnable.Length; ++i)
        {
            triggersToEnable[i].triggerEnabled = true;
        }
    }
}
