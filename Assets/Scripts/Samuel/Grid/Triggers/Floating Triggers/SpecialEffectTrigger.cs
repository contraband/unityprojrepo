﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialEffectTrigger : FloatingTrigger
{
    //Set in inspector

    private const float transitionTime = 5f;
    private const float endFOV = 180f;
    public Camera background3DCamera { get; set; }

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();

        if(!background3DCamera) background3DCamera = FindObjectOfType<Background3DCamera>().GetComponent<Camera>();
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (!triggerEnabled) return;
        Actor actor = collider.transform.parent.GetComponent<Actor>();
        if (!actor) return;

        StartCoroutine(Transition());
        player.audioObject.PlayOnce(gameManager.specialEffectClip);
    }

    private IEnumerator Transition()
    {
        float elapsedTime = 0;
        float startFOV = background3DCamera.fieldOfView;

        while (elapsedTime < transitionTime)
        {
            float t = elapsedTime / transitionTime;
            background3DCamera.fieldOfView = Mathf.Lerp(startFOV, endFOV, Mathf.SmoothStep(0f, 1f, t));

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
}
