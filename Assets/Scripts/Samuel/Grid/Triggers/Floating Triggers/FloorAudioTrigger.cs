﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : FloorAudioTrigger.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FloorAudioTypes { Metal, Marble, Wood }

public class FloorAudioTrigger : FloatingTrigger
{
    //Set in inspector
    public FloorAudioTypes floorAudioType;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (!triggerEnabled) return;
        Actor actor = collider.transform.parent.GetComponent<Actor>();
        if (!actor) return;
    }
}
