﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : MessageTrigger.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageTrigger : FloatingTrigger
{
    //Set in inspector
    public GameObject messagePrefab;
    public string messageText;
    public bool useCustomObjective = false;
    public string customObjectiveText;
    public Actor user;

    protected override void Awake()
    {
        base.Awake();

        //Get user, which is the player by default
        if (user == null) user = player;
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (!triggerEnabled) return;
        Actor actor = collider.transform.parent.GetComponent<Actor>();
        if (!actor) return;
        if (actor != user) return;

        //Message
        if (messagePrefab != null)
        {
            //Default message text
            if(messageText == "") levelManager.DispatchMessage(messagePrefab);
            //Non-default message text
            else   levelManager.DispatchMessage(messageText, messagePrefab);
        }

        //Current objective
        if(useCustomObjective)
        {
            FindObjectOfType<CurrentObjective>().customObjective = customObjectiveText;
        }

        //Destroy
        Destroy(gameObject);
    }
}
