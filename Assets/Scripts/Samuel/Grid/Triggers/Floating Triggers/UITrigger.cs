﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITrigger : FloatingTrigger
{
    public enum UIElements { None, RemainingHP, RemainingMoves, EndTurn }

    //Set in inspector
    public UIElements UIElement = UIElements.None;
    public Actor user;

    //Const

    //Properties
    private LevelManager levelManager { get; set; }
    private HUDElement uiElement { get; set; }

    protected override void Awake()
    {
        base.Awake();

        levelManager = FindObjectOfType<LevelManager>();

        //Get user, which is the player by default
        if (user == null) user = player;
    }

    protected override void Start()
    {
        base.Start();

        GetUIElement();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Actor actor = collider.transform.parent.GetComponent<Actor>();
        if (!actor) return;
        if (actor != user) return;

        //UI element
        if (UIElement != UIElements.None)
        {
            uiElement.Transition(true);
        }

        //Destroy
        Destroy(gameObject);
    }

    private void GetUIElement()
    {
        if(UIElement == UIElements.EndTurn)
        {
            uiElement = FindObjectOfType<EndTurn>();
        }
        else if (UIElement == UIElements.RemainingHP)
        {
            uiElement = FindObjectOfType<RemainingHP>();
        }
        else if (UIElement == UIElements.RemainingMoves)
        {
            uiElement = FindObjectOfType<RemainingActions>();
        }

        if (!uiElement) return;

        uiElement.stayOffscreen = true;
    }
}
