﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Exit.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : TileTrigger
{

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start ()
    {
        base.Start();

        if(!grid.hideExitFeedback)
        {
            Exit[] exits = (Exit[])GameObject.FindObjectsOfType(typeof(Exit));
            if ((Vector2)transform.position == grid.exitCoordinates) Instantiate(FindObjectOfType<GameManager>().exitFeedbackPrefab, transform.position, Quaternion.identity, transform);
        }
    }

    protected override void Update ()
    {
        base.Update();
    }

    public void ExitAttempt()
    {
        if (player.canExit) levelManager.EnterWinState();
    }
}
