﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Trigger : MonoBehaviour
{
    public Player player { get; set; }
    public Grid grid { get; set; }
    public Tile tile { get; set; }
    public LevelManager levelManager { get; set; }
    public GameManager gameManager { get; set; }

    protected virtual void Awake ()
	{
        player = FindObjectOfType<Player>();
        grid = FindObjectOfType<Grid>();
        levelManager = FindObjectOfType<LevelManager>();
    }

    protected virtual void Start ()
	{
        gameManager = FindObjectOfType<GameManager>();
        tile = grid.GetTileRelative(transform);
    }

    protected virtual void Update ()
	{

	}
}
