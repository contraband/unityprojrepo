﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : LevelManager.cs
// Author : Samuel Schimmel
// Purpose : Manages non-persistent global data.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public enum GameStates { None, Transition, Intro, LoadMainMenu, MainMenu, Apartment, LoadInfiltration, Infiltration, LoadHacking, Hacking, Win, Loss, Credits, LoadEmail, Email };
public enum Transitions { None, FadeIn, FadeOut, HUD, Camera };
public enum LevelTypes { Stealth, Tutorial, Safe };
public enum MusicTypes { Stealth, Office, Tutorial, Ending, Outro };

public class LevelManager : MonoBehaviour
{
    //Set in inspector
    public string nextScene;
    public bool showIntroAndMainMenu = false;
    public bool outro = false;
    public bool osirisLevel = false;
    public LevelTypes levelType = LevelTypes.Stealth;
    public MusicTypes musicType = MusicTypes.Stealth;
    public string startMessage;
    public GameObject gameManagerPrefab;
    public TextAsset apartmentTextAsset;
    // ADDED BY GRANT - 4/8/2017
    public string SecretPuzzleAnswer;

    //States and transitions
    static public GameStates currentGameState;
    public GameStates previousGameState { get; set; }
    public GameStates nextGameState { get; set; }
    public Transitions currentTransition { get; set; }
    
    public LevelData currentLevelData { get; set; }
    public GameObject currentHackInterfacePrefab { get; set; }
    public List<TextAsset> currentEmailTextAssets { get; set; }
    public TerminalTypes currentTerminalType { get; set; }
    private Grid grid;
    private Actors actors;
    private Player player;
    private FadeImage fadeImage;
    private HUD hud;
    private PauseMenuLogic pauseMenuLogic;
    private GameManager gameManager;
    private bool infiltrationStarted;
    private Canvas messageCanvas;
    // ADDED BY GRANT - 4/10/2017
    public NarrativeLevelData currentNarrativeLevelData { get; set; }

    //Cameras
    public MainCamera mainCamera { get; set; }
    public HUDCamera hudCamera { get; set; }
    public ImageEffectCamera background3DCamera { get; set; }

    //Audio
    private AudioObject audioObject;

    //Timing
    const float LOAD_TIME = 1f;
    private const float endTransitionDelay = 2f;
    private const float endDelay = 3f;

    void Awake()
    {
        grid = FindObjectOfType<Grid>();

        //Game manager
        if (!FindObjectOfType<GameManager>()) gameManager = Instantiate(gameManagerPrefab, Vector3.zero, Quaternion.identity).GetComponent<GameManager>();
        else gameManager = FindObjectOfType<GameManager>();

        Instantiate(gameManager.infiltrationCanvasesPrefab, Vector3.zero, Quaternion.identity);
        actors = FindObjectOfType<Actors>();
        hud = FindObjectOfType<HUD>();
        messageCanvas = FindObjectOfType<MessageCanvas>().GetComponent<Canvas>();
        player = FindObjectOfType<Player>();
        Instantiate(gameManager.imageEffectCanvasesPrefab, Vector3.zero, Quaternion.identity);
        fadeImage = FindObjectOfType<FadeImage>();
        pauseMenuLogic = gameManager.GetComponent<PauseMenuLogic>();
        currentGameState = GameStates.None;
        audioObject = gameObject.AddComponent<AudioObject>();
        gameObject.AddComponent<StandaloneInputModule>();
        if(outro) Instantiate(gameManager.outroCanvasPrefab, Vector3.zero, Quaternion.identity);
        mainCamera = (Instantiate(gameManager.mainCameraPrefab, Vector3.zero, Quaternion.identity)).GetComponent<MainCamera>();
        hudCamera = (Instantiate(gameManager.hudCameraPrefab, Vector3.zero, Quaternion.identity)).GetComponent<HUDCamera>();
    }

    void Start()
    {
        Instantiate(gameManager.apartmentCanvasesPrefab, Vector3.zero, Quaternion.identity);

        if (showIntroAndMainMenu) TransitionToIntro();
        else TransitionToApartmentState();

        // ADDED BY GRANT - 4/13/17
        if(SecretPuzzleAnswer != "winnebago")
        {
            Debug.Log("RESETTING SECRET PUZZLE ANSWER: " + SecretPuzzleAnswer);
            gameManager.InitializeSecretPuzzle(SecretPuzzleAnswer);
        }
        GameManager.ResetHackingTimeBoosts();
    }

    void OnEnable()
    {
        actors.onNewPhase += NewPhaseStarted;
        pauseMenuLogic.onPause += Paused;
    }

    void OnDisable()
    {
        actors.onNewPhase -= NewPhaseStarted;
        pauseMenuLogic.onPause -= Paused;
    }

    public void NewPhaseStarted(bool playerPhase)
    {
        if (musicType != MusicTypes.Stealth) return;

        if (playerPhase)
        {
            gameManager.playerAudioObject.FadeIn();
            gameManager.enemyAudioObject.FadeOut();
        }
        else
        {
            gameManager.playerAudioObject.FadeOut();
            gameManager.enemyAudioObject.FadeIn();
        }
    }

    public void Paused(bool paused)
    {
        print("set paused to " + paused);
        
        //Music
        gameManager.beatAudioObject.ToggleLowPassFilter(paused);
        gameManager.playerAudioObject.ToggleLowPassFilter(paused);
        gameManager.enemyAudioObject.ToggleLowPassFilter(paused);
        gameManager.alarmAudioObject.ToggleLowPassFilter(paused);
        gameManager.hackingThemeAudioObject.ToggleLowPassFilter(paused);
        gameManager.hackingActionAudioObject.ToggleLowPassFilter(paused);
        gameManager.otherMusicAudioObject.ToggleLowPassFilter(paused);
    }

    public void TransitionEnded()
    {
        mainCamera.onCameraMoveEnd -= TransitionEnded;
        fadeImage.onTransitionEnd -= TransitionEnded;
        if (hud) hud.onTransitionEnd -= TransitionEnded;

        //Clean up previous state
        switch (previousGameState)
        {
            case GameStates.Hacking:
                Destroy(FindObjectOfType<HackPuzzleLogic>().gameObject);
                mainCamera.ToggleMonitorEffects(false, false, 0f);
                break;

            case GameStates.Email:
                Destroy(FindObjectOfType<EmailClient>().transform.parent.parent.gameObject);
                mainCamera.ToggleMonitorEffects(false, false, 0f);
                break;

            case GameStates.Infiltration:
                player.readyForPlayerInput = false;
                break;
        }

        EnterNextState();
    }

    void EnterNextState()
    {
        switch (nextGameState)
        {
            case GameStates.LoadMainMenu:
                LoadMainMenu();
                break;

            case GameStates.MainMenu:
                EnterMainMenuState();
                break;

            case GameStates.Apartment:
                EnterApartmentState();
                break;

            case GameStates.LoadInfiltration:
                LoadInfiltration();
                break;

            case GameStates.Infiltration:
                EnterInfiltrationState();
                break;

            case GameStates.LoadHacking:
                LoadHacking();
                break;

            case GameStates.Hacking:
                EnterHackingState();
                break;

            case GameStates.LoadEmail:
                LoadEmail();
                break;

            case GameStates.Email:
                EnterEmailState();
                break;

            case GameStates.Credits:
                GoToCredits();
                break;
        }
    }

    private void EnterTransitionState(Transitions transition_, GameStates nextState_ = GameStates.None, float loadTime = 0f)
    {
        previousGameState = currentGameState;
        currentGameState = GameStates.Transition;
        nextGameState = nextState_;
        currentTransition = transition_;

        Invoke("EnterTransition", loadTime);  
    }

    private void EnterTransition()
    {
        switch (currentTransition)
        {
            case Transitions.Camera:
                mainCamera.ViewPosition(player.transform.position);
                if (nextGameState != GameStates.None)
                    mainCamera.onCameraMoveEnd += TransitionEnded;
                break;

            case Transitions.FadeIn:
                fadeImage.Transition(0f);
                if (nextGameState != GameStates.None)
                    fadeImage.onTransitionEnd += TransitionEnded;
                break;

            case Transitions.FadeOut:
                fadeImage.Transition(1f);
                if (nextGameState != GameStates.None)
                    fadeImage.onTransitionEnd += TransitionEnded;
                break;
        }
    }

    public void TransitionToIntro()
    {
        pauseMenuLogic.enabled = false;
        EnterTransitionState(Transitions.FadeIn, GameStates.Intro);
        fadeImage.onTransitionEnd += FindObjectOfType<Intro>().FadeInEnded;
    }

    public void EnterIntro()
    {
        currentGameState = GameStates.Intro;
    }

    public void ExitIntro()
    {
        EnterTransitionState(Transitions.FadeOut, GameStates.LoadMainMenu);
    }

    public void LoadMainMenu()
    {
        if(FindObjectOfType<Intro>()) Destroy(FindObjectOfType<Intro>().gameObject);
        pauseMenuLogic.enabled = false;
        EnterTransitionState(Transitions.FadeIn, GameStates.MainMenu);
    }

    public void EnterMainMenuState()
    {
        if (FindObjectOfType<Intro>()) Destroy(FindObjectOfType<Intro>().gameObject);
        FindObjectOfType<MainMenu>().InitializeMenu();
        currentGameState = GameStates.MainMenu;
    }

    public void TransitionToApartmentState()
    {
        if(FindObjectOfType<Intro>()) Destroy(FindObjectOfType<Intro>().gameObject);
        fadeImage.Transition(0f, 0f);
        EnterApartmentState();
        Destroy(FindObjectOfType<MainMenu>().gameObject);
    }

    void EnterApartmentState()
    {
        currentGameState = GameStates.Apartment;
        pauseMenuLogic.enabled = false;
        Apartment apartment = FindObjectOfType<Apartment>();

        if (outro)
        {
            string file = null;
            if (gameManager.choices["Ending"] == 1) file = "Ending1";
            else if (gameManager.choices["Ending"] == 2) file = "Ending2";
            string nextScriptPath = "TextAssets/Apartment/" + file;

            TextAsset outroScript = Resources.Load<TextAsset>(nextScriptPath);

            if (outroScript)
            {
                apartment.SetScript(outroScript.ToString());
                return;
            }
        }

        if (apartmentTextAsset) apartment.SetScript(apartmentTextAsset.ToString());
        else TransitionToInfiltrationState();
    }

    public void TransitionToInfiltrationState()
    {
        FindObjectOfType<Apartment>().audioObject.FadeOut(false, false, LOAD_TIME);
        Invoke("InfiltrationTransition", LOAD_TIME);
    }

    private void InfiltrationTransition()
    {
        LoadInfiltration();
        FindObjectOfType<DiegeticMonitor>().Transition(false);
        FindObjectOfType<DiegeticMonitorBackground>().Transition(false);
    }

    private void LoadInfiltration()
    {
        FindObjectOfType<Apartment>().audioObject.FadeOut(false, false, LOAD_TIME);
        currentGameState = GameStates.LoadInfiltration;
        GameManager.startInInfiltration = true;

        if (infiltrationStarted)
        {
            EnterTransitionState(Transitions.FadeIn, GameStates.Infiltration);
        }
        else
        {
            EnterTransitionState(Transitions.Camera, GameStates.Infiltration);
            fadeImage.Transition(0f);

            RestartAllMusic();
            gameManager.otherMusicAudioObject.Stop();
            if (musicType == MusicTypes.Stealth) gameManager.beatAudioObject.FadeIn(0f);
            else if (musicType == MusicTypes.Office) gameManager.otherMusicAudioObject.PlayLooping(gameManager.officeClip, true);
            else if (musicType == MusicTypes.Tutorial) gameManager.otherMusicAudioObject.PlayLooping(gameManager.tutorialClip, true);
            else if (musicType == MusicTypes.Ending) gameManager.otherMusicAudioObject.PlayLooping(gameManager.endingClip, true);
            else if (musicType == MusicTypes.Outro) gameManager.otherMusicAudioObject.PlayOnce(gameManager.outroClip, false, true);
        }
    }

    public void EnterInfiltrationState()
    {
        if (outro)
        {
            FindObjectOfType<Outro>().scrolling = true;
            return;
        }

        if (infiltrationStarted)
        {
            if (player.inCutscene)
            {
                ActivateReadyForPlayerInput();
            }
            else
            {
                hud.Transition(true);
            }
        }
        else
        {
            if (levelType == LevelTypes.Safe)
            {
                actors.StartPlayerTurn();

                if(!player.inCutscene)
                {
                    FindObjectOfType<CurrentObjective>().Transition(true);
                    FindObjectOfType<RemainingHP>().Transition(true);
                }
            }
            else
            {
                if (player.inCutscene)
                {
                    StartPlayerTurn();
                }
                else
                {
                    hud.onTransitionEnd += StartPlayerTurn;
                    hud.Transition(true);
                }
            }
        }

        if (!infiltrationStarted) if (startMessage != "") DispatchMessage(startMessage);
        infiltrationStarted = true;
        GameManager.startInInfiltration = true;
        pauseMenuLogic.enabled = true;
        currentGameState = GameStates.Infiltration;
    }

    public void StartPlayerTurn()
    {
        actors.StartPlayerTurn();
        hud.onTransitionEnd -= StartPlayerTurn;
    }

    public void ActivateReadyForPlayerInput()
    {
        player.readyForPlayerInput = true;
        hud.onTransitionEnd -= ActivateReadyForPlayerInput;
    }

    public void TransitionToEmailState(List<TextAsset> emailTextAssets)
    {
        player.readyForPlayerInput = false;
        hud.Transition(false);
        EnterTransitionState(Transitions.FadeOut, GameStates.LoadEmail);
        pauseMenuLogic.enabled = false;
        currentEmailTextAssets = emailTextAssets;
        DispatchMessage("Accessing Evelyn's Email");
    }

    private void LoadEmail()
    {
        currentGameState = GameStates.LoadEmail;
        Instantiate(gameManager.emailCanvasesPrefab, Vector3.zero, Quaternion.identity);
        FindObjectOfType<EmailClient>().InitializeEmails(currentEmailTextAssets);
        EnterTransitionState(Transitions.FadeIn, GameStates.Email, LOAD_TIME);
        fadeImage.onTransitionEnd += FindObjectOfType<EmailClient>().TransitionIn;
    }

    private void EnterEmailState()
    {
        currentGameState = GameStates.Email;
        fadeImage.onTransitionEnd -= FindObjectOfType<EmailClient>().TransitionIn;
    }

    public void ExitEmailState()
    {
        if (currentGameState != GameStates.Email) return;
        EnterTransitionState(Transitions.FadeOut, GameStates.LoadInfiltration);
        hud.onTransitionEnd += player.EmailsRead;
        player.emailsRead = true;
        player.currentTerminal.used = true;

        if (osirisLevel)
        {
            Instantiate(gameManager.osirisPrefab, actors.transform).name = "Osiris";
            FindObjectOfType<CurrentObjective>().customObjective = "TALK TO OSIRIS";
        }
    }

    public void TransitionToHackingState(LevelData levelData, TerminalTypes terminalType, GameObject hackInterfacePrefab, NarrativeLevelData narrativeLevelData = null)
    {
        player.readyForPlayerInput = false;
        hud.Transition(false);

        currentHackInterfacePrefab = hackInterfacePrefab;
        currentLevelData = levelData;
        // ADDED BY GRANT 4/10/2017
        currentNarrativeLevelData = narrativeLevelData;

        currentTerminalType = terminalType;
        EnterTransitionState(Transitions.FadeOut, GameStates.LoadHacking);
        pauseMenuLogic.enabled = false;

        //Message
        string description = "";
        if (terminalType == TerminalTypes.DataVault) description += "data vault";
        else if (terminalType == TerminalTypes.Security) description += "security";
        DispatchMessage("Accessing " + description);
    }

    void LoadHacking()
    {
        if (musicType == MusicTypes.Stealth) gameManager.hackingThemeAudioObject.FadeIn();
        currentGameState = GameStates.LoadHacking;
        Instantiate(currentHackInterfacePrefab, Vector3.zero, Quaternion.identity);
        EnterTransitionState(Transitions.FadeIn, GameStates.Hacking, LOAD_TIME);
    }

    private void EnterHackingState()
    {
        currentGameState = GameStates.Hacking;
    }

    public void ExitHackingState(ExitHackingResults exitHackingResult)
    {
        if (currentGameState != GameStates.Hacking) return;

        EnterTransitionState(Transitions.FadeOut, GameStates.LoadInfiltration);
        hud.onTransitionEnd += player.HackingComplete;

        if (exitHackingResult == ExitHackingResults.Success)
        {
            player.currentTerminal.locked = true;

            if (currentTerminalType == TerminalTypes.DataVault)
            {
                player.hackingResult = HackingResults.DataVaultSuccess;
            }
            else if (currentTerminalType == TerminalTypes.Security)
            {
                player.hackingResult = HackingResults.SecurityTerminalSuccess;

                // ADDED BY GRANT - 4/8/2017
                for (int i = 0; i < currentLevelData.SecretLetters.Length; ++i)
                {
                    gameManager.SecretLetters[currentLevelData.TerminalID + i] = currentLevelData.SecretLetters[i];
                }
            }
        }
        else if(exitHackingResult == ExitHackingResults.Failure)
        {
            player.hackingResult = HackingResults.Failure;
            GameManager.IncrementHackingTimeBoosts();
        }
        else
        {
            player.hackingResult = HackingResults.None;
        }

        //Music
        //Fade out hack theme unless player has hacked data vault (just now, or earlier)
        if (!(player.hackingResult == HackingResults.DataVaultSuccess || player.dataVaultHacked)) gameManager.hackingThemeAudioObject.FadeOut();
    }

    public void EnterWinState()
    {
        player.readyForPlayerInput = false;
        Dialogue.SkipAllDialogues();
        currentGameState = GameStates.Win;
        hud.Transition(false);
        GameManager.startInInfiltration = false;
        gameManager.ResetCheckpoint();
        audioObject.PlayOnce(gameManager.winClip);
        FadeOutAllMusic();
        Message.DestroyAllMessages();
        FindObjectOfType<DiegeticMonitor>().Transition(true);
        FindObjectOfType<DiegeticMonitorBackground>().Transition(true);

        if (levelType == LevelTypes.Safe) GameManager.previousLevelSuspicion = player.suspicionLevel;
        else GameManager.previousLevelSuspicion = 0;

        if (player.inCutscene) FindObjectOfType<LetterboxCanvas>().ToggleVisibility(false);
        else DispatchMessage("EXIT REACHED\nFLOOR CLEARED");
    }

    public void GoToNextChapter()
    {
        SceneManager.LoadScene(nextScene);
    }

    public void EnterLossState()
    {
        player.readyForPlayerInput = false;
        currentGameState = GameStates.Loss;
        hud.Transition(false);
        Message.DestroyAllMessages();
        DispatchMessage("EVELYN WAS APPREHENDED\nFLOOR FAILED");
        Invoke("StartLossTransition", endTransitionDelay);
        audioObject.PlayOnce(gameManager.lossClip);
        FadeOutAllMusic();
    }

    void StartLossTransition()
    {
        fadeImage.Transition(new Color(1, 1, 1, 1), endTransitionDelay);
        Invoke("LossStateRestart", endDelay);
    }

    private void LossStateRestart()
    {
        RestartLevel(true);
    }

    public void RestartLevel(bool fromCheckpoint)
    {
        if (!fromCheckpoint) gameManager.ResetCheckpoint();
        FadeOutAllMusic(0f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void RaiseAlarm()
    {
        if (musicType != MusicTypes.Stealth) return;

        gameManager.alarmAudioObject.FadeIn();
    }

    public void EnterHackingAction()
    {
        if (musicType != MusicTypes.Stealth) return;

        gameManager.hackingActionAudioObject.FadeIn();
    }

    public void ExitHackingAction()
    {
        if (musicType != MusicTypes.Stealth) return;

        gameManager.hackingActionAudioObject.FadeOut();
    }

    public void FadeOutAllMusic(float fadeOutTime = AudioObject.FADE_OUT_TIME)
    {
        //Music
        gameManager.beatAudioObject.FadeOut(false, false, fadeOutTime);
        gameManager.playerAudioObject.FadeOut(false, false, fadeOutTime);
        gameManager.enemyAudioObject.FadeOut(false, false, fadeOutTime);
        gameManager.alarmAudioObject.FadeOut(false, false, fadeOutTime);
        gameManager.hackingThemeAudioObject.FadeOut(false, false, fadeOutTime);
        gameManager.hackingActionAudioObject.FadeOut(false, false, fadeOutTime);
        gameManager.otherMusicAudioObject.FadeOut(false, false, fadeOutTime);
    }

    private void RestartAllMusic()
    {
        //Music
        gameManager.beatAudioObject.Restart();
        gameManager.playerAudioObject.Restart();
        gameManager.enemyAudioObject.Restart();
        gameManager.alarmAudioObject.Restart();
        gameManager.hackingThemeAudioObject.Restart();
        gameManager.hackingActionAudioObject.Restart();
        gameManager.otherMusicAudioObject.Restart();
    }

    void GoToCredits()
    {
        SceneManager.LoadScene("Outro");
    }

    public void DispatchMessage(string text, GameObject prefab)
    {
        GameObject message = Instantiate(prefab, messageCanvas.transform, false);
        message.GetComponent<Message>().ShowMessage(text);
    }

    //Default text
    public void DispatchMessage(GameObject prefab)
    {
        GameObject message = Instantiate(prefab, messageCanvas.transform, false);

        //Get default text
        string text = message.GetComponent<TextMeshProUGUI>().text;

        message.GetComponent<Message>().ShowMessage(text);
    }

    //Default prefab
    public void DispatchMessage(string text)
    {
        //Use default prefab
        GameObject prefab = gameManager.stateChangeMessagePrefab;

        GameObject message = Instantiate(prefab, messageCanvas.transform, false);
        message.GetComponent<Message>().ShowMessage(text);
    }

    void Update()
    {
        grid.GridUpdate();

        Message.DispatchQueue();
    }
}