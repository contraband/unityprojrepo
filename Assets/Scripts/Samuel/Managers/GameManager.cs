﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : GameManager.cs
// Author : Samuel Schimmel
// Purpose : Manages persistent global data, prefabs, and checkpoints.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    //Design data
    public const int securityTerminalSuspicion = -40;
    public const int hackFailedSuspicion = 10;
    public const int startTurnSuspicion = 5;
    public const int coworkerSuspicion = 40;
    public const int cameraSuspicion = 40;
    public const int lightSecuritySuspicion = 60;
    public const int predictEnemyCost = 2;
    public const int maxPlayerActions = 6;
    public const int maxEnemyActions = 4;
    public const int maxHeavySecurityActions = 6;
    // ADDED BY GRANT - 4/13/17
    public const int maxHackingTimeBoosts = 3;

    //Game state
    static public bool startInInfiltration { get; set; }
    static public Vector2 playerCheckpointPosition { get; set; }
    static public int playerCheckpointHP { get; set; }
    static public bool dataVaultHackedCheckpoint { get; set; }
    static public bool checkpointSaved { get; set; }
    static private float timeSinceCheckpoint;
    static public int previousLevelSuspicion { get; set; }
    // ADDED BY GRANT - 4/13/17
    static public int hackingTimeBoosts { get; set; }

    //Choices
    public Dictionary<string, int> choices { get; set; }

    //Secret puzzles
    public bool nextSecretEmailUnlocked { get; set; }

    //Music audio clips
    public AudioClip pianoClip;
    public AudioClip apartmentClip;
    public AudioClip tutorialClip;
    public AudioClip officeClip;
    public AudioClip endingClip;
    public AudioClip outroClip;
    public AudioClip beatClip;
    public AudioClip playerClip;
    public AudioClip enemyClip;
    public AudioClip alarmClip;
    public AudioClip hackingThemeClip;
    public AudioClip hackingActionClip;

    //SFX audio clips
    public AudioClip clickClip;
    public AudioClip selectionClip;
    public AudioClip outOfMovesClip;
    public AudioClip badClickClip;
    public AudioClip winClip;
    public AudioClip lossClip;
    public AudioClip walkClip;
    public AudioClip actorDespawnClip;
    public AudioClip lightSecuritySpawnClip;
    public AudioClip heavySecuritySpawnClip;
    public AudioClip cameraRotationClip;
    public AudioClip predictionClip;
    public AudioClip dialogueStartClip;
    public AudioClip alarmRaisedClip;
    public AudioClip playerTurnStartedClip;
    public AudioClip enemyPhaseStartedClip;
    public AudioClip turnAccelerationClip;
    public AudioClip resetViewClip;
    public AudioClip zoomClip;
    public AudioClip cityClip;
    public AudioClip suspicionIncreaseClip;
    public AudioClip suspicionDecreaseClip;
    public AudioClip doorsOpeningClip;
    public AudioClip specialEffectClip;
    public AudioClip defaultNewCharacterClip;
    public AudioClip defaultNewLineClip;

    //Audio objects
    public AudioObject beatAudioObject { get; set; }
    public AudioObject playerAudioObject { get; set; }
    public AudioObject enemyAudioObject { get; set; }
    public AudioObject alarmAudioObject { get; set; }
    public AudioObject hackingThemeAudioObject { get; set; }
    public AudioObject hackingActionAudioObject { get; set; }
    public AudioObject otherMusicAudioObject { get; set; }

    //Prefabs
    public GameObject stateChangeMessagePrefab;
    public GameObject infiltrationCanvasesPrefab;
    public GameObject imageEffectCanvasesPrefab;
    public GameObject mainCameraPrefab;
    public GameObject hudCameraPrefab;
    public GameObject apartmentCanvasesPrefab;
    public GameObject emailCanvasesPrefab;
    public GameObject emailPrefab;
    public GameObject terminalIconPrefab;
    public GameObject miniterminalIconPrefab;
    public GameObject audioObjectPrefab;
    public GameObject background3DCameraPrefab;
    public GameObject spawnEnemyPrefab;
    public GameObject preRenderedBackgroundsPrefab;
    public GameObject interactionFeedbackPrefab;
    public GameObject coneOfVisionLinePrefab;
    public GameObject predictionLinePrefab;
    public GameObject highlightedPredictionLinePrefab;
    public GameObject speechIconPrefab;
    public GameObject exclamationIconPrefab;
    public GameObject actorVisionPrefab;
    public GameObject remainingActionsElementPrefab;
    public GameObject buildingPrefab;
    public GameObject exitFeedbackPrefab;
    public GameObject outroCanvasPrefab;
    public GameObject osirisPrefab;

    // ADDED BY GRANT - 4/6/2017
    public List<char> SecretLetters;
    // ADDED BY GRANT - 4/12/2017
    private string currentSecretPuzzleAnswer;
    public string CurrentSecretPuzzleAnswer { get { return currentSecretPuzzleAnswer; } }
    // ADDED BY GRANT - 4/8/2017
    public bool SecretPuzzleSolved { get; set; }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        InitializeChoices();
        InitializeMusic();

        // ADDED BY GRANT - 4/6/2017
        SecretLetters = new List<char>();
    }

    void Start()
    {
        // ADDED BY GRANT - 4/8/2017
        SecretPuzzleSolved = false;
    }

    void Update()
    {
        timeSinceCheckpoint += Time.deltaTime;
    }

    private void InitializeChoices()
    {
        choices = new Dictionary<string, int>();
        choices.Add("Ending", 0);
    }

    private void InitializeMusic()
    {
        beatAudioObject = (Instantiate(audioObjectPrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioObject>();
        beatAudioObject.isMusic = true;
        DontDestroyOnLoad(beatAudioObject);
        beatAudioObject.PlayLooping(beatClip, false, 0f);

        playerAudioObject = (Instantiate(audioObjectPrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioObject>();
        playerAudioObject.isMusic = true;
        DontDestroyOnLoad(playerAudioObject);
        playerAudioObject.PlayLooping(playerClip, false, 0f);

        enemyAudioObject = (Instantiate(audioObjectPrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioObject>();
        enemyAudioObject.isMusic = true;
        DontDestroyOnLoad(enemyAudioObject);
        enemyAudioObject.PlayLooping(enemyClip, false, 0f);

        alarmAudioObject = (Instantiate(audioObjectPrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioObject>();
        alarmAudioObject.isMusic = true;
        DontDestroyOnLoad(alarmAudioObject);
        alarmAudioObject.PlayLooping(alarmClip, false, 0f);

        hackingThemeAudioObject = (Instantiate(audioObjectPrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioObject>();
        hackingThemeAudioObject.isMusic = true;
        DontDestroyOnLoad(hackingThemeAudioObject);
        hackingThemeAudioObject.PlayLooping(hackingThemeClip, false, 0f);

        hackingActionAudioObject = (Instantiate(audioObjectPrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioObject>();
        hackingActionAudioObject.isMusic = true;
        DontDestroyOnLoad(hackingActionAudioObject);
        hackingActionAudioObject.PlayLooping(hackingActionClip, false, 0f);

        otherMusicAudioObject = (Instantiate(audioObjectPrefab, Vector3.zero, Quaternion.identity)).GetComponent<AudioObject>();
        otherMusicAudioObject.isMusic = true;
        DontDestroyOnLoad(otherMusicAudioObject);
    }

    public void SaveCheckpoint(Vector2 playerCheckPointPosition_, int playerCheckpointHP_, bool dataVaultHackedCheckpoint_)
    {
        playerCheckpointPosition = playerCheckPointPosition_;
        playerCheckpointHP = playerCheckpointHP_;
        dataVaultHackedCheckpoint = dataVaultHackedCheckpoint_;
        timeSinceCheckpoint = 0;
        checkpointSaved = true;
    }

    public void ResetCheckpoint()
    {
        playerCheckpointPosition = Vector2.zero;
        playerCheckpointHP = 0;
        dataVaultHackedCheckpoint = false;
        timeSinceCheckpoint = 0;
        checkpointSaved = false;
    }

    static public string GetTimeSinceCheckpoint()
    {
        if (checkpointSaved)
        {
            int minutes = (int)(timeSinceCheckpoint / 60);

            if(minutes == 1) return minutes + " minute since checkpoint.";
            else return minutes + " minutes since checkpoint.";
        }
        else
        {
            return "No checkpoint saved.";
        }
    }

    // ADDED BY GRANT - 4/12/17
    public void InitializeSecretPuzzle(string answer_)
    {
        currentSecretPuzzleAnswer = answer_;
        ResetSecretLetters(answer_.Length);

        // ADDED BY GRANT - 5/31/2018
        SecretPuzzleSolved = false;
    }

    // ADDED BY GRANT - 4/12/17
    public void ResetSecretLetters(int length_)
    {
        SecretLetters.Clear();
        for (int i = 0; i < length_; ++i)
        {
            SecretLetters.Add('?');
        }
    }

    // ADDED BY GRANT - 4/13/17
    static public void ResetHackingTimeBoosts()
    {
        hackingTimeBoosts = 0;
    }

    // ADDED BY GRANT - 4/13/17
    static public void IncrementHackingTimeBoosts()
    {
        if(hackingTimeBoosts < maxHackingTimeBoosts)
        {
            ++hackingTimeBoosts;
        }
        Debug.Log("HACKING TIME BOOSTS: " + hackingTimeBoosts);
    }
}