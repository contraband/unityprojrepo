﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : CameraBase.cs
// Author : Samuel Schimmel
// All content © 2017 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraBase : MonoBehaviour
{
    new public Camera camera { get; set; }
    protected MainCamera mainCamera;
    public AudioListener audioListener { get; set; }
    public GameManager gameManager { get; set; }

    protected virtual void Awake()
    {
        camera = GetComponent<Camera>();
        audioListener = GetComponent<AudioListener>();
    }

    protected virtual void Start ()
    {
        mainCamera = FindObjectOfType<MainCamera>();
        gameManager = FindObjectOfType<GameManager>();
	}

    protected virtual void Update ()
    {
		
	}

    public void ToggleCamera(bool enable)
    {
        camera.enabled = enable;
    }
}
