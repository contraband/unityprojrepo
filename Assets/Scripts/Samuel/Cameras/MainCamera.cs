﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Samuel Schimmel
// Author : Author name
// Purpose : controls all camera movementBehavior, transitions, zooming, and returning
//			 focus to current actor.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class MainCamera : CameraBase
{
    public delegate void EventHandler();
    public event EventHandler onCameraMoveEnd;

    private new Rigidbody2D rigidbody2D;
    private Player player;
    private Grid grid;
    private LevelManager levelManager;
    private new Camera camera;
    private Actors actors;
    private float zPosition = -20;
    private bool passedIntro;

    //Movement
    private float MANUAL_MOVEMENT_SPEED = 30f;
    private float drag = 5f;
    private float maxDistanceFromGrid = 0f;
    private Vector3 acceleratedPosition;
    private float mouseSensitivity = 0.1f;
    private Vector3 lastPosition;

    //Position
    public const float DEFAULT_Z = -10f;
    public const float MIN_Z = -15f;
    public const float MAX_Z = -10f;
    public const float Z_DELTA = 3f;
    public const float ZOOM_TIME = 0.2f;
    private Coroutine zoomCoroutine;
    private bool zooming;
    [HideInInspector] public Vector3 overviewPosition;
    private readonly Vector3 introStartCoordinates = new Vector3(0, 50f, -50f);

    //Image effects
    private VignetteAndChromaticAberration vignetteAndChromaticAberration;
    private NoiseAndGrain noiseAndGrain;
    private Fisheye fisheye;
    private Antialiasing antialiasing;
    private const float imageEffectTransitionTime = 2f;
    private bool interpolatingMonitorEffects;
    private Coroutine monitorEffectsCoroutine;
    
    //Image effect values
    private const float mainMenuMaxChromaticAberration = 5f;
    private const float otherMaxChromaticAberration = 2f;
    private const float mainMenuMaxVignette = 0.3f;
    private const float otherMaxVignette = 0.2f;
    private const float mainMenuMaxFilmGrain = 1.5f;
    private const float otherMaxFilmGrain = 0.4f;
    private const float mainMenuMaxFisheye = 0.1f;
    private const float otherMaxFisheye = 0.1f;
    private const float maxFilmGrainIntensityMultiplier = 0.25f;

    //Follow
    public Transform followTarget { get; set; }

    //Reset view
    private ResetView resetView;

    //Smooth time and max velocity
    private float currentSmoothTime;
    private float defaultSmoothTime = 0.8f;
    private float acceleratedSmoothTime = 0.2f;
    private float introSmoothTime = 1.5f;
    private float currentMaxSpeed;
    private float defaultMaxSpeed = 30f;
    private float acceleratedMaxSpeed = 100f;
    private float introMaxSpeed = 20f;

    //Audio
    public AudioObject audioObject;

    protected override void Awake()
    {
        base.Awake();

        audioObject = gameObject.AddComponent<AudioObject>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<Player>();
        rigidbody2D.drag = drag;
        grid = GameObject.Find("Grid").GetComponent<Grid>();
        levelManager = FindObjectOfType<LevelManager>();
        camera = GetComponent<Camera>();
        acceleratedPosition = new Vector3(grid.width / 2, grid.height / 2, zPosition);
        actors = FindObjectOfType<Actors>();

        //Image effects
        vignetteAndChromaticAberration = GetComponent<VignetteAndChromaticAberration>();
        noiseAndGrain = GetComponent<NoiseAndGrain>();
        fisheye = GetComponent<Fisheye>();
        antialiasing = GetComponent<Antialiasing>();

        currentSmoothTime = introSmoothTime;
        currentMaxSpeed = introMaxSpeed;
        transform.position = introStartCoordinates;
        transform.position += new Vector3(0, 0, MIN_Z);

        SetOverviewPosition();
    }

    protected override void Start()
    {
        base.Start();

        resetView = FindObjectOfType<ResetView>();
    }

    protected override void Update()
    {
        base.Update();

        //Manual control
        if (player && player.readyForPlayerInput)
        {
            ManualCameraMovement();
            ManualCameraZoom();
        }
        //Following an actor
        else if (followTarget)
        {
            FollowTarget();
        }

        lastPosition = Input.mousePosition;
    }

    private void SetOverviewPosition()
    {
        overviewPosition.x = grid.width / 2;
        overviewPosition.y = grid.height / 2;

        float adjustedWidth = grid.width * (16 / 9);
        float z = Mathf.Max(adjustedWidth, grid.height);
        overviewPosition.z = -z;
    }

    void ManualCameraMovement()
    {
        Vector3 manualMovementVector = new Vector3();
        bool input = false;

        //Middle mouse button
        if (Input.GetMouseButton(1))
        {
            Vector3 delta = Input.mousePosition - lastPosition;
            manualMovementVector -= delta * mouseSensitivity;
            input = true;
        }
        //Keyboard
        else
        {
            //Up
            if (Input.GetKey("w") || Input.GetKey("up")) { manualMovementVector += Vector3.up; input = true; }
            //Down
            if (Input.GetKey("s") || Input.GetKey("down")) { manualMovementVector += Vector3.down; input = true; }
            //Left
            if (Input.GetKey("a") || Input.GetKey("left")) { manualMovementVector += Vector3.left; input = true; }
            //Right
            if (Input.GetKey("d") || Input.GetKey("right")) { manualMovementVector += Vector3.right; input = true; }
        }

        //Constrain to gameplay area
        //Up
        if (transform.position.y > grid.height + maxDistanceFromGrid)
            manualMovementVector.y = Mathf.Clamp(manualMovementVector.y, manualMovementVector.y, 0);
        //Down
        if (transform.position.y < -maxDistanceFromGrid)
            manualMovementVector.y = Mathf.Clamp(manualMovementVector.y, 0, manualMovementVector.y);
        //Left
        if (transform.position.x < -maxDistanceFromGrid)
            manualMovementVector.x = Mathf.Clamp(manualMovementVector.x, 0, manualMovementVector.x);
        //Right
        if (transform.position.x > grid.width + maxDistanceFromGrid)
            manualMovementVector.x = Mathf.Clamp(manualMovementVector.x, manualMovementVector.x, 0);

        manualMovementVector *= MANUAL_MOVEMENT_SPEED;
        rigidbody2D.AddForce(manualMovementVector);
        rigidbody2D.velocity = Vector3.ClampMagnitude(rigidbody2D.velocity, MANUAL_MOVEMENT_SPEED);

        //No input
        if (!input) rigidbody2D.AddForce(-rigidbody2D.velocity);

        if (manualMovementVector != Vector3.zero) resetView.Show();
    }

    public void ViewPosition(Vector3 destination, bool arc = false, float z = DEFAULT_Z)
    {
        destination.z = z;
        player.readyForPlayerInput = false;
        StartCoroutine(InterpolatePosition(destination, arc));
    }

    private IEnumerator InterpolatePosition(Vector3 endPosition_, bool arc = false)
    {
        float tolerance = 0.25f;
        Vector3 velocity = Vector3.zero;
        Vector3 position = transform.position;
        Vector3 endPosition = endPosition_;
        float startMagnitude = (endPosition - position).sqrMagnitude;

        //Calculate how much the camera should zoom out between actors
        float maxMagnitude = 100f;
        float minZ = MIN_Z * (startMagnitude / maxMagnitude);
        minZ = Mathf.Clamp(minZ, MIN_Z, MAX_Z);

        float currentMagnitude = Mathf.Infinity;
        while (currentMagnitude > tolerance)
        {
            position = transform.position;
            float progress = currentMagnitude / startMagnitude;
            Vector3 zAdjustedPosition = endPosition;
            zAdjustedPosition.z = Mathf.SmoothStep(MAX_Z, minZ, progress);

            if (arc)
            {
                transform.position = Vector3.SmoothDamp(transform.position, zAdjustedPosition, ref velocity, currentSmoothTime, currentMaxSpeed);
                currentMagnitude = ((Vector2)endPosition - (Vector2)position).sqrMagnitude;
            }
            else
            {
                transform.position = Vector3.SmoothDamp(transform.position, endPosition_, ref velocity, currentSmoothTime, currentMaxSpeed);
                currentMagnitude = (endPosition - position).sqrMagnitude;
            }

            yield return null;
        }

        if (onCameraMoveEnd != null)
            onCameraMoveEnd();
    }

    void ManualCameraZoom()
    {
        float scrollWheel = Input.GetAxis("Mouse ScrollWheel");

        if (scrollWheel == 0)
            return;

        if (zooming)
            return;

        float newZ = transform.position.z;

        if (scrollWheel > 0)
        {
            newZ += Z_DELTA;
        }
        else
        {
            newZ -= Z_DELTA;
        }

        newZ = Mathf.Clamp(newZ, MIN_Z, MAX_Z);

        if (newZ == MIN_Z || newZ == MAX_Z) return;

        audioObject.PlayOnce(gameManager.zoomClip);
        resetView.Show();
        zoomCoroutine = StartCoroutine(InterpolateZoom(newZ));
    }

    private IEnumerator InterpolateZoom(float newZ)
    {
        Vector3 startPosition = transform.position;
        Vector3 endPosition = startPosition;
        endPosition.z = newZ;

        zooming = true;
        float elapsedTime = 0;

        while (elapsedTime < ZOOM_TIME || (endPosition - transform.position).sqrMagnitude > 1f)
        {
            float t = elapsedTime / ZOOM_TIME;
            transform.position = Vector3.Lerp(startPosition, endPosition, Mathf.SmoothStep(0f, 1f, t));

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        zooming = false;
    }

    public void Accelerate()
    {
        currentSmoothTime = acceleratedSmoothTime;
        currentMaxSpeed = acceleratedMaxSpeed;
    }

    public void Decelerate()
    {
        currentSmoothTime = defaultSmoothTime;
        currentMaxSpeed = defaultMaxSpeed;
    }

    private void FollowTarget()
    {
        Vector3 velocity = Vector3.zero;
        float smoothTime = 0.3f;

        Vector3 targetPosition = followTarget.transform.position;
        targetPosition.z += DEFAULT_Z;

        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    }

    public void ShowMapOverview()
    {
        StartCoroutine(InterpolatePosition(overviewPosition));
    }

    public void ToggleMonitorEffects(bool on, bool apartment = false, float transitionTime = imageEffectTransitionTime)
    {
        if (interpolatingMonitorEffects)
        {
            StopCoroutine(monitorEffectsCoroutine);
            interpolatingMonitorEffects = false;
        }

        antialiasing.enabled = !on;

        monitorEffectsCoroutine = StartCoroutine(InterpolateMonitorEffects(on, transitionTime, apartment));
    }

    private IEnumerator InterpolateMonitorEffects(bool on, float transitionTime, bool apartment)
    {
        interpolatingMonitorEffects = true;
        float elapsedTime = 0;

        //Chromatic aberration
        float startChromaticAberration = vignetteAndChromaticAberration.chromaticAberration;
        float endChromaticAberration = 0f;
        //Vignette
        float startVignette = vignetteAndChromaticAberration.intensity;
        float endVignette = 0f;
        //Film grain
        float startFilmGrain = noiseAndGrain.generalIntensity;
        float endFilmGrain = 0f;
        float startFilmGrainIntensityMultiplier = noiseAndGrain.intensityMultiplier;
        float endFilmGrainIntensityMultiplier = 0f;
        //Fisheye
        float startFisheye = fisheye.strengthX;
        float endFisheye = 0f;

        if(on)
        {
            if(apartment)
            {
                //Chromatic aberration
                endChromaticAberration = mainMenuMaxChromaticAberration;
                //Vignette
                endVignette = mainMenuMaxVignette;
                //Film grain
                endFilmGrain = mainMenuMaxFilmGrain;
                //Fisheye
                endFisheye = mainMenuMaxFisheye;
            }
            else
            {
                //Chromatic aberration
                endChromaticAberration = otherMaxChromaticAberration;
                //Vignette
                endVignette = otherMaxVignette;
                //Film grain
                endFilmGrain = otherMaxFilmGrain;
                //Fisheye
                endFisheye = otherMaxFisheye;
            }

            endFilmGrainIntensityMultiplier = maxFilmGrainIntensityMultiplier;
        }

        do
        {
            float t = elapsedTime / transitionTime;

            //Chromatic aberration
            vignetteAndChromaticAberration.chromaticAberration = Mathf.Lerp(startChromaticAberration, endChromaticAberration, Mathf.SmoothStep(0f, 1f, t));
            //Vignette
            vignetteAndChromaticAberration.intensity = Mathf.Lerp(startVignette, endVignette, Mathf.SmoothStep(0f, 1f, t));
            //Film grain
            noiseAndGrain.generalIntensity = Mathf.Lerp(startFilmGrain, endFilmGrain, Mathf.SmoothStep(0f, 1f, t));
            noiseAndGrain.intensityMultiplier = Mathf.Lerp(startFilmGrainIntensityMultiplier, endFilmGrainIntensityMultiplier, Mathf.SmoothStep(0f, 1f, t));
            //Fisheye
            fisheye.strengthX = Mathf.Lerp(startFisheye, endFisheye, Mathf.SmoothStep(0f, 1f, t));
            fisheye.strengthY = Mathf.Lerp(startFisheye, endFisheye, Mathf.SmoothStep(0f, 1f, t));

            elapsedTime += Time.deltaTime;
            yield return null;
        }
        while (elapsedTime < transitionTime);

        //Chromatic aberration
        vignetteAndChromaticAberration.chromaticAberration = endChromaticAberration;
        //Vignette
        vignetteAndChromaticAberration.intensity = endVignette;
        //Film grain
        noiseAndGrain.generalIntensity = endFilmGrain;
        noiseAndGrain.intensityMultiplier = endFilmGrainIntensityMultiplier;
        //Fisheye
        fisheye.strengthX = endFisheye;
        fisheye.strengthY = endFisheye;

        interpolatingMonitorEffects = false;
    }
}
