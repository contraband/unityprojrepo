﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : SecretElementBlink.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecretElementBlink : MonoBehaviour
{
    private Text myText;
    private Color myColor;
    private float alpha;
    private int lerpDir;

    private const float maxAlpha = 0.8f;
    private const float minAlpha = 0.2f;
    private const float lerpSpeed = 1f;

    void Awake()
    {
        myText = GetComponent<Text>();
        myColor = myText.color;
    }

    void Start()
    {
        alpha = maxAlpha;
    }

    void Update()
    {
        myText.color = new Color(myColor.r, myColor.g, myColor.b, alpha);

        if(alpha >= maxAlpha)
        {
            lerpDir = -1;
        }
        else if(alpha <= minAlpha)
        {
            lerpDir = 1;
        }

        alpha += Time.deltaTime * lerpSpeed * lerpDir;
    }
}
