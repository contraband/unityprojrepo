﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : PuzzleData.cs
// Author : Grant Joyner
// Purpose : A template for an individual puzzle in a hacking set
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PuzzleData
{
    public enum PuzzleDifficulty
    {
        Easiest,
        Easy,
        Normal,
        Hard
    };

    // Puzzle Difficulty - read-only
    private PuzzleDifficulty difficulty;
    private int normalCharAmount;
    private int ignoreCharAmount;
    private int firstCharAmount;
    private int lastCharAmount;
    private int switchCapsAmount;
    private int multiCharAmount;
    private string promptString;
    private List<CharElement> customPromptText;
    private List<CharSolutionElement> customPromptSolution;

    // Getters and setters
    public PuzzleDifficulty Difficulty { get { return difficulty; } }
    public int NormalCharAmount { get { return normalCharAmount; } }
    public int IgnoreCharAmount { get { return ignoreCharAmount; } }
    public int FirstCharAmount { get { return firstCharAmount; } }
    public int LastCharAmount { get { return lastCharAmount; } }
    public int SwitchCapsAmount { get { return switchCapsAmount; } }
    public int MultiCharAmount { get { return multiCharAmount; } }
    public List<CharElement> CustomPromptText { get { return customPromptText; } }
    public List<CharSolutionElement> CustomPromptSolution { get { return customPromptSolution; } }

    public string Text { get { return promptString; } }

    public PuzzleData(
        //int length_,
        PuzzleDifficulty difficulty_,
        int normalCharAmount_,
        int ignoreCharAmount_ = 0,
        int firstCharAmount_ = 0,
        int lastCharAmount_ = 0,
        int switchCapsAmount_ = 0,
        int multiCharAmount_ = 0,
        string prompt_ = null,
        List<CharElement> customPromptText_ = null,
        List<CharSolutionElement> customPromptSolution_ = null
        )
    {
        //length = length_;
        difficulty = difficulty_;
        normalCharAmount = normalCharAmount_;
        ignoreCharAmount = ignoreCharAmount_;
        firstCharAmount = firstCharAmount_;
        lastCharAmount = lastCharAmount_;
        switchCapsAmount = switchCapsAmount_;
        multiCharAmount = multiCharAmount_;
        promptString = prompt_;
        customPromptText = customPromptText_;
        customPromptSolution = customPromptSolution_;
    }
}

// OLD CODE - DONT DELETE MAYBE
/*
using UnityEngine;
using System.Collections;

public class PuzzleData
{
    public enum PuzzleDifficulty
    {
        Easiest,
        Easy,
        Normal,
        Hard
    };

    // Puzzle length - read-only
    // private int length;

    // Puzzle Difficulty - read-only
    private PuzzleDifficulty difficulty;

    private int normalCharAmount;

    // Number of IgnoreChars in prompt - read-only
    private int ignoreCharAmount;

    // FirstCharAmount in prompt - read-only
    private int firstCharAmount;

    // LastCharAmount in prompt - read-only
    private int lastCharAmount;

    // Number of SwitchCaps characters in the prompt - read-only
    private int switchCapsAmount;

    // Number of Multi characters in the prompt - read-only
    private int multiCharAmount;

    public PuzzleData(
        //int length_,
        PuzzleDifficulty difficulty_,
        int normalCharAmount_,
        int ignoreCharAmount_ = 0,
        int firstCharAmount_ = 0,
        int lastCharAmount_ = 0,
        int switchCapsAmount_ = 0,
        int multiCharAmount_ = 0)
    {
        //length = length_;
        difficulty = difficulty_;
        normalCharAmount = normalCharAmount_;
        ignoreCharAmount = ignoreCharAmount_;
        firstCharAmount = firstCharAmount_;
        lastCharAmount = lastCharAmount_;
        switchCapsAmount = switchCapsAmount_;
        multiCharAmount = multiCharAmount_;
    }

    // How many characters will the user have to type in?
    //public int Length
    //{
    //    get
    //    {
    //        return length;
    //    }
    //}

    // How difficult will the puzzle be (on a simple scale)?
    public PuzzleDifficulty Difficulty
    {
        get
        {
            return difficulty;
        }
    }

    public int NormalCharAmount
    {
        get
        {
            return normalCharAmount;
        }
    }

    // Read-only number of Ignore Characters
    public int IgnoreCharAmount
    {
        get
        {
            return ignoreCharAmount;
        }
    }

    // Read-only number of First Characters
    public int FirstCharAmount
    {
        get
        {
            return firstCharAmount;
        }
    }

    // Read-only number of Last Characters
    public int LastCharAmount
    {
        get
        {
            return lastCharAmount;
        }
    }

    // Read-only number of SwitchCaps characters
    public int SwitchCapsAmount
    {
        get
        {
            return switchCapsAmount;
        }
    }

    public int MultiCharAmount
    {
        get
        {
            return multiCharAmount;
        }
    }
}
*/