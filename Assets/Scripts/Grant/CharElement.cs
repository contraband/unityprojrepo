﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : CharElement.cs
// Author : Grant Joyner
// Purpose : Template class for colored characters to be displayed on the prompt
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class CharElement
{
    // Character to be displayed
    public char MyChar;
    // Is this a special character (does it have a rule attached to it)?
    // CharElements become complex when a color is assigned to them
    private bool complex = false;
    // What color is the character?
    private Color myColor = new Color();

    private bool switchChar;
    // How many times is this character entered (only for multi chars)
    private int multiChar;

    private int charID;

    // Getter for complex bool
    public bool Complex { get { return complex; } }

    // Getter and Setter for character color
    // Setter  makes the character a complex character
    public Color MyColor { get { return myColor; } set { SetMyColor(value); } }

    public bool SwitchChar { get { return switchChar; } }
    public int MultiChar { get { return multiChar; } }

    public int ID { get { return charID; } }

    // Accepts both a character and a color for the character (complex)
    public CharElement(Color myColor_, int charID_, char myChar_ = '\0', bool switchChar_ = false, int multiChar_ = 1)
    {
        MyChar = myChar_;
        SetMyColor(myColor_);
        switchChar = switchChar_;
        multiChar = multiChar_;
        charID = charID_;
    }

    /*** METHODS ***/
    // Sets the character color and makes it a complex character
    private void SetMyColor(Color color_)
    {
        myColor = color_;
        complex = true;
    }
}

/*
using UnityEngine;
using System.Collections;

public class CharElement
{
    // Character to be displayed
    public char MyChar;
    // Is this a special character (does it have a rule attached to it)?
    // CharElements become complex when a color is assigned to them
    private bool complex = false;
    // What color is the character?
    private Color myColor = new Color();
    // How many times is this character entered (only for multi chars)
    private int multiChar;

    // Getter for complex bool
    public bool Complex
    {
        get
        {
            return complex;
        }
    }
    
    // Getter and Setter for character color
    // Setter  makes the character a complex character
    public Color MyColor
    {
        get
        {
            return myColor;
        }
        set
        {
            SetMyColor(value);
        }
    }

    public int MultiChar
    {
        get
        {
            return multiChar;
        }
    }

    // CONSTRUCTORS
    // Constructor which accepts just a character (not complex)
    //public CharElement(char myChar_)
    //{
    //    MyChar = myChar_;
    //}

    // Accepts both a character and a color for the character (complex)
    public CharElement(char myChar_, Color myColor_, int multiChar_ = 1)
    {
        MyChar = myChar_;
        SetMyColor(myColor_);
        multiChar = multiChar_;
    }

    // METHODS
    // Sets the character color and makes it a complex character
    private void SetMyColor(Color color_)
    {
        myColor = color_;
        complex = true;
    }
}
*/
