﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackInfoCarrierLogic.cs
// Author : Grant Joyner
// Purpose : Carries information between infiltration and hacking
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class HackInfoCarrierLogic : MonoBehaviour
{
    private LevelData leveldata;
    private bool completed;

    private string currentScene;
    private string lastScene;

	void Awake()
	{
        DontDestroyOnLoad(gameObject);
        leveldata = null;
        completed = false;
        currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        lastScene = currentScene;
	}

    public LevelData Level
    {
        get
        {
            return leveldata;
        }

        set
        {
            leveldata = value;
        }
    }

    public bool Completed
    {
        set
        {
            completed = value;
        }
    }

    public string CurrentSceneName
    {
        set
        {
            lastScene = currentScene;
            currentScene = value;
            Debug.Log(currentScene);
        }
    }

    void Update()
    {
        if(currentScene != lastScene && currentScene == "HackingTerminal")
        {
            PassInfoToTTL();
        }
    }

    private void PassInfoToTTL()
    {
        Debug.Log("IM DEAD");
        //GameObject.FindObjectOfType<TestTerminalLogic>().CanHack = !completed;
        Destroy(gameObject);
    }
}
