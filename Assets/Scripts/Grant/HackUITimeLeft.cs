﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUITimeLeft.cs
// Author : Grant Joyner
// Purpose : Controls the dimensions of the Time Left bar in hacking
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class HackUITimeLeft : HackUIImageElement
{
    protected RectTransform myTransform;

    override public void Awake()
    {
        base.Awake();
        myTransform = gameObject.GetComponent<RectTransform>();
    }

    public void UpdateWidth(float width_)
    {
        myTransform.sizeDelta = new Vector2(width_, myTransform.sizeDelta.y);
    }
}
