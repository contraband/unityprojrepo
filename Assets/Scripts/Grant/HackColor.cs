﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackColor.cs
// Author : Grant Joyner
// Purpose : Static data and conversion method for colors in the hacking game
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class HackColor : MonoBehaviour
{
    static public readonly Color Gold = new Color(175, 163, 118);               // [Normal]
    static public readonly Color OffWhite = new Color(242, 237, 233);
    static public readonly Color Navy = new Color(39, 40, 45);
    static public readonly Color LightNavy = new Color(70, 72, 81);
    static public readonly Color Red = new Color(150, 40, 27);                  // [Skip / Error]
    static public readonly Color Green = new Color(38, 166, 91);                // [Success / First]
    static public readonly Color Teal = new Color(65, 131, 215);                // [Switch]
    static public readonly Color Purple = new Color(103, 65, 114);              // [Last]
    static public readonly Color Salmon = new Color(226, 106, 106);             // [Switch]
    static public readonly Color Orange = new Color(243, 156, 18);              // [Repeat]

    static public Color ConvertToFloat(Color color_)
    {
        return new Color(color_.r / 255.0f, color_.g / 255.0f, color_.b / 255.0f);
    }
}
