﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUIElement.cs
// Author : Grant Joyner
// Purpose : Abstract template class for the text UI elements in hacking
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class HackUIElement : MonoBehaviour
{
    protected Text myText;

    public virtual void Awake()
    {
        myText = this.GetComponent<Text>();
    }

    public virtual void SetText(string text_)
    {
        if(!myText)
        {
            myText = this.GetComponent<Text>();
        }

        myText.text = text_;
    }

    public virtual void SetColor(Color color_)
    {
        myText.color = HackColor.ConvertToFloat(color_);
    }

    public virtual void SetFontSize(int fontsize_)
    {
        myText.fontSize = fontsize_;
    }
}
