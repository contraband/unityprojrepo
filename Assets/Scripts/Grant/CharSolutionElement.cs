﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : CharSolutionElement.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharSolutionElement
{
    private char myChar;
    private int myID;

    public char Char { get { return myChar; } set { myChar = value; } }
    public int ID { get { return myID; } }

    public CharSolutionElement(char myChar_, int myID_)
    {
        myChar = myChar_;
        myID = myID_;
    }
}
