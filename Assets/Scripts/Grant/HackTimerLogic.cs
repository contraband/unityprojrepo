﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackTimerLogic.cs
// Author : Grant Joyner
// Purpose : Logic for the numeric hacking timer in the early hacking game
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

public class HackTimerLogic : MonoBehaviour
{
    public float Timer;
    public bool bActive;

    private HackUITimer hackuiTimer;

    void Awake ()
    {
        hackuiTimer = GetComponent<HackUITimer>();
    }

    // Use this for initialization
    void Start ()
    {
        bActive = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        /* If timer is active, decrement timer */
        if (bActive)
        {
            Timer -= Time.deltaTime;

            /* Set PuzzleTimer to 0 if OoB */
            if (Timer <= 0)
            {
                Timer = 0;
                bActive = false;
            }

            /* Update timer text */
            UpdateTimerText();
        }
    }

    public void Activate()
    {
        bActive = true;
    }

    public void Deactivate()
    {
        bActive = false;
    }

    /*
    public void Reset()
    {
        Timer = HACK_TIMER_SET;
        UpdateTimerText();
        bActive = true;
    }
    */

    public void AddTime(float value_, bool perfect_)
    {
        Timer += value_;
        UpdateTimerText(value_, perfect_);
    }

    public void SetTime(float value_)
    {
        Timer = value_;
        UpdateTimerText();
    }

    private void UpdateTimerText()
    {
        /* Create hacktimerText string */
        StringBuilder sb = new StringBuilder();
        sb.Append("TIME: ");
        sb.Append(Timer.ToString("0.00"));

        /* Set hacktimerText text */
        SetTimerText(sb.ToString());

        if (Timer != 0)
        {
            hackuiTimer.SetColor(Color.white);
        }
        else
        {
            hackuiTimer.SetColor(Color.red);
        }
    }

    private void UpdateTimerText(float bonus_, bool perfect_)
    {
        /* Create hacktimerText string */
        StringBuilder sb = new StringBuilder();
        sb.Append("TIME: ");
        sb.Append(Timer.ToString("0.00"));
        sb.Append(" <color=#ffffffff>+");
        sb.Append(bonus_);
        if(perfect_)
        {
            sb.Append(" PERFECT!");
        }
        sb.Append("</color>");

        /* Set hacktimerText text */
        SetTimerText(sb.ToString());

        if (Timer != 0)
        {
            hackuiTimer.SetColor(Color.white);
        }
        else
        {
            hackuiTimer.SetColor(Color.red);
        }
    }

    public void SetTimerText(string text_)
    {
        hackuiTimer.SetText(text_);
    }
}
