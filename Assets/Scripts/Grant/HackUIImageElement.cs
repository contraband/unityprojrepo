﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUIImageElement.cs
// Author : Grant Joyner
// Purpose : Abstract template class for image UI elements in the hacking game
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class HackUIImageElement : MonoBehaviour
{
    protected Image myImage;

    public virtual void Awake()
    {
        myImage = this.GetComponent<Image>();
    }

    public virtual void UpdateColor(Color color_)
    {
        myImage.color = new Color(color_.r, color_.g, color_.b);
    }
}
