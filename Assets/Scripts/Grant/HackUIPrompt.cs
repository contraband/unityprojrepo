﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUIPrompt.cs
// Author : Grant Joyner
// Purpose : Places colored text on the Prompt UI element for the hacking game
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class HackUIPrompt : HackUIElement
{
    private StringBuilder sb;

    public override void Awake()
    {
        base.Awake();
        sb = new StringBuilder();
    }

    public void UpdatePrompt(List<CharElement> prompt_)
    {
        sb.Remove(0, sb.Length);

        for (int i = 0; i < prompt_.Count; ++i)
        {
            if (prompt_[i].Complex)
            {
                int red =   (int)prompt_[i].MyColor.r;
                int green = (int)prompt_[i].MyColor.g;
                int blue =  (int)prompt_[i].MyColor.b;

                sb.Append("<color=#");
                sb.Append(red.ToString("X2"));
                sb.Append(green.ToString("X2"));
                sb.Append(blue.ToString("X2"));

                sb.Append("FF>");
                sb.Append(prompt_[i].MyChar);
                sb.Append("</color>");
            }
            else
            {
                sb.Append(prompt_[i].MyChar);
            }
        }

        SetText(sb.ToString());
    }
}
