﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUIPuzzleNum.cs
// Author : Grant Joyner
// Purpose : Controls the PuzzleNum UI element in hacking
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Text;

public class HackUIPuzzleNum : HackUIElement
{	
	public void UpdatePuzzleNum(int puzzlenum_, int puzzletotal_)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append(puzzlenum_);
        sb.Append("/");
        sb.Append(puzzletotal_);

        /* Set PuzzleNumText to new text */
        SetText(sb.ToString());
    }
}
