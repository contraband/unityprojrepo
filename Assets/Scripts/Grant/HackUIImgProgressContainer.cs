﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUIImgProgressContainer.cs
// Author : Grant Joyner
// Purpose : Adjusts the width of the progress bar container in the hacking game
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class HackUIImgProgressContainer : HackUIImageElement
{
    protected RectTransform myTransform;

    override public void Awake()
    {
        base.Awake();
        myTransform = this.GetComponent<RectTransform>();
    }

    public void UpdateWidth(float width_)
    {
        myTransform.sizeDelta = new Vector2(width_, myTransform.sizeDelta.y);
    }
}
