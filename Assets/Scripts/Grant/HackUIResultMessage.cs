﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUIResultMessage.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HackUIResultMessage : MonoBehaviour
{
    private Text resultMessage;

    void Awake()
    {
        resultMessage = gameObject.GetComponent<Text>();
    }

    public void SetResultValue(float value_)
    {
        resultMessage.text = resultMessage.text.Replace("%", value_.ToString());
    }
}