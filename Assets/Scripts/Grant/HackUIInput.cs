﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUIInput.cs
// Author : Grant Joyner
// Purpose : Controls the Input UI text element in the hacking game
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class HackUIInput : HackUIElement
{
    private StringBuilder sb;

    public override void Awake()
    {
        base.Awake();
        sb = new StringBuilder();
    }

    public void UpdateInput(List<char> currentInput_)
    {
        if(sb == null)
        {
            sb = new StringBuilder();
        }

        sb.Remove(0, sb.Length);

        for (int i = 0; i < currentInput_.Count; ++i)
        {
            sb.Append(currentInput_[i]);
        }

        SetText(sb.ToString());
    }
}
