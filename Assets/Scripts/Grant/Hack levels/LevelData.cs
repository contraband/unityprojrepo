// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : LevelData.cs
// Author : Grant Joyner
// Purpose : Template for LevelData in the hacking game
// All content � 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public abstract class LevelData : PuzzleSetData
{
    public float StartTime;
    public string SecretLetters;
    public int TerminalID;
}
