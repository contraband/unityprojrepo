﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter3DVNarrative.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter3DVNarrative : NarrativeLevelData
{
    public Chapter3DVNarrative()
    {
        Puzzles = new PuzzleData[]
        {
            // THIS IS A DUMMY PROMPT
            // I don't know why but it always gets skipped
            // Leave it in
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "hello"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "heLLo"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "iKNOW"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                7,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "WHATyou"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                8,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "areDOING"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                9,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "youareNOT"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                9,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "inCONTROL"
                )
         };
    }
}
