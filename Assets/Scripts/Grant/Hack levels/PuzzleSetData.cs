﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : PuzzleSetData.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PuzzleSetData : MonoBehaviour
{
    public PuzzleData[] Puzzles;
}
