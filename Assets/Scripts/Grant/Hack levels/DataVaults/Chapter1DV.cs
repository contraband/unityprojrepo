﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter1DV.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter1DV : LevelData
{
	public Chapter1DV()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                7,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "skipRED"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                8,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "REDisBAD"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                6,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                )
         };

        StartTime = 30.00f;
    }
}