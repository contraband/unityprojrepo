﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter4DV.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter4DV : LevelData
{
	public Chapter4DV()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                11,      //Normal
                0,      //Ignore
                1,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "GREENisFIRST"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                11,      //Normal
                0,      //Ignore
                3,      //First
                0,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "typeGREENfirst"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                0,      //Ignore
                2,      //First
                0,      //Last
                0,      //SwitchCaps
                1       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                6,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                2,      //Ignore
                2,      //First
                0,      //Last
                0,      //SwitchCaps
                1       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                1,      //Ignore
                1,      //First
                0,      //Last
                1,      //SwitchCaps
                1       //Multi
                )
         };

        StartTime = 30.00f;
    }
}