﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter5DV.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter5DV : LevelData
{
	public Chapter5DV()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                11,      //Normal
                0,      //Ignore
                0,      //First
                1,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "PURPLEisLAST"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                1,      //Ignore
                0,      //First
                1,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                0,      //Ignore
                0,      //First
                2,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                0,      //Ignore
                2,      //First
                2,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                0,      //Ignore
                0,      //First
                1,      //Last
                0,      //SwitchCaps
                1       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                0,      //Ignore
                0,      //First
                2,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                3,      //Ignore
                2,      //First
                2,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),
         };

        StartTime = 30.00f;
    }
}