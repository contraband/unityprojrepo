﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter6DV.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter6DV : LevelData
{
	public Chapter6DV()
    {
        Puzzles = new PuzzleData[]
        {
            // All First + ignore (01)
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                0,      //Normal
                2,      //Ignore
                5,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            // First/Normal + ignore (02)
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                2,      //Ignore
                5,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            // First + Switch/Multi (03)
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                0,      //Normal
                0,      //Ignore
                5,      //First
                0,      //Last
                2,      //SwitchCaps
                1       //Multi
                ),

            // Last + Switch/Multi (04)
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                0,      //Normal
                0,      //Ignore
                0,      //First
                5,      //Last
                2,      //SwitchCaps
                1       //Multi
                ),

            // First/Switch/Last
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                0,      //Normal
                0,      //Ignore
                2,      //First
                2,      //Last
                3,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                0,      //Normal
                2,      //Ignore
                1,      //First
                2,      //Last
                6,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                0,      //Ignore
                3,      //First
                3,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                1,      //Normal
                1,      //Ignore
                2,      //First
                2,      //Last
                2,      //SwitchCaps
                1       //Multi
                )
        };

        StartTime = 30.00f;
    }
}