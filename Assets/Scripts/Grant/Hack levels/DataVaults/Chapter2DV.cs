﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter2DV.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter2DV : LevelData
{
	public Chapter2DV()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                8,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                0,       //Multi
                "UPPERCASE"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                7,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                2,      //SwitchCaps
                0,       //Multi
                "lowercase"
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                7,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

             new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                8,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                2,      //SwitchCaps
                0       //Multi
                )
         };

        StartTime = 30.00f;
    }
}