﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter4Terminal.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter4Terminal : LevelData
{
	public Chapter4Terminal()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                1       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                1       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                2,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                1       //Multi
                )
        };

        StartTime = 15.00f;
    }
}