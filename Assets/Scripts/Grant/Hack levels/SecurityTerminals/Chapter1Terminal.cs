﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter1Terminal.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter1Terminal : LevelData
{
	public Chapter1Terminal()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Easiest,
                8,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "TYPEthis"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Easy,
                8,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "pAsSwOrD"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Easy,
                4,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                )
        };

        StartTime = 15.00f;
    }
}