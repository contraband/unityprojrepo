﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter6_2Terminal.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter6_2Terminal : LevelData
{
	public Chapter6_2Terminal()
    {
        Puzzles = new PuzzleData[]
        {
            // Mash all (except Multi)
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                2,      //Normal
                2,      //Ignore
                2,      //First
                2,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

            // Non-normal mash
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                0,      //Normal
                1,      //Ignore
                3,      //First
                3,      //Last
                2,      //SwitchCaps
                0       //Multi
                ),

            // Mash all (except Switch)
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                2,      //Normal
                1,      //Ignore
                2,      //First
                2,      //Last
                0,      //SwitchCaps
                1       //Multi
                )
        };

        StartTime = 15.00f;
    }
}