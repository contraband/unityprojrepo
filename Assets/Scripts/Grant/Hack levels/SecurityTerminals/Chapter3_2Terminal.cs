﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter3_2Terminal.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter3_2Terminal : LevelData
{
	public Chapter3_2Terminal()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                6,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                7,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                2,      //SwitchCaps
                0       //Multi
                )
        };

        StartTime = 15.00f;
    }
}