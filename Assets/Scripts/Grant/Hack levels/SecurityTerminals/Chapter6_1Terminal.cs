﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Chapter6_1Terminal.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter6_1Terminal : LevelData
{
	public Chapter6_1Terminal()
    {
        Puzzles = new PuzzleData[]
        {
            // Ignore overload
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                8,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            // First/last mish-mash
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                0,      //Normal
                0,      //Ignore
                3,      //First
                3,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            // Switch overload
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                0,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                7,      //SwitchCaps
                0       //Multi
                ),

            // First/Normal/Last mish-mash
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                0,      //Ignore
                3,      //First
                3,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),
            
            // Mash of everything except first/last
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                2,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                2,      //SwitchCaps
                1       //Multi
                )
        };

        StartTime = 15.00f;
    }
}