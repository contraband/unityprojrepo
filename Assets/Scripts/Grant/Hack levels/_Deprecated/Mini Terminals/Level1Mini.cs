﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Level1Mini.cs
// Author : Grant Joyner
// Purpose : Level data for the mini terminals of level 1
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class Level1Mini : LevelData
{
    public Level1Mini()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                7,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                0       //Multi
                )
        };

        StartTime = 60.00f;
    }
}
