﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level3DV : LevelData
{
    public Level3DV()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                10,      //Normal
                0,      //Ignore
                1,      //First
                0,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "BLUEisFIRST"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                10,      //Normal
                0,      //Ignore
                3,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "typeBLUEfirst"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                0,      //Ignore
                2,      //First
                0,      //Last
                0,      //SwitchCaps
                1       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                6,      //Normal
                1,      //Ignore
                1,      //First
                0,      //Last
                0,//1,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                2,      //Ignore
                2,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                1,      //Ignore
                2,      //First
                0,      //Last
                0,//1,      //SwitchCaps
                1       //Multi
                )
        };

        StartTime = 60.00f;
    }
}