﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDV : LevelData
{
    public TutorialDV()
    {
        Puzzles = new PuzzleData[]
        {
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                7,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "skipRED"
                ),

            /*
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                8,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "REDiSBAD"
                ),
            */

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                8,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                1,      //SwitchCaps
                0,       //Multi
                "UPPERCASE"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                0,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                1,       //Multi
                "REPEAT"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                10,      //Normal
                0,      //Ignore
                3,      //First
                0,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "typeBLUEfirst"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                11,      //Normal
                0,      //Ignore
                0,      //First
                3,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "typePURPLElast"
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                5,      //Normal
                1,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                6,      //Normal
                2,      //Ignore
                0,      //First
                0,      //Last
                0,      //SwitchCaps
                0       //Multi
                )
        };

        StartTime = 60.00f;
    }
}