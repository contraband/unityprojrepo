﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level4DV : LevelData
{
    public Level4DV()
    {
        Puzzles = new PuzzleData[]
        {
            //1
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                11,      //Normal
                0,      //Ignore
                0,      //First
                1,      //Last
                0,      //SwitchCaps
                0,       //Multi
                "PURPLEisLAST"
                ),

            //2
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                11,      //Normal
                0,      //Ignore
                0,      //First
                3,      //Last
                0,      //SwitchCaps
                0,      //Multi
                "typePURPLElast"
                ),

            //3
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                1,      //Ignore
                0,      //First
                1,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            //4
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                0,      //Ignore
                0,      //First
                2,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

            //5
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                0,      //Ignore
                2,      //First
                2,      //Last
                0,      //SwitchCaps
                0       //Multi
                ),

            //6
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                4,      //Normal
                0,      //Ignore
                0,      //First
                1,      //Last
                0,      //SwitchCaps
                1       //Multi
                ),

            //7
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                0,      //Ignore
                0,      //First
                2,      //Last
                1,      //SwitchCaps
                0       //Multi
                ),

            //8
            new PuzzleData(
                PuzzleData.PuzzleDifficulty.Hard,
                3,      //Normal
                0,      //Ignore
                0,      //First
                2,      //Last
                1,      //SwitchCaps
                0       //Multi
                )
        };

        StartTime = 60.00f;
    }
}