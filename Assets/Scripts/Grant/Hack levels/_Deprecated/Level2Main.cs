﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Level2Main.cs
// Author : Grant Joyner
// Purpose : Level data for the main terminal of level 2
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class Level2Main : LevelData
{
    public Level2Main()
    {
        Puzzles = new PuzzleData[]
        {
            //             Length                           Difficulty                        Rule          IgnoreCharAmount    LeadingChar     EndingChar
            //----------------------------------------------------------------------------------------------------------------------------------------------
            //new PuzzleData(3,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         3,                  0,          0),         //01
            //new PuzzleData(5,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         1,                  0,          0),         //02
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Easiest,    PuzzleData.PuzzleRule.None,         0,                  1,           0),         //03
            //new PuzzleData(5,   PuzzleData.PuzzleDifficulty.Easiest,    PuzzleData.PuzzleRule.None,         0,                  1,           0),         //04
            //new PuzzleData(5,   PuzzleData.PuzzleDifficulty.Easiest,    PuzzleData.PuzzleRule.None,         2,                  1,           0),         //05
            //new PuzzleData(5,   PuzzleData.PuzzleDifficulty.Easy,       PuzzleData.PuzzleRule.None,         3,                  1,           0),         //06
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         6,                  1,           0),         //07
            //new PuzzleData(6,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         4,                  1,           0),         //08
        };

        StartTime = 30.00f;
    }
}
