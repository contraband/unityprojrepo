﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Level1Main.cs
// Author : Grant Joyner
// Purpose : Level data for the main terminal of level 1
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class Level1Main : LevelData
{
    public Level1Main()
    {
        Puzzles = new PuzzleData[]
        {
            //             Length                           Difficulty                        Rule          IgnoreCharAmount    LeadingChar     EndingChar
            //----------------------------------------------------------------------------------------------------------------------------------------------
            //new PuzzleData(3,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         0,                  0,          0),         //01
            //new PuzzleData(3,   PuzzleData.PuzzleDifficulty.Easiest,    PuzzleData.PuzzleRule.None,         1,                  0,          0),         //02
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Normal,     PuzzleData.PuzzleRule.None,         2,                  0,          0),         //03
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         2,                  0,          0),         //04
            //new PuzzleData(5,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         3,                  0,          0),         //05
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Normal,     PuzzleData.PuzzleRule.None,         6,                  0,          0),         //06
        };

        StartTime = 30.00f;
    }
}
