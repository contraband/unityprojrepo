﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : Level3Main.cs
// Author : Grant Joyner
// Purpose : Level data for the main terminal of level 3
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class Level3Main : LevelData
{
    public Level3Main()
    {
        Puzzles = new PuzzleData[]
        {
            //             Length                           Difficulty                        Rule          IgnoreCharAmount    FirstCharAmount     LastCharAmount
            //----------------------------------------------------------------------------------------------------------------------------------------------
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         0,                  1,                  0),             //01
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Easiest,    PuzzleData.PuzzleRule.None,         0,                  0,                  1),             //02
            //new PuzzleData(5,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         0,                  1,                  1),             //03
            //new PuzzleData(6,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         0,                  3,                  0),             //04
            //new PuzzleData(6,   PuzzleData.PuzzleDifficulty.Easiest,    PuzzleData.PuzzleRule.None,         0,                  0,                  3),             //05
            //new PuzzleData(8,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         0,                  3,                  3),             //06
            //new PuzzleData(7,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         0,                  3,                  3),             //07
            //new PuzzleData(8,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         0,                  3,                  3),             //08
            //new PuzzleData(9,   PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         2,                  3,                  3),             //09
            //new PuzzleData(12,  PuzzleData.PuzzleDifficulty.Hard,       PuzzleData.PuzzleRule.None,         4,                  4,                  4)              //10
        };

        StartTime = 30.00f;
    }
}
