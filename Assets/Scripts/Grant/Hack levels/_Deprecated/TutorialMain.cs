﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : TutorialMain.cs
// Author : Grant Joyner
// Purpose : Level data for the main terminal of the tutorial
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class TutorialMain : LevelData
{
    public TutorialMain()
    {
        Puzzles = new PuzzleData[]
        {
            //             Length                           Difficulty                        Rule          IgnoreCharAmount    LeadingChar     EndingChar
            //----------------------------------------------------------------------------------------------------------------------------------------------
            //new PuzzleData(3,   PuzzleData.PuzzleDifficulty.Easiest,    PuzzleData.PuzzleRule.None,         0,                  0,          0),         //01
            //new PuzzleData(3,   PuzzleData.PuzzleDifficulty.Easy,       PuzzleData.PuzzleRule.None,         0,                  0,          0),         //02
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Easy,       PuzzleData.PuzzleRule.None,         0,                  0,          0),         //03
            //new PuzzleData(4,   PuzzleData.PuzzleDifficulty.Normal,     PuzzleData.PuzzleRule.None,         0,                  0,          0),         //04
            //new PuzzleData(6,   PuzzleData.PuzzleDifficulty.Normal,     PuzzleData.PuzzleRule.None,         0,                  0,          0),         //05
        };

        StartTime = 30.00f;
    }
}
