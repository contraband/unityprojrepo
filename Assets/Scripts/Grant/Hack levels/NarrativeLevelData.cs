﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : NarrativeLevelData.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NarrativeLevelData : PuzzleSetData
{
}
