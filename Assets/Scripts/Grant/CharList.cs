﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : CharList.cs
// Author : Grant Joyner
// Purpose : Static data of legal characters to be used in hacking puzzles
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class CharList : MonoBehaviour
{
    //public static char[] chars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'r', 's', 't', 'u', 'v', 'w', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

    //public static char[] chars = { 'a', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 's' };

    /*
     * GET RID OF NUMBERS
     */
    public static char[] row1 = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    public static char[] row2 = { 'q', 'e', 'r', 't', 'y', 'u', 'p' };
    public static char[] row3 = { 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k' };
    public static char[] row4 = { 'z', 'x', 'c', 'v', 'b', 'n', 'm' };

    /*
    static public bool IsChar(char char_)
    {
        bool result = false;

        for (int i = 0; i < chars.Length; ++i)
        {
            if (char_ == chars[i])
            {
                result = true;
                break;
            }
        }

        return result;
    }
    */
}