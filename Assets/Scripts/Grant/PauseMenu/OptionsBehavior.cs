﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : OptionsBehavior.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsBehavior : MonoBehaviour
{
    private Button FullscreenButton;
    private Text FullscreenButtonText;
    //private Button ScreenResolutionButton;
    //private Text ScreenResolutionButtonText;
    private Button AudioButton;
    private Text AudioButtonText;

    void Awake()
    {
        FullscreenButton = transform.FindChild("Fullscreen").GetComponent<Button>();
        FullscreenButtonText = FullscreenButton.transform.FindChild("Text").GetComponent<Text>();
        FullscreenButton.onClick.AddListener(OnFullscreenButtonClick);

        //ScreenResolutionButton = transform.FindChild("ScreenResolution").GetComponent<Button>();
        //ScreenResolutionButtonText = ScreenResolutionButton.transform.FindChild("Text").GetComponent<Text>();
        //ScreenResolutionButton.onClick.AddListener(OnScreenResolutionButtonClick);

        AudioButton = transform.FindChild("Audio").GetComponent<Button>();
        AudioButtonText = AudioButton.transform.FindChild("Text").GetComponent<Text>();
        AudioButton.onClick.AddListener(OnAudioButtonClick);
    }

    void Start()
    {
        SetFullscreenButtonText();
        //SetScreenResolutionButtonText();
        SetAudioButtonText();
    }

    void OnFullscreenButtonClick()
    {
        switch(OptionsManager.GetCurrentFullscreenState())
        {
            case OptionsManager.FullscreenState.FULLSCREEN:
                OptionsManager.SetFullscreen(OptionsManager.FullscreenState.WINDOWED);
                SetFullscreenButtonText(OptionsManager.FullscreenState.WINDOWED);
                break;

            case OptionsManager.FullscreenState.WINDOWED:
                OptionsManager.SetFullscreen(OptionsManager.FullscreenState.FULLSCREEN);
                SetFullscreenButtonText(OptionsManager.FullscreenState.FULLSCREEN);
                break;
        }
    }

    /*
    void OnScreenResolutionButtonClick()
    {
        switch (OptionsManager.GetCurrentScreenResolutionState())
        {
            case OptionsManager.ScreenResolutionState.SR_1920_1080:
                OptionsManager.SetScreenResolution(OptionsManager.ScreenResolutionState.SR_1080_720);
                SetScreenResolutionButtonText(OptionsManager.ScreenResolutionState.SR_1080_720);
                break;

            case OptionsManager.ScreenResolutionState.SR_1080_720:
                OptionsManager.SetScreenResolution(OptionsManager.ScreenResolutionState.SR_1920_1080);
                SetScreenResolutionButtonText(OptionsManager.ScreenResolutionState.SR_1920_1080);
                break;

            case OptionsManager.ScreenResolutionState.OTHER:
                OptionsManager.SetScreenResolution(OptionsManager.ScreenResolutionState.SR_1920_1080);
                SetScreenResolutionButtonText(OptionsManager.ScreenResolutionState.SR_1920_1080);
                break;
        }
    }
    */

    void OnAudioButtonClick()
    {
        switch (OptionsManager.CurrentAudioState)
        {
            case OptionsManager.AudioState.ALL_ON:
                AudioObject.ToggleMusic(false);
                OptionsManager.CurrentAudioState = OptionsManager.AudioState.SFX_ONLY;
                break;

            case OptionsManager.AudioState.SFX_ONLY:
                AudioListener.volume = 0;
                OptionsManager.CurrentAudioState = OptionsManager.AudioState.ALL_OFF;
                break;

            case OptionsManager.AudioState.ALL_OFF:
                AudioListener.volume = 1;
                AudioObject.ToggleMusic(true);
                OptionsManager.CurrentAudioState = OptionsManager.AudioState.ALL_ON;
                break;
        }

        SetAudioButtonText();
    }
    
    void SetFullscreenButtonText(OptionsManager.FullscreenState state_ = OptionsManager.FullscreenState.NULL)
    {
        string fText = null;

        if(state_ != OptionsManager.FullscreenState.NULL)
        {
            fText = OptionsManager.FullscreenStateName[(int)state_];
        }
        else
        {
            fText = OptionsManager.FullscreenStateName[(int)OptionsManager.GetCurrentFullscreenState()];
        }

        FullscreenButtonText.text = fText;
    }

    /*
    void SetScreenResolutionButtonText(OptionsManager.ScreenResolutionState state_ = OptionsManager.ScreenResolutionState.NULL)
    {
        string srText = null;

        if(state_ != OptionsManager.ScreenResolutionState.NULL)
        {
            srText = OptionsManager.ScreenResolutionStateName[(int)state_];
        }
        else
        {
            if (OptionsManager.GetCurrentScreenResolutionState() != OptionsManager.ScreenResolutionState.OTHER)
            {
                srText = OptionsManager.ScreenResolutionStateName[(int)OptionsManager.GetCurrentScreenResolutionState()];
            }
            else
            {
                srText = "Other (" + Screen.currentResolution.width.ToString() + " x " + Screen.currentResolution.height.ToString() + ")";
            }
        }

        ScreenResolutionButtonText.text = srText;
    }
    */

    void SetAudioButtonText()
    {
        AudioButtonText.text = OptionsManager.AudioStateName[(int)OptionsManager.CurrentAudioState];
    }
}
