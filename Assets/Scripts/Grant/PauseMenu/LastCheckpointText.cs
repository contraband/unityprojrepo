﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : LastCheckpointText.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LastCheckpointText : MonoBehaviour
{
    private Text myText;

	// Use this for initialization
	void Awake()
    {
        myText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update()
    {
        string message = "(" + GameManager.GetTimeSinceCheckpoint() + ")";
		myText.text = message;
    }
}
