﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : OptionsManager.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsManager
{
    public enum FullscreenState
    {
        NULL = -1,
        FULLSCREEN,
        WINDOWED
    }

    static public readonly string[] FullscreenStateName = { "FULLSCREEN", "WINDOWED" };

    /*
    public enum ScreenResolutionState
    {
        NULL = -1,
        SR_1920_1080,
        SR_1080_720,
        OTHER
    }

    static public readonly string[] ScreenResolutionStateName = { "1920 x 1080", "1080 x 720" };
    */

    public enum AudioState
    {
        NULL = -1,
        ALL_ON,
        SFX_ONLY,
        ALL_OFF
    }
    static public readonly string[] AudioStateName = { "ALL ON", "SFX ONLY", "ALL OFF" };

    static public AudioState CurrentAudioState;

    static public void SetFullscreen(FullscreenState state_)
    {
        Screen.fullScreen = !Screen.fullScreen;
    }

    /*
    static public void SetScreenResolution(ScreenResolutionState state_)
    {
        switch(state_)
        {
            case ScreenResolutionState.SR_1920_1080:
                Screen.SetResolution(1920, 7210800, Screen.fullScreen);
                break;

            case ScreenResolutionState.SR_1080_720:
                Screen.SetResolution(1080, 720, Screen.fullScreen);
                break;
        }
    }
    */

    static public FullscreenState GetCurrentFullscreenState()
    {
        FullscreenState state_ = FullscreenState.NULL;

        if(Screen.fullScreen)
        {
            state_ = FullscreenState.FULLSCREEN;
        }
        else
        {
            state_ = FullscreenState.WINDOWED;
        }

        return state_;
    }

    /*
    static public ScreenResolutionState GetCurrentScreenResolutionState()
    {
        ScreenResolutionState state_ = ScreenResolutionState.NULL;

        if (Screen.currentResolution.width == 1920 &&
            Screen.currentResolution.height == 1080)
        {
            state_ = ScreenResolutionState.SR_1920_1080;
        }
        else if (Screen.currentResolution.width == 1080 &&
            Screen.currentResolution.height == 720)
        {
            state_ = ScreenResolutionState.SR_1080_720;
        }
        else
        {
            state_ = ScreenResolutionState.OTHER;
        }

        return state_;
    }
    */
}
