﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : RestartCheckpointButtonBehavior.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestartCheckpointButtonBehavior : MonoBehaviour
{
    private Button myButton;
    private Text buttonText;

    void Awake()
    {
        myButton = GetComponent<Button>();
        buttonText = transform.FindChild("Text").GetComponent<Text>();
    }

    void Start()
    {
        if (!GameManager.checkpointSaved)
        {
            myButton.interactable = false;
            buttonText.color = new Color(buttonText.color.r, buttonText.color.g, buttonText.color.b, 0.5f);
        }
	}
}
