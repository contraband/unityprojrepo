﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackUIPreActiveMessages.cs
// Author : Grant Joyner
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HackUIPreActiveMessages : MonoBehaviour
{
    private Text punishmentMessage;
    private Text rewardMessage;

    void Awake()
    {
        Transform punishmentMessageObj = gameObject.transform.FindChild("PunishmentMessage");

        if(punishmentMessageObj)
        {
            punishmentMessage = punishmentMessageObj.gameObject.GetComponent<Text>();
        }

        Transform rewardMessageObj = gameObject.transform.FindChild("RewardMessage");

        if (rewardMessageObj)
        {
            rewardMessage = rewardMessageObj.gameObject.GetComponent<Text>();
        }
    }

    public void SetResultValues(float punishment_, float reward_)
    {
        if(punishmentMessage)
        {
            punishmentMessage.text = punishmentMessage.text.Replace("%", punishment_.ToString());
        }
        if(rewardMessage)
        {
            rewardMessage.text = rewardMessage.text.Replace("%", reward_.ToString());
        }
    }
}