﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackTimeLogic.cs
// Author : Grant Joyner
// Purpose : Logic for the current timer in the hacking game
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;

public class HackTimeLogic : MonoBehaviour
{
    public float maxTime;

    private HackUITimeLeft timeleft;
    private Text timeText;
    private float fTimer;
    public float Timer { get { return fTimer; } }
    public CultureInfo ci = new CultureInfo("en-us");

    public void Awake()
    {
        timeleft = GetComponentInChildren<HackUITimeLeft>();
        timeText = gameObject.transform.FindChild("TimerBackground").FindChild("TimerText").GetComponent<Text>();
    }

    public void Start()
    {
        fTimer = 0.0f;
    }

    public void UpdateTimeLeft()
    {
        fTimer -= Time.deltaTime;
        if(fTimer < 0)
        {
            fTimer = 0;
        }
        AdjustTimeLeftWidth();
    }

    public void SetTime(float time_)
    {
        fTimer = time_;
        AdjustTimeLeftWidth();
    }

    public void AddTime(float time_, bool perfect_)
    {
        fTimer += time_;
        if(fTimer > maxTime)
        {
            fTimer = maxTime;
        }
        AdjustTimeLeftWidth();
    }

    private void AdjustTimeLeftWidth()
    {
        float barWidth = fTimer * 17f;
        timeleft.UpdateWidth(barWidth);
        timeText.text = fTimer.ToString("N", ci);
    }
}
