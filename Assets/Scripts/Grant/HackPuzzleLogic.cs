﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : HackPuzzleLogic.cs
// Author : Grant Joyner
// Purpose : Handles the generation and logic of hacking puzzles from level data
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class HackPuzzleLogic : MonoBehaviour
{
    /*** GAMEPLAY CONSTANTS ***/
    // Amount of time player must wait after making a mistake
    private const float ERROR_TIMER_WAIT = 0.5f;
    // Amount of time between completing prompt and generating new prompt
    private const float SUCCESS_TIMER_WAIT = 0.75f;
    // Amount of time gained for perfect and normal completions
    private const float PERFECT_TIME_BONUS = 10.00f;
    private const float NORMAL_TIME_BONUS = 4.5f;
    private const float HACKING_TIME_BOOST = 10.0f;

    /*** COLORS ***/
    // Colors of different UI text
    private readonly Color cWelcomeMessage = HackColor.Gold;
    private readonly Color cInformationMessage = HackColor.Navy;
    private readonly Color cInputNormal = HackColor.OffWhite;
    private readonly Color cInputError = HackColor.Red;
    private readonly Color cInputSuccess = HackColor.Green;
    private readonly Color cVictoryMessage = HackColor.Green;
    private readonly Color cFailureMessage = HackColor.Red;
    private readonly Color cProgressCompleted = HackColor.Green;
    private readonly Color cProgressCurrent = HackColor.OffWhite;
    private readonly Color cProgressUpcoming = HackColor.LightNavy;
    // Color data for each of the unique character rules
    private readonly Color cPromptNormal = HackColor.Gold;
    private readonly Color cPromptFirst = HackColor.Green;
    private readonly Color cPromptLast = HackColor.Purple;
    private readonly Color cPromptIgnore = HackColor.Red;
    private readonly Color cPromptSwitch = HackColor.Teal;
    private readonly Color cPromptRepeat = HackColor.Orange;

    /*** LEVEL DATA ***/
    // GameManager
    private GameManager gameManager;
    // LevelManager
    private LevelManager levelManager;
    // The LevelData file that is read from to generate the puzzles
    private LevelData CurrentLevel;
    private NarrativeLevelData NarrativeLevel;
    private PuzzleSetData CurrentPuzzleSet;

    /*** AUDIO ***/
    // Audio source for sound effects
    private AudioSource hackAudio;
    // Sound effects which get set from the editor
    public AudioClip StartupClip;
    public AudioClip InputClip;
    public AudioClip SuccessClip;
    public AudioClip ErrorClip;
    public AudioClip CompleteClip;
    public AudioClip FailureClip;
    public AudioClip SecretInterfaceClip;
    public AudioClip SecretPuzzleSolvedClip;
    public AudioClip AbortClip;
    public AudioClip OsirisClip;
    public AudioClip InitiateClip;

    /*** UI ELEMENTS ***/
    // Hack UI scripts
    private HackUIPreActiveMessages hackuiPreMessages;
    private HackUIPrompt hackuiPrompt;
    private HackUIInput hackuiInput;
    private HackUIProgress hackuiProgress;
    private HackUIImgProgressContainer hackuiProgContainer;
    private HackUIResultMessage hackuiPunishmentMessage;
    private HackUIResultMessage hackuiRewardMessage;
    private HackUISecretLetter hackuiSecretLetter;
    private HackUISecretPuzzle hackuiSecretPuzzle;
    private HackUIInput hackuiSecretInput;
    private Button hackuiAbortButton;
    // Timer Logic script
    private HackTimeLogic htll;

    private GameObject uiCanvas;
    private GameObject uiTimer;
    private GameObject preactiveCanvas;
    private GameObject activeCanvas;
    private GameObject successCanvas;
    private GameObject failureCanvas;
    private GameObject secretinterfaceCanvas;
    private GameObject secretPuzzlePrompt;
    private GameObject secretAllowInput;
    private GameObject secretDisallowInput;
    private GameObject secretComplete;
    private GameObject stopCanvas;

    //Presentation shit
    //private GameObject SecretPuzzleCanvas;
    //private GameObject SecretPuzzleAnswer;

    /*** STATES ***/
    // PuzzleState enum
    private enum PuzzleState { PreActive, Active, Error, Failure, Success, Complete, SecretInterface, Narrative, NarrativeStop };
    private string[] PuzzleStateNames = { "PreActive", "Active", "Error", "Failure", "Success", "Complete", "SecretInterface", "Narrative", "NarrativeStop" };
    // Puzzle states
    private PuzzleState prevState;
    private PuzzleState currentState;
    private PuzzleState nextState;

    /*** PUZZLE CHARACTERS ***/
    // Lists of prompt and input chars
    private List<CharSolutionElement> currentPrompt;
    private List<char> currentInput;
    // List of chars that the player can enter
    private List<char> legalcharpool;

    // StateChangeTimer is used to time the transition to the next state; ALWAYS set in initialize for current state
    // Example: set at beginning of Error state to transition back to Active state
    private float StateChangeTimer;
    // Index of current puzzle
    private int PuzzleNum;
    // Can the player enter input?
    private bool CanEnterInput;
    // Tracks status for perfect time bonus
    private bool currentPerfect;
    // Is the timer currently active?
    private bool bTimerActive = false;
    private bool aborted;

    private float StartingTime;

    void Awake()
    {
        // Get the LevelManager and CurrentLevel
        gameManager = FindObjectOfType<GameManager>();
        levelManager = FindObjectOfType<LevelManager>();
        CurrentLevel = levelManager.currentLevelData;
        // !!!
        NarrativeLevel = levelManager.currentNarrativeLevelData;

        // Get the AudioSource to play audio
        hackAudio = gameObject.GetComponent<AudioSource>();

        // Get HackTimerLogic component
        htll = FindObjectOfType<HackTimeLogic>();

        // Create new List<char> for both prompt and input
        currentPrompt = new List<CharSolutionElement>();
        currentInput = new List<char>();

        // Generate a list of keys that can be entered for gameplay
        // No numbers allowed!!
        legalcharpool = new List<char>();
        AddToCharPool(CharList.row2, legalcharpool);    //QWERTYUOP (no 'I')
        AddToCharPool(CharList.row3, legalcharpool);    //ASDFGHJKL
        AddToCharPool(CharList.row4, legalcharpool);    //ZXCVBNM

        RectTransform canvasBorder = (RectTransform)gameObject.transform.FindChild("HackCanvas").FindChild("CanvasBorder");

        uiCanvas = canvasBorder.FindChild("UI").gameObject;
        uiTimer = uiCanvas.transform.FindChild("HackTimer").Find("TimerBackground").gameObject;
        preactiveCanvas = canvasBorder.FindChild("PreActive").gameObject;
        activeCanvas = canvasBorder.FindChild("Active").gameObject;
        successCanvas = canvasBorder.FindChild("Success").gameObject;
        failureCanvas = canvasBorder.FindChild("Failure").gameObject;

        RectTransform stopCanvasObj = (RectTransform)canvasBorder.FindChild("Stop");
        if(stopCanvasObj)
        {
            stopCanvas = stopCanvasObj.gameObject;
        }

        RectTransform secretinterfaceObj = (RectTransform)canvasBorder.FindChild("SecretInterface");
        if (secretinterfaceObj)
        {
            secretinterfaceCanvas = secretinterfaceObj.gameObject;
            secretPuzzlePrompt = secretinterfaceCanvas.transform.FindChild("SecretPuzzle").gameObject;
            secretAllowInput = secretinterfaceCanvas.transform.FindChild("AllowInput").gameObject;
            secretDisallowInput = secretinterfaceCanvas.transform.FindChild("DisallowInput").gameObject;
            secretComplete = secretinterfaceCanvas.transform.FindChild("PuzzleSolved").gameObject;
        }

        // Get Hack UI script components
        hackuiPreMessages = preactiveCanvas.GetComponent<RectTransform>().FindChild("PreActiveMessages").gameObject.GetComponent<HackUIPreActiveMessages>();
        hackuiPrompt = activeCanvas.GetComponent<RectTransform>().FindChild("HackPrompt").gameObject.GetComponent<HackUIPrompt>();
        hackuiInput = activeCanvas.GetComponent<RectTransform>().FindChild("HackInput").gameObject.GetComponent<HackUIInput>();
        hackuiProgress = uiCanvas.GetComponent<RectTransform>().FindChild("HackProgress").gameObject.GetComponent<HackUIProgress>();
        hackuiProgContainer = uiCanvas.GetComponent<RectTransform>().FindChild("HackProgress").GetComponent<HackUIImgProgressContainer>();
        hackuiPunishmentMessage = failureCanvas.GetComponent<RectTransform>().FindChild("PunishmentMessage").GetComponent<HackUIResultMessage>();
        hackuiRewardMessage = successCanvas.GetComponent<RectTransform>().FindChild("RewardMessage").GetComponent<HackUIResultMessage>();
        hackuiAbortButton = preactiveCanvas.transform.FindChild("AbortButton").GetComponent<Button>();
        hackuiAbortButton.onClick.AddListener(OnAbort);

        RectTransform secretletterObj = (RectTransform)successCanvas.GetComponent<RectTransform>().FindChild("SecretLetter");
        if(secretletterObj)
        {
            hackuiSecretLetter = secretletterObj.gameObject.GetComponent<HackUISecretLetter>();
        }

        if(secretinterfaceCanvas)
        {
            hackuiSecretPuzzle = secretinterfaceCanvas.GetComponent<RectTransform>().FindChild("SecretPuzzle").GetComponent<HackUISecretPuzzle>();
            hackuiSecretInput = secretAllowInput.transform.FindChild("InputBox").GetComponent<HackUIInput>();
        }
    }

    void Start()
    {
        aborted = false;

        // Set current, prev, and next states to PreActive
        currentState = PuzzleState.PreActive;
        prevState = currentState;
        nextState = currentState;

        // Set StateChangeTimer to 0
        StateChangeTimer = 0;

        // Initialize PreActive state
        InitializeState();

        StartingTime = CurrentLevel.StartTime + (GameManager.hackingTimeBoosts * HACKING_TIME_BOOST);
        if(StartingTime >= 60f)
        {
            StartingTime = 60f;
        }
    }

    void Update()
    {
        /*** Switching states ***/
        // Check to see if a state change is happening
        if (currentState != nextState)
        {
            // If the StateChangeTimer is past 0, switch states
            if (StateChangeTimer <= 0)
            {
                prevState = currentState;
                currentState = nextState;
                InitializeState();
            }
            // If the StateChangeTimer has time left, subtract dT
            else
            {
                StateChangeTimer -= Time.deltaTime;
            }
        }

        // If the timer is active, update it
        if (bTimerActive)
        {
            htll.UpdateTimeLeft();
        }

        /*** State Updates ***/
        switch (currentState)
        {
            case PuzzleState.PreActive:
                if(LevelManager.currentGameState != GameStates.Hacking || aborted)
                {
                    return;
                }

                /* Press Space to start */
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    ChangeStateTo(PuzzleState.Active, 0.00f);
                }

                if(Input.GetKeyDown(KeyCode.Slash) && (Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift)))
                {
                    if(secretinterfaceCanvas)
                    {
                        ChangeStateTo(PuzzleState.SecretInterface, 0.00f);
                    }
                }
                break;

            case PuzzleState.SecretInterface:
                /* Press Backspace to go back */
                if (Input.GetKeyDown(KeyCode.Backspace))
                {
                    ChangeStateTo(PuzzleState.PreActive, 0.00f);
                }

                if(Input.GetKeyDown(KeyCode.Alpha1))
                {
                    if(Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
                    {
                        if(!gameManager.SecretPuzzleSolved)
                        {
                            gameManager.SecretLetters = new List<char>(gameManager.CurrentSecretPuzzleAnswer.ToCharArray());
                            InitializeState();
                        }
                    }
                }

                if(Input.anyKeyDown)
                {
                    if (secretAllowInput.activeSelf && !gameManager.SecretPuzzleSolved)
                    {
                        // Put dat text in there boi
                        char[] input = Input.inputString.ToUpper().ToCharArray();
                        for (int i = 0; i < input.Length; ++i)
                        {
                            if(!char.IsLetter(input[i]))
                            {
                                continue;
                            }

                            char charToAdd = input[i];

                            currentInput.Add(charToAdd);
                            hackAudio.PlayOneShot(InputClip);
                            hackuiSecretInput.UpdateInput(currentInput);
                        }

                        // Check it boi
                        if(hackuiSecretInput.gameObject.GetComponent<Text>().text.Length >= gameManager.CurrentSecretPuzzleAnswer.Length)
                        {
                            if(hackuiSecretInput.gameObject.GetComponent<Text>().text == gameManager.CurrentSecretPuzzleAnswer)
                            {
                                gameManager.SecretPuzzleSolved = true;
                                gameManager.nextSecretEmailUnlocked = true;
                                secretPuzzlePrompt.SetActive(false);
                                secretComplete.SetActive(true);
                                hackAudio.PlayOneShot(SecretPuzzleSolvedClip);
                            }
                            else
                            {
                                hackAudio.PlayOneShot(ErrorClip);
                                ClearInputText(hackuiSecretInput);
                            }
                        }
                    }
                }

                break;

            case PuzzleState.Active:
            case PuzzleState.Narrative:
                /* Receive key input and check */
                if (Input.anyKeyDown && CanEnterInput)
                {
                    char[] input = Input.inputString.ToLower().ToCharArray();

                    for (int i = 0; i < input.Length; ++i)
                    {
                        char charToAdd = input[i];

                        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                        {
                            charToAdd = char.ToUpper(charToAdd);
                        }

                        currentInput.Add(charToAdd);
                        hackAudio.PlayOneShot(InputClip);
                        hackuiInput.UpdateInput(currentInput);

                        if(CheckInput())
                        {
                            break;
                        }
                    }
                }

                //Cheats for cheaters
                HackCheats();

                if (nextState == PuzzleState.Active && htll.Timer == 0)
                {
                    ChangeStateTo(PuzzleState.Failure, 0.00f);
                }

                break;

            case PuzzleState.Error:
                break;

            case PuzzleState.Failure:
                /* Press Space to start */
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    GoToInfiltration(ExitHackingResults.Failure);
                }
                break;

            case PuzzleState.Success:
                break;

            case PuzzleState.Complete:
                /* Press Space to start */
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    GoToInfiltration(ExitHackingResults.Success);
                }
                break;

            default:
                Debug.Log("Invalid state");
                break;
        }
    }

    /*** State initialization and management ***/
    private void InitializeState()
    {
        switch (currentState)
        {
            // PreActive State initialization
            case PuzzleState.PreActive:
                // Audio
                if(prevState != PuzzleState.SecretInterface)
                {
                    hackAudio.PlayOneShot(StartupClip);
                }
                // Bool Values
                bTimerActive = false;
                CanEnterInput = false;
                // Canvas
                SetActiveCanvas(preactiveCanvas, true);
                uiTimer.SetActive(false);

                // Set PreActive Messages
                hackuiPreMessages.SetResultValues(GameManager.hackFailedSuspicion, GameManager.securityTerminalSuspicion);
                // Set Timer
                htll.SetTime(StartingTime);
                // Reset PuzzleNum
                PuzzleNum = 0;
                // Reset progress bar
                UpdateCurrentProgress();
                // Reset progress bar container
                hackuiProgContainer.UpdateWidth(105 + (100 * CurrentLevel.Puzzles.Length) + 5);
                break;

            case PuzzleState.SecretInterface:
                // Audio
                if(!gameManager.SecretPuzzleSolved)
                {
                    hackAudio.PlayOneShot(SecretInterfaceClip);
                }
                // Bool Values
                CanEnterInput = false;
                // Canvas
                SetActiveCanvas(secretinterfaceCanvas, false);
                
                SetActiveSecretPuzzleComponents();

                break;

            // Active State initialization
            case PuzzleState.Active:
                // Bool Values
                bTimerActive = true;
                CanEnterInput = true;

                // Did we just start the hacking game?
                if (prevState == PuzzleState.PreActive)
                {
                    SetCurrentPuzzleSet(CurrentLevel);
                    SetActiveCanvas(activeCanvas, true);
                    CreateNewPuzzle();
                    htll.SetTime(StartingTime);
                    hackuiInput.SetColor(cInputNormal);
                    //hackAudio.Play();
                    hackAudio.PlayOneShot(InitiateClip);
                    levelManager.EnterHackingAction();
                }

                // Had the player just messed up?
                if (prevState == PuzzleState.Error)
                {
                    // Search through input text and remove incorrect chars
                    for (int i = 0; i < currentInput.Count; ++i)
                    {
                        if (currentInput[i] != currentPrompt[i].Char)
                        {
                            currentInput.RemoveAt(i);
                            --i;
                        }
                    }

                    hackuiInput.UpdateInput(currentInput);
                    hackuiInput.SetColor(cInputNormal);
                }

                // Did the player just advance to a new puzzle?
                if (prevState == PuzzleState.Success)
                {
                    ++PuzzleNum;
                    CreateNewPuzzle();
                    hackuiInput.SetColor(cInputNormal);
                }
                break;

            // Error State initialization
            case PuzzleState.Error:
                // Audio
                hackAudio.PlayOneShot(ErrorClip);
                // Bool values
                CanEnterInput = false;
                // Input text
                hackuiInput.SetColor(cInputError);

                // Take away "perfect" status
                if (currentPerfect)
                {
                    currentPerfect = false;
                }

                ChangeStateTo(prevState, ERROR_TIMER_WAIT);
                break;

            // Failure State initialization
            case PuzzleState.Failure:
                // Audio
                hackAudio.Stop();
                hackAudio.PlayOneShot(FailureClip);
                levelManager.ExitHackingAction();
                //ExitHackingAction()

                // Bool values
                bTimerActive = false;
                CanEnterInput = false;
                // Canvas
                SetActiveCanvas(failureCanvas, true);
                hackuiPunishmentMessage.SetResultValue(GameManager.hackFailedSuspicion);
                break;

            // Success State initialization
            case PuzzleState.Success:
                // Audio
                hackAudio.PlayOneShot(SuccessClip);
                //ExitHackingAction()

                // Bool values
                bTimerActive = false;
                CanEnterInput = false;
                // Prompt text
                hackuiInput.SetColor(cInputSuccess);

                // Is there another puzzle to do? What's the next state?
                if ((PuzzleNum + 1) < CurrentPuzzleSet.Puzzles.Length)
                {
                    // Completion reward
                    if (prevState == PuzzleState.Active)
                    {
                        htll.AddTime(NORMAL_TIME_BONUS, currentPerfect);
                    }

                    ChangeStateTo(prevState, SUCCESS_TIMER_WAIT);
                }
                else
                {
                    PuzzleState gotoState = PuzzleState.Complete;

                    if(NarrativeLevel)
                    {
                        if (prevState == PuzzleState.Active)
                        {
                            gotoState = PuzzleState.Narrative;
                        }

                        if (prevState == PuzzleState.Narrative)
                        {
                            gotoState = PuzzleState.NarrativeStop;
                        }
                    }

                    ChangeStateTo(gotoState, SUCCESS_TIMER_WAIT);
                }
                break;

            // Complete State initialization
            case PuzzleState.Complete:
                // Audio
                hackAudio.Stop();
                hackAudio.PlayOneShot(CompleteClip);
                levelManager.ExitHackingAction();
                // Bool values
                bTimerActive = false;
                CanEnterInput = false;
                // Canvas
                SetActiveCanvas(successCanvas, true);
                UpdateCurrentProgress(true);

                if(hackuiRewardMessage)
                {
                    hackuiRewardMessage.SetResultValue(Mathf.Abs(GameManager.securityTerminalSuspicion));
                }
                if(hackuiSecretLetter)
                {
                    hackuiSecretLetter.SetText(CurrentLevel.SecretLetters.ToString());
                }
                break;

            case PuzzleState.Narrative:
                if(CurrentPuzzleSet != NarrativeLevel)
                {
                    SetCurrentPuzzleSet(NarrativeLevel);
                    CreateNewPuzzle();
                }

                // Had the player just messed up?
                if (prevState == PuzzleState.Error)
                {
                    // Search through input text and remove incorrect chars
                    for (int i = 0; i < currentInput.Count; ++i)
                    {
                        if (currentInput[i] != currentPrompt[i].Char)
                        {
                            currentInput.RemoveAt(i);
                            --i;
                        }
                    }

                    hackuiInput.UpdateInput(currentInput);
                    hackuiInput.SetColor(cInputNormal);
                }

                // Did the player just advance to a new puzzle?
                if (prevState == PuzzleState.Success)
                {
                    ++PuzzleNum;
                    CreateNewPuzzle();
                    hackuiInput.SetColor(cInputNormal);
                }

                // Audio
                levelManager.FadeOutAllMusic(0f);

                bTimerActive = false;
                CanEnterInput = true;

                SetActiveCanvas(activeCanvas, true);
                uiTimer.SetActive(false);
                SetProgressBarRed();
                break;

            case PuzzleState.NarrativeStop:
                SetActiveCanvas(stopCanvas, true);
                uiTimer.SetActive(false);
                hackAudio.PlayOneShot(OsirisClip);
                ChangeStateTo(PuzzleState.Complete, 3.8f);
                break;

            default:
                Debug.Log("Invalid state");
                break;
        }
    }

    private void ChangeStateTo(PuzzleState state_, float time_)
    {
        nextState = state_;
        StateChangeTimer = time_;
    }

    /*** Input Checking ***/
    private bool CheckInput()
    {
        bool toReturn = false;

        if (!InputEqualsPrompt())
        {
            ChangeStateTo(PuzzleState.Error, 0.00f);
            toReturn = true;
        }

        if (InputEqualsPrompt() && (currentInput.Count == currentPrompt.Count))
        {
            ChangeStateTo(PuzzleState.Success, 0.00f);
            toReturn = true;
        }

        return toReturn;
    }

    private bool InputEqualsPrompt()
    {
        bool result = true;

        for (int i = 0; i < currentInput.Count; ++i)
        {
            if (currentInput[i] != currentPrompt[i].Char)
            {
                result = false;
                break;
            }
        }

        return result;
    }

    private bool IsLegalChar(char char_)
    {
        bool result = false;

        for (int i = 0; i < legalcharpool.Count; ++i)
        {
            if (char_ == legalcharpool[i])
            {
                result = true;
                break;
            }
        }

        return result;
    }

    /*** Puzzle Generation ***/
    // Generates a new puzzle!
    // This gets called when the Active state is initialized after a PreActive or Success state
    private void CreateNewPuzzle()
    {
        CreateNewPrompt();
        ClearInputText(hackuiInput);
        UpdateCurrentProgress();
        currentPerfect = true;
    }

    // Generates a puzzle and the prompt text to be displayed
    private void CreateNewPrompt()
    {
        // Clear the current prompt
        currentPrompt.Clear();

        //Debug.Log("PUZZLENUM: " + PuzzleNum);
        PuzzleData currentPuzzle = CurrentPuzzleSet.Puzzles[PuzzleNum];

        // Create pool of possible characters based on difficulty
        List<char> charpool = new List<char>();
        PuzzleData.PuzzleDifficulty currentDiff = currentPuzzle.Difficulty;

        // Add the homerow to charpool (all difficulties will use it)
        AddToCharPool(CharList.row3, charpool);

        // Generate the rest of the charpool (based on difficulty)
        if (currentDiff > PuzzleData.PuzzleDifficulty.Easy)
        {
            AddToCharPool(CharList.row2, charpool);

            if (currentDiff == PuzzleData.PuzzleDifficulty.Hard)
            {
                AddToCharPool(CharList.row4, charpool);
            }
        }

        // Creates TEXT THAT WILL BE DISPLAYED
        List<CharElement> currentPromptText = new List<CharElement>();
        int charID = 0;

        // Time to actually make the puzzle, one char type at a time
        /*** NORMAL ***/
        int normalChars = currentPuzzle.NormalCharAmount;

        for (int i = 0; i < normalChars; ++i)
        {
            char charToAdd = GenerateCharFromCharpool(charpool, currentDiff);

            currentPrompt.Add(new CharSolutionElement(charToAdd, charID));
            currentPromptText.Add(new CharElement(cPromptNormal, charID, charToAdd));

            ++charID;
        }

        /*** SWITCH CAPS ***/
        int switchCapsAmount = currentPuzzle.SwitchCapsAmount;

        for (int i = 0; i < switchCapsAmount; ++i)
        {
            char charToAdd = GenerateCharFromCharpool(charpool, currentDiff);
            int insertIndex = Random.Range(0, currentPrompt.Count - 1);

            currentPrompt.Insert(insertIndex, new CharSolutionElement(charToAdd, charID));

            if (char.IsUpper(charToAdd))
            {
                charToAdd = char.ToLower(charToAdd);
            }
            else
            {
                charToAdd = char.ToUpper(charToAdd);
            }

            currentPromptText.Insert(insertIndex, new CharElement(cPromptSwitch, charID, charToAdd, true));

            ++charID;
        }

        /*** REPEAT ***/
        int multiCharAmount = currentPuzzle.MultiCharAmount;

        for (int i = 0; i < multiCharAmount; ++i)
        {
            char charToAdd = GenerateCharFromCharpool(charpool, currentDiff);
            int insertIndex = Random.Range(0, currentPrompt.Count - 1);
            int multiCharNum = Random.Range(2, 5);

            for (int j = 0; j < multiCharNum; ++j)
            {
                currentPrompt.Insert(insertIndex, new CharSolutionElement(charToAdd, charID));
            }

            currentPromptText.Insert(insertIndex,
                new CharElement(cPromptRepeat, charID, charToAdd, false, multiCharNum));

            ++charID;
        }

        /*** FIRST ***/
        int firstCharAmount = currentPuzzle.FirstCharAmount;
        int insertPromptIndex = 0;

        for (int i = 0; i < firstCharAmount; ++i)
        {
            char charToAdd = GenerateCharFromCharpool(charpool, currentDiff);
            insertPromptIndex = Random.Range(insertPromptIndex, currentPromptText.Count);

            currentPrompt.Insert(i, new CharSolutionElement(charToAdd, charID));
            currentPromptText.Insert(insertPromptIndex, new CharElement(cPromptFirst, charID, charToAdd));

            ++insertPromptIndex;
            ++charID;
        }

        /*** LAST ***/
        int lastCharAmount = currentPuzzle.LastCharAmount;
        insertPromptIndex = currentPromptText.Count;

        for (int i = 0; i < lastCharAmount; ++i)
        {
            char charToAdd = GenerateCharFromCharpool(charpool, currentDiff);
            insertPromptIndex = Random.Range(0, insertPromptIndex);

            currentPrompt.Insert(currentPrompt.Count - i, new CharSolutionElement(charToAdd, charID));

            currentPromptText.Insert(insertPromptIndex, new CharElement(cPromptLast, charID, charToAdd));

            ++charID;
        }

        // Change chars to text
        if (currentPuzzle.Text != null)
        {
            for (int i = 0; i < currentPuzzle.Text.Length; ++i)
            {
                currentPromptText[i].MyChar = currentPuzzle.Text[i];

                for (int j = 0; j < currentPrompt.Count; ++j)
                {
                    if (currentPrompt[j].ID == currentPromptText[i].ID)
                    {
                        char charToChange = currentPuzzle.Text[i];
                        currentPrompt[j].Char = charToChange;

                        if (currentPromptText[i].SwitchChar)
                        {
                            if (char.IsUpper(charToChange))
                            {
                                charToChange = char.ToLower(charToChange);
                            }
                            else
                            {
                                charToChange = char.ToUpper(charToChange);
                            }

                            currentPromptText[i].MyChar = charToChange;
                        }
                    }
                }
            }
        }

        // Place the ignore characters
        int redChars = CurrentPuzzleSet.Puzzles[PuzzleNum].IgnoreCharAmount;
        for (int i = 0; i < redChars; ++i)
        {
            char redchar = charpool[Random.Range(0, charpool.Count)];
            currentPromptText.Insert(Random.Range(0, currentPromptText.Count), new CharElement(cPromptIgnore, -1, redchar));
        }

        // CENSOR CHECK
        // lol it aint here

        // Display the answer cuz why not
        StringBuilder sb = new StringBuilder();
        sb.Append(currentDiff.ToString());
        sb.Append(": ");
        for (int i = 0; i < currentPrompt.Count; ++i)
        {
            sb.Append(currentPrompt[i].Char);
        }
        //Debug.Log(sb.ToString());

        // Add in the numbers for multi chars
        for (int i = 0; i < currentPromptText.Count; ++i)
        {
            if (currentPromptText[i].MultiChar > 1)
            {
                // @@@
                currentPromptText.Insert(i+1, new CharElement(cPromptRepeat, -1, currentPromptText[i].MultiChar.ToString()[0]));
                ++i;
            }
        }

        // Display the prompt!
        hackuiPrompt.UpdatePrompt(currentPromptText);
    }

    private void AddToCharPool(char[] chararray_, List<char> charpool_)
    {
        for (int i = 0; i < chararray_.Length; ++i)
        {
            charpool_.Add(chararray_[i]);
        }
    }

    // Clears the input string and updates the input text
    private void ClearInputText(HackUIInput inputObj_)
    {
        currentInput.Clear();
        inputObj_.UpdateInput(currentInput);
    }

    private void UpdateCurrentProgress(bool complete_ = false)
    {
        List<CharElement> progress = new List<CharElement>();
        int length = CurrentLevel.Puzzles.Length;

        for (int i = 0; i < length; ++i)
        {
            if(complete_)
            {
                progress.Add(new CharElement(cProgressCompleted, -1, '*'));
                continue;
            }

            if (i < PuzzleNum)
            {
                progress.Add(new CharElement(cProgressCompleted, -1, '*'));
            }
            else if (i == PuzzleNum)
            {
                progress.Add(new CharElement(cProgressCurrent, -1, '*'));
            }
            else
            {
                progress.Add(new CharElement(cProgressUpcoming, -1, '*'));
            }
        }

        hackuiProgress.UpdateProgress(progress);
    }

    private void SetProgressBarRed()
    {
        List<CharElement> progress = new List<CharElement>();
        int length = CurrentLevel.Puzzles.Length;

        for (int i = 0; i < length; ++i)
        {
            progress.Add(new CharElement(cInputError, -1, '*'));
        }

        hackuiProgress.UpdateProgress(progress);
    }

    // Exits hacking module with information about whether the player was successful
    private void GoToInfiltration(ExitHackingResults result_)
    {
        levelManager.ExitHackingState(result_);
    }

    // Cheats for filthy cheaters
    private void HackCheats()
    {
        if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
        {
            if (Input.GetKey(KeyCode.Alpha9))
            {
                ChangeStateTo(PuzzleState.Complete, 0.00f);
            }

            if (Input.GetKey(KeyCode.Alpha0))
            {
                htll.AddTime(100, false);
            }

            if (Input.GetKey(KeyCode.Alpha8))
            {
                htll.SetTime(0.00f);
            }
        }
    }

    private char GenerateCharFromCharpool(List<char> charpool_, PuzzleData.PuzzleDifficulty currentDiff_)
    {
        char charToAdd = charpool_[Random.Range(0, charpool_.Count)];

        if (currentDiff_ >= PuzzleData.PuzzleDifficulty.Easy)
        {
            int Capitalization = Random.Range(0, 2);
            if (Capitalization > 0)
            {
                charToAdd = char.ToUpper(charToAdd);
            }
        }

        return charToAdd;
    }

    private void SetActiveCanvas(GameObject canvas_, bool uiActive_)
    {
        uiTimer.SetActive(true);
        uiCanvas.SetActive(false);
        preactiveCanvas.SetActive(false);
        activeCanvas.SetActive(false);
        successCanvas.SetActive(false);
        failureCanvas.SetActive(false);

        if(secretinterfaceCanvas)
        {
            secretinterfaceCanvas.SetActive(false);
        }

        if (stopCanvas)
        {
            stopCanvas.SetActive(false);
        }

        canvas_.SetActive(true);
        uiCanvas.SetActive(uiActive_);
    }

    private void SetActiveSecretPuzzleComponents()
    {
        secretPuzzlePrompt.SetActive(false);
        secretAllowInput.SetActive(false);
        secretDisallowInput.SetActive(false);
        secretComplete.SetActive(false);

        if (gameManager.SecretPuzzleSolved)
        {
            secretAllowInput.SetActive(true);
            secretComplete.SetActive(true);
            hackuiSecretInput.SetText(gameManager.CurrentSecretPuzzleAnswer);
            return;
        }
        else
        {
            secretPuzzlePrompt.SetActive(true);
            hackuiSecretPuzzle.SetText(new string(gameManager.SecretLetters.ToArray()).Replace('\0', '?'));

            if (hackuiSecretPuzzle.gameObject.GetComponent<Text>().text.Contains("?"))
            {
                secretDisallowInput.SetActive(true);
                return;
            }
            else
            {
                secretAllowInput.SetActive(true);
                ClearInputText(hackuiSecretInput);
            }
        }
    }

    private void OnAbort()
    {
        if(LevelManager.currentGameState != GameStates.Hacking)
        {
            return;
        }

        aborted = true;
        hackAudio.PlayOneShot(AbortClip);
        hackuiAbortButton.onClick.RemoveListener(OnAbort);
        hackuiAbortButton.interactable = false;
        GoToInfiltration(ExitHackingResults.Abort);
    }

    private void SetCurrentPuzzleSet(PuzzleSetData set_)
    {
        CurrentPuzzleSet = set_;
        PuzzleNum = 0;
    }
}