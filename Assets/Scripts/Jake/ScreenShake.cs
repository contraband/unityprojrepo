﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : ScreenShake.cs
// Author : Jacob Cuddihy
// Purpose : interpolates the camera position to create screen-shake.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour
{
    // Default value for how long the shake should last
    public const float fDuration = 0.5f;
    // Default value for how fast the camera should move
    public const float fSpeed = 1.5f;
    // Default value for how far the camera should move
    public const float fMagnitude = 0.7f;

    public void ShakeIt(GameObject goObjectToMove_, float fDuration_ = fDuration, float fSpeed_ = fSpeed, float fMagnitude_ = fMagnitude)
    {
        StopAllCoroutines();
        StartCoroutine(Shake(goObjectToMove_, fDuration_, fSpeed, fMagnitude_));
    }

    IEnumerator Shake(GameObject goObjectToMove_, float fDuration_, float fSpeed_, float fMagnitude_)
    {
        // Counter to store current frame time
        float fCurrentTime = 0.0f;
        // Random start seed
        float fRandomStart = Random.Range(-1000.0f, 1000.0f);
        // Store starting camera position
        Vector3 v3StartPos = goObjectToMove_.transform.position;

        // Check that elapsed frame time is less than total time allowed
        while (fCurrentTime < fDuration_)
        {
            // Increment frame time counter
            fCurrentTime += Time.deltaTime;
            float fPercentComplete = fCurrentTime / fDuration_;
            // Reduce shake strength to zero over total time of shake
            float fDamper = 1.0f - Mathf.Clamp(4.0f * fPercentComplete - 3.0f, 0.0f, 1.0f);
            // Get a random seed value to generate noise from
            float fSeed = fRandomStart + fSpeed_ * fPercentComplete;

            // Create a new position to move the camera to
            Vector3 v3NewCamPos;
            v3NewCamPos.x = (Mathf.PerlinNoise(fSeed, 0.0f) * 2.0f - 1.0f) * fMagnitude_ * fDamper;
            v3NewCamPos.x += v3StartPos.x;
            v3NewCamPos.y = (Mathf.PerlinNoise(0.0f, fSeed) * 2.0f - 1.0f) * fMagnitude_ * fDamper;
            v3NewCamPos.y += v3StartPos.y;
            v3NewCamPos.z = v3StartPos.z;
            goObjectToMove_.transform.position = v3NewCamPos;

            yield return null;
        }

        // Reset camera position to start
        goObjectToMove_.transform.position = v3StartPos;
    }
}