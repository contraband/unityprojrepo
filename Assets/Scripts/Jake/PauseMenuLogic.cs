﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : PauseMenuLogic.cs
// Author : Jacob Cuddihy
// Purpose : Creates the pause menu, actually pauses the game, switches between
// 			 all of the different pause menu screens.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PauseMenuLogic : MonoBehaviour
{
    public delegate void PauseDelegate(bool paused);
    public event PauseDelegate onPause;

    public enum PauseState { None, Main, HowToPlay, Options, Credits, Quit, RestartCheckpointConfirm, RestartLevelConfirm }

    public GameObject pauseMenuCanvasPrefab;
    private GameObject pauseMenuCanvas;
    private GameObject MainScreen;
    private GameObject RestartCheckpointScreen;
    private GameObject RestartLevelScreen;
    private GameObject HowToPlayScreen;
    private GameObject OptionsScreen;
    private GameObject CreditsScreen;
    private GameObject QuitScreen;
    private bool bIsPaused = false;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!bIsPaused)
            {
                CreatePauseMenu();
                Time.timeScale = 0;
            }
            else
            {
                DestroyPauseMenu();
                Time.timeScale = 1;
            }
        }
    }

    public void DestroyPauseMenu()
    {
        if (onPause != null) onPause(false);
        bIsPaused = false;
        MainScreen = null;
        HowToPlayScreen = null;
        OptionsScreen = null;
        CreditsScreen = null;
        QuitScreen = null;
        RestartCheckpointScreen = null;
        RestartLevelScreen = null;
        Destroy(pauseMenuCanvas);
        pauseMenuCanvas = null;
    }

    public void CreatePauseMenu()
    {
        if (onPause != null) onPause(true);
        bIsPaused = true;
        pauseMenuCanvas = Instantiate(pauseMenuCanvasPrefab);
        MainScreen = pauseMenuCanvas.transform.FindChild("MainScreen").gameObject;
        RestartCheckpointScreen = pauseMenuCanvas.transform.FindChild("RestartCheckpointConfirmScreen").gameObject;
        RestartLevelScreen = pauseMenuCanvas.transform.FindChild("RestartLevelConfirmScreen").gameObject;
        HowToPlayScreen = pauseMenuCanvas.transform.FindChild("HowToPlayScreen").gameObject;
        OptionsScreen = pauseMenuCanvas.transform.FindChild("OptionsScreen").gameObject;
        CreditsScreen = pauseMenuCanvas.transform.FindChild("CreditsScreen").gameObject;
        QuitScreen = pauseMenuCanvas.transform.FindChild("QuitScreen").gameObject;
        GoToState(PauseState.Main);
    }

    /*
    public void GoToMain()
    {
        //MainScreen.gameObject.GetComponent<CanvasGroup>().alpha = 1;
        //HowToPlayScreen.GetComponent<CanvasGroup>().alpha = 0;
        //CreditsScreen.GetComponent<CanvasGroup>().alpha = 0;
        //QuitScreen.GetComponent<CanvasGroup>().alpha = 0;
        
        MainScreen.SetActive(true);
        HowToPlayScreen.SetActive(false);
        CreditsScreen.SetActive(false);
        QuitScreen.SetActive(false);
        
    }

    public void GoToHowToPlay()
    {
        //MainScreen.gameObject.GetComponent<CanvasGroup>().alpha = 0;
        //HowToPlayScreen.GetComponent<CanvasGroup>().alpha = 1;
        
        MainScreen.SetActive(false);
        HowToPlayScreen.SetActive(true);
    }

    public void GoToOptions()
    {
        MainScreen.SetActive(false);
        OptionsScreen.SetActive(true);
    }

    public void GoToCredits()
    {
        //MainScreen.gameObject.GetComponent<CanvasGroup>().alpha = 0;
        //CreditsScreen.GetComponent<CanvasGroup>().alpha = 1;
        
        MainScreen.SetActive(false);
        CreditsScreen.SetActive(true);
        
    }

    public void GoToQuit()
    {
        //MainScreen.gameObject.GetComponent<CanvasGroup>().alpha = 0;
        //QuitScreen.GetComponent<CanvasGroup>().alpha = 1;
        
        MainScreen.SetActive(false);
        QuitScreen.SetActive(true);
        
    }
    */

    public void GoToState(PauseState state_)
    {
        MainScreen.SetActive(false);
        HowToPlayScreen.SetActive(false);
        OptionsScreen.SetActive(false);
        CreditsScreen.SetActive(false);
        QuitScreen.SetActive(false);
        RestartCheckpointScreen.SetActive(false);
        RestartLevelScreen.SetActive(false);

        GetScreen(state_).SetActive(true);
    }

    private GameObject GetScreen(PauseState state_)
    {
        GameObject result = null;

        switch(state_)
        {
            case PauseState.Main:
                result = MainScreen;
                break;

            case PauseState.HowToPlay:
                result = HowToPlayScreen;
                break;

            case PauseState.Options:
                result = OptionsScreen;
                break;

            case PauseState.Credits:
                result = CreditsScreen;
                break;

            case PauseState.Quit:
                result = QuitScreen;
                break;

            case PauseState.RestartCheckpointConfirm:
                result = RestartCheckpointScreen;
                break;

            case PauseState.RestartLevelConfirm:
                result = RestartLevelScreen;
                break;
        }

        return result;
    }
}