﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : IntroOutroSceneManager.cs
// Author : Jacob Cuddihy
// Purpose : Creates the DigiPen splash screen and outro credits screens.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class IntroOutroSceneManager : MonoBehaviour
{
    public GameObject gameManagerPrefab;
    private GameManager gameManager;
    public const float WAIT_TIME = 2f;
    public bool Intro;

    private FadeImage fadeImage;
    private bool CheckForExitKey;
    private string firstScene = "Chapter1.1";

    void Awake()
    {
        CheckForExitKey = false;

        //Game manager
        if (FindObjectsOfType(typeof(GameManager)).Length == 0) gameManager = Instantiate(gameManagerPrefab, Vector3.zero, Quaternion.identity).GetComponent<GameManager>();

        Instantiate(gameManager.imageEffectCanvasesPrefab, Vector3.zero, Quaternion.identity).name = "ImageEffectCanvases";

        if(Intro) gameManager.otherMusicAudioObject.PlayOnce(gameManager.pianoClip, false);
    }

    void Start()
    {
        fadeImage = FindObjectOfType<FadeImage>();

        FadeIn();
    }

    void Update()
    {
        if (!Intro && CheckForExitKey)
        {
            if(Input.GetKeyDown(KeyCode.Return))
            {
                Application.Quit();
            }
        }
    }

    void FadeIn()
    {
        fadeImage.Transition(0f);
        fadeImage.onTransitionEnd += FadeInEnded;
    }

    void FadeInEnded()
    {
        fadeImage.onTransitionEnd -= FadeInEnded;

        if (Intro)
        {
            Invoke("FadeOut", WAIT_TIME);
        }
        else
        {
            CheckForExitKey = true;
        }
    }

    void FadeOut()
    {
        fadeImage.Transition(1f);
        fadeImage.onTransitionEnd += FadeOutEnded;
    }

    void FadeOutEnded()
    {
        SceneManager.LoadScene(firstScene);
    }
}