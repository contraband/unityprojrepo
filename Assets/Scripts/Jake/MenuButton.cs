﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : MenuButton.cs
// Author : Jacob Cuddihy
// Purpose : Class that controls all different types of menu buttons,
// 			 carries out each type of button's functionality.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

[RequireComponent(typeof(AudioSource))]

public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Button myButton;
    private PauseMenuLogic PauseMenu;
    private AudioSource ButtonSound;
    public Texture2D cursorTexture;
    public Vector2 hotSpot = Vector2.zero;

    public enum Buttons
    {
        RETURN,
        RESUME,
        HOW_TO,
        OPTIONS,
        QUIT,
        QUIT_CONFIRM,
        CREDITS,
        RESTART_CHECKPOINT_CONFIRM,
        RESTART_CHECKPOINT,
        RESTART_LEVEL_CONFIRM,
        RESTART_LEVEL
    }

    public Buttons buttonType;

    void Awake()
    {
        myButton = gameObject.GetComponent<Button>();
        myButton.onClick.AddListener(OnButtonClick);
        ButtonSound = gameObject.GetComponent<AudioSource>();
        PauseMenu = FindObjectOfType<PauseMenuLogic>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        CustomCursor.CursorShouldAnimate = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        CustomCursor.CursorShouldAnimate = false;
    }

    void OnButtonClick()
    {
        ButtonSound.Play();

        PauseMenuLogic.PauseState gotoState = PauseMenuLogic.PauseState.None;

        switch (buttonType)
        {
            case Buttons.RETURN:
                gotoState = PauseMenuLogic.PauseState.Main;
                break;

            case Buttons.RESUME:
                ExitPauseMenu();
                return;

            case Buttons.HOW_TO:
                gotoState = PauseMenuLogic.PauseState.HowToPlay;
                break;

            case Buttons.OPTIONS:
                gotoState = PauseMenuLogic.PauseState.Options;
                break;

            case Buttons.QUIT_CONFIRM:
                gotoState = PauseMenuLogic.PauseState.Quit;
                break;

            case Buttons.QUIT:
                Application.Quit();
                return;

            case Buttons.CREDITS:
                gotoState = PauseMenuLogic.PauseState.Credits;
                break;

            case Buttons.RESTART_CHECKPOINT_CONFIRM:
                gotoState = PauseMenuLogic.PauseState.RestartCheckpointConfirm;
                break;

            case Buttons.RESTART_CHECKPOINT:
                FindObjectOfType<LevelManager>().RestartLevel(true);
                ExitPauseMenu();
                break;

            case Buttons.RESTART_LEVEL_CONFIRM:
                gotoState = PauseMenuLogic.PauseState.RestartLevelConfirm;
                break;

            case Buttons.RESTART_LEVEL:
                FindObjectOfType<LevelManager>().RestartLevel(false);
                ExitPauseMenu();
                break;
        }

        PauseMenu.GoToState(gotoState);
    }

    void ExitPauseMenu()
    {
        PauseMenu.DestroyPauseMenu();
        Time.timeScale = 1;
    }
}