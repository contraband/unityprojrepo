﻿// --------------------------------------------------------------------------------
// Project Name : Being Evelyn
// File Name : CustomCursor.cs
// Author : Jacob Cuddihy
// Purpose : Switches the hardware cursor to a custom cursor and animates the
// 			 cursor when over a button.
// All content © 2016 DigiPen (USA) Corporation, all rights reserved.
// ---------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class CustomCursor : MonoBehaviour
{
    public Texture2D cursorTexture1;
    public Texture2D cursorTexture2;
    public Texture2D cursorTexture3;
    public Texture2D cursorTexture4;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public float CursorAnimIntervals;
    static public bool CursorShouldAnimate;
    private bool Anim1;
    private bool Anim2;
    private bool Anim3;
    private bool Anim4;
    private float CurrentTime;

    //Cursor clamping
    Vector2 cursorPosition;
    Vector2 lastMousePosition;
    private bool clampCursor;
    private int minX;
    private int maxX;
    private int minY;
    private int maxY;

    void Awake()
    {
        Cursor.SetCursor(cursorTexture1, hotSpot, CursorMode.ForceSoftware);
        Anim1 = true;
        Anim2 = false;
        Anim3 = false;
        Anim4 = false;
        CurrentTime = 0;
    }
    void AnimateCursor()
    {
        CurrentTime += 1;
        if (CurrentTime % CursorAnimIntervals == 0 && Anim1)
        {
            Cursor.SetCursor(cursorTexture2, hotSpot, CursorMode.ForceSoftware);
            Anim1 = false;
            Anim2 = true;
            CurrentTime = 0;
        }
        else if (CurrentTime % CursorAnimIntervals == 0 && Anim2)
        {
            Cursor.SetCursor(cursorTexture3, hotSpot, CursorMode.ForceSoftware);
            Anim2 = false;
            Anim3 = true;
            CurrentTime = 0;
        }
        else if (CurrentTime % CursorAnimIntervals == 0 && Anim3)
        {
            Cursor.SetCursor(cursorTexture4, hotSpot, CursorMode.ForceSoftware);
            Anim3 = false;
            Anim4 = true;
            CurrentTime = 0;
        }
        else if (CurrentTime % CursorAnimIntervals == 0 && Anim4)
        {
            Cursor.SetCursor(cursorTexture1, hotSpot, CursorMode.ForceSoftware);
            Anim4 = false;
            Anim1 = true;
            CurrentTime = 0;
        }
    }

    void OnGUI()
    {
        ClampCursor();

        if (CursorShouldAnimate)
        {
            AnimateCursor();
        }
        else
        {
            if (!Anim1)
            {
                Cursor.SetCursor(cursorTexture1, hotSpot, CursorMode.ForceSoftware);
                Anim1 = true;
                Anim2 = false;
                Anim3 = false;
                Anim4 = false;
            }
        }
    }

    public void StartClampingCursor(int minX_, int maxX_, int minY_, int maxY_)
    {
        clampCursor = true;
        UnityEngine.Cursor.visible = false;

        minX = minX_;
        maxX = maxX_;
        minY = minY_;
        maxY = maxY_;
        cursorPosition = Input.mousePosition;
        lastMousePosition = cursorPosition;
    }

    public void StopClampingCursor()
    {
        clampCursor = false;
        UnityEngine.Cursor.visible = true;
    }

    private void ClampCursor()
    {
        if (!clampCursor) return;

        //Get mouse position delta
        Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 mouseDelta = mousePosition - lastMousePosition;

        //Update cursor position
        cursorPosition.x += mouseDelta.x;
        cursorPosition.y -= mouseDelta.y;

        //Clamp cursor position
        cursorPosition.x = Mathf.Clamp(cursorPosition.x, minX, maxX);
        cursorPosition.y = Mathf.Clamp(cursorPosition.y, minY, maxY);

        //Draw cursor
        Rect rect = new Rect(cursorPosition.x - (cursorTexture1.width / 2), cursorPosition.y - (cursorTexture1.height / 2), cursorTexture1.width, cursorTexture1.height);
        GUI.DrawTexture(rect, cursorTexture2);

        //Update last mouse position
        lastMousePosition = Input.mousePosition;
    }
}